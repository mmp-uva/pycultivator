Changelog
=========

Future
------

* Implement messaging system between commands and the interface

Version 1.0.6
-------------

* Restructured core objects, separate configurable object from PCObject
* Upgrade contexts to official package for managing a pycultivator environments

Version 1.0.5
-------------

* Improved console
* Improved arguments
* Commands are more independent from the objects executing them
* Introduction of context to bridge gap between commands and their environment

Version 1.0.4
-------------

* Restructured relation subpackage.

Version 1.0.2
-------------

* redesigned configuration system
* entry points for configuration source handlers

Version 1.0.2
-------------

* rename modules to lower case
* test for Python 3 compatibility (Python 3.6)
* improve and rename base objects
* implement event system
* implement a console system for direct python access to devices

Version 1.0.1
-------------

* Rename foundation to core
* Improve XML module


Before Version 1.0.0.dev1
-------------------------

* Basic structure
* Use control and measure functions
