"""Module testing the uvaSettings module"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

import unittest
from pycultivator.core import pcSettings


class SettingsTest(unittest.TestCase):

    _subject_class = pcSettings.PCSettings

    def __init__(self, methodName='runTest'):
        super(SettingsTest, self).__init__(methodName=methodName)

    @classmethod
    def getSubjectClass(cls):
        """Return reference to the subject class

        :return:
        :rtype: (...) -> pycultivator.core.pcSettings.PCSettings
        """
        return cls._subject_class

    def getSubject(self):
        """Return the instance of the subject class

        :return:
        :rtype: pycultivator.core.pcSettings.PCSettings
        """
        return self.getSubjectClass()()

    def test_filter_dict(self):
        # simple case
        d = {
            "b.a": 0,
            "a.b": 1
        }
        self.assertEqual(
            self.getSubjectClass().filter_dict(d, "a"),
            {"a.b": 1}
        )
        # no matching elements
        d = {
            'b.a': 0
        }
        self.assertEqual(
            self.getSubjectClass().filter_dict(d, "a"),
            {}
        )
        # empty namespace
        self.assertEqual(
            self.getSubjectClass().filter_dict(d, ""),
            d
        )

    def test_has_namespace(self):
        self.assertTrue(self.getSubjectClass().has_namespace('a.b', 'a'))
        self.assertFalse(self.getSubjectClass().has_namespace('b.a', 'a'))
        self.assertTrue(self.getSubjectClass().has_namespace('b.a', ''))
        self.assertTrue(self.getSubjectClass().has_namespace('', ''))
        self.assertFalse(self.getSubjectClass().has_namespace('', 'a'))
        self.assertFalse(self.getSubjectClass().has_namespace(None, 'a'))
        self.assertFalse(self.getSubjectClass().has_namespace('a', None))

    def test_prepend_namespace(self):
        # simple case
        self.assertEqual(
            self.getSubjectClass().prepend_namespace("a", "b"), "b.a"
        )
        # old is empty
        self.assertEqual(
            self.getSubjectClass().prepend_namespace("", "b"), "b"
        )
        self.assertEqual(
            self.getSubjectClass().prepend_namespace(None, "b"), "b"
        )
        # new is empty
        self.assertEqual(
            self.getSubjectClass().prepend_namespace("a", ""), "a"
        )
        self.assertEqual(
            self.getSubjectClass().prepend_namespace("a", None), "a"
        )
        # both empty
        self.assertEqual(
            self.getSubjectClass().prepend_namespace("", ""), ""
        )
        self.assertEqual(
            self.getSubjectClass().prepend_namespace(None, None), ""
        )

    def test_prepend(self):
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="")
        s.prepend('d')
        self.assertEqual(
            s.to_dict(), {'d.a': 1, 'd.b': 'c'}
        )
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="d")
        s.prepend("")
        self.assertEqual(
            s.to_dict(), {'d.a': 1, 'd.b': 'c'}
        )

    def test_reduce_namespace(self):
        s = self.getSubjectClass()({}, namespace="d")
        # simple case
        self.assertEqual(
            s.reduce_namespace("a.b", "a"), "b"
        )
        self.assertEqual(
            s.reduce_namespace("d.b", "d"), "b"
        )
        self.assertEqual(
            s.reduce_namespace("a.b", ""), "a.b"
        )

    def test_reduce(self):
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="d")
        s.reduce('d')
        self.assertEqual(
            s.to_dict(), {'a': 1, 'b': 'c'}
        )
        s.reduce(None)
        self.assertEqual(
            s.to_dict(), {'a': 1, 'b': 'c'}
        )

    def test_has(self):
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="d")
        self.assertTrue(s.has('a'))
        self.assertFalse(s.has('e'))
        self.assertFalse(s.has('d.a'))
        self.assertTrue(s.has('d.a', namespace=""))
        self.assertTrue(s.has('a', namespace="d"))

    def test_get(self):
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="d")
        self.assertEqual(s.get('a'), 1)
        self.assertEqual(s.get('b'), 'c')
        self.assertEqual(s.get('d.a', namespace=""), 1)
        self.assertEqual(s.get('b', namespace="d"), 'c')
        # check default
        self.assertEqual(s.get('e'), None)
        self.assertEqual(s.get('e', default=1), 1)

    def test_set(self):
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="d")
        s.set('a', 2)
        self.assertEqual(s.get('a'), 2)
        s.set('d', 3)
        self.assertEqual(s.get('d'), 3)
        self.assertTrue('d.d' in s.settings)

    def test_remove(self):
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="d")
        s.remove('a')
        self.assertTrue('d.a' not in s.settings)
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="d")
        with self.assertRaises(KeyError):
            s.remove('e')

    def test_update(self):
        # update -> only change settings already in source
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        t = self.getSubjectClass()({'b': 'c'}, namespace="")
        s.update(t)
        self.assertTrue('d.a' in s.settings)
        self.assertFalse('d.b' in s.settings)
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        t = self.getSubjectClass()({'b': 'c'}, namespace="")
        s.update(t, "d")
        self.assertTrue('d.a' in s.settings)
        self.assertFalse('d.b' in s.settings)
        s = self.getSubjectClass()({'a': 1, 'b': 'a'}, namespace="d")
        self.assertEqual(s.get('b'), 'a')
        t = self.getSubjectClass()({'b': 'c'}, namespace="")
        s.update(t)
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('d.b' in s.settings)
        self.assertEqual(s.get('b'), 'c')
        s = self.getSubjectClass()({'a': 1, 'b': 'a'}, namespace="d")
        self.assertEqual(s.get('b'), 'a')
        t = self.getSubjectClass()({'b': 'c'}, namespace="d")
        s.update(t, "")
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('d.b' in s.settings)
        self.assertEqual(s.get('b'), 'c')
        s = self.getSubjectClass()({'a': 1, 'b': 'a'}, namespace="d")
        s.update({'b': 'c'})
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('d.b' in s.settings)
        self.assertEqual(s.get('b'), 'c')
        s = self.getSubjectClass()({'a': 1, 'b': 'a'}, namespace="d")
        s.update({'b': 'c'}, '')
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('d.b' in s.settings)
        self.assertEqual(s.get('b'), 'a')
        s = self.getSubjectClass()({'a': 1, 'b': 'a'}, namespace="d")
        s.update({'a': 'c'}, 'd')
        self.assertEqual(s.get('a'), 'c')

    def test_union(self):
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        t = self.getSubjectClass()({'b': 'c'}, namespace="")
        s.union(t)
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('d.b' in s.settings)
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        t = self.getSubjectClass()({'b': 'c'}, namespace="")
        s.union(t, "d")
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('d.b' in s.settings)
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        t = self.getSubjectClass()({'b': 'c'}, namespace="d")
        s.union(t, "")
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('d.b' in s.settings)
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        s.union({'b': 'c'}, '')
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('b' in s.settings)
        s.union({'b': 'c'})
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('d.b' in s.settings)
        s.union({'a': 'c'}, 'd')
        self.assertEqual(s.get('a'), 'c')

    def test_getitem(self):
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        self.assertEqual(s["a"], 1)
        with self.assertRaises(KeyError):
            s['b']

    def test_setitem(self):
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        self.assertEqual(s["a"], 1)
        s['a'] = 2
        self.assertEqual(s["a"], 2)
        with self.assertRaises(KeyError):
            s['b'] = 3

    def test_merge(self):
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        t = self.getSubjectClass()({'b': 'c'}, namespace="")
        v = self.getSubjectClass().merge(s, t)
        self.assertTrue('d.a' in s.settings)
        self.assertTrue('b' in t.settings)
        self.assertTrue('d.a' in v.settings)
        self.assertTrue('d.b' in v.settings)
        s = self.getSubjectClass()({'a': 1}, namespace="d")
        t = self.getSubjectClass()({'b': 'c'}, namespace="")
        v = self.getSubjectClass().merge(s, t, "")
        self.assertTrue('d.a' in v.settings)
        self.assertTrue('b' in t.settings)
        t = self.getSubjectClass()({'b': 'c'}, namespace="")
        v = self.getSubjectClass().merge(s, t, "d")
        self.assertTrue('d.a' in v.settings)
        self.assertTrue('d.b' in v.settings)

    def test_to_dict(self):
        s = self.getSubjectClass()()
        self.assertEqual(
            s.to_dict(), {}
        )
        s = self.getSubjectClass()({'a': 1, 'b': 'c'}, namespace="d")
        self.assertEqual(
            s.to_dict(), {'d.a': 1, 'd.b': 'c'}
        )

    def test_copy(self):
        s = self.getSubjectClass()({'a': 1})
        self.assertEqual(s.to_dict(), {'a': 1})
        s1 = s.copy()
        self.assertEqual(s1.to_dict(), {'a': 1})
        s["a"] = 2
        self.assertEqual(s1.to_dict(), {'a': 1})
        # test mutable objects
        s = self.getSubjectClass()({'a': [1, 2]})
        self.assertEqual(s.to_dict(), {'a': [1, 2]})
        s1 = s.copy()
        self.assertEqual(s1.to_dict(), {'a': [1, 2]})
        s["a"].append(3)
        self.assertEqual(s.to_dict(), {'a': [1, 2, 3]})
        self.assertEqual(s1.to_dict(), {'a': [1, 2]})

