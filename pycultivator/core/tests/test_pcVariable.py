"""Module testing the uvaSettings module"""

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.core import pcVariable

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PCVariableTest(UvaSubjectTestCase):

    _subject_cls = pcVariable.Variable
    _abstract = False

    default_value = 5

    def __init__(self, methodName='runTest'):
        super(PCVariableTest, self).__init__(methodName=methodName)

    @classmethod
    def getSubjectClass(cls):
        """Return reference to the settings class

        :return:
        :rtype: (...) -> pycultivator.core.pcVariable.Variable
        """
        return super(PCVariableTest, cls).getSubjectClass()

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.core.pcVariable.Variable
        """
        return super(PCVariableTest, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(value=self.default_value)

    def test_init(self):
        self.assertEqual(self.subject, self.default_value)
        self.assertEqual(self.subject.value, self.default_value)

    def test_value(self):
        self.assertEqual(self.subject.value, self.default_value)
        self.subject.value = 6
        self.assertEqual(self.subject.value, 6)
        self.assertEqual(self.subject.get(), 6)

    def test_set(self):
        self.assertEqual(self.subject.value, self.default_value)
        self.subject.set(6)
        self.assertEqual(self.subject.value, 6)
        self.assertEqual(self.subject.get(), 6)

    def test_copy(self):
        m1 = self.getSubjectClass()(1)
        self.assertEqual(m1.value, 1)
        m2 = m1.copy()
        self.assertEqual(m2.value, 1)
        m2.value = 2
        self.assertEqual(m2.value, 2)
        self.assertEqual(m1.value, 1)

    def test_private(self):
        s = self.getSubjectClass()(1, private=True)
        self.assertTrue(s.is_private)


class TrackingVariableTest(PCVariableTest):

    _subject_cls = pcVariable.TrackingVariable
    _abstract = False

    def __init__(self, methodName='runTest'):
        super(TrackingVariableTest, self).__init__(methodName=methodName)

    @classmethod
    def getSubjectClass(cls):
        """Return reference to the settings class

        :return:
        :rtype: (...) -> pycultivator.core.pcVariable.TrackingVariable
        """
        return super(TrackingVariableTest, cls).getSubjectClass()

    def getSubject(self):
        """Return the instance of the subject class

        :return:
        :rtype: pycultivator.core.pcVariable.TrackingVariable
        """
        return super(TrackingVariableTest, self).getSubject()

    def test_init(self):
        super(TrackingVariableTest, self).test_init()
        self.assertFalse(self.subject.has_changed)

    def test_has_changed(self):
        self.assertFalse(self.subject.has_changed)
        self.subject.value = 6
        self.assertTrue(self.subject.has_changed)

    def test_commit(self):
        self.assertFalse(self.subject.has_changed)
        self.subject.value = 6
        self.assertTrue(self.subject.has_changed)
        self.subject.commit()
        self.assertEqual(self.subject.value, 6)
        self.assertFalse(self.subject.has_changed)

    def test_restore(self):
        self.assertFalse(self.subject.has_changed)
        self.subject.value = 6
        self.assertTrue(self.subject.has_changed)
        self.subject.restore()
        self.assertEqual(self.subject.value, self.default_value)
        self.assertFalse(self.subject.has_changed)


class ConstrainedVariableTest(PCVariableTest):

    _subject_cls = pcVariable.ConstrainedVariable
    _abstract = False

    default_maximum = 10
    default_minimum = 0
    default_includes = True

    @classmethod
    def getSubjectClass(cls):
        """Return reference to the settings class

        :return:
        :rtype: (...) -> pycultivator.core.pcVariable.ConstrainedVariable
        """
        return super(ConstrainedVariableTest, cls).getSubjectClass()

    def getSubject(self):
        """Return the instance of the subject class

        :return:
        :rtype: pycultivator.core.pcVariable.ConstrainedVariable
        """
        return super(ConstrainedVariableTest, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(
            self.default_value, minimum=self.default_minimum, maximum=self.default_maximum,
            includes=self.default_includes
        )

    def test_init(self):
        super(ConstrainedVariableTest, self).test_init()
        self.assertEqual(self.subject.minimum, self.default_minimum)
        self.assertEqual(self.subject.maximum, self.default_maximum)

    def test_set_minimum(self):
        self.subject.value = 5
        self.subject.minimum = 3
        self.assertEqual(self.subject.value, 5)
        import warnings
        with warnings.catch_warnings(record=True) as w:
            self.subject.minimum = 6
        self.assertEqual(len(w), 1)
        self.assertIsNone(self.subject.value)

    def test_set_maximum(self):
        self.subject.value = 5
        self.subject.maximum = 6
        self.assertEqual(self.subject.value, 5)
        import warnings
        with warnings.catch_warnings(record=True) as w:
            self.subject.maximum = 4
        self.assertEqual(len(w), 1)
        self.assertIsNone(self.subject.value)

    def test_validate_lower(self):
        pass

    def test_validate(self):
        pass

    def test_set(self):
        pass

    def test_copy(self):
        super(ConstrainedVariableTest, self).test_copy()
        m1 = self.getSubjectClass()(1, minimum=9, maximum=10)
        self.assertEqual(m1, 1)
        self.assertEqual(m1.value, 1)
        self.assertEqual(m1.minimum, 9)
        m2 = m1.copy()
        self.assertEqual(m2, 1)
        self.assertEqual(m2.value, 1)
        self.assertEqual(m2.minimum, 9)
        m2.minimum = 3
        self.assertEqual(m2.minimum, 3)
        self.assertEqual(m1.minimum, 9)
