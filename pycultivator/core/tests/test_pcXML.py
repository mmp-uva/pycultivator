# coding=utf-8
"""
Test file for xml.py
"""

from pycultivator.core.tests import UvaTestCase, UvaSubjectTestCase
import os

from lxml import etree as et
from pycultivator.core import pcXML

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class AbstractXMLTest(UvaSubjectTestCase):
    """Tests the functions of the _AbstractXML Class"""

    _abstract = False
    _subject_class = pcXML._AbstractXML

    SAMPLE_XML_STRING = "<a value='0'><b id='5'>c</b><b id='6'>d</b></a>"
    SAMPLE_XML = pcXML.et.XML(SAMPLE_XML_STRING)

    #TODO: change to pkginfo
    CONFIG_DIR = os.path.realpath(
        os.path.join(os.path.dirname(__file__), "..", "..", "config", "schema")
    )
    SCHEMA_FILE = os.path.join(CONFIG_DIR, "pycultivator.xsd")
    CONFIG_FILE = os.path.join(CONFIG_DIR, "configuration.xml")

    def assertEqualXML(self, first, second, **kwargs):
        return self.assertEqual(et.tostring(first), et.tostring(second), **kwargs)

    def __init__(self, methodName='runTest'):
        super(AbstractXMLTest, self).__init__(methodName=methodName)
        self._subject = None

    @classmethod
    def getSubjectClass(cls):
        """Return reference to the settings class

        :return:
        :rtype: (...) -> pycultivator.core.pcXML._AbstractXML
        """
        return cls._subject_class

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()()

    def getSubject(self):
        """Return freshly made subject object

        :return:
        :rtype: pycultivator.core.pcXML._AbstractXML
        """
        return self._subject

    def setUp(self):
        self._subject = self.initSubject()

    def test_isSourceXMLElement(self):
        xml = self.SAMPLE_XML
        xmlt = xml.getroottree()
        self.assertTrue(self.getSubjectClass().isSourceXMLElement(xml))
        self.assertFalse(self.getSubjectClass().isSourceXMLElement(xmlt))

    def test_isSourceXMLElementTree(self):
        xml = self.SAMPLE_XML
        xmlt = xml.getroottree()
        self.assertFalse(self.getSubjectClass().isSourceXMLElementTree(xml))
        self.assertTrue(self.getSubjectClass().isSourceXMLElementTree(xmlt))

    def test_isSourceXML(self):
        xmls = self.SAMPLE_XML_STRING
        xml = self.SAMPLE_XML
        self.assertTrue(self.getSubjectClass().isSourceXML(xml))
        self.assertFalse(self.getSubjectClass().isSourceXML(xmls))

    def test_isSourceXMLString(self):
        xmls = self.SAMPLE_XML_STRING
        xml = self.SAMPLE_XML
        self.assertTrue(self.getSubjectClass().isSourceXMLString(xmls))
        self.assertFalse(self.getSubjectClass().isSourceXMLString(xml))

    def test_isSourceFile(self):
        xmls = self.SAMPLE_XML_STRING
        xmlp = self.CONFIG_FILE
        xml = self.SAMPLE_XML
        self.assertTrue(self.getSubjectClass().isSourceFile(xmlp))
        self.assertFalse(self.getSubjectClass().isSourceFile(xmls))
        self.assertFalse(self.getSubjectClass().isSourceFile(xml))

    def test_readXMLString(self):
        xml = self.SAMPLE_XML_STRING
        s = self.getSubjectClass().readXMLString(xml)
        self.assertIsNotNone(s)
        self.assertTrue(isinstance(s, pcXML.et._ElementTree))
        with self.assertRaises(pcXML.et.XMLSyntaxError):
            self.getSubjectClass().readXMLString("<a>b</a")

    def test_readFile(self):
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().readFile(xmlp)
        self.assertIsNotNone(s)
        self.assertTrue(isinstance(s, pcXML.et._ElementTree))

    def test_readSource(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().readSource(xml)
        self.assertIsNotNone(s)
        self.assertTrue(isinstance(s, pcXML.et._ElementTree))
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().readSource(xmlp)
        self.assertIsNotNone(s)
        self.assertTrue(isinstance(s, pcXML.et._ElementTree))


class XMLTest(AbstractXMLTest):

    _abstract = False
    _subject_class = pcXML.XML

    def test_readSource(self):
        xml = self.SAMPLE_XML
        xmlo = self.getSubjectClass().readSource(xml)
        s = self.getSubjectClass().readSource(xmlo)
        self.assertIsNotNone(s)
        self.assertIs(s, xmlo)

    def test_createFromSource(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertIsNotNone(s)
        self.assertTrue(isinstance(s, self.getSubjectClass()))
        xmlps = self.SCHEMA_FILE
        s = self.getSubjectClass().createFromSource(xml, xmlps)
        self.assertIsNotNone(s)
        xmls = self.getSubjectClass().readSource(xmlps)
        s = self.getSubjectClass().createFromSource(xml, xmls)
        self.assertIsNotNone(s)
        schema = pcXML.XMLSchema.createFromSource(xmlps)
        s = self.getSubjectClass().createFromSource(xml, schema)
        self.assertIsNotNone(s)
        schema = pcXML.et.XMLSchema(xmls)
        s = self.getSubjectClass().createFromSource(xml, schema)
        self.assertFalse(s.hasSchema())

    def test_getSchemaName(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertIsNone(s.getSchemaName())
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        self.assertEqual(s.getSchemaName(), "pycultivator_device.xsd")

    def test_getSchemaNameSpace(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertIsNone(s.getSchemaNameSpace())
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        self.assertEqual(s.getSchemaNameSpace(), "pyCultivator")

    def test_getFilePath(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertIsNone(s.getFilePath())
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        self.assertEqual(s.getFilePath(), xmlp)

    def test_loadSchema(self):
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        self.assertIsNotNone(s)
        schema = s.loadSchema()
        self.assertIsNotNone(schema)
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertIsNotNone(s)
        schema = s.loadSchema()
        self.assertIsNone(schema)

    def test_isValid(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertIsNotNone(s)
        self.assertTrue(s.isValid())
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        self.assertIsNotNone(s)
        self.assertTrue(s.hasSchema())
        self.assertTrue(s.isValid())
        s.root.set("schema_version", "1.4")
        self.assertFalse(s.isValid())

    def test_getElementsByXPath(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        elements = s.getElementsByXPath("b")
        self.assertEqual(len(elements), 2)
        elements = s.getElementsByXPath("b[@id=$id]", id='5')
        self.assertEqual(len(elements), 1)
        elements = s.getElementsByXPath("b", namespace="", prefix="")
        self.assertEqual(len(elements), 2)
        elements = s.getElementsByXPath("pc:b", namespace="pyCultivator", prefix="pc")
        self.assertEqual(len(elements), 0)
        elements = s.getElementsByXPath("pc:b", prefix="pc")
        self.assertEqual(len(elements), 0)
        elements = s.getElementsByXPath("b", namespace="pyCultivator")
        self.assertEqual(len(elements), 2)
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        elements = s.getElementsByXPath("device")
        self.assertEqual(len(elements), 0, msg="Get device elements without prefix and namespace")
        elements = s.getElementsByXPath("pc:device", namespace="pyCultivator", prefix="pc")
        self.assertEqual(len(elements), 1, msg="Get channels elements with prefix and namespace")
        elements = s.getElementsByXPath("pc:device", prefix="pc")
        self.assertEqual(len(elements), 1, msg="Get device elements with prefix")
        elements = s.getElementsByXPath("device", prefix="")
        self.assertEqual(len(elements), 0, msg="Get channels elements without prefix")

    def test_getElementTag(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        """:type: uvaCultivator.uvaXML.XML"""
        self.assertEqual(s.getElementTag(s.root), "a")
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        """:type: uvaCultivator.uvaXML.XML"""
        self.assertEqual(s.getElementTag(s.root), "{pyCultivator}config")

    def test_getCleanElementTag(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        """:type: uvaCultivator.uvaXML.XML"""
        self.assertEqual(s.getCleanElementTag(s.root), "a")
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        """:type: uvaCultivator.uvaXML.XML"""
        self.assertEqual(s.getCleanElementTag(s.root), "config")
        xml = pcXML.et.Element("a", nsmap={None: "bla"})
        self.assertEqual(s.getCleanElementTag(xml), "a")

    def test_filterElementsByTag(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        """:type: uvaCultivator.uvaXML.XML"""
        elements = s.getElementsByTag("*", scope="//")
        self.assertGreater(len(elements), 0)
        b_elements = s.getElementsByTag("b", scope="//")
        self.assertEqual(len(b_elements), 2)
        f_elements = list(s.filterElementsByTag(elements, "b"))
        self.assertEqual(len(f_elements), len(b_elements))

    def test_getElementsByTag(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        elements = s.getElementsByTag("b")
        self.assertEqual(len(elements), 2, msg='Number of "b" elements not equal to 2')
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        elements = s.getElementsByTag("channel", prefix="pc")
        self.assertEqual(len(elements), 0, msg="Number of local 'channel' elements not equal to 0")
        elements = s.getElementsByTag("channel", prefix="pc", scope=".//")
        self.assertLessEqual(len(elements), 2, msg="Number of global 'channel' elements larger than 1")

    def test_getElementByTag(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        elements = s.getElementsByTag("b")
        self.assertEqual(len(elements), 2)
        element = s.getElementByTag("b")
        self.assertTrue(isinstance(element, pcXML.et._Element))
        self.assertEqual(element, elements[0])

    def tst_getElementsById(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        elements = s.getElementsById('5', scope=".//")
        self.assertEqual(len(elements), 1, "Did not find 1 element with Id = 5 with global scope")
        elements = s.getElementsById('5', scope="./")
        self.assertEqual(len(elements), 0, "Did find more than 0 elements with Id = 6 with local scope")

    def test_filterElementsById(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        """:type: uvaCultivator.uvaXML.XML"""
        elements = s.getElementsByTag("b", scope="//")
        self.assertEqual(len(elements), 2)
        id_elements = s.getElementsById("5", scope="//")
        self.assertEqual(len(id_elements), 1)
        f_elements = list(s.filterElementsById(elements, "5"))
        self.assertEqual(len(f_elements), len(id_elements))

    def test_hasElementAttribute(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertTrue(s.hasElementAttribute(s.root, "value"))
        self.assertFalse(s.hasElementAttribute(s.root, "foo"))

    def test_getElementAttribute(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertEqual(s.getElementAttribute(s.root, "value"), '0')
        self.assertEqual(s.getElementAttribute(s.root, "value", _type=int), 0)
        s.setElementAttribute(s.root, "value", "a")
        self.assertEqual(s.getElementAttribute(s.root, "value"), 'a')
        self.assertEqual(s.getElementAttribute(s.root, "value", _type=int), None)
        # restore xml
        s.setElementAttribute(s.root, "value", 0)

    def test_setElementAttribute(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertEqual(s.getElementAttribute(s.root, "value"), '0')
        s.setElementAttribute(s.root, "value", "a")
        self.assertEqual(s.getElementAttribute(s.root, "value"), 'a')
        s.setElementAttribute(s.root, "value", 0)
        self.assertEqual(s.getElementAttribute(s.root, "value"), '0')
        s.setElementAttribute(s.root, "value", '0')
        self.assertEqual(s.getElementAttribute(s.root, "value"), '0')

    def test_getElementState(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        # test default
        self.assertFalse(s.getElementState(s.root))
        self.assertTrue(s.getElementState(s.root, default=True))
        s.setElementAttribute(s.root, "state", "true")
        self.assertTrue(s.getElementState(s.root))
        s.setElementAttribute(s.root, "state", "1")
        self.assertTrue(s.getElementState(s.root))
        s.setElementAttribute(s.root, "state", "false")
        self.assertFalse(s.getElementState(s.root))
        s.setElementAttribute(s.root, "state", "blabla")
        self.assertFalse(s.getElementState(s.root))
        s.setElementAttribute(s.root, "state", "blabla")
        self.assertFalse(s.getElementState(s.root, default=True))

    def test_setElementState(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertEqual(s.getElementState(s.root), False)
        s.setElementState(s.root, True)
        self.assertEqual(s.getElementState(s.root), True)
        self.assertEqual(s.getElementAttribute(s.root, "state"), 'true')
        s.setElementState(s.root, False)
        self.assertEqual(s.getElementAttribute(s.root, "state"), 'false')

    def test_getElementValue(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        self.assertEqual(s.getElementAttribute(s.root, "value"), '0')
        self.assertEqual(s.getElementValue(s.root), "0")
        self.assertEqual(s.getElementValue(s.root), s.getElementAttribute(s.root, "value"))

    def test_createElement(self):
        xml = self.SAMPLE_XML
        s = self.getSubjectClass().createFromSource(xml)
        """:type: pycultivator.core.pcXML.XML"""
        element = s.createElement("a")
        self.assertEqual(element.nsmap, s.getRoot().nsmap)
        # check if we can find the element
        elements = s.getElementsByXPath("/a")
        self.assertEqual(len(elements), 1)
        xmlp = self.CONFIG_FILE
        s = self.getSubjectClass().createFromSource(xmlp)
        """:type: pycultivator.core.pcXML.XML"""
        element = s.createElement("b")
        self.assertEqual(element.nsmap, {None: s.getXMLNameSpace()})
        # check if we can find the element
        elements = s.getElementsByXPath("/pc:b", root=element, prefix="pc")
        self.assertEqual(len(elements), 1)


class XMLSchemaTest(AbstractXMLTest):

    _abstract = False
    _subject_class = pcXML.XMLSchema

    def test_isSourceFile(self):
        xmls = self.SAMPLE_XML_STRING
        xmlp = self.CONFIG_FILE
        xmlps = self.SCHEMA_FILE
        self.assertTrue(self.getSubjectClass().isSourceFile(xmlps))
        self.assertFalse(self.getSubjectClass().isSourceFile(xmlp))
        self.assertFalse(self.getSubjectClass().isSourceFile(xmls))

    def test_isSourceSchema(self):
        xmlps = self.SCHEMA_FILE
        xmls = self.getSubjectClass().readSource(xmlps)
        xmlp = self.CONFIG_FILE
        xml = pcXML.XML.readSource(xmlp)
        self.assertTrue(self.getSubjectClass().isSourceSchema(xmlps))
        self.assertTrue(self.getSubjectClass().isSourceSchema(xmls))
        self.assertFalse(self.getSubjectClass().isSourceSchema(xmlp))
        self.assertFalse(self.getSubjectClass().isSourceSchema(xml))

    def test_createFromSource(self):
        xmlps = self.SCHEMA_FILE
        xmls = self.getSubjectClass().readSource(xmlps)
        s = self.getSubjectClass().createFromSource(xmls)
        self.assertIsNotNone(s)
        self.assertTrue(isinstance(s, self.getSubjectClass()))
        s = self.getSubjectClass().createFromSource(xmlps)
        self.assertIsNotNone(s)
        self.assertTrue(isinstance(s, self.getSubjectClass()))

    def test_find_schema(self):
        xmlps = self.SCHEMA_FILE
        fd = os.path.dirname(xmlps)
        fn = os.path.basename(xmlps)
        f = self.getSubjectClass().find_schema(fd, fn)
        self.assertEqual(len(f), 1)
