# coding=utf-8
"""
Test file to Test pcLogger.py
"""

from pycultivator.core.tests import UvaSubjectTestCase
from unittest.case import skip
from testfixtures import LogCapture, OutputCapture
from pycultivator.core import pcLogger

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SubjectTestUvALog(UvaSubjectTestCase):

    _abstract = False

    def setUp(self):
        self._subject = pcLogger.createLogger(__name__, parent="")
        self._subject.addHandler(pcLogger.UVAStreamHandler())

    def test_subject_create(self):
        self.assertEqual(self._subject.name, __name__)

    def test_subjectfixtures(self):
        with LogCapture() as l:
            logger = pcLogger.getLogger()
            logger.info('test fixtures #1')
        l.check(('root', 'INFO', 'test fixtures #1'))

    def test_subject_critical(self):
        self.setUp()
        log_name = self._subject.name
        with LogCapture(log_name) as l:
            self._subject.critical("test_subject_critical #1")
        l.check((log_name, "CRITICAL", "test_subject_critical #1"))

    def test_subject_propagate(self):
        parent_name = self._subject.name
        child_name = "{}.{}".format(self._subject.name, "child")
        with LogCapture((parent_name, child_name)) as l:
            child_subject = pcLogger.createLogger("child", self._subject.name)
            child_subject.propagate = False
            child_subject.info("test_subject_propagate #1")
            l.check((child_name, "INFO", "test_subject_propagate #1"))
        with LogCapture((parent_name, child_name)) as l:
            child_subject.propagate = True
            child_subject.info("test_subject_propagate #2")
            l.check(
                (child_name, "INFO", "test_subject_propagate #2"),
                (child_name, "INFO", "test_subject_propagate #2"))

    def test_no_handlers(self):
        while len(self.subject.root.handlers) > 0:
            hdlr = self.subject.root.handlers[0]
            self.subject.root.removeHandler(hdlr)
        # normal critical message
        log_name = self._subject.name
        with LogCapture(log_name) as c:
            self._subject.critical("test_no_handlers #1")
        c.check((log_name, "CRITICAL", "test_no_handlers #1"))
        log = pcLogger.createLogger("no_handler_test", parent="")
        with OutputCapture() as c:
            log.critical("test_no_handlers #2")
        c.captured.endswith("no_handler_test [CRITICAL]: test_no_handlers #2")
