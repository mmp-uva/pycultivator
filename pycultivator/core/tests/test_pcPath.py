"""
Test file to Test pcObject.py
"""

from pycultivator.core.tests import UvaTestCase
from pycultivator.core import pcPath
from pycultivator.core.objects import HierarchicalObject

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SimpleObject(HierarchicalObject):

    def __init__(self, identity, parent=None, **kwargs):
        super(SimpleObject, self).__init__(identity, parent=parent, **kwargs)
        self._children = []
        if parent is not None:
            self.parent.addChild(self)

    def addChild(self, obj):
        if obj not in self._children:
            self._children.append(obj)

    def removeChild(self, obj):
        if obj in self._children:
            self._children.remove(obj)

    @property
    def children(self):
        return self._children


class ComplexObject(SimpleObject):

    def __init__(self, identity, parent=None, color="blue", **kwargs):
        super(ComplexObject, self).__init__(identity, parent=parent, **kwargs)
        self._color = color

    def makeIdentity(self, name):
        return int(name)

    @property
    def color(self):
        return self._color

    foo = "bar"


class TestPath(UvaTestCase):

    def test_segments(self):
        root = SimpleObject("/", None)
        self.assertListEqual(root.path.segments, [])
        root = SimpleObject("/", None)
        self.assertListEqual(root.path.segments, [])
        child = SimpleObject("a", root)
        self.assertListEqual(child.path.segments, ["a"])
        child = ComplexObject(1, root)
        self.assertListEqual(child.path.segments, [1])

    def test_path(self):
        root = SimpleObject("0", None)
        self.assertEqual(root.path.__str__(), "/")
        child_a = SimpleObject("a", root)
        self.assertEqual(child_a.path.__str__(), "/a")
        child_1 = ComplexObject(1, root)
        self.assertEqual(child_1.path.__str__(), "/1")

    def test_glue(self):
        self.assertEqual(pcPath.Path.glue(['a']), 'a')
        self.assertEqual(pcPath.Path.glue(['a', 1]), 'a/1')
        self.assertEqual(pcPath.Path.glue(['a', 1, "c"]), 'a/1/c')
        self.assertEqual(pcPath.Path.glue(['a', 1, None]), 'a/1/None')

    def test_resolve_segment(self):
        root = SimpleObject("root", None)
        child_a = SimpleObject("a", root)
        child_1 = ComplexObject(1, root)
        child_2 = ComplexObject(2, root, color="red")
        # match by identity
        matches = list(root.path.match_segment("$identity=1"))
        self.assertEqual(matches, [child_1])
        # match by type
        matches = list(root.path.match_segment("$type=ComplexObject"))
        self.assertEqual(matches, [child_1, child_2])
        matches = list(root.path.match_segment("$type=SimpleObject"))
        self.assertEqual(matches, [child_a])
        # match by property color
        # all blue
        matches = list(root.path.match_segment("$color=blue"))
        self.assertEqual(matches, [child_1])
        # all red
        matches = list(root.path.match_segment("$color=red"))
        self.assertEqual(matches, [child_2])
        # not matching
        matches = list(root.path.match_segment("$color=black]"))
        self.assertEqual(matches, [])
        # match by attribute foo
        matches = list(root.path.match_segment("$foo=bar"))
        self.assertEqual(matches, [child_1, child_2])
        # match by combination
        matches = list(root.path.match_segment("$foo=bar,$color=blue"))
        self.assertEqual(matches, [child_1])
        matches = list(root.path.match_segment("$foo=bar,$color=red"))
        self.assertEqual(matches, [child_2])
        matches = list(root.path.match_segment("$foo=bar,$type=SimpleObject"))
        self.assertEqual(matches, [])
        # match invalid pattern
        matches = list(root.path.match_segment("$foo=bar,type=SimpleObject"))
        self.assertEqual(matches, [])
        # not matching
        matches = list(root.path.match_segment("$foo=boo"))
        self.assertEqual(matches, [])
        # match by name
        matches = list(root.path.match_segment("$name=a"))
        self.assertEqual(matches, [child_a])
        matches = list(root.path.match_segment("$name=2"))
        self.assertEqual(matches, [child_2])
        matches = list(root.path.match_segment("$name=d"))
        self.assertEqual(matches, [])
        # match by parent (RIDICULOUS BUT COOL!)
        matches = list(root.path.match_segment("$parent=root"))
        self.assertEqual(matches, [child_a, child_1, child_2])

    def test_resolve(self):
        root = SimpleObject("0", None)
        child_a = SimpleObject("1", root)
        child_b = SimpleObject("2", root)
        child_aa = SimpleObject("aa", child_a)
        child_ab = SimpleObject("ab", child_a)
        child_ba = SimpleObject("ba", child_b)
        items = list(root.path.resolve(""))
        self.assertEqual(items, [root])
        items = list(root.path.resolve("."))
        self.assertEqual(items, [root])
        items = list(root.path.resolve("/"))
        self.assertEqual(items, [root])
        # FIXME: following result is unintuitive -> should return [root]
        items = list(root.path.resolve("*"))
        self.assertEqual(items, [child_a, child_b])
        items = list(root.path.resolve("./1"))
        self.assertEqual(items, [child_a])
        items = list(root.path.resolve("/1"))
        self.assertEqual(items, [child_a])
        items = list(root.path.resolve("/[0-9]"))
        self.assertEqual(items, [child_a, child_b])
        items = list(root.path.resolve("/*"))
        self.assertEqual(items, [child_a, child_b])
        items = list(root.path.resolve("/*/*"))
        self.assertEqual(items, [child_aa, child_ab, child_ba])
        items = list(child_aa.path.resolve("../*"))
        self.assertEqual(items, [child_aa, child_ab])
        items = list(child_a.path.resolve(".."))
        self.assertEqual(items, [root])
        items = list(child_a.path.resolve("."))
        self.assertEqual(items, [child_a])
        # non-specific searches for children are accepted
        items = list(child_aa.path.resolve("./*"))
        self.assertEqual(items, [])
        # test wrong paths
        # point to non-existent parent
        items = list(root.path.resolve(".."))
        self.assertEqual(items, [])
        # point to non-existent child
        items = list(root.path.resolve("./9"))
        self.assertEqual(items, [])

    def test_relative_to(self):
        # root  -> child_a  -> child_aa
        #                   -> child_ab
        #       -> child_b  -> child_ba
        root = SimpleObject("0", None)
        child_a = SimpleObject("a", root)
        child_b = SimpleObject("b", root)
        child_aa = SimpleObject("aa", child_a)
        child_ab = SimpleObject("ab", child_a)
        child_ba = SimpleObject("ba", child_b)
        child = SimpleObject("1", None)
        # to self should always return "."
        self.assertEqual(root.path.relative_to(root), ".")
        self.assertEqual(child_aa.path.relative_to(child_aa), ".")
        # parent relative_to to child should always return ".."
        self.assertEqual(root.path.relative_to(child_a), "..")
        self.assertEqual(child_a.path.relative_to(child_aa), "..")
        # child relative to parent should return "./child"
        self.assertEqual(child_a.path.relative_to(root), "./a")
        self.assertEqual(child_aa.path.relative_to(child_a), "./aa")
        # sibling relative to sibling should return "../sibling"
        self.assertEqual(child_b.path.relative_to(child_a), "../b")
        self.assertEqual(child_ab.path.relative_to(child_aa), "../ab")
        # child of sibling should return "../sibling/child"
        self.assertEqual(child_ba.path.relative_to(child_a), "../b/ba")
        # to remote-sibling should return "../../parent-sibling/remote-sibling"
        self.assertEqual(child_ba.path.relative_to(child_aa), "../../b/ba")
        # non connected
        with self.assertRaises(pcPath.PathException):
            root.path.relative_to(child)
        with self.assertRaises(pcPath.PathException):
            child.path.relative_to(root)
        with self.assertRaises(pcPath.PathException):
            child.path.relative_to(child_ba)
        with self.assertRaises(pcPath.PathException):
            child_ba.path.relative_to(child)

    def test_resolve_absolute(self):
        """test if resolve returns the correct object using absolute path"""
        root = SimpleObject("0", None)
        child_a = SimpleObject("a", root)
        child_b = SimpleObject("b", root)
        child_aa = SimpleObject("aa", child_a)
        child_ab = SimpleObject("ab", child_a)
        child_ba = SimpleObject("ba", child_b)
        child = SimpleObject("1", None)
        # absolute path to root gives root when using root as root
        items = list(root.path.resolve(root.path.__str__()))
        self.assertEqual(items, [root])
        # absolute path to child gives child when using root as root
        items = list(root.path.resolve(child_a.path.__str__()))
        self.assertEqual(items, [child_a])
        # absolute path to child_aa gives child_aa when using root as root
        items = list(root.path.resolve(child_aa.path.__str__()))
        self.assertEqual(items, [child_aa])
        # absolute path to child_aa gives child_aa when using child_a as root
        items = list(child_a.path.resolve(child_aa.path.__str__()))
        self.assertEqual(items, [child_aa])
        # absolute path to root gives root when using child_a as root
        items = list(child_b.path.resolve(root.path.__str__()))
        self.assertEqual(items, [root])
        # absolute path to unconnected root gives root when using root as root
        items = list(root.path.resolve(child.path.__str__()))
        self.assertEqual(items, [root])
        # absolute path to child_a gives error when using unconnected root as root
        items = list(child.path.resolve(child_a.path.__str__()))
        self.assertEqual(items, [])
        # absolute path to child_ab gives child_ab when using child_ba as root
        items = list(child_ba.path.resolve(child_ab.path.__str__()))
        self.assertEqual(items, [child_ab])

    def test_reciprocal_relative(self):
        """test if resolve returns the correct object using relative_to"""
        root = SimpleObject("0", None)
        child_a = SimpleObject("a", root)
        child_b = SimpleObject("b", root)
        child_aa = SimpleObject("aa", child_a)
        child = SimpleObject("1", None)
        # relative path of root to root gives root when using root as root
        items = list(root.path.resolve(root.path.relative_to(root)))
        self.assertEqual(items, [root])
        # relative path of child_a to root gives child_a when using root as root
        items = list(root.path.resolve(child_a.path.relative_to(root)))
        self.assertEqual(items, [child_a])
        # relative path of child_a to root gives error when using unconnected root
        with self.assertRaises(pcPath.PathException):
            root.path.resolve(child_a.path.relative_to(child))
        # relative path of child_aa to root gives child_aa when using root as root
        items = list(root.path.resolve(child_aa.path.relative_to(root)))
        self.assertEqual(items, [child_aa])
        # relative path of child_aa to child_a gives child_aa when using child_a as root
        items = list(child_a.path.resolve(child_aa.path.relative_to(child_a)))
        self.assertEqual(items, [child_aa])
        # relative path of child_aa to child_a gives error when using root as root
        items = list(root.path.resolve(child_aa.path.relative_to(child_a)))
        self.assertListEqual(items, [])


