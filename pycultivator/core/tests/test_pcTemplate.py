"""Module testing the uvaSettings module"""

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.core import pcTemplate, pcVariable

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class ExampleTemplate(pcTemplate.TemplateBase):

    name = pcVariable.Variable(5)
    age = pcVariable.Variable()
    weight = pcVariable.Variable(private=True)


class SubExampleTemplate(ExampleTemplate):

    height = pcVariable.Variable(180)


class PCTemplateTest(UvaSubjectTestCase):

    _subject_cls = ExampleTemplate
    _abstract = False

    default_name = "value"
    default_value = 6

    def __init__(self, methodName='runTest'):
        super(PCTemplateTest, self).__init__(methodName=methodName)

    @classmethod
    def getSubjectClass(cls):
        """Return reference to the settings class

        :return:
        :rtype: (...) -> ExampleTemplate
        """
        return super(PCTemplateTest, cls).getSubjectClass()

    def getSubject(self):
        """

        :return:
        :rtype: ExampleTemplate
        """
        return super(PCTemplateTest, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(name=self.default_value)

    def test_init(self):
        self.assertEqual(self.getSubject().name, self.default_value)
        self.assertIsNone(self.getSubject().age)

    def test_variables(self):
        self.assertListEqual(
            sorted(self.getSubject().variables.keys()),
            sorted(["name", "age", "weight"])
        )

    def test_private(self):
        self.assertTrue(hasattr(self.getSubject(), "name"))
        self.assertFalse(hasattr(self.getSubject(), "weight"))

    def test_variable_set(self):
        self.getSubject().age = 0
        self.assertEqual(self.getSubject().age, 0)
        self.assertEqual(self.getSubject()._age.get(), 0)

    def test_object_independence(self):
        s1 = self.getSubjectClass()(name=self.default_value, age=1)
        s2 = self.getSubjectClass()(name="foo", age=2)
        self.assertNotEqual(s1.age, s2.age)
        self.assertNotEqual(s1._age.get(), s2._age.get())
        self.assertNotEqual(s1.name, s2.name)
        self.assertNotEqual(s1._name.get(), s2._name.get())
        s1.name = "bar"
        self.assertEqual(s1.name, "bar")
        self.assertNotEqual(s1.name, s2.name)
        self.assertNotEqual(s1._name.get(), s2._name.get())
        s1._name.set("bar")
        self.assertEqual(s1.name, "bar")
        self.assertEqual(s1._name.get(), "bar")
        self.assertNotEqual(s1.name, s2.name)
        self.assertNotEqual(s1._name.get(), s2._name.get())

    def test_inheritance(self):
        s = SubExampleTemplate()
        self.assertEqual(s.height, 180)
        self.assertEqual(s.name, 5)
        self.assertFalse(hasattr(s, "weight"))


class ExampleTrackingTemplate(pcTemplate.TrackingTemplate):

    name = pcVariable.TrackingVariable("test")
    age = pcVariable.TrackingVariable()


class TrackingTemplateTest(UvaSubjectTestCase):

    _subject_cls = ExampleTrackingTemplate
    _abstract = False

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()()

    def test_has_changed(self):
        self.assertFalse(self.getSubject().hasChanged())
        self.assertFalse(self.getSubject().hasChanged("name"))
        self.getSubject().name = "foo"
        self.assertTrue(self.getSubject().hasChanged())
        self.assertTrue(self.getSubject().hasChanged("name"))
        self.assertFalse(self.getSubject().hasChanged("age"))

    def test_commit_all(self):
        self.assertFalse(self.getSubject().hasChanged())
        self.getSubject().name = "foo"
        self.assertTrue(self.getSubject().hasChanged())
        self.getSubject().commit()
        self.assertFalse(self.getSubject().hasChanged())

    def test_commit_name(self):
        self.assertFalse(self.getSubject().hasChanged())
        self.getSubject().name = "foo"
        self.getSubject().age = 1
        self.assertTrue(self.getSubject().hasChanged())
        self.getSubject().commit("name")
        self.assertTrue(self.getSubject().hasChanged())

    def test_restore_all(self):
        self.assertFalse(self.getSubject().hasChanged())
        self.getSubject().name = "foo"
        self.getSubject().age = 99
        self.assertTrue(self.getSubject().hasChanged())
        self.getSubject().restore()
        self.assertNotEqual(self.getSubject().name, "foo")
        self.assertFalse(self.getSubject().hasChanged())

    def test_restore_name(self):
        self.assertFalse(self.getSubject().hasChanged())
        self.getSubject().name = "foo"
        self.getSubject().age = 99
        self.assertTrue(self.getSubject().hasChanged())
        self.getSubject().restore("name")
        self.assertTrue(self.getSubject().hasChanged())
        self.assertFalse(self.getSubject().hasChanged("name"))
