import unittest


class TestSimple(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        raise unittest.SkipTest("Skip whole Case")

    def test_true(self):
        self.assertTrue(True)

    def test_false(self):
        self.assertTrue(False, msg="Is not True")

    def test_skip(self):
        raise unittest.SkipTest("Skip this test")


class TestSubSimple(TestSimple):

    @classmethod
    def setUpClass(cls):
        raise unittest.SkipTest("Skip subclass")

    def test_subclass(self):
        self.assertTrue(True)


# class SubPycharmTest(TestPyCharm):
#
#     @classmethod
#     def setUpClass(cls):
#         raise unittest.SkipTest("Skip subclass")
#
#     def test_init(self):
#         self.assertTrue(True)


if __name__ == "__main__":
    unittest.main()
