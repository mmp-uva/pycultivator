"""Core UnitTests for pyCultivator including base TestCase class"""

import unittest2
from pycultivator.core import pcLogger

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class UvaTestCase(unittest2.TestCase):
    """The basic TestCase class for the pyCultivator package

    Facilitates:
    - logging functions using the pyCultivator uvaLog module
    - auto logging to console
    - skipping of TestCases written against abstract classes (set _abstract = True)
    """

    _abstract = False

    _log_level = pcLogger.LEVELS["WARNING"]
    _log = None

    @classmethod
    def isAbstract(cls):
        return cls._abstract is True

    @classmethod
    def getLog(cls):
        """ Return reference to logging object

        :return:
        :rtype: pycultivator.foundation.pcLogger.PCLogger
        """
        return cls._log

    @classmethod
    def setUpClass(cls):
        cls._log = pcLogger.getLogger()
        cls.getLog().addHandler(pcLogger.UVAStreamHandler())
        cls.getLog().setLevel(level=cls._log_level)

    def setUp(self):
        if self.isAbstract():
            raise unittest2.SkipTest("Abstract TestCase; skip %s" % __name__)
        super(UvaTestCase, self).setUp()


class UvaSubjectTestCase(UvaTestCase):
    """A TestCase class for the pyCultivator package that is designed specifically for testing classes.

    Uses the idea of a test "subject" (in this case the instance of the class that is tested).
    Before each test the subject is created and after each test destructed.

    Facilities:
    - automatic creation and destruction of subject objects
    """

    _subject_cls = None
    _abstract = True

    def __init__(self, methodName='runTest'):
        super(UvaSubjectTestCase, self).__init__(methodName=methodName)
        self._subject = None

    def setUp(self):
        """Sets the conditions for the test case"""
        result = super(UvaSubjectTestCase, self).setUp()
        self.setSubject(self.initSubject())
        if self.getSubject() is None:
            self.fail("Failed to create the Subject object!")
        return result

    def tearDown(self):
        self.clearSubject()
        super(UvaSubjectTestCase, self).tearDown()

    @classmethod
    def getSubjectClass(cls):
        """Return class of the subject used in this test

        :rtype: callable -> pycultivator.core.tests.UvaSubjectTestCase._subject_cls
        """
        return cls._subject_cls

    def initSubject(self, *args, **kwargs):
        """Return an initialized version of the subject class

        :rtype: pycultivator.core.tests.UvaSubjectTestCase._subject_cls
        """
        return self.getSubjectClass()(*args, **kwargs)

    def getSubject(self):
        """Return the subject used in a test

        :rtype: pycultivator.core.tests.UvaSubjectTestCase._subject_cls
        """
        return self._subject

    @property
    def subject(self):
        """Access to the initialized subject used for this test

        :rtype: pycultivator.core.tests.UvaSubjectTestCase._subject_cls
        """
        return self.getSubject()

    @subject.setter
    def subject(self, s):
        self.setSubject(s)

    def setSubject(self, s):
        """Set the subject to a new object"""
        self._subject = s
        return self.getSubject()

    def clearSubject(self):
        """Resets the subject to None"""
        self.setSubject(None)

