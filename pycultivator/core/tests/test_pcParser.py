"""
Test file to Test pcObject.py
"""

from pycultivator.core.tests import UvaTestCase
from pycultivator.core import pcParser

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestParser(UvaTestCase):

    def test_to_list(self):
        p = pcParser.Parser
        self.assertEqual(p.to_list(None), [])
        self.assertEqual(p.to_list(None, [1]), [1])
        self.assertEqual(p.to_list(None, 1), [1])
        self.assertEqual(p.to_list(None, "a"), ["a"])
        self.assertEqual(p.to_list(1), [1])
        self.assertEqual(p.to_list("a"), ['a'])
        self.assertEqual(p.to_list({'a'}), ['a'])
        self.assertEqual(p.to_list(('a', 'b')), ['a', 'b'])
        self.assertEqual(p.to_list({'a': 1}), [1])


class TestAssertRange(UvaTestCase):

    def test_name(self):
        with self.assertRaisesRegex(ValueError, r"a \(= <int> -100\)"):
            pcParser.assert_range(-100, lwr=0, name="a")
        with self.assertRaisesRegex(ValueError, r"a \(= <str> a\)"):
            pcParser.assert_range("a", upr=0, name="a")

    def test_unconstrained(self):

        def f(v):
            return pcParser.assert_range(v)

        # no constrain
        self.assertTrue(f(-100))
        self.assertTrue(f(-5))
        self.assertTrue(f(0))
        self.assertTrue(f(5))
        self.assertTrue(f(-100))

    def test_lwr_constrain(self):

        def f(v):
            return pcParser.assert_range(v, lwr=-5)

        # constrain lower to -5 without include
        self.assertTrue(f(0))
        self.assertTrue(f(5))
        self.assertTrue(f(100))
        self.assertTrue(f(None))

        with self.assertRaises(ValueError):
            f(-100)
            f(-5)

        # constrain lower to -5 with include
        self.assertTrue(f(-5))
        self.assertTrue(f(0))
        self.assertTrue(f(5))
        self.assertTrue(f(100))

        with self.assertRaises(ValueError):
            f(-100)

    def test_upr_constrain(self):
        # constrain upper to 5 without include
        def f(v):
            return pcParser.assert_range(v, upr=5)

        self.assertTrue(f(-100))
        self.assertTrue(f(-5))
        self.assertTrue(f(0))

        with self.assertRaises(ValueError):
            pcParser.assert_range(5, upr=5)
            pcParser.assert_range(100, upr=5)

        # constrain upper to 5 with include
        def f(v):
            return pcParser.assert_range(v, upr=5, include=True)

        self.assertTrue(f(-100))
        self.assertTrue(f(-5))
        self.assertTrue(f(0))
        self.assertTrue(f(5))

        with self.assertRaises(ValueError):
            pcParser.assert_range(f(100))

    def test_both_constrained(self):
        # constrain to [-5,5] without include
        def f(v):
            return pcParser.assert_range(v, upr=5)

        self.assertTrue(f(0))

        with self.assertRaises(ValueError):
            f(-100)
            f(-5)
            f(-5)
            f(100)

        # constrain upper to 5 with include
        def f(v):
            return pcParser.assert_range(v, upr=5, include=True)

        self.assertTrue(f(-5))
        self.assertTrue(f(0))
        self.assertTrue(f(5))

        with self.assertRaises(ValueError):
            f(-100)
            f(100)