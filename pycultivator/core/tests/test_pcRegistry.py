# coding=utf-8

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.core import pcRegistry, pcLogger


class SubjectTestRegistry(UvaSubjectTestCase):

    _subject_cls = pcRegistry.Registry

    def setUp(self):
        self._registry = {
            'a': 1,
        }
        self._subject = self._subject_cls(self._registry)

    def test_init(self):
        self.assertEqual(self.subject._registry, {'a': 1})
        self.assertFalse(self.subject.requires_descendant)
        with self.assertRaises(ValueError):
            self._subject_cls({}, str, False, False)

    def test_requires_descendant(self):
        self.assertFalse(self.subject.requires_descendant)
        s = self._subject_cls(self._registry, str)
        self.assertTrue(s.requires_descendant)

    def test_is_descendant(self):
        s = self._subject_cls(self._registry, str)
        self.assertTrue(s.is_descendant(str))
        self.assertTrue(s.is_descendant("a"))
        self.assertFalse(s.is_descendant(int))
        self.assertFalse(s.is_descendant(1))

    def test_is_allowed(self):
        # accept everything
        s = self._subject_cls({})
        self.assertTrue(s.is_allowed(1))
        self.assertTrue(s.is_allowed("a"))
        self.assertTrue(s.is_allowed(str))
        self.assertTrue(s.is_allowed(pcRegistry.BaseObject))
        self.assertTrue(s.is_allowed(pcRegistry.BaseObject()))
        # accept string (objects/classes)
        s = self._subject_cls({}, str)
        self.assertFalse(s.is_allowed(1))
        self.assertTrue(s.is_allowed("a"))
        self.assertTrue(s.is_allowed(str))
        self.assertFalse(s.is_allowed(pcRegistry.BaseObject))
        self.assertFalse(s.is_allowed(pcRegistry.BaseObject()))
        # accept string (objects)
        s = self._subject_cls({}, str, allow_class=False)
        self.assertFalse(s.is_allowed(1))
        self.assertTrue(s.is_allowed("a"))
        self.assertFalse(s.is_allowed(str))
        self.assertFalse(s.is_allowed(pcRegistry.BaseObject))
        self.assertFalse(s.is_allowed(pcRegistry.BaseObject()))
        # accept string (classes)
        s = self._subject_cls({}, str, allow_instance=False)
        self.assertFalse(s.is_allowed(1))
        self.assertFalse(s.is_allowed("a"))
        self.assertTrue(s.is_allowed(str))
        self.assertFalse(s.is_allowed(pcRegistry.BaseObject))
        self.assertFalse(s.is_allowed(pcRegistry.BaseObject()))

    def test_match(self):
        s = self._subject_cls({'a': 1, 'a.b': 2, 'b.c.d': 3})
        # complete match, do not find everything
        self.assertEqual(s.match("a"), "a")
        self.assertEqual(s.match("b.c.d"), "b.c.d")
        # no match, do not find everything
        self.assertIsNone(s.match(3))
        # partial match do not find everything
        self.assertEqual(s.match("b.c"), "b.c.d")
        self.assertIsNotNone(s.match("b"))
        # partial match find everything
        self.assertEqual(s.match("a", True), ["a", "a.b"])
        self.assertEqual(s.match("b.c", True), ["b.c.d"])

    def test_parse(self):
        # accept everything
        s = self._subject_cls({})
        self.assertEqual(s.parse(1), ("int", 1))
        self.assertEqual(s.parse(1, "a"), ("a", 1))
        self.assertEqual(s.parse("a"), ("a", "a"))
        self.assertEqual(s.parse(str), ("str", str))
        # require BaseObjects
        s = self._subject_cls({}, pcRegistry.BaseObject)
        o = pcRegistry.BaseObject()
        self.assertEqual(s.parse("a"), (None, None))
        self.assertEqual(s.parse(o), ("pycultivator.core.objects.base.BaseObject", o))
        self.assertEqual(s.parse(o, "a"), ("a", o))
        # same class via path
        self.assertEqual(
            s.parse("pycultivator.core.BaseObject"),
            ("pycultivator.core.objects.base.BaseObject", pcRegistry.BaseObject)
        )

    def test_parse_item(self):
        # accept everything
        s = self._subject_cls({})
        self.assertEqual(s.parse_item(1), 1)
        self.assertEqual(s.parse_item("a"), "a")
        self.assertEqual(s.parse_item(str), str)
        # accept str classes and objects
        s = self._subject_cls({}, str)
        # valid object
        self.assertEqual(s.parse_item("a"), "a")
        # valid class
        self.assertEqual(s.parse_item(str), str)
        # accept only str classes
        s = self._subject_cls({}, str, allow_instance=False)
        self.assertEqual(s.parse_item(str), str)
        self.assertIsNone(s.parse_item("a"))
        # accept only str objects
        s = self._subject_cls({}, str, allow_class=False)
        self.assertIsNone(s.parse_item(str), str)
        self.assertEqual(s.parse_item("a"), "a")
        # require BaseObjects
        s = self._subject_cls({}, pcRegistry.BaseObject)
        o = pcRegistry.BaseObject()
        self.assertIsNone(s.parse_item("a"))
        #
        self.assertIs(s.parse_item(o), o)
        # same class via path
        self.assertIs(s.parse_item("pycultivator.core.BaseObject"), pcRegistry.BaseObject)
        # descendant via path
        self.assertIsNotNone(s.parse_item("pycultivator.core.pcPath.Path"))
        # invalid path
        self.assertIsNone(s.parse_item("pycultivator.core.BaseObjec"))
        # different class via path
        self.assertIsNone(s.parse_item("str"))

    def test_parse_name(self):
        from datetime import timedelta
        s = self._subject_cls({}, str)
        # Valid names
        # if name is given as str, use str
        self.assertEqual(s.parse_name("a", "b"), "b")
        # if name is given as other object, use class name
        self.assertEqual(s.parse_name("a", 1), 'int')
        # if name is given and is type, use class name
        self.assertEqual(s.parse_name("a", str), "str")
        # if name is given and is BaseObject use dot_path
        self.assertEqual(s.parse_name(
            pcRegistry.BaseObject(), pcRegistry.BaseObject), "pycultivator.core.objects.base.BaseObject"
        )
        # NO NAME
        # if no name is given for string, use string
        self.assertEqual(s.parse_name("a"), "a")
        # if no name is given for object, use class name
        self.assertEqual(s.parse_name(1), "int")
        # if no name is given for type, use class name
        self.assertEqual(s.parse_name(str), "str")
        # if no name is given for BaseObject, use dot_path
        self.assertEqual(s.parse_name(
            pcRegistry.BaseObject()), "pycultivator.core.objects.base.BaseObject"
        )
        # Invalid names
        # item is object and name overlaps with module, use class name
        self.assertEqual(s.parse_name(timedelta(), 'datetime'), "timedelta")
        # item is type and name overlaps with module, use class name
        self.assertEqual(s.parse_name(timedelta, 'datetime'), "timedelta")
        # item is object and name overlaps with dot_path of BaseObject, uses dot_path
        self.assertEqual(s.parse_name(
            pcRegistry.BaseObject(), "pycultivator"), "pycultivator.core.objects.base.BaseObject"
        )
        # item is type and name overlaps with dot_path of BaseObject, uses dot_path
        self.assertEqual(s.parse_name(
            pcRegistry.BaseObject, "pycultivator"), "pycultivator.core.objects.base.BaseObject"
        )

    def test_collect_entry_points(self):
        from pycultivator.config.xmlConfig import XMLConfig
        self.assertEqual(
            self._subject_cls.collect_entry_points("pycultivator.config"),
            {'xml': [XMLConfig]}
        )
        results = self._subject_cls.collect_entry_points("pycultivator.console.commands")
        self.assertIsInstance(results, dict)
        self.assertTrue('pycultivator' in results.keys())
        self.assertIsInstance(results.values()[0], list)
        self.assertTrue(len(results.values()[0]) > 0)

    def test_register(self):
        # test with some scenarios
        self.assertEqual(self.subject.register(1, "a"), {"a": 1})
        self.assertEqual(self.subject.register([1], "a"), {"a": 1})
        self.assertEqual(self.subject.register([1, 2], "a"), {"int": 2})
        self.assertEqual(self.subject.register({'b': 1}, "a"), {"b": 1})
        self.assertEqual(self.subject.register([int, str], "b"), {'int': int, 'str': str})
        self.assertEqual(self.subject.register({'a': int, 'b': 3}), {'a': int, 'b': 3})
        # test against actual entry points
        from pycultivator.config.xmlConfig import XMLConfig
        entries = self._subject_cls.collect_entry_points("pycultivator.config")
        self.assertEqual(
            self.subject.register(entries), {'xml': XMLConfig}
        )
        entries = self._subject_cls.collect_entry_points("pycultivator.console.commands")
        registry = self.subject.register(entries)
        self.assertTrue(len(registry.keys()) > 0)
        self.assertTrue(
            "pycultivator.contrib.console.commands.console.HelpCommand" in registry.keys()
        )
