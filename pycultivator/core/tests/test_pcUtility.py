# coding=utf-8
"""
Test file for xml.py
"""

from pycultivator.core.tests import UvaTestCase, UvaSubjectTestCase
import os

from pycultivator.core import pcUtility

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class FindFilesTest(UvaTestCase):

    _abstract = False

    def list_files(self, file_dir):
        return self.filter_files(os.listdir(file_dir), file_dir)

    def filter_files(self, files, file_dir=None):
        if file_dir is None:
            file_dir = ""

        def f(fp, fd):
            path = os.path.join(fd, fp)
            return os.path.isfile(path) and not fp.startswith(".")
        return list(filter(lambda x: f(x, file_dir), files))

    def test_location(self):
        location = os.path.dirname(__file__)
        plocation = os.path.join(location, "..")
        name = "test_pcUtility"
        extension = ".py"
        f = pcUtility.find_files(location)
        g = os.listdir(location)
        g = self.filter_files(g, file_dir=location)
        self.assertEqual(len(f), len(g))
        f = pcUtility.find_files([location, plocation])
        g = self.list_files(location) + self.list_files(plocation)
        self.assertEqual(len(f), len(g))

    def test_name(self):
        location = os.path.dirname(__file__)
        name = "test_pcUtility"
        extension = ".py"
        f = pcUtility.find_files(location, name, extension)
        self.assertEqual(len(f), 1)
        f = pcUtility.find_files(location, ["test_pcUtility", "test_pcXML"], extension)
        self.assertEqual(len(f), 2)
        f = pcUtility.find_files(location, ["test_pcUtility.py", "test_pcXML.py"], extension)
        self.assertEqual(len(f), 2)

    def test_combination(self):
        location = os.path.dirname(__file__)
        name = "test_pcUtility"
        extension = ".py"
        f = pcUtility.find_files(location, name, extension)
        self.assertEqual(len(f), 1)
        f = pcUtility.find_files(location, name, extension.strip("."))
        self.assertEqual(len(f), 1)
        f = pcUtility.find_files(location, "{}{}".format(name, extension))
        self.assertEqual(len(f), 1)
        f = pcUtility.find_files(location, "{}{}".format(name, extension), extension=".pyc")
        self.assertEqual(len(f), 1)

    def test_extension(self):
        location = os.path.dirname(__file__)
        extension = ".py"
        f = pcUtility.find_files(location, extension=extension)
        self.assertLess(len(f), len(os.listdir(location)))
        g = pcUtility.find_files(location, extension=extension.strip("."))
        self.assertEqual(f, g)
        f = pcUtility.find_files(location, extension=[".py", ".pyc"])
        self.assertEqual(len(f), len(os.listdir(location)))


class ImportFromStringTest(UvaTestCase):

    _abstract = False

    def test_absolute_import(self):
        # test function
        o = pcUtility.import_object_from_str("pycultivator.core.pcUtility.import_object_from_str")
        self.assertTrue(o == pcUtility.import_object_from_str)
        # test class
        from pycultivator.core.pcSettings import PCSettings
        c = pcUtility.import_object_from_str("pycultivator.core.pcSettings.PCSettings")
        self.assertIs(c, PCSettings)

    def test_relative_import(self):
        # relative to package
        o = pcUtility.import_object_from_str(".pcUtility.import_object_from_str", "pycultivator.core")
        self.assertTrue(o == pcUtility.import_object_from_str)
        # relative to child package
        o = pcUtility.import_object_from_str("..pcUtility.import_object_from_str", "pycultivator.core.tests")
        self.assertTrue(o == pcUtility.import_object_from_str)
        # without preceding dot
        with self.assertRaises(ImportError):
            pcUtility.import_object_from_str("pcUtility.import_object_from_str", "pycultivator.core")
        # with package prefix
        o = pcUtility.import_object_from_str("pycultivator.core.pcUtility.import_object_from_str", "pycultivator.core")
        self.assertTrue(o == pcUtility.import_object_from_str)

    def test_wrong_import(self):
        with self.assertRaises(ImportError):
            # imaginary module without path
            pcUtility.import_object_from_str("abc")
            # imaginary module with path
            pcUtility.import_object_from_str("ab.c")
            # imaginary module with path
            pcUtility.import_object_from_str("ab.c")
