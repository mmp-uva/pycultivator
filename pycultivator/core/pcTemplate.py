"""Implement template model classes

These classes can be described using variable definitions

"""

import six
from pycultivator.core import pcVariable
from pycultivator.core.objects.configurable import *

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class DeferredAttribute(object):
    """A attribute that will update a Variable object in the background"""

    def __init__(self, name, registry="_variables"):
        """Initialize a deferred attribute"""
        super(DeferredAttribute, self).__init__()
        self._field = name
        self._registry = registry

    def __get__(self, instance, owner=None):
        if instance is None:
            return self
        fields = getattr(instance, self._registry, None)
        if fields is None:
            raise AttributeError("No {} attribute in instance".format(self._registry))
        field = fields.get(self._field, None)
        if field is None:
            raise AttributeError("No variable {} defined in instance")
        return field.get()

    def __set__(self, instance, value):
        # look up the field in the instance
        fields = getattr(instance, self._registry, None)
        if fields is None:
            raise AttributeError("No {} attribute in instance".format(self._registry))
        field = fields.get(self._field, None)
        if field is None:
            raise AttributeError("No variable {} defined in instance")
        return field.set(value)


class TemplateMeta(type):
    """MetaClass of a TemplateBase class

    The TemplateMeta Class looks for Variable definitions in the class.

    * Every variable object is stored in the `_fields` dictionary, using the attribute name as key.
    * Variable attribute names starting with "_" are saved without the leading "_" in the _fields dictionary.
    * All variable objects can be accessed as private attributes, using name prefixed with "_".
    * The `_fields` dictionary can be accessed with the public `fields()` class method.
    * For all *public* variables a read-write property is created (without leading "_" if present).

    """

    @staticmethod
    def __new__(mcs, name, bases=None, attrs=None):
        return super(TemplateMeta, mcs).__new__(mcs, name, bases, attrs)

    def __init__(cls, name, bases=None, attrs=None):
        super(TemplateMeta, cls).__init__(name, bases, attrs)
        if bases is None:
            bases = []
        if attrs is None:
            attrs = {}
        # create class
        cls.class_fields = {}
        # fill meta with meta from parents
        for base in bases:
            if isinstance(base, TemplateMeta):
                cls.class_fields.update(base.class_fields)
        # process variables
        for obj_name, obj in attrs.items():
            if isinstance(obj, pcVariable.Variable):
                # remove _ from obj_name
                pub_name = obj_name.lstrip("_")
                # store variable under public name in meta
                cls.class_fields[pub_name] = obj
                # if variable is public: create public property
                if not obj.is_private:
                    # replace with property
                    setattr(cls, pub_name, DeferredAttribute(pub_name))
                # if private while defined as a public attribute: remove attribute
                elif not obj_name.startswith("_"):
                    delattr(cls, obj_name)


class TemplateBase(six.with_metaclass(TemplateMeta, BaseObject)):
    """A class with dynamic variable definitions

    This class can contain and process variable definitions:

    >>> class ExampleTemplate(TemplateBase):
    ...     name = pcVariable.Variable()
    ... c = ExampleTemplate(5)
    ... assert c.name == 5

    The value of public variables can be manipulated directly using it's public property.
    The variable objects themselves can be accessed using the `variables` property.
    """

    _variable_type = pcVariable.Variable

    def __init__(self, *args, **kwargs):
        """Initialize the Template with it's variables

        To prevent sharing of variable values between template instances, a deepcopy is made of each variable.
        The __init__ method should also be used to register methods to the event hooks of the variables.
        """
        self._variables = {}
        # add variables
        # populate variables dictionary from the _meta
        import copy
        for k in self.class_fields.keys():
            variable = copy.deepcopy(self.class_fields[k])
            self._registerVariable(k, variable)
        # remove all variables from kwargs
        variables = {}
        idx = 0
        while idx < len(kwargs):
            kwarg = list(kwargs.keys())[idx]
            if kwarg in self.variables.keys():
                self.variables[kwarg].set(kwargs[kwarg])
                # remove keyword from kwargs
                kwargs.pop(kwarg)
            else:
                idx += 1
        # initialize object
        super(TemplateBase, self).__init__(*args, **kwargs)

    @property
    def variables(self):
        """Dictionary of all the variables in this object

        :return: All variables in this object
        :rtype: dict[str, pycultivator.core.pcVariable.Variable]
        """
        return self._variables

    def _registerVariable(self, name, variable):
        self._variables[name] = variable
        # store variable object under private name in class
        setattr(self, "_{}".format(name), variable)


class TrackingTemplate(TemplateBase):
    """A template with tracking variables

    Objects of this class can restore the values in it's variables to previously recorded states
    """

    _variable_type = pcVariable.TrackingVariable

    @property
    def variables(self):
        """Dictionary of all the variables in this class

        :return:
        :rtype: dict[str, pycultivator.core.pcVariable.TrackingVariable]
        """
        return super(TrackingTemplate, self).variables

    def hasChanged(self, names=None):
        """Checks all variables on change"""
        result = False
        if names is None:
            names = list(self.variables.keys())
        if not isinstance(names, (tuple, list)):
            names = [names]
        for name in names:
            if name in self.variables.keys():
                result = self.variables[name].has_changed or result
        return result

    @property
    def has_changed(self):
        return self.hasChanged()

    def commit(self, names=None, only_changed=True):
        """Commit to the current value of the variable(s) of this object

        If no names are supplied; commit all variables

        :param names: Name(s) of the variable(s) to be committed.
        :type names: None | str | list[str]
        :param only_changed: Run commit only on variables that are changed
        :type only_changed: bool
        """
        if names is None:
            names = list(self.variables.keys())
        if not isinstance(names, (tuple, list)):
            names = [names]
        for name in names:
            if name in self.variables.keys():
                v = self.variables[name]
                if not only_changed or v.has_changed:
                    self.variables[name].commit()

    def restore(self, names=None, only_changed=True):
        """Restores to the previous value of the variable(s) of this object

        If no names are supplied; commit all variables

        :param names: Name(s) of the variable(s) to be committed.
        :type names: None | str | list[str]
        :param only_changed: Run restore only on variables that are changed
        :type only_changed: bool
        """
        if names is None:
            names = list(self.variables.keys())
        if not isinstance(names, (tuple, list)):
            names = [names]
        for name in names:
            if name in self.variables.keys():
                v = self.variables[name]
                if not only_changed or v.has_changed:
                    self.variables[name].restore()


class ConfigurableTemplateMeta(ConfigurableMeta, TemplateMeta):
    """Combines template and configurable meta classes"""

    def __new__(mcs, name, bases=None, attrs=None):
        return super(ConfigurableTemplateMeta, mcs).__new__(mcs, name, bases, attrs)

    def __init__(cls, name, bases=None, attrs=None):
        super(ConfigurableTemplateMeta, cls).__init__(name, bases, attrs)


class ConfigurableTemplate(
    six.with_metaclass(ConfigurableTemplateMeta, TemplateBase, ConfigurableObject)
):

    pass


class ConfigurableTrackingTemplate(
    six.with_metaclass(ConfigurableTemplateMeta, TrackingTemplate, ConfigurableObject)
):

    pass
