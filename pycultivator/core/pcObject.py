# coding=utf-8
"""The pcObject is the core module of the whole pyCultivator and brings essential functions to all objects

TODO: Move some functions to separate modules

Three classes are implemented:

* PCObject: The mother of all objects. Implements logging and system-wide functions
* ConfigurableObject: Implements settings for objects
* HierarchicalObject: Implements parent-son structures for objects

"""

from pycultivator.core.objects import BaseObject, PCObject
from pycultivator.core.objects.configurable import ConfigurableObject
from pycultivator.core.objects.hierarchical import HierarchicalObject
from pycultivator.core.objects.eventful import EventFulObject

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

__all__ = [
    "BaseObject", "PCObject",
    "ConfigurableObject",
    "HierarchicalObject",
    "EventFulObject"
]
