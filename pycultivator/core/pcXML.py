#!/usr/bin/python
"""This module provides an API to the lxml module"""

from pycultivator.core.objects import BaseObject
from pycultivator.core.pcUtility import find_files
from pycultivator.core import Parser

import os
from lxml import etree as et

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class _AbstractXML(BaseObject):
    """Abstract XML class, acts as a basic shell around lxml"""

    XSI = "http://www.w3.org/2001/XMLSchema-instance"

    XPATH_GLOBAL_SCOPE = "/"
    """Select all direct children (first level) of the document"""
    XPATH_GLOBAL_DEEP_SCOPE = "//"
    """Select all children (any depth) of the document"""
    XPATH_LOCAL_SCOPE = "./"
    """Select all direct children (first level) of the current element"""
    XPATH_LOCAL_DEEP_SCOPE = ".//"
    """Select all children (any depth) of the current element"""

    def __init__(self, xml=None, path=None, schema=None):
        """Initialise new xml object."""
        super(_AbstractXML, self).__init__()
        self._xml = xml
        self._path = path
        self._schema = schema

    @staticmethod
    def isSourceXML(source):
        """Determine whether the source is a xml object"""
        return isinstance(source, (et._Element, et._ElementTree))

    @staticmethod
    def isSourceXMLElement(source):
        """Determine whether the source is a """
        return isinstance(source, et._Element)

    @staticmethod
    def isSourceXMLElementTree(source):
        return isinstance(source, et._ElementTree)

    @staticmethod
    def isSourceXMLString(source):
        """Determine whether the source qualifies as a XML String

        Looks if "<" and ">" are present in the string.

        :param source: The source to check
        :type source: str
        :return: Whether the source is recognized as xml
        :rtype: bool
        """
        return isinstance(source, str) and "<" in source.strip() and ">" in source.strip()

    @staticmethod
    def isSourceFile(source):
        """Determine if the source qualifies as a path to a xml File

        Looks if the string ends with ".xml".

        :param source: The source to check
        :type source: str
        :return: Whether the source is recognized as a path
        :rtype: bool
        """
        return isinstance(source, str) and source.strip().endswith(".xml")

    @classmethod
    def createFromSource(cls, source):
        """Create a XML Object from a XML Source.

        :raises: pycultivator.foundation.uvaXML.XMLException

        :return: The created object or none if unsuccessful
        :rtype: None or pycultivator.core.pcXML._AbstractXML
        """
        result, xml, path = None, None, None
        try:
            xml = cls.readSource(source)
        except et.XMLSyntaxError as xse:
            cls.getLog().error(xse)
        if xml is not None:
            if isinstance(source, str) and not cls.isSourceXMLString(source):
                path = source
            result = cls(xml, path)
        return result

    @classmethod
    def readSource(cls, source):
        """Read the source into a lxml ElementTree object

        :raisses: lxml.etree.XMLSyntaxError

        :type source: str | lxml.etree._Element._Element | lxml.etree._ElementTree._ElementTree
        :rtype: lxml.etree._ElementTree._ElementTree
        """
        if cls.isSourceXML(source):
            if cls.isSourceXMLElement(source):
                source = source.getroottree()
            result = source
        elif cls.isSourceXMLString(source):
            result = cls.readXMLString(source)
        else:
            result = cls.readFile(source)
        return result

    @classmethod
    def readFile(cls, xml_file):
        """Class method: Load a XML file into a ElementTree instance

        :raises: lxml.etree.XMLSyntaxError
        :param xml_file: Path to XML File
        :type xml_file: str
        :rtype: lxml.etree._ElementTree._ElementTree
        """
        result = None
        if xml_file is None or not os.path.isfile(xml_file):
            cls.getLog().warning("Unable to read and parse {}".format(xml_file))
        else:
            parser = et.XMLParser(remove_blank_text=True)
            result = et.parse(xml_file, parser=parser)
        return result

    @classmethod
    def readXMLString(cls, source):
        """Class method: Load XML into a ElementTree instance

        :raises: lxml.etree.XMLSyntaxError
        :param source: String with XML contents
        :type source: str
        :rtype: lxml.etree._ElementTree._ElementTree
        """
        return et.XML(source).getroottree()

    @classmethod
    def writeFile(cls, path, xml, encoding="utf-8", pretty_print=True):
        """Class method: Writes XML to a file

        :type path: str
        :type xml: lxml.etree._ElementTree._ElementTree | lxml.etree._Element._Element
        :return: Whether writing was successful
        :rtype: bool
        """
        result = False
        if not os.path.exists(os.path.dirname(path)):
            cls.getLog().warning("Unable to write to {}".format(path))
        else:
            try:
                with open(path, "w") as f:
                    f.write(
                        et.tostring(xml, encoding=encoding, pretty_print=pretty_print)
                    )
                    result = True
            except IOError:
                cls.getLog().warning("IOError occurred while writing to {}".format(path))
        return result

    @classmethod
    def readSchemaInfo(cls, xml, xsi=None):
        """Get the schema information from a XML Document

        :type xml: lxml.etree._ElementTree._ElementTree
        :return: Tuple with Namespace and Schema name (in that order)
        :rtype: tuple[str]
        """
        if xsi is None:
            xsi = cls.XSI
        result = (None, None)
        schema_location = xml.xpath("./@xsi:schemaLocation", namespaces={'xsi': xsi})
        if len(schema_location) > 0:
            schema = schema_location[0]
            result = schema.strip().split()
        return result

    @classmethod
    def readSchemaNameSpace(cls, xml):
        """Get the namespace of the default schema defined in the given XML Document

        :param xml: XML Document to get the schema information from.
        :type xml: lxml.etree._ElementTree._ElementTree
        :return: The namespace of the schema
        :rtype: None or str
        """
        return cls.readSchemaInfo(xml)[0]

    @classmethod
    def readSchemaName(cls, xml):
        """Get the name of the schema file referenced in the given XML Document

        :param xml: XML Document to get the schema information from.
        :type xml: lxml.etree._ElementTree._ElementTree
        :return: The namespace of the schema
        :rtype: None or str
        """
        return cls.readSchemaInfo(xml)[1]

    # GETTERS AND SETTERS

    def getXML(self):
        """Return the XML Document contained in this object

        :rtype: lxml.etree._ElementTree._ElementTree
        """
        return self._xml

    @property
    def xml(self):
        return self.getXML()

    def hasXML(self):
        return self.getXML() is not None

    def getRoot(self):
        """Return the root element of the xml document

        :return:
        :rtype: lxml.etree._Element._Element
        """
        return self.getXML().getroot()

    @property
    def root(self):
        return self.getRoot()

    def getSchema(self):
        """Get the Schema Object of this document

        :rtype: pycultivator.core.pcXML.XMLSchema or None
        """
        return self._schema

    @property
    def schema(self):
        """The schema object loaded for this document

        :rtype: pycultivator.core.pcXML.XMLSchema or None
        """
        return self.getSchema()

    def hasSchema(self):
        """Whether a schema object has been loaded (or found)"""
        return self.schema is not None

    def getXMLString(self):
        """Return the String representation of the XML

        :rtype: str
        """
        return et.tostring(self._xml)

    def getSchemaNameSpace(self):
        """Get the namespace of the default schema file

        :rtype: None or str
        """
        result = None
        if self.hasXML():
            result = self.readSchemaNameSpace(self.xml)
        return result

    @property
    def schema_namespace(self):
        return self.getSchemaNameSpace()

    def hasSchemaNameSpace(self):
        return self.schema_namespace is not None

    def getSchemaInfo(self):
        """Get the name and location of the schema

        :return: Tuple with namespace and schema name (in that order)
        :rtype: tuple[str, str]
        """
        result = [None, None]
        if self.hasXML():
            result = self.readSchemaInfo(self.xml)
        return result

    def getSchemaName(self):
        """Get the name (file name) of the schema file as defined in the XML root

        :rtype: None or str
        """
        result = None
        schema_info = self.getSchemaInfo()
        if len(schema_info) > 1:
            result = schema_info[1]
        return result

    @property
    def schema_name(self):
        return self.getSchemaName()

    def hasSchemaName(self):
        return self.schema_name is not None

    def getXMLNameSpace(self):
        """Default namespace of the document (as known by the root element)

        :rtype: str
        """
        result = None
        nsmap = self.root.nsmap
        if len(nsmap.keys()) > 0 and None in nsmap.keys():
            result = nsmap[None]
        return result

    @property
    def xml_namespace(self):
        return self.getXMLNameSpace()

    def getFilePath(self):
        """Return the path to the xml file

        :return: The path to the xml file, if no path is set returns None
        :rtype: None or str
        """
        return self._path

    @property
    def file_path(self):
        return self.getFilePath()

    def hasFilePath(self):
        return self.getFilePath() is not None

    def getFileDir(self):
        """Return the path to the directory of the XML file path

        :rtype: None or str
        """
        result = None
        path = self.getFilePath()
        if path is not None:
            result = os.path.split(self.getFilePath())[0]
        return result

    def xpath(self, xpath, root=None, namespace=None, prefix=None, **_variables):
        """Return elements that match the xpath

        Wrapper to lxml.etree._Element.xpath

        :param root: Element that serves as starting point for Xpath (if None use document root)
        :param namespace: target Namespace used in the XPath
        :param prefix: Prefix for the target namespace
        :param _variables: Support for variables in xpath
        :return: List of element that match the xpath
        :rtype: list[lxml.etree._Element._Element]
        """
        results = []
        namespaces = None
        if root is None:
            root = self.getRoot()
        if prefix is None:
            prefix = ""
        if namespace is None:
            namespace = self.getXMLNameSpace()
        if namespace is not None and prefix != "":
            namespaces = {prefix: namespace}
        try:
            results = root.xpath(xpath, namespaces=namespaces, **_variables)
        except et.XPathEvalError as xee:
            self.getLog().warning("Invalid XPath query: {} ({})".format(xpath, xee))
        return results

    def getElementsByXPath(self, xpath, root=None, namespace=None, prefix=None, **_variables):
        return self.xpath(xpath, root=root, namespace=namespace, prefix=prefix, **_variables)

    def find(self, tag, scope=None, root=None, namespace=None, prefix=None, **variables):
        """Finds a element (within a scope, relative to root) by tag

        :param tag: The tag of the desired element
        :type  tag: str
        :param scope: Where to search for the elements ('./' relative to root, './/' all descending from root, etc.)
        :type scope: str
        :param root: The root element to search from; search from document root if None
        :type root: lxml.etree._Element._Element or None
        :param namespace: XML Namespace to use
        :param prefix: Prefix to select namespace with
        :return: All elements that match the criteria
        :rtype: list[lxml.etree._Element._Element]
        """
        if root is None:
            root = self.getRoot()
        if scope is None:
            scope = self.XPATH_GLOBAL_SCOPE
        if namespace is None:
            namespace = self.getXMLNameSpace()
        predicate = ""
        prefix_str = "" if prefix is None else "{}:".format(prefix)
        xpath = "{}{}{}{}".format(scope, prefix_str, tag, predicate)
        elements = self.xpath(
            xpath, root=root, prefix="pc", namespace=namespace, **variables
        )
        return elements

    @classmethod
    def getElementTag(cls, element):
        """Return the tag of the given element

        :param element: Element to get tag from
        :type element: lxml.etree._Element._Element
        :return: The tag
        :rtype: str
        """
        return element.tag

    @classmethod
    def getCleanElementTag(cls, element):
        """Return a cleaned form of the tag of the given element

        Removes namespace, whitespace and changes to lowercase.

        :param element: Element to get tag from
        :type element: lxml.etree._Element._Element
        :return: Cleaned tag
        :rtype: str
        """
        import re
        tag = cls.getElementTag(element)
        return re.sub("{[a-zA-Z0-9]*}", "", tag).strip().lower()

    @classmethod
    def filterElementsByTag(cls, elements, tag):
        """Filter a list of elements by their tag

        :param elements: List of elements to filter
        :type elements: list[lxml.etree._Element._Element]
        :param tag: Value of tag to match against
        :type tag: str
        :return: All elements with a tag equal to `tag`
        :rtype: list[lxml.etree._Element._Element]
        """
        if not isinstance(tag, str):
            tag = str(tag)
        return filter(lambda e: cls.getCleanElementTag(e) == tag, elements)

    def getElementsByTag(self, tag, root=None, namespace=None, prefix=None, scope=XPATH_LOCAL_SCOPE):
        """Return all matching subelements with the given tag

        :param tag: Tag to search for.
        :type tag: str
        :param root: Root element to search from
        :type root: lxml.etree._Element._Element
        :param namespace: Namespace to use in xpath, None for default namespace
        :type namespace: None | str
        :param prefix: Prefix to use in xpath for namespace (without `:`)
        :type prefix: None | str
        :param scope: Scope of XPath search
        :type scope: str
        :rtype: list[lxml.etree._Element._Element]
        """
        if root is None:
            root = self.getRoot()
        if prefix is None:
            prefix = ""
        xpath = scope
        if prefix != "":
            xpath = "{}{}:".format(xpath, prefix)
        xpath = "{}{}".format(xpath, tag)
        return self.xpath(xpath, root=root, namespace=namespace, prefix=prefix)

    def getElementByTag(self, tag, root=None, namespace=None, prefix=None, scope=XPATH_LOCAL_SCOPE):
        """Return the first matching subelement with the given tage

        :param tag: Tag to search for.
        :type tag: str
        :param root: Root element to search from
        :type root: lxml.etree._Element._Element
        :param namespace: Namespace to use in xpath, None for default namespace
        :type namespace: None | str
        :param prefix: Prefix to use in xpath for namespace (without `:`)
        :type prefix: None | str
        :param scope: Scope of XPath search
        :type scope: str
        :rtype: None or lxml.etree._Element._Element
        """
        result = None
        elements = self.getElementsByTag(
            tag=tag, root=root, namespace=namespace, prefix=prefix, scope=scope
        )
        if len(elements) > 0:
            result = elements[0]
        return result

    @classmethod
    def hasElementAttribute(cls, element, name):
        """Determines whether the element has an attribute named `name`

        :param element: The element to load the attribute from
        :type element: lxml.etree._Element._Element
        :param name: The name of the attribute
        :type name: str
        :rtype: bool
        """
        return name in element.attrib

    @classmethod
    def getElementAttribute(cls, element, name, default=None, _type=str):
        """Get a attribute form an element

        :param element: The element to load the attribute from
        :type element: lxml.etree._Element._Element
        :param name: The name of the attribute
        :type name: str
        :param default: Default value to return when attribute is not found or parsing failed
        :type default: None or str or object
        :param _type: Desired type of the value
        :type _type: type | str
        :rtype: None or str or object
        """
        v = default
        if element is not None:
            v = element.get(name, default=default)
            v = Parser.parse(v, _type=_type, default=default)
        return v

    @classmethod
    def setElementAttribute(cls, element, name, value):
        """Set the value of an attribute, named `name` to the value `value`

        :param element: The element
        :type element: lxml.etree._Element._Element
        :param name: The name of the attribute
        :type name: str
        :param value: The new value of the attribute, everything is cast to a string!
        :type value: object
        """
        if not isinstance(value, str):
            value = Parser.to_string(value, default="")
        element.set(name, value)
        return True

    @classmethod
    def filterElementsByAttribute(cls, elements, attribute, value, default=None):
        """Filter a list of elements by matching attribute values to `value`

        :param elements: List of elements to filter
        :type elements: list[lxml.etree._Element._Element]
        :param attribute: Name of attribute to match value of
        :type attribute: str
        :param value: Value of attribute to match against
        :type value: str
        :param default: The default value to use when reading the attribute
        :return: All elements with a tag equal to `tag`
        :rtype: list[lxml.etree._Element._Element]
        """
        if not isinstance(value, str):
            value = str(value)
        return filter(lambda e: cls.getElementAttribute(e, attribute, default=default) == value, elements)

    def getElementsByAttribute(
            self, attribute, value, tag=None, root=None, prefix=None, namespace=None, scope=XPATH_LOCAL_SCOPE
    ):
        """Return all elements with the given tag and a attribute value equal to value

        :param attribute: Name of attribute to filter by.
        :type attribute: str | None
        :param value: Value of attribute to filter with.
        :type value: str | None
        :param tag: Element tag to search for. If None, use wildcard
        :type tag: str | None
        :param root: Root element to search under, None for document root.
        :type root: lxml.etree._Element._Element | None
        :param prefix: Prefix used to select namespace
        :param namespace: Namespace to use in xpath, None for default namespace
        :type namespace: str | None
        :param scope: Scope of search, Defaults to direct children of root.
        :type scope: str
        :return: List of found elements
        :rtype: list[lxml.etree._Element._Element]
        """
        if tag is None:
            tag = "*"
        if root is None:
            root = self.getRoot()
        if prefix is None:
            prefix = ""
        ctag = tag
        if prefix != "":
            ctag = "{}{}:".format(prefix, tag)
        # build xpath
        xpath = "{scope}{tag}[@{attribute}=$value]".format(scope=scope, tag=ctag, attribute=attribute)
        return self.xpath(xpath, root=root, namespace=namespace, prefix=prefix, value=value)

    def getElementByAttribute(
            self, attribute, value, tag=None, root=None, prefix=None, namespace=None, scope=XPATH_LOCAL_SCOPE
    ):
        """Return first element with the given tag and a attribute value equal to value

        :param attribute: Name of attribute to filter by.
        :type attribute: str | None
        :param value: Value of attribute to filter with.
        :type value: str | None
        :param tag: Element tag to search for. If None, use wildcard
        :type tag: str | None
        :param root: Root element to search under, None for document root.
        :type root: lxml.etree._Element._Element | None
        :param prefix: Prefix used to select namespace
        :param namespace: Namespace to use in xpath, None for default namespace
        :type namespace: str | None
        :param scope: Scope of search, Defaults to direct children of root.
        :type scope: str
        :return: List of found elements
        :rtype: lxml.etree._Element._Element | None
        """
        result = None
        results = self.getElementsByAttribute(
            attribute, value, tag=tag, root=root, prefix=prefix, namespace=namespace, scope=scope
        )
        if len(results) > 0:
            result = results[0]
        return result

    def __str__(self):
        result = ""
        if self.hasXML():
            result = et.tostring(self.xml)
        return result


class XML(_AbstractXML):
    """Basic XML class with function to load, check and save XML files"""

    DT_FORMAT = "%Y-%m-%dT%H:%M:%S"
    COMPATIBLE_VERSION = "1.3"

    def __init__(self, xml=None, path=None, schema=None):
        """Initialise a new XML Document"""
        super(XML, self).__init__(xml=xml, path=path, schema=schema)
        self._ns = None
        self._parser = None
        if not self.hasSchema():
            self.loadSchema()
        # check if schema is a schema object
        if self.schema is not None:
            self._schema = XMLSchema.createFromSource(self.schema)

    @classmethod
    def isSourceXMLObject(cls, source):
        return isinstance(source, cls)

    @classmethod
    def createFromSource(cls, source, schema=None):
        """Factory method: Creates an XML instance from a XML Source

        :param source: XML to parse or path to an xml file to parse
        :type source: str
        :param schema: XML Schema Document
        :type schema: None | pycultivator.core.pcXML.XMLSchema
        :return: A XML Object or None if failed
        :rtype: pycultivator.core.pcXML.XML | None
        """
        result, xml, path = None, None, None
        try:
            if cls.isSourceXMLObject(source):
                result = source
            else:
                xml = cls.readSource(source)
        except et.XMLSyntaxError as xse:
            cls.getLog().error(xse)
        if xml is not None:
            if isinstance(source, str) and not cls.isSourceXMLString(source):
                path = source
            result = cls(xml, path, schema=schema)
        return result

    def load(self, path=None):
        """Load xml from a (new) file into the object

        :param path: Path to the XML file
        :type path: str
        :return: Whether loading the XML went successful
        :rtype: bool
        """
        if path is None:
            path = self._path
        if path is None:
            raise ValueError("Unable load XML: No path given.")
        xml = self.readFile(path)
        if xml is not None:
            self._xml = xml
            self._path = path
        return xml is not None

    def loadSchema(self, name=None, path=None):
        """Load Schema Object

        :rtype: pycultivator.core.pcXML.XMLSchema or None
        """
        result, schema = None, None
        if name is None:
            name = self.getSchemaName()
        if path is None:
            path = self.getFilePath()
        if path is not None and not os.path.isdir(path):
            path = os.path.dirname(path)
        if name is not None:
            schema_files = XMLSchema.find_schema(path, name, extend=True)
            if len(schema_files) > 0:
                schema_file = schema_files[0]
                schema = XMLSchema.createFromSource(schema_file)
        if schema is not None:
            self._schema = schema
        return self.schema

    def save(self, path=None, encoding="utf-8", pretty_print=True):
        """Save the XML - in this object - to a (new) file

        :param path: Path to the location of the (new) XML file
        :type path: str
        :param encoding: Encoding of the file
        :type encoding: str
        :param pretty_print: Whether to format the XML in a nice way
        :type pretty_print: bool
        :return: Whether saving the XML was successful
        :rtype: bool
        """
        if path is None:
            path = self._path
        if path is None:
            raise ValueError("Unable to save XML: No path given.")
        result = self.writeFile(path, self.xml, encoding=encoding, pretty_print=pretty_print)
        if result:
            self._path = path
        return result

    @classmethod
    def isSourceValid(cls, source, schema=None):
        """Checks if the source file is valid

        :param source: XML Source (XML string, Path, XML Document etc.)
        :type source: str | lxml.etree._ElementTree._ElementTree | lxml.etree._Element
        :param schema: XML Schema to validate against
        :type schema: uvaCultivator.uvaXML.XMLSchema | None
        :rtype: bool
        """
        result = False
        xml = cls.createFromSource(source)
        if xml is not None:
            result = xml.isValid(schema=schema)
        return result

    def validate(self, schema=None):
        """Validate the XML Document against it's schema

        :raises: lxml.etree.DocumentInvalid
        :param schema: The schema to validate against
        :type schema: uvaCultivator.uvaXML.XMLSchema | None
        :raises:
        """
        if schema is None:
            schema = self.schema
        result = schema is None
        if not result:
            result = schema.validate(self.xml)
        return result

    def isValid(self, schema=None):
        """Silently validate the XML Document against it's schema

        This will tell you if the document is compatible with the schema and
        whether the document complies with the schema.

        :param schema: The schema to validate against
        :type schema: uvaCultivator.uvaXML.XMLSchema | None
        :rtype: bool
        """
        if schema is None:
            schema = self.schema
        result = schema is None
        if not result:
            result = schema.isValid(self.xml)
        return result

    def isCompatible(self, schema=None):
        """Check if the given schema is compatible with the document

        !! DEPRECATED !!

        :param schema: The schema to check the version of
        :type schema: uvaCultivator.uvaXML.XMLSchema
        :rtype: bool
        """
        result = False
        if schema is None:
            schema = self.schema
        if schema is not None:
            result = schema.version == self.getSchemaVersion()
        return result

    def getSchemaVersion(self):
        """Return the version of the used XMLSchema"""
        return self.getElementAttribute(self.getRoot(), "schema_version", None)

    @classmethod
    def getElementId(cls, element, _type=str, default="0"):
        """Return the value of the id attribute of the given element

        :param element: Element to get the identifier from
        :type element: lxml.etree._Element._Element
        :param _type: The type to cast the found id value into
        :param default: Default value to return
        :type default: object
        :rtype: str
        """
        return cls.getElementAttribute(element, name="id", _type=_type, default=default)

    @classmethod
    def setElementId(cls, element, value):
        """Set the ID attribute of the given element

        :param element: Element to get the identifier from
        :type element: lxml.etree._Element._Element
        :param value: The value to set the id attribute to
        :type value: object
        :rtype: bool
        """
        return cls.setElementAttribute(element, name="id", value=value)

    @classmethod
    def filterElementsById(cls, elements, identifier):
        """Filter a list of elements by the identifier

        :param elements: List of elements to filter
        :type elements: list[lxml.etree._Element._Element]
        :param identifier: Identifier to match against
        :type identifier: str
        :return: All elements where the identifier attribute is equal to `identifier`
        :rtype: list[lxml.etree._Element._Element]
        """
        return cls.filterElementsByAttribute(
            elements, "id", identifier, default="0"
        )

    def getElementsById(self, identifier, root=None, namespace=None, prefix=None, scope=None):
        """Get all the elements with the given identifier

        Searches for elements where the attribute `id` is equal to `identifier`

        :param identifier: Name value to match
        :type identifier: str
        :param root: Root element to search from
        :type root: lxml.etree._Element._Element
        :param namespace: Namespace to use in xpath, None for default namespace
        :type namespace: None | str
        :param prefix: Prefix to use in xpath for namespace (without `:`)
        :type prefix: None | str
        :param scope: Scope of XPath search
        :type scope: str
        :return: A list of elements that have an attribute name and its value equals "name".
        :rtype: list[lxml.etree._Element._Element]
        """
        return self.getElementsByAttribute(
            "id", identifier, root=root, namespace=namespace, prefix=prefix, scope=scope
        )

    def getElementById(self, identifier, root=None, namespace=None, prefix=None, scope=None):
        """Get the first element with the given identifier

        Searches for elements where the attribute `id` is equal to `identifier`

        :param identifier: Name value to match
        :type identifier: str
        :param root: Root element to search from
        :type root: lxml.etree._Element._Element
        :param namespace: Namespace to use in xpath, None for default namespace
        :type namespace: None | str
        :param prefix: Prefix to use in xpath for namespace (without `:`)
        :type prefix: None | str
        :param scope: Scope of XPath search
        :type scope: str
        :return: A list of elements that have an attribute name and its value equals "name".
        :rtype: None | lxml.etree._Element._Element
        """
        return self.getElementByAttribute(
            "id", identifier, root=root, namespace=namespace, prefix=prefix, scope=scope
        )

    @classmethod
    def getElementName(cls, element, default="0"):
        """Return the value of the `name` attribute of the given element

        :param element: Element to get the name of
        :type element: lxml.etree._Element._Element
        :param default: Default value to return
        :type default: str
        :rtype: str
        """
        return cls.getElementAttribute(element, name="name", default=default)

    @classmethod
    def filterElementsByName(cls, elements, name):
        """Filter a list of elements by name

        :param elements: List of elements to filter
        :type elements: list[lxml.etree._Element._Element]
        :param name: Name to match against
        :type name: str
        :return: All elements where the identifier attribute is equal to `identifier`
        :rtype: list[lxml.etree._Element._Element]
        """
        return cls.filterElementsByAttribute(
            elements, "name", name
        )

    def getElementsByName(self, name, root=None, namespace=None, prefix=None, scope=None):
        """Return all elements with an attributed named "name" that matches the given name value.

        :param name: Value of name attribute to search for.
        :type name: str
        :param root: Root element to search from
        :type root: lxml.etree._Element._Element
        :param namespace: Namespace to use in xpath, None for default namespace
        :type namespace: None | str
        :param prefix: Prefix to use in xpath for namespace (without `:`)
        :type prefix: None | str
        :param scope: Scope of XPath search
        :type scope: str
        :return: A list of elements that have an attribute name and its value equals "name".
        :rtype: list[lxml.etree._Element._Element]
        """
        return self.getElementsByAttribute(
            "name", name, root=root, namespace=namespace, prefix=prefix, scope=scope
        )

    def getElementByName(self, name, root=None, namespace=None, prefix=None, scope=None):
        """Return the first element with an attributed named 'name" that matches the given name value

        :param name: Value of name attribute to search for.
        :type name: str
        :param root: Root element to search from
        :type root: lxml.etree._Element._Element
        :param namespace: Namespace to use in xpath, None for default namespace
        :type namespace: None | str
        :param prefix: Prefix to use in xpath for namespace (without `:`)
        :type prefix: None | str
        :param scope: Scope of XPath search
        :type scope: str
        :return: A list of elements that have an attribute name and its value equals "name".
        :rtype: None or lxml.etree._Element._Element
        """
        return self.getElementByAttribute(
            "name", name, root=root, namespace=namespace, prefix=prefix, scope=scope
        )

    def getElementValue(self, element, default=None, _type=str):
        """Get the value of the `value` attribute of an element (if defined).

        :param element: Element to retrieve the `value` from.
        :type element: lxml.etree._Element._Element
        :param default: Default value to return
        :type default: object | None
        :param _type: Desired value type to return
        :type _type: type | str | object
        :return: The value of the `value` attribute
        :rtype: None | str
        """
        return self.getElementAttribute(element, "value", default=default, _type=_type)

    def getElementState(self, element, default=False):
        """Get the value of the `state` attribute of an element (if defined).

        :param element: Element to retrieve the `state` from.
        :type element: lxml.etree._Element._Element
        :param default: Default value to return when attributes is not found or state is not readable.
        :type default: bool
        :return: The state of the `state` attribute, or the default value
        :rtype: bool
        """
        result = default
        if self.hasElementAttribute(element, "state"):
            state = self.getElementAttribute(element, "state", default)
            result = Parser.to_bool(state, default=default)
        return result

    def setElementState(self, element, state):
        """Set the value of the `state` attribute of an element

        :param element: Element to retrieve the `state` from.
        :type element: lxml.etree._Element._Element
        :param state: The state to set
        :type state: bool
        :return: The xml object
        :rtype: bool
        """
        value = "true" if state is True else "false"
        return self.setElementAttribute(element, "state", value)

    def getElementActive(self, element, default=False):
        """Set the value of the `active` attribute of an element

        :param element: Element which `active` attribute to set.
        :type element: lxml.etree._Element._Element
        :param default: Default value if attribute was not found
        :type default: bool
        :return: The xml object
        :rtype: bool
        """
        result = default
        if self.hasElementAttribute(element, "active"):
            state = self.getElementAttribute(element, "active", default)
            result = Parser.to_bool(state, default=default)
        return result

    def setElementActive(self, element, state):
        """Set the value of the `active` attribute of an element

        :param element: Element which `active` attribute to set.
        :type element: lxml.etree._Element._Element
        :param state: The state to set
        :type state: bool
        :return: The xml object
        :rtype: bool
        """
        if state:
            # if state is true -> set to true
            self.setElementAttribute(element, "active", "true")
            # if not active remove attribute
        elif "active" in element.attrib.keys():
            del element.attrib["active"]
        return True

    def getElementClass(self, element, default=None):
        """Get the value of the `class` attribute of an element (if defined).

        :param element: The element to retrieve the attribute from.
        :type element: lxml.etree._Element._Element
        :param default: The default value to use when reading the class value
        :type default: None | str
        :return: The value of the `class` attribute
        :rtype: str
        """
        return self.getElementAttribute(element, "class", default=default)

    # Add elements

    def createElement(self, tag, attributes=None, prefix=None, namespace=None):
        """Create a new element (without parent)"""
        nsmap = None
        if namespace is None:
            namespace = self.getXMLNameSpace()
        if namespace not in (None, ""):
            nsmap = {prefix: namespace}
            tag = et.QName(nsmap[prefix], tag)
        return et.Element(tag, attributes, nsmap=nsmap)

    def addElement(self, root, tag, attributes=None, prefix=None, namespace=None, position=None):
        """Add a new element to the root element

        :param root: Root element to add the new element to
        :type root: lxml.etree._Element._Element
        :param tag: Tag of the new element
        :type tag: str
        :param attributes: Attributes of the element
        :type attributes: dict[str, object]
        :param namespace: Namespace of the element
        :param position: Index of position to place element in root
        :return: New element as direct child of root
        """
        nsmap = None
        if namespace is None:
            namespace = self.getXMLNameSpace()
        if namespace not in (None, ""):
            nsmap = {prefix: namespace}
            tag = et.QName(nsmap[prefix], tag)
        if position is None:
            result = et.SubElement(root, tag, attributes, nsmap=nsmap)
        else:
            result = et.Element(tag, attrib=attributes, nsmap=nsmap)
            root.insert(position, result)
        return result

    def removeElement(self, element):
        """Remove the given element from the XML

        :param element: Root element to add the new element to
        :type element: lxml.etree._Element._Element
        :rtype: bool
        """
        result = False
        parent = element.getparent()
        if parent is not None:
            parent.remove(element)
            result = True
        return result


class XMLSchema(_AbstractXML):
    """A XML Schema class"""

    def __init__(self, xml=None, path=None):
        """Initialize the XML Schema object

        :raises: lxml.etree.XMLSchemaParseError

        :param xml: Raw lxml ElementTree
        :type xml: None | lxml.etree._ElementTree._ElementTree
        :param path: Location to the schema
        :type path: None | str
        """
        super(XMLSchema, self).__init__(xml, path)
        parser = None
        if self.hasXML():
            parser = et.XMLSchema(self.xml)
        self._parser = parser

    @classmethod
    def isSourceSchema(cls, source):
        """Whether the source can be parsed as XMLSchema"""
        result = False
        xml = cls.readSource(source)
        try:
            et.XMLSchema(xml)
            result = True
        except et.XMLSchemaParseError:
            pass
        return result

    @staticmethod
    def isSourceXMLSchema(source):
        """Whether the source is a `lxml.etree.XMLSchema` Object"""
        return isinstance(source, et.XMLSchema)

    @classmethod
    def isSourceSchemaObject(cls, source):
        """Whether the source is a `uvaCultivator.uvaXML.XMLSchema` Object"""
        return isinstance(source, cls)

    @classmethod
    def createFromDocument(cls, document):
        """Create from a XML Document"""
        # TODO: Implement
        return None

    @classmethod
    def createFromSource(cls, source):
        """Factory method: Creates an XML Schema instance from a XML Source

        :param source: XML to parse or path to an xml file to parse
        :type source: str
        :return: A XML Object or None if failed
        :rtype: None | uvaCultivator.uvaXML.XMLSchema
        """
        result = None
        try:
            if cls.isSourceSchemaObject(source):
                result = source
            elif cls.isSourceXMLSchema(source):
                raise ValueError("Need a XML Source, not a Schema Parser")
            else:
                result = super(XMLSchema, cls).createFromSource(source)
        except et.XMLSchemaParseError as xspe:
            cls.getLog().error("XML is not a schema: {}".format(xspe))
        except ValueError as ve:
            cls.getLog().error("Invalid input: {}".format(ve))
        return result

    @classmethod
    def find_schema(cls, locations=None, names=None, extensions=None, extend=False):
        """Find all schema files in the given location and using the given names

        :param locations: The locations to search in
        :param names: The names to look for
        :param extensions: The extension to look for
        :param extend: Whether to add commonly used values to lists of locations, names and extensions.
        :rtype: str
        """
        if locations is None:
            locations = []
        if not isinstance(locations, (tuple, list)):
            locations = [locations]
        if extend or len(locations) == 0:
            locations.extend(["", ".", os.getcwd()])
        if names is None:
            names = []
        if not isinstance(names, (tuple, list)):
            names = [names]
        if extend or len(names) == 0:
            names.extend(["schema"])
        if extensions is None:
            extensions = []
        if not isinstance(extensions, (tuple, list)):
            extensions = [extensions]
        if extend or len(extensions) == 0:
            extensions.extend([".xsd"])
        return find_files(locations, names, extensions)

    @staticmethod
    def isSourceFile(source):
        """Determine if the source qualifies as a path for a XML Schema

        Looks if the string ends with ".xsd".

        :param source: The source to check
        :type source: str
        :return: Whether the source is recognized as a path
        :rtype: bool
        """
        return isinstance(source, str) and source.endswith(".xsd")

    def validate(self, xml):
        """Validate the xml document

        :raises: lxml.etree.DocumentInvalid
        :param xml: XML Document to validate
        :type xml: lxml.etree._ElementTree._ElementTree
        """
        # validate; raises error if not valid
        self._parser.assertValid(xml)
        return True

    def validateSource(self, source):
        """Validate a XML Source

        :raises: lxml.etree.XMLSyntaxError
        :raises: lxml.etree.DocumentInvalid

        :param source:
        :type source: str | lxml.etree._ElementTree._ElementTree | lxml.etree._Element
        :return:
        :rtype: bool
        """
        # read source; raises error if not valid
        xml = self.readSource(source)
        return self.validate(xml)

    def sourceIsValid(self, source):
        """Whether the source is a valid XML Document according to this schema

        Fails if:
        - XML Syntax is invalid
        - Document does not comply to schema

        """
        result = False
        try:
            result = self.validateSource(source)
        except et.XMLSyntaxError as xse:
            self.getLog().error("XML Syntax is not valid:\n\t\t{}".format(xse))
        except et.DocumentInvalid as di:
            self.getLog().warning("XML Document is not valid:\n\t\t{}".format(di))
        return result

    def isValid(self, xml):
        """Whether the xml document is valid according to this schema.

        Catches document invalid Errors.

        :type xml: lxml.etree._ElementTree._ElementTree
        :rtype: bool
        """
        result = False
        try:
            result = self.validate(xml)
        except et.DocumentInvalid as di:
            self.getLog().warning("XML Document is not valid:\n\t\t{}".format(di))
        return result

    def getVersion(self):
        """Returns the version of this schema"""
        return self.root.get("version", None)

    @property
    def version(self):
        return self.getVersion()


class XMLException(Exception):
    """An Exception raised in the uvaXML module of pycultivator"""

    pass
