"""Module providing the path structure in hierarchical objects"""

from pycultivator.core.objects.base import BaseObject
from pycultivator.core.pcException import PCException

import re


class Path(BaseObject):
    """"""

    DELIMITER = "/"
    SELECTOR_PATTERN_TEXT = r"\$([^\s=]+)=([^\s,]+)"
    SELECTOR_PATTERN = re.compile(SELECTOR_PATTERN_TEXT)
    SELECTOR_LIST_PATTERN = re.compile(r"^({p})(?:,({p}))*$".format(p=SELECTOR_PATTERN_TEXT))

    def __init__(self, owner, delimiter=None, **kwargs):
        """Initialize a new path object for this object"""
        super(Path, self).__init__(**kwargs)
        self._owner = owner
        if delimiter is None:
            delimiter = self.DELIMITER
        self._delimiter = delimiter

    @classmethod
    def split(cls, path, delimiter=None):
        """Breaks a path into smaller segments using the delimiter

        :type path: str
        :type delimiter: str
        :rtype: list[str]
        """
        if delimiter is None:
            delimiter = cls.DELIMITER
        return path.split(delimiter)

    @classmethod
    def glue(cls, segments, delimiter=None):
        """Glues path segments together using the delimiter as separator

        :type segments: list[str]
        :type delimiter: str
        :rtype: str
        """
        if delimiter is None:
            delimiter = cls.DELIMITER
        return delimiter.join([ str(s) for s in segments])

    @property
    def owner(self):
        """The object corresponding to this path

        :rtype: pycultivator.core.objects.hierarchical.HierarchicalObject
        """
        return self._owner

    @property
    def delimiter(self):
        """The delimiter used to separate the elements in the path

        :rtype: str
        """
        return self._delimiter

    @property
    def segments(self):
        """Returns the absolute path to this object as a list

        :rtype: list[str]
        """
        result = []
        if self.owner.hasParent():
            # has parent, get path segments
            result = self.owner.parent.path.segments[:]
            # add yourself
            result.append(self.owner.identity)
        return result

    @property
    def path(self):
        """Return the absolute path to the owner

        :rtype: str
        """
        return "/" + self.glue(self.segments)

    def absolute(self, path):
        """Turn a relative path into an absolute path

        :rtype: str
        """
        obj = self.resolve_first(path)
        return obj.path.__str__()

    def match_segment(self, segment):
        if segment in ["", "."]:
            yield self.owner
        elif segment == "..":
            if self.owner.hasParent():
                yield self.owner.parent
        elif re.match(self.SELECTOR_LIST_PATTERN, segment):
            for match in self.match_segment_pattern(segment):
                yield match
        else:
            for match in self.match_segment_identifier(segment):
                yield match

    def match_segment_pattern(self, pattern):
        for child in self.owner.children:
            result = True
            # test all selectors
            for match in re.finditer(self.SELECTOR_PATTERN, pattern):
                attr, value = match.groups()
                if attr == "type":
                    # select all children of type
                    result = result and value in [child.__class__.__name__, child.dot_path()]
                else:
                    # select by property
                    p = getattr(child, attr, None)
                    if isinstance(p, (set, tuple, list)):
                        # not value is always a string!
                        result = result and value in p
                    else:
                        # match as string
                        result = result and p is not None and str(p) == value
            if result:
                yield child

    def match_segment_identifier(self, segment):
        if segment == "*":
            segment = ".*"
        for child in self.owner.children:
            if hasattr(child, "identity") and re.match(segment, str(child.identity)):
                yield child
            elif hasattr(child, "name") and re.match(segment, str(child.name)):
                yield child

    def resolve_segments(self, segments):
        if not isinstance(segments, list):
            raise TypeError("Expected a list of segments")
        if len(segments) > 0:
            segment = segments[0]
            for match in self.match_segment(segment):
                if len(segments) > 1:
                    for item in match.path.resolve(segments[1:]):
                        yield item
                else:
                    yield match
        else:
            yield self.owner

    def resolve(self, path):
        """Find the object(s) corresponding to this path

        :raises: TypeError, PathException

        :type path: str or list[str]
        :rtype: generator[pycultivator.core.objects.hierarchical.HierarchicalObject]
        """
        segments = path
        if isinstance(path, str):
            # handle special case with root
            if path.startswith("/"):
                path = path.replace("/", "", 1)
                return self.owner.root.path.resolve(path)
            segments = self.split(path)
        return self.resolve_segments(segments)

    def resolve_first(self, path):
        """Find the first object corresponding to this path

        :rtype: pycultivator.core.objects.hierarchical.HierarchicalObject or None
        """
        return next(self.resolve(path), None)

    def relative_to(self, other):
        """Return the relative path to the owner relative to the other object

        :type other: pycultivator.core.objects.hierarchical.HierarchicalObject
        :rtype: str
        """
        current_path = self.path.__str__()
        other_path = other.path.__str__()
        # test if we can find a path
        if self.owner.root is not other.root:
            raise PathException("Objects do not share root object")
        # paths are equal -> point to this object
        if current_path == other_path:
            result = "."
        # if we are at root, just append the start
        elif other_path == "/":
            result = current_path.replace("/", "./", 1)
        # current_path is sibling of other_path
        elif current_path.startswith(other_path):
            result = current_path.replace(other_path, ".", 1)
        # find common ground
        elif other.hasParent():
            parent_path = self.relative_to(other.parent)
            if parent_path.startswith("./"):
                parent_path = parent_path.replace("./", "", 1)
            result = self.glue(["..", parent_path])
        # no common ground
        else:
            raise PathException("Unable to find common ground")
        # prune lose dots at the end
        while result.endswith("/."):
            result = result[:result.rfind("/.")]
        return result

    def __str__(self):
        return self.path


class PathException(PCException):
    """Exception raised by path objects"""

    def __init__(self, msg):
        super(PathException, self).__init__(msg=msg)
