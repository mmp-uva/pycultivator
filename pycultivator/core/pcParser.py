"""Helper methods for parsing strings into other types"""

from objects import BaseObject
import re

__all__ = [
    "Parser",
    "compare",
    "assert_range",
]


class _Parser(BaseObject):

    # default constants
    DT_FORMAT = "%Y-%m-%d %H:%M:%S"
    NUMBER_PATTERN = r"^-?\d+(\.\d+)?$"
    LIST_PATTERN = r"\[([^\]]+)\]"

    def __init__(self, number_pattern=None, list_pattern=None, dt_format=None, **kwargs):
        super(_Parser, self).__init__(**kwargs)
        if number_pattern is None:
            number_pattern = self.NUMBER_PATTERN
        self._number_pattern = number_pattern
        if list_pattern is None:
            list_pattern = self.LIST_PATTERN
        self._list_pattern = list_pattern
        if dt_format is None:
            dt_format = self.DT_FORMAT
        self._dt_format = dt_format

    @property
    def number_pattern(self):
        return self._number_pattern

    @property
    def list_pattern(self):
        return self._list_pattern

    @property
    def dt_format(self):
        return self._dt_format

    def is_number(self, v, pattern=None):
        """Whether the given string is a number, according to the given test (or if None, use default test)

        :param v: String to test
        :type v: str
        :param pattern: regex pattern to use, defaults to global NUMBER_TEST
        :type pattern: str or None
        :return: Whether the string is a number
        :rtype: bool
        """
        if pattern is None:
            pattern = self.number_pattern
        return re.match(pattern, v) is not None

    def is_list(self, v, pattern=None):
        """Whether the given string can be interpreted as a list (using the pattern)

        :param v: String to test
        :type v: str
        :param pattern: regex pattern to use, defaults to global LIST_TEST
        :type pattern: str or None
        :return: Whether the string is a list
        :rtype: bool
        """
        if pattern is None:
            pattern = self.list_pattern
        return re.match(pattern, v) is not None

    def to_bool(self, value, default=False):
        """Parse a value into a boolean

        :param value: Value to convert
        :type value: object
        :param default: Default value if conversion fails
        :type default: object
        :return: The parsed value or the default value
        """
        result = default
        if isinstance(value, bool):
            return value
        try:
            if isinstance(value, (int, float)):
                result = value == 1
            if isinstance(value, str):
                result = value.lower() in ["1", "true"]
        except (TypeError, ValueError):
            pass
        return result

    def to_int(self, value, default=None):
        """Parse a value into a integer

        :param value: Value to convert
        :type value: object
        :param default: Default value if conversion fails
        :type default: object
        :return: The parsed value or the default value
        """
        result = default
        if isinstance(value, int):
            return value
        try:
            if isinstance(value, str):
                result = int(float(value.replace(",", ".")))
            else:
                result = int(value)
        except (ValueError, TypeError):
            pass
        return result

    def to_float(self, value, default=None):
        """Parse a value into a float

        :param value: Value to convert
        :type value: object
        :param default: Default value if conversion fails
        :type default: object
        :return: The parsed value or the default value
        """
        result = default
        if isinstance(value, float):
            return value
        try:
            if isinstance(value, str):
                result = float(value.replace(",", "."))
            else:
                result = float(value)
        except (ValueError, TypeError):
            pass
        return result

    def to_string(self, value, default=None):
        """Parses a value to a String

        :param value: Value to read
        :type value: object
        :param default: Default value to return when parsing fails
        :type default: None or str
        :return: Default value or parsed value
        :rtype: None or str
        """
        result = default
        if value is None:
            return value
        if isinstance(value, bool):
            result = "true" if value else "false"
        if isinstance(value, str):
            return value
        try:
            result = str(value)
        except (ValueError, TypeError):
            pass
        return result

    def to_list(self, value, default=None):
        result = value
        if result is None:
            result = default
        if result is None:
            return []
        if isinstance(result, (set, tuple)):
            return list(result)
        if isinstance(result, dict):
            return result.values()
        if isinstance(result, list):
            return result
        return [result]

    def str_to_list(self, value, default=None, pattern=None, separator=",", item_type=str, **kwargs):
        """Parse a string to a list

        :param value:
        :type value:
        :return:
        :rtype:
        """
        result = default
        if pattern is None:
            pattern = self.list_pattern
        result = re.findall(pattern, value)
        if len(result) > 0:  # else -> empty list we can return!
            result = result[0].split(separator)
            if item_type is not str:
                for idx, item in enumerate(result):
                    result[idx] = self.parse(item, item_type)
        return result

    def to_datetime(self, value, default=None, _format=None, **kwargs):
        """Parses a value to a DateTime object

        :param value: Value to read
        :type value: object
        :param default: Default value to return when parsing fails
        :type default: None or datetime.datetime
        :param _format: Format to use when value is a string.
        :type _format: None or str
        :return: Default value or parsed value
        :rtype: None or datetime.datetime
        """
        from datetime import datetime as dt
        result = default
        if _format is None:
            _format = self.dt_format
        if isinstance(value, dt):
            result = value
        try:
            if isinstance(value, str):
                result = dt.strptime(value, _format)
        except ValueError:
            pass
        return result

    def parse(self, value, _type, default=None, **kwargs):
        """Parse a value into a different type

        :param value: The value that should be casted
        :type value: object
        :param _type: Type to cast the value to
        :type _type: T
        :param default: The default value to return if casting failed
        :type default: None or bool or str or int or float or datetime.datetime
        :return: value with the type of v_type
        :rtype: T
        """
        from datetime import datetime as dt
        result = default
        try:
            if isinstance(_type, bool) or _type in [bool, "bool"]:
                result = self.to_bool(value, default=default)
            elif isinstance(_type, int) or _type in [int, "int", "integer"]:
                result = self.to_int(value, default=default)
            elif isinstance(_type, float) or _type in [float, "float"]:
                result = self.to_float(value, default=default)
            elif isinstance(_type, dt) or _type in [dt, "dt", "datetime"]:
                result = self.to_datetime(value, default=default, **kwargs)
            elif isinstance(_type, str) or _type in [str, "str", "string"]:
                result = self.to_string(value, default=default)
            elif isinstance(_type, list) or _type in [list, "list"]:
                result = self.to_list(value, default=default, **kwargs)
            else:
                result = value
        except (ValueError, TypeError):
            pass
        return result

    def copy(self, memo=None):
        return self


# create a global instance
Parser = _Parser()


def compare(value, limit=None, is_equal=True, is_larger=False, is_smaller=False):
    """Compare a value to a limit"""
    result = True
    if None not in [value, limit]:
        result = (is_equal and value == limit) or (is_larger and value > limit) or (is_smaller and value < limit)
    return result


def assert_range(value, lwr=None, upr=None, name=None, include=True):
    """Check that the value is within the given range otherwise raise ValueError"""
    # satisfies lwr: if lwr is None or (value >= lwr and include is True) or value > lwr
    satisfies_lwr = compare(value, lwr, is_equal=include, is_larger=True)
    # satisfies upr: if upr is None or (value <= upr and include is True) or value > upr
    satisfies_upr = compare(value, upr, is_equal=include, is_smaller=True)
    # if one of them fails; throw exception
    if False in (satisfies_lwr, satisfies_upr):
        v = "<{}> {}".format(value.__class__.__name__, value)
        msg = "{} violates constrain, not in [{},{}]".format(
            "{} (= {})".format(name, v) if name is not None else v,
            lwr if lwr is not None else "-Inf",
            upr if upr is not None else "Inf"
        )
        raise ValueError(msg)
    return True
