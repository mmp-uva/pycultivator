"""Variable classes"""

from pycultivator.core.pcException import PCException
from pycultivator.core.pcParser import compare, assert_range
from pycultivator.core.objects.eventful import EventFulObject


class Variable(EventFulObject):
    """A simple variable class to be used with `pycultivator.core.pcTemplate.Template` classes

    The value of this variable can be obtained or set using the value property or the `get()` and `set(...)` methods.
    The private setting tells the template class whether it should create public properties for this variable.

    Methods can be registered to hooks provided by the variable, which are triggered at specified moments.

    Hooks
    -----

    * `get`: called when the value of the variable is retrieved
    * `set`: called when the value of the variable is set

    """

    def __init__(self, value=None, private=False, **kwargs):
        super(Variable, self).__init__(**kwargs)
        self._value = value
        self._private = private

    @property
    def value(self):
        return self.get()

    @value.setter
    def value(self, v):
        self.set(v)

    def get(self):
        self.triggerEvent("get")
        return self._value

    def set(self, v):
        self._value = v
        self.triggerEvent("set", v)

    @property
    def is_private(self):
        return self._private

    def copy(self, memo=None):
        """Create a clone of the other object

        :param other: The variable to be copied
        :type other: object
        :rtype: pycultivator.core.pcVariable.Variable
        :return: This object with all data from other copied into this object
        """
        import copy
        result = type(self)(
           value=copy.deepcopy(self.value, memo), private=self.is_private
        )
        # copy eventful attributes
        result._event_hooks = copy.deepcopy(self._event_hooks, memo)
        result._event_slots = copy.deepcopy(self._event_slots, memo)
        result._event_observers = copy.copy(self._event_observers)
        return result

    def __eq__(self, other):
        return other == self._value

    def __ne__(self, other):
        return not self == other


class TrackingVariable(Variable):
    """Tracking Variables keep track of the previous value of the variable

    * `commit()`: Update the previous value to the current value
    * `restore()`: Restore the current value to the previous value
    * `has_changed` or `hasChanged()`: Whether the current value is equal to the previous value
    """

    def __init__(self, value=None, private=False, **kwargs):
        super(TrackingVariable, self).__init__(value=value, private=private, **kwargs)
        self._prev_value = value

    @property
    def previous(self):
        return self._prev_value

    def hasChanged(self):
        """Whether the variable has changed compared to last commit

        :rtype: bool
        """
        return self._value != self._prev_value

    @property
    def has_changed(self):
        return self.hasChanged()

    def commit(self):
        """Will update the previous value to the current value"""
        self._prev_value = self._value
        self.triggerEvent("commit", self._value)

    def restore(self):
        """Restores the current value to the previous value"""
        self._value = self._prev_value
        self.triggerEvent("restore", self._value)

    def copy(self, memo=None):
        result = super(TrackingVariable, self).copy(memo=memo)
        import copy
        result._prev_value = copy.deepcopy(self.previous, memo)
        return result


class ConstrainedVariable(Variable):
    """A constrained variable will check whether values being set are within the constrains"""

    def __init__(self, value=None, private=False, minimum=None, maximum=None, includes=True, **kwargs):
        super(ConstrainedVariable, self).__init__(value=value, private=private, **kwargs)
        self._minimum = minimum
        self._maximum = maximum
        self._includes = includes

    @property
    def minimum(self):
        return self._minimum

    @minimum.setter
    def minimum(self, v):
        self._minimum = v
        if not self.validate_lower(self.value, self.minimum, include=self.includes):
            import warnings
            msg = "Current value ({}) is out-of-bound after changing minimum to ({}), setting value to None"
            warnings.warn(msg.format(self.value, self.minimum))
            self._value = None

    def hasMinimum(self):
        return self.minimum is not None

    @property
    def maximum(self):
        return self._maximum

    @maximum.setter
    def maximum(self, v):
        self._maximum = v
        if not self.validate_upper(self.value, self.maximum, include=self.includes):
            import warnings
            msg = "Current value ({}) is out-of-bound after changing maximum to ({}), setting value to None"
            warnings.warn(msg.format(self.value, self.maximum))
            self._value = None

    def hasMaximum(self):
        return self.maximum is not None

    @property
    def includes(self):
        return self._includes

    @staticmethod
    def validate_lower(value, limit, include=True):
        return compare(value, limit, is_equal=include, is_larger=True)

    @staticmethod
    def validate_upper(value, limit, include=True):
        return compare(value, limit, is_equal=include, is_smaller=True)

    @classmethod
    def validate(cls, value, lwr=None, upr=None, name=None, include=True):
        """Tests if a value is within the limits (or equal to)

        >>> ConstrainedVariable.validate(0)
        >>> ConstrainedVariable.validate(0, -1, 1)
        >>> ConstrainedVariable.validate(0, 0, 0)
        >>> ConstrainedVariable.validate(0, 0, 0, include=False) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
            ...
        ValueError: bla
        >>> ConstrainedVariable.validate(0, lwr=-1)
        >>> ConstrainedVariable.validate(0, lwr=0)
        >>> ConstrainedVariable.validate(0, lwr=0, include=False)  # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
            ...
        ValueError: bla
        >>> ConstrainedVariable.validate(0, lwr=1)  # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
            ...
        ValueError: bla
        >>> ConstrainedVariable.validate(0, upr=1)
        >>> ConstrainedVariable.validate(0, upr=0)
        >>> ConstrainedVariable.validate(0, upr=0, include=False)  # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
            ...
        ValueError: bla
        >>> ConstrainedVariable.validate(0, upr=-1)  # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
            ...
        ValueError: bla

        :param value: Value to test
        :type value: object
        :param lwr: Lower bound of the value
        :type lwr: None or int or float
        :param upr: Upper bound of the value
        :type upr: None or int or float
        :param name: Name of the variable
        :type name: str
        :param include: Whether to accept values at the boundaries
        :type include: bool
        :raises ValueError: If given value is outside the boundaries
        """
        # satisfies lwr: if lwr is None or (value >= lwr and include is True) or value > lwr
        satisfies_lwr = cls.validate_lower(value, lwr, include=include)
        # satisfies upr: if upr is None or (value <= upr and include is True) or value > upr
        satisfies_upr = cls.validate_upper(value, upr, include=include)
        # if one of them fails; throw exception
        if not satisfies_lwr or not satisfies_upr:
            v = "<{}> {}".format(value.__class__.__name__, value)
            msg = "{} violates constrain, not in [{},{}]".format(
                "{} (= {})".format(name, v) if name is not None else v,
                lwr if lwr is not None else "-Inf",
                upr if upr is not None else "Inf"
            )
            raise ValueError(msg)

    @classmethod
    def constrain_value(cls, value, limit, include=True, is_larger=False, is_smaller=False):
        if not compare(value, limit, is_equal=include, is_larger=is_larger, is_smaller=is_smaller):
            return limit
        return value

    def constrain(self, value, lwr=None, upr=None, name=None):
        result = value
        v = "<{}> {}".format(value.__class__.__name__, value)
        if lwr is None:
            lwr = self.minimum
        if lwr is not None:
            result = self.constrain_value(value, lwr, is_larger=True)
            if result > value:
                self.log.info("{v} is below minimum ({m}): set to {m} instead".format(
                   v="{} (= {})".format(name, v) if name is not None else v, m=lwr)
                )
        if upr is None:
            upr = self.maximum
        if upr is not None:
            result = self.constrain_value(value, upr, is_smaller=True)
            if result < value:
                self.log.info("{v} is above maximum ({m}): set to {m} instead".format(
                    v="{} (= {})".format(name, v) if name is not None else v, m=upr)
                )
        return result

    def set(self, v):
        self.validate(v, self.minimum, self.maximum, include=self.includes)
        super(ConstrainedVariable, self).set(v)

    def copy(self, memo=None):
        result = super(ConstrainedVariable, self).copy(memo=memo)
        result._minimum = self.minimum
        result._maximum = self.maximum
        result._includes = self.includes
        return result


class TrackingConstrainedVariable(ConstrainedVariable, TrackingVariable):
    """Combine tracking and constrain abilities into one variable"""

    def __init__(self, value=None, private=False, **kwargs):
        super(TrackingConstrainedVariable, self).__init__(value=value, private=private, **kwargs)
