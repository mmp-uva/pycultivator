"""Implement a settings object"""

from pycultivator.core.objects import BaseObject


class PCSettings(BaseObject):

    @classmethod
    def merge(cls, destination, source, namespace=None):
        """Merge two settings objects or dict objects

        After update: all keys from `source` are in `destination`

        >>> d1 = PCSettings({'a.b': 0})
        >>> d2 = {'b': 1}
        >>> PCSettings.merge(d1, d2, "a").to_dict()
        {'a.b': 1}

        :param destination: Settings object to copy settings to.
        :type destination: pycultivator.core.pcSettings.PCSettings or dict
        :param source: Settings object to copy settings from.
        :type source: pycultivator.core.pcSettings.PCSettings or dict
        :param namespace: Namespace to prepend to the settings in source
        :type namespace: None or str
        :return: updated settings object
        :rtype: pycultivator.core.pcSettings.PCSettings
        """
        # make a copy
        if isinstance(destination, cls):
            result = destination.copy()
        elif isinstance(destination, dict):
            result = cls(destination.copy())
        else:
            raise ValueError("Invalid destination, not a dict or settings object")
        return result.union(source, namespace=namespace)

    def __init__(self, settings=None, namespace=None, **kwargs):
        """Initialize a settings object

        >>> PCSettings({'a': 1, 'b': 2}, 'x').to_dict()
        {'x.a': 1, 'x.b': 2}

        """
        super(PCSettings, self).__init__()
        if settings is None:
            settings = {}
        if isinstance(settings, PCSettings):
            settings = settings.copy()
        self._settings = settings
        settings.update(kwargs)
        self._namespace = ""
        # prepend namespace
        self.prepend(namespace)

    @property
    def namespace(self):
        """The default namespace of this settings object"""
        return self._namespace

    @property
    def settings(self):
        """The names of the settings contained in this object"""
        return self._settings.keys()

    def keys(self):
        return self._settings.keys()

    def items(self):
        """key-value pairs of the settings"""
        return self._settings.items()

    def filter(self, namespace):
        """Remove all settings that do not start with the given namespace

        :type namespace: str
        :rtype: pycultivator.core.pcSettings.PCSettings
        """
        self._settings = self.filter_dict(self._settings, namespace=namespace)
        return self

    @classmethod
    def filter_dict(cls, settings, namespace):
        """Remove all keys in the the given settings dictionary that do not start with the given namespace

        :type settings: dict[str, object]
        :type namespace: str
        :return: Copy of the settings dictionary with the filtered items
        :rtype: dict[str, object]
        """
        new_settings = {}
        for setting in settings.keys():
            if cls.has_namespace(setting, namespace):
                new_settings[setting] = settings[setting]
        return new_settings

    @staticmethod
    def has_namespace(name, namespace):
        return isinstance(namespace, str) and isinstance(name, str) and name.startswith(namespace)

    def prepend(self, namespace):
        """Prepend the given namespace to all keys in this settings object

        >>> d = PCSettings({'a.b.c': 1, 'c': 2})
        >>> d.prepend('a.b').to_dict()
        {'a.b.a.b.c': 1, 'a.b.c': 2}

        :param namespace: Namespace to prepend to each setting in this object
        :type namespace: str
        :rtype: pycultivator.core.pcSettings.PCSettings
        """
        self._settings = self.prepend_dict(self._settings, namespace=namespace)
        self._namespace = self.prepend_namespace(self.namespace, namespace)
        return self

    @classmethod
    def prepend_dict(cls, settings, namespace):
        """Prepend the namespace to all keys in the given settings dictionary

        :type settings: dict[str, object]
        :type namespace: str
        :return: Copy of the settings dictionary where all keys are prepended with the namespace
        :rtype: dict[str, object]
        """
        new_settings = {}
        for setting in settings.keys():
            name = cls.prepend_namespace(setting, namespace)
            new_settings[name] = settings[setting]
        return new_settings

    @classmethod
    def prepend_namespace(cls, name, namespace):
        """Prepend a settings name with namespace.

        >>> PCSettings.prepend_namespace('c', 'd')
        'd.c'

        :param name: Name of the setting to prepend
        :type name: str
        :param namespace: Namespace to prepend to settings, or None to use current namespace.
        :type name: str
        :rtype: str
        """
        if name is None:
            name = ""
        if namespace is None:
            namespace = ""
        if name == "":
            result = namespace
        elif namespace == "":
            result = name
        else:
            result = "{}.{}".format(namespace, name)
        return result

    def reduce(self, namespace):
        """Remove the given namespace from all keys in the settings object

        >>> d = PCSettings({'a.b.c': 1, 'c': 2})
        >>> d.reduce('a.b').to_dict()
        {'c': 1}

        :param namespace: Namespace to remove from all settings
        :type namespace: str
        :rtype: pycultivator.core.pcSettings.PCSettings
        """
        self._settings = self.reduce_dict(self._settings, namespace=namespace)
        self._namespace = self.reduce_namespace(self.namespace, namespace)
        return self

    @classmethod
    def reduce_dict(cls, settings, namespace):
        """Remove the given namespace from all keys in the given settings dictionary

        :type settings: dict[str, object]
        :type namespace: str
        :return: Copy of the settings dictionary with the namespace from all keys
        :rtype: dict[str, object]
        """
        new_settings = {}
        for setting in settings.keys():
            name = cls.reduce_namespace(setting, namespace=namespace)
            new_settings[name] = settings[setting]
        return new_settings

    @classmethod
    def reduce_namespace(cls, name, namespace):
        """Remove the namespace from the setting name"""
        if name is None:
            name = ""
        result = name
        if namespace is None:
            namespace = ""
        namespace = "{}.".format(namespace)
        if name.startswith(namespace):
            result = name.replace(namespace, "", 1)
        return result

    def has(self, name, namespace=None):
        """Whether the settings object has a setting with the given name (and namespace)"""
        if namespace is None:
            namespace = self.namespace
        name = self.prepend_namespace(name, namespace=namespace)
        return name in self._settings.keys()

    def get(self, name, default=None, namespace=None):
        """Get the value of a setting stored in this object"""
        if namespace is None:
            namespace = self.namespace
        result = default
        if self.has(name, namespace=namespace):
            name = self.prepend_namespace(name, namespace=namespace)
            result = self._settings[name]
        return result

    def set(self, name, value, namespace=None):
        """Set the value """
        if namespace is None:
            namespace = self.namespace
        name = self.prepend_namespace(name, namespace=namespace)
        self._settings[name] = value
        return self

    def remove(self, name, namespace=None):
        """Remove a setting"""
        if namespace is None:
            namespace = self.namespace
        if not self.has(name, namespace=namespace):
            raise KeyError("Unknown setting: {}".format(self.prepend_namespace(name, namespace)))
        name = self.prepend_namespace(name, namespace=namespace)
        self._settings.pop(name)

    def union(self, other, namespace=None):
        """Combines this settings with an other settings or dict object

        After union: all keys in `self` are from `other` and `self`

        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'b': 3}
        >>> sorted(d1.union(d2).to_dict().items())
        [('a.a', 1), ('a.b', 3)]
        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'a.b': 3}
        >>> sorted(d1.union(d2).to_dict().items())
        [('a.a', 1), ('a.a.b', 3), ('a.b', 2)]
        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'b': 3}
        >>> sorted(d1.union(d2, "a").to_dict().items())
        [('a.a', 1), ('a.b', 3)]
        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'c': 3}
        >>> sorted(d1.union(d2).to_dict().items())
        [('a.a', 1), ('a.b', 2), ('a.c', 3)]

        :param other: The other settings object or dictionary to update to
        :param namespace: namespace to prepend before other values
        """
        if other is None:
            other = {}
        if isinstance(other, PCSettings):
            other = other.to_dict()
        if not isinstance(other, dict):
            raise ValueError("Invalid value for other: excepted a dict no {}".format(type(other)))
        import copy
        for setting in other.keys():
            value = copy.deepcopy(other.get(setting))
            self.set(setting, value, namespace=namespace)
        return self

    def update(self, other, namespace=None):
        """Update the settings of self with values from other for settings that are already in self

        After update: all settings in `self` that are also in `other` have value of `other`

        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'c': 3}
        >>> sorted(d1.update(d2).to_dict().items())
        [('a.a', 1), ('a.b', 2)]
        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'b': 3}
        >>> sorted(d1.update(d2).to_dict().items())
        [('a.a', 1), ('a.b', 3)]
        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'a': 4, 'b': 3}
        >>> sorted(d1.update(d2).to_dict().items())
        [('a.a', 4), ('a.b', 3)]
        >>> d1 = PCSettings({"b": 2}, 'a')
        >>> d2 = {'a': 4, 'b': 3}
        >>> sorted(d1.update(d2).to_dict().items())
        [('a.b', 3)]
        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'a.b': 3}
        >>> sorted(d1.update(d2).to_dict().items())
        [('a.a', 1), ('a.b', 2)]
        >>> d1 = PCSettings({'a': 1, "b": 2}, 'a')
        >>> d2 = {'b': 3}
        >>> sorted(d1.update(d2, "a").to_dict().items())
        [('a.a', 1), ('a.b', 3)]

        :param other: The other settings object or dictionary to update to
        :param namespace: namespace to prepend before other values
        """
        if other is None:
            other = {}
        if isinstance(other, PCSettings):
            other = other.to_dict()
        if not isinstance(other, dict):
            raise ValueError("Invalid value for other: excepted a dict no {}".format(type(other)))
        import copy
        for setting in other.keys():
            if self.has(setting, namespace=namespace):
                value = copy.deepcopy(other.get(setting))
                self.set(setting, value, namespace=namespace)
        return self

    def copy(self, memo=None):
        """Copies this settings object to the other

        >>> d1 = PCSettings({'a': 1})
        >>> d2 = d1.copy()
        >>> d1 is not d2
        True
        >>> d1 == d2
        True

        """
        import copy
        result = type(self)(namespace=copy.copy(self.namespace))
        for setting in self.settings:
            value = copy.deepcopy(self.get(setting, namespace=""), memo=memo)
            result.set(setting, value, namespace="")
        return result

    __copy__ = copy
    __deepcopy__ = copy

    def __eq__(self, other):
        result = isinstance(other, type(self))
        if result:
            for setting in self.settings:
                this = self.get(setting, namespace="")
                that = other.get(setting, namespace="")
                result = this == that and result
        return result

    def __getitem__(self, item):
        """Access to a setting using self[item]

        >>> d = PCSettings({"a": 1})
        >>> d["a"]
        1

        :param item: Name of the setting
        :type item: str
        :return: Value of the setting
        :rtype: object
        """
        if not self.has(item):
            raise KeyError("Unknown setting: {}".format(item))
        return self.get(item)

    def __setitem__(self, key, value):
        """Access to a setting using self[item] = value

        >>> d = PCSettings({'a': 1})
        >>> d['a'] = 2
        >>> d['a']
        2

        :param key: Name of the setting
        :type key: str
        :param value: New value of the setting
        :type value: object
        """
        if not self.has(key):
            raise KeyError("Unknown settings: {}".format(key))
        self.set(key, value)

    def __str__(self):
        return str(self.to_dict())

    def to_dict(self, namespace=None, flatten=False):
        """Create a dictionary from this object

        :param namespace: Only return settings with the given namespace, if None return all settings
        :param flatten: Whether to remove the namespace from all elements
        :rtype: dict[str, any]
        """
        results = self._settings
        if namespace is not None:
            results = self.filter(namespace)
            if flatten:
                results = self.reduce(namespace)
        return results
