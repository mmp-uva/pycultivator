# coding=utf-8
"""
Module that provides basic support for registering classes to URL schemes
"""

import importlib

from .objects import BaseObject


class Registry(BaseObject):
    """Storage of items by name"""

    def __init__(self, registry=None, require_descendant=None, allow_class=True, allow_instance=True, **kwargs):
        """Initialize a new registry

        :param registry: Initial collection of items to be added to the registry
        :type registry: dict
        :param require_descendant: Should the registry only accept certain types of classes
        :type require_descendant:
        :return:
        :rtype:
        """
        super(Registry, self).__init__()
        self._registry = {}
        self._require_descendant = require_descendant
        self._allow_class = allow_class is True
        self._allow_instance = allow_instance is True
        if self.requires_descendant and not (self.allows_classes or self.allows_instances):
            raise ValueError(
                "Requirement of descendant in combination of not allowing classes and instances is not allowed"
            )
        # parse given registry
        if registry is None:
            registry = {}
        for name in registry.keys():
            self.register(registry[name], name)

    @property
    def requires_descendant(self):
        """Checks whether this registry only accepts certain types of classes

        :return: Whether a type is required
        :rtype: bool
        """
        return self._require_descendant is not None

    @property
    def allows_classes(self):
        return self._allow_class

    @property
    def allows_instances(self):
        return self._allow_instance

    def is_descendant(self, item):
        """Checks whether the class is a subclass of the required super class

        :return: Whether the given class is a subclass
        :rtype: bool
        """
        if not isinstance(item, type):
            item = item.__class__
        return self.requires_descendant and issubclass(item, self._require_descendant)

    def is_allowed(self, item):
        # if no descendant is specified, allow everything
        if not self.requires_descendant:
            return True
        is_type = isinstance(item, type)
        # if we are picky: item is an instance and no instance are allowed
        if not is_type and not self.allows_instances:
            return False
        # if we are picky: item is an class and no classes are allowed
        if is_type and not self.allows_classes:
            return False
        # otherwise determine if is accept by type
        return self.is_descendant(item)

    def has(self, name):
        """Checks whether the name is registered in the registry

        :param name: Name of the klass that could be registered in the registery
        :type name: str
        :return: Whether
        :rtype: bool
        """
        return name in self._registry.keys()

    def get(self, name):
        """Return the item registered under the given name"""
        return self._registry[name]

    def match(self, name, match_all=False):
        """Returns the names that match the given string"""
        results = set()
        if not isinstance(name, str):
            return None
        # total match
        if self.has(name):
            if not match_all:
                return name
            results.add(name)
        # check one by one
        for key in self._registry.keys():
            if key.startswith(name) or key.endswith(name):
                results.add(key)
                if not match_all:
                    return key
        # in case of not match_all we already returned if we found something
        # so this means we did NOT find anything, return None
        if not match_all:
            return None
        # otherwise return all results
        return list(results)

    def names(self):
        """Return all names currently in the registry"""
        return self._registry.keys()

    def register(self, item, name=None):
        results = {}
        if isinstance(item, dict):
            for key in item.keys():
                results.update(self.register(item[key], key))
        elif isinstance(item, (list, tuple, set)):
            if len(item) > 1:
                # if we have multiple items we do not use the same name
                for entry in item:
                    results.update(self.register(entry))
            if len(item) == 1:
                results.update(self.register(item[0], name))
        else:
            name = self.register_item(item, name=name)
            results[name] = item
        return results

    def register_entry_points(self, group, entries=None):
        """Collects and load entry points into the registry"""
        if entries is None:
            entries = self.collect_entry_points(group)
        return self.register(entries, group)

    def register_item(self, item, name=None):
        """Register a new item

        :param item: The item to register
        :type item: str | type | object
        :param name: Name to register the class to
        :type name: str
        :return: Name to which the klass was registered
        :rtype: str
        """
        name, definition = self.parse(item, name)
        if None in [name, definition]:
            self.log.warning("Was not able to load {} into the registry".format(item))
            return None
        if self.has(name):
            self.log.warning("Will overwrite entry registered to - %s -" % name)
        self._registry[name] = definition
        return name

    def unregister(self, name):
        """Remove a name from the registry

        :param name: Name of the binding to remove from the registry
        :type name: str
        """
        self._registry.pop(name)

    def parse(self, item, name=None):
        """Turns the item into a registrable instance"""
        result = self.parse_item(item)
        if result is None:
            return None, None
        name = self.parse_name(result, name)
        return name, result

    def parse_item(self, item):
        # accept everything if no restriction is applied
        if not self.requires_descendant:
            return item
        result = item
        # if we are not storing strings and we have string: try to interpret it
        # there is a small caveat: what if we require str *classes*?
        if self._require_descendant is not str and isinstance(item, str):
            try:
                from pcUtility import import_object_from_str
                result = import_object_from_str(item)
            except ImportError:
                result = None
        if not self.is_allowed(result):
            return None
        return result

    @classmethod
    def parse_name(cls, item, name=None):
        if name is None:
            return cls.parse_name(item, item)
        # if string check if this is appropriate
        if isinstance(name, str):
            if not isinstance(item, type):
                item = item.__class__
            if issubclass(item, BaseObject):
                if item.dot_path().startswith(name):
                    name = item.dot_path()
            elif item.__module__.startswith(name):
                name = item.__name__
            return name
        elif isinstance(name, type):
            if issubclass(name, BaseObject):
                return cls.parse_name(item, name.dot_path())
            return cls.parse_name(item, name.__name__)
        return cls.parse_name(item, name.__class__)

    @staticmethod
    def collect_entry_points(group):
        """Collect entry points to """
        from pcUtility import collect_entry_points
        return collect_entry_points(group)

    def load_entry_points(self, group, entries=None):
        self.register_entry_points(group=group, entries=entries)
        return self


class URLRegistry(Registry):

    def __init__(self, registry=None, require_descendant=None):
        Registry.__init__(self, registry=registry, require_descendant=require_descendant)


    def retrieve(self, name):
        scheme = name if self._scheme_divider not in name else self.getScheme(name)
        return Registry.retrieve(self, scheme)

    @classmethod
    def containsScheme(cls, url):
        """
        Returns whether a scheme definition is given in the URL. A scheme is defined by some text and the presence of
        "://"
        :param url: URL String
        :type url: str
        :return: Whether a scheme definition was found
        :rtype: bool
        """
        divider = cls.getSchemeDivider()
        return divider in url and len(url.split(divider)) >= 1 and '' != url.split(divider)[0]

    @classmethod
    def getScheme(cls, url):
        """
        Returns the scheme definition of the given URL
        :param url: URL string with ://
        :type url: str
        :return: scheme definition
        :rtype: str
        """
        if not cls.containsScheme(url):
            raise ValueError("No prefix found in - %s - " % url)
        return url.split(cls.getSchemeDivider())[0]

    def isRegistered(self, name):
        result = Registry.isRegistered(self, name)
        if not result:
            result = self.containsScheme(name) and Registry.isRegistered(self, self.getScheme(name))
        return result

    @classmethod
    def stripScheme(cls, url):
        result = url
        if cls.getSchemeDivider() not in url:
            cls.getLog().info("No scheme found in url - %s - " % url)
        else:
            result = url.split("://")[1]
        return result

    @classmethod
    def getSchemeDivider(cls):
        return cls._scheme_divider
