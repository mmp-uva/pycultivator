# coding=utf-8
"""Implementation of abasic module"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PCException(Exception):
    """General pyCultivator exception"""

    def __init__(self, msg):
        Exception.__init__(self)
        self._msg = msg

    def getName(self):
        """Returns the name of this exception class"""
        return self.__class__.__name__

    def getMessage(self):
        """Returns the message of this object"""
        return self._msg

    def __str__(self):
        # return "[{}]: {}".format(self.getName(), self.getMessage())
        return "{}".format(self.getMessage())
