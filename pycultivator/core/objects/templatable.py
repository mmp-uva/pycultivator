"""Implement a generic class type supporting attributes merged by inheritance

Example
-------

Templates support the definition of InheritingAttributes.
These attributes are not overwritten by inheritance but instead are extended.

When the class is created

>>> class A(Template):
...     values = InheritingAttribute(['a'])
>>> A.values
['a']
>>> A().values
['a']

Notice that you access the value directly, the InheritingAttribute class is no longer involved. They are stored
separately in the private _inheriting_attributes attribute.

>>> A._inheriting_attributes.keys()
['values']
>>> isinstance(A._inheriting_attributes['values'], InheritingAttribute)
True

Now use let's extend our class.

>>> class B(A):
...     values = InheritingAttribute(['b'])
>>> B().values
['a', 'b']
>>> A.values
['a']

Instead of redefining the attribute the template merges the two objects.

Private Class Attributes
------------------------

By default the attributes will not change the name of the attribute. However, when we explicitly tell the attribute
to be private it will check whether the name reflects this state.

>>> class X(Template):
...     values = InheritingAttribute(['x'], is_private=True)
>>> X._values
['x']
>>> X.values
Traceback (most recent call last):
    ...
AttributeError: type object 'X' has no attribute 'values'
>>> X._inheriting_attributes.keys()
['values']

Be careful though when mixing classes that define the same attribute to be both public and private:

>>> class Z(A):
...     values = InheritingAttribute(['z'], is_private=True)
>>> Z._values
['a', 'z']

>>> Z.values
['a']

Advanced inheritance schemes
----------------------------

In some situations we may want create inheriting attributes that can be inherited, without inheriting from the parents.

>>> class C(A):
...     values = InheritingAttribute(['c'], inherits=False)
>>> C.values
['c']

The inherits option is not passed to the children, so any child without explicitly blocking inheritance,
will just inherit.

>>> class D(C):
...     values = InheritingAttribute(['d'])
>>> D.values
['c', 'd']

So what happens if we now have multiple inheritance?

>>> class E(B, D):
...     values = InheritingAttribute(['e'])
>>> E.values
['a', 'b', 'c', 'd', 'e']

Well since we only touch the values of the immediate parents, we will get the full history.

Custom merging
--------------

What if we want to apply this to something else than lists or dictionaries? Will it still work?


"""

from pycultivator.core.objects import BaseObject


def is_immediate_parent(base, source, bases):
    """Returns whether the given base is (one of) the immediate parent of the source

    An immediate parent is defined as the class given as parent in the class definition::

        class <name> (<immediate parents>):

    In short, any parent of the immediate parent is not an immediate parent of the source.

    :type base: type
    :type source: str
    :type bases: list[type] or tuple[type]
    :return: Whether the given base is the immediate parent of the source
    :rtype: bool
    """
    result = base in bases
    for b in bases:
        if issubclass(b, base) and b is not base and b.__name__ != source:
            result = False
            break
    return result


class InheritingAttribute(BaseObject):
    """Attributes that are extended when inherited"""

    def __init__(self, value, inherits=True, is_private=False, **kwargs):
        super(InheritingAttribute, self).__init__(**kwargs)
        self._value = value
        self._inherits = inherits
        self._is_private = is_private

    @property
    def inherits(self):
        return self._inherits is True

    @property
    def value(self):
        return self._value

    @property
    def is_private(self):
        return self._is_private is True

    def update(self, other):
        if isinstance(other, InheritingAttribute):
            other = other.value
        if isinstance(self._value, (dict, set)):
            if isinstance(other, (dict, set)):
                v = other.copy()
                v.update(self._value)
                self._value = v
        elif isinstance(self._value, (list, tuple)):
            if isinstance(other, (list, tuple)):
                self._value = other + self._value

    def copy(self, memo=None):
        import copy
        result = type(self)(
            copy.deepcopy(self.value, memo=memo),
            inherits=self.inherits, is_private=self.is_private
        )
        return result


class TemplateMeta(type):

    @staticmethod
    def __new__(mcs, name, bases, attrs=None):
        # collect inheriting attributes
        inheriting_attrs = {}
        # collect from bases
        for base in reversed(bases):
            if hasattr(base, "_inheriting_attributes"):
                for k, v in getattr(base, "_inheriting_attributes").items():
                    if k in inheriting_attrs:
                        inheriting_attrs[k].update(v)
                    else:
                        inheriting_attrs[k] = v.copy()
        # load from attributes
        for k, v in attrs.items():
            if isinstance(v, InheritingAttribute):
                if k in inheriting_attrs.keys():
                    # inherit if required
                    if v.inherits:
                        v.update(inheriting_attrs[k])
                inheriting_attrs[k] = v
            # remove items overloaded to a different type
            elif k in inheriting_attrs.keys():
                inheriting_attrs.pop(k)
        for k in inheriting_attrs.keys():
            if k in attrs.keys():
                attrs.pop(k)
        attrs["_inheriting_attributes"] = inheriting_attrs
        # create class
        return super(TemplateMeta, mcs).__new__(mcs, name, bases, attrs)

    def __init__(cls, name, bases, attrs=None):
        super(TemplateMeta, cls).__init__(name, bases, attrs)


class Template(BaseObject):

    __metaclass__ = TemplateMeta

    def __init__(self, *args, **kwargs):
        super(Template, self).__init__(*args, **kwargs)
        # resolve
        import copy
        for k, attribute in self._inheriting_attributes.items():
            name = k
            if attribute.is_private and not k.startswith("_"):
                name = "_{}".format(k)
            setattr(self, name, copy.deepcopy(attribute.value))
