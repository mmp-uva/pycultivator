
from base import BaseObject
from configurable import ConfigurableObject
from hierarchical import HierarchicalObject

__all__ = [
    "BaseObject", "PCObject",
    "ConfigurableObject",
    "HierarchicalObject",
]


class PCObject(ConfigurableObject):
    """Core class of pyCultivator most class should extend from here.

    - Support for configuration sources
    - Some utility functions

    """

    # Constants
    DT_FORMAT = "%Y-%m-%d %H:%M:%S"
    DT_FORMAT_YMD = "%Y%m%d"

    def __init__(self, *args, **kwargs):
        super(PCObject, self).__init__(*args, **kwargs)
        self._config = None

    @property
    def config(self):
        """Reference to configuration object

        :return: Reference to configuration handler responsible for this object
        :rtype: pycultivator.config.baseConfig.BaseObjectConfig
        """
        return self._config

    @config.setter
    def config(self, c):
        from pycultivator.config.baseConfig import BaseObjectConfig
        if c is not None and not isinstance(c, BaseObjectConfig):
            raise TypeError("New configuration object must be None or a BaseObjectConfig")
        self._config = c

    def refresh(self):
        """Reset this object to the information from the configuration source"""
        # refresh source
        self.config.source.refresh()
        # refresh object
        self.config.update(self)

    def serialize(self):
        """Write information from this object to the configuration source"""
        self.config.save(self)

    @staticmethod
    def sequence(minimum, maximum, n, start=None, step=1):
        """Implements a circular sequence generator.

        Will generate `n` numbers between minimum and maximum,
        starting at the `start` value and with steps of `step`.

        >>> assert PCObject.sequence(0, 10, 10) == [0,1,2,3,4,5,6,7,8,9]
        >>> assert PCObject.sequence(0, 10, 10, step=-1) == [9,8,7,6,5,4,3,2,1,0]

        >>> assert PCObject.sequence(0, 10, 10, 5, step=-1) == [4,3,2,1,0,9,8,7,6,5]

        >>> assert PCObject.sequence(0, 5, 10, start=3) == [3,4,0,1,2,3,4,0,1,2]
        >>> assert PCObject.sequence(0, 10, 10) == [0,1,2,3,4,5,6,7,8,9]
        >>> assert PCObject.sequence(0, 10, 12) == [0,1,2,3,4,5,6,7,8,9,0,1]

        :param minimum: Values in sequence >= minimum
        :param maximum: Values in sequence < maximum
        :param n: Number of items in sequence
        :param start: Value to start (when step size is negative: start = start + step)
        :param step: Step size (defaults to 1)
        :return: sequence s[x_i]: 0 <= i <= n, minimum <= x_i < maximum, x_(i+1)-x_i = step
        :rtype: list[int]
        """
        result = []
        idx = minimum if start is None else start
        if step < 0:
            idx += step
        while len(result) < n:
            if idx < minimum:
                idx = maximum - abs(step)
            if idx >= maximum:
                idx = minimum
            result.append(idx)
            idx += step
        return result

    @classmethod
    def constrain(cls, value, lwr=None, upr=None, name=None, include=True):
        """Tests if a value is within the limits (or equal to)

        :param value: Value to test
        :type value: int or float
        :param lwr: Lower bound of the value
        :type lwr: None or int or float
        :param upr: Upper bound of the value
        :type upr: None or int or float
        :param name: Name of the variable
        :type name: str
        :param include: Whether to accept values at the boundaries
        :type include: bool
        :raises ValueError: If given value is outside the boundaries
        """
        cls.getLog().warning("Constrain is DEPRECATED; use a Constrained Variable instead", exc_info=1)
        from pycultivator.core.pcVariable import ConstrainedVariable
        ConstrainedVariable.validate(value, lwr=lwr, upr=upr, name=name, include=include)