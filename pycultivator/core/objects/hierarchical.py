""""""

from .configurable import BaseObject
from pycultivator.core import pcPath

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class HierarchicalObject(BaseObject):

    def __init__(self, identity, parent=None, name=None, **kwargs):
        super(HierarchicalObject, self).__init__(**kwargs)
        self._identity = None
        self.setIdentity(identity)
        self._name = None
        self.setName(name)
        self._parent = parent
        self._path = pcPath.Path(self)

    # Identifier methods

    def makeIdentity(self, name):
        """Helper to convert a identity to the correct type

        :return: Name in the correct type
        :rtype: str
        """
        return str(name)

    def getIdentity(self):
        """Returns the id (name) of this object

        :rtype: str
        """
        return self._identity

    @property
    def identity(self):
        return self.getIdentity()

    def setIdentity(self, identity):
        """Sets the identity of this object"""
        self._identity = self.makeIdentity(identity)
        return self

    @identity.setter
    def identity(self, identity):
        self.setIdentity(identity)

    def getName(self):
        """Returns the name of this object

        This is NO LONGER necessarily the same as the ID of the object.
        A name is a label for the object, usually given by the parent.

        :rtype: str
        """
        result = self._name
        if result is None:
            result = self.makeName(self.getIdentity())
        return result

    @property
    def name(self):
        return self.getName()

    def makeName(self, name):
        if name is None:
            return None
        return str(name)

    def setName(self, name):
        name = self.makeName(name)
        self._name = name

    @name.setter
    def name(self, name):
        self.setName(name)

    # Parent methods

    def getParent(self):
        """Returns the parent of this device (usually a protocol)

        :return: The parent of this device
        :rtype: None or pycultivator.core.objects.hierarchical.HierarchicalObject
        """
        return self._parent

    @property
    def parent(self):
        return self.getParent()

    def hasParent(self):
        """Returns whether this object has a parent"""
        return self.getParent() is not None

    def setParent(self, p):
        """Sets the parent of this object

        :param p: None or pycultivator.core.objects.hierarchical.HierarchicalObject
        :return: reference to self
        :rtype: pycultivator.core.objects.hierarchical.HierarchicalObject
        """
        self._parent = p
        return self

    @parent.setter
    def parent(self, p):
        self.setParent(p)

    def getRoot(self):
        """Return the root object in this hierarchy (i.e. the first object without parent)

        :rtype: pycultivator.core.objects.hierarchical.HierarchicalObject
        """
        result = self
        if self.hasParent() and isinstance(self.parent, HierarchicalObject):
            result = self.getParent().getRoot()
        return result

    @property
    def root(self):
        return self.getRoot()

    # copy methods

    def copy(self, memo=None):
        """Create a new deepcopy of this hierarchical object

        :return: A copy of this object
        :rtype: pycultivator.core.objects.hierarchical.HierarchicalObject
        """
        result = type(self)(
            identity=self.getIdentity(), parent=self.getParent(), name=self._name, settings=self.settings
        )
        # do more
        return result

    # Path methods

    def getPath(self):
        return self.path

    @property
    def path(self):
        """Returns the absolute path to this object as a string

        :rtype: pycultivator.core.pcPath.Path
        """
        return self._path

    @property
    def children(self):
        """Return all children attached to this object

        :rtype: list[pycultivator.core.objects.hierarchical.HierarchicalObject]
        """
        return []

    def get_children(self):
        return self.children

    def __str__(self):
        """Returns the name of the object (if no name, name == identity)"""
        return str(self.name)

    def __eq__(self, other):
        if isinstance(other, HierarchicalObject):
            return other.identity == self.identity and other.parent == self.parent
        return self.identity == self.makeIdentity(other)

    def __ne__(self, other):
        return not (self == other)

    def __contains__(self, item):
        return item in self.children
