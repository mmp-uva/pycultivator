
import six
from .base import BaseObject
from pycultivator.core import pcSettings, pcException


class ConfigurableMeta(type):

    def __init__(cls, name, bases=None, attrs=None):
        if bases is None:
            bases = []
        if attrs is None:
            attrs = {}
        super(ConfigurableMeta, cls).__init__(name, bases, attrs)
        # load inheritance setting: reset to true if not specifically set
        inherits = True
        v = cls.load_from_dict(attrs, "inherits_default_settings", None)
        if v is not None:
            inherits = v
        cls.inherits_default_settings = inherits
        # load default settings: only from attrs
        defaults = {}
        defaults.update(cls.load_from_dict(attrs, "default_settings", {}))
        # store defaults
        cls.default_settings = cls._getDefaultSettings(
            name, defaults, bases, inherits, cls.getNameSpace()
        )

    @classmethod
    def load_from_bases(mcs, bases, name, default=None):
        value = default
        for base in bases:
            v = mcs.load_from_class(base, name, None, remove=False)
            if v is not None:
                value = v
        return value

    @staticmethod
    def load_from_class(class_, name, default=None, remove=True):
        value = default
        if hasattr(class_, name):
            value = getattr(class_, name)
            if remove:
                delattr(class_, name)
        return value

    @staticmethod
    def load_from_dict(attrs, name, default=None, remove=True):
        """Load a value from a dictionary by key and remove the key if necessary

        :param attrs:
        :type attrs: dict[str, object]
        :param name:
        :type name:
        :param default:
        :type default:
        :param remove:
        :type remove:
        :return:
        :rtype:
        """
        value = default
        if name in attrs.keys():
            value = attrs.get(name)
            if remove:
                attrs.pop(name)
        return value

    @staticmethod
    def _getDefaultSettings(name, defaults, bases, inherits=True, namespace=""):
        """Resolves the default settings for this class

        CAUTION: This may take some time!

        :rtype: dict[str, object]
        """
        result = pcSettings.PCSettings()
        bases = list(bases)[:]
        # reverse order: furthest away first
        bases.reverse()
        # remove bases that were inherited by other bases (except this class)
        # direct_bases = ConfigurableMeta.filterDistantBases(name, bases)
        # iterate over base classes
        if inherits:
            for base in bases:
                if isinstance(base, ConfigurableMeta): # and base.inheritsDefaultSettings():
                    settings = base.getDefaultSettings()
                    if isinstance(settings, pcSettings.PCSettings):
                        settings = settings.to_dict()
                    settings = pcSettings.PCSettings.reduce_dict(settings, base.getNameSpace())
                    result.union(settings, namespace="")
        # add current settings
        result.union(defaults, namespace="")
        # prepend with current namespace
        result.prepend(namespace)
        return result

    @staticmethod
    def filterDistantBases(name, bases):
        return filter(lambda b: ConfigurableMeta.isDirectBase(name, b, bases), bases)

    @staticmethod
    def isDirectBase(name, base, bases):
        """Determines whether the given base is a direct base of name

        A direct base has no bases that are a subclass or is the source class

        """
        result = True
        for b in bases:
            if b != base and issubclass(b, base) and b.__name__ != name:
                result = False
                break
        return result

    def getNameSpace(cls):
        return cls.namespace

    def inheritsDefaultSettings(cls):
        """Whether this class inherits it's default settings from it's parent

        :rtype: bool
        """
        return cls.inherits_default_settings

    def getDefaultSettings(cls):
        return cls.default_settings


@six.add_metaclass(ConfigurableMeta)
class ConfigurableObject(BaseObject):
    """Configurable objects use pcSettings to store system-wide settings.

    Look at pycultivatore.core.pcSettings.PCSettings for information on the setting storage.

    NOTE: Eats any **kwargs passed to this object and stores them into the settings.

    During the initialization of the object the settings object is constructed.
    At first the default settings are deduced. The default settings are defined on a class level and can be inherited
    (see _inherits_default_settings and inheritsDefaultSettings()). The getDefaultSettings() classmethod collects the
    default settings of each class in the mro() until it finds a class prohibiting to inherit settings from higher up
    the mro chain. In the diamond shaped inheritance patterns this may lead to missing some of the default settings.

    When combining settings

    """

    namespace = ""
    inherits_default_settings = True
    # default_settings = None
    default_settings = {
        # default settings for this class
    }

    def __init__(self, settings=None, *args, **kwargs):
        """Initialize the object

        Merges the given settings dictionary:
        1. Merge default settings with given settings dictionary
        2. Merge settings dictionary with given keyword-arguments

        :param settings: Dictionary with all settings
        :type settings: dict
        :param initialised: Set to false
        """
        super(ConfigurableObject, self).__init__(*args)
        # If not settings are given, start with empty dict
        if settings is None:
            settings = {}
        # set default settings as initial settings
        # create base settings object
        self._settings = pcSettings.PCSettings(namespace=self.getNameSpace())
        # now update with default settings
        self._settings = self._settings.union(self.getDefaultSettings(), namespace="")
        # merge with the given settings dict, do prepend with namespace
        self._settings = self._settings.union(settings)
        # merge with kwargs, do prepend namespace
        self._settings = self._settings.update(kwargs)

    # only implement if needed
    # def __del__(self):
    #     """Cleans the object before it is deleted"""

    @classmethod
    def getNameSpace(cls):
        """Namespace used by this class

        :rtype: str
        """
        return cls.namespace

    @classmethod
    def getDefaultSettings(cls):
        return cls.default_settings

    def getSettings(self):
        """Returns the settings dictionary of this object

        :return: PCSettings dictionary
        :rtype: pycultivator.core.pcSettings.PCSettings
        """
        return self._settings

    @property
    def settings(self):
        return self.getSettings()

    def hasSetting(self, name, namespace=None):
        """Returns whether the given setting key is stored in this object

        :param name: Name of the setting
        :type name: str
        :param namespace: Namespace of the setting, defaults to the namespace of this object
        :type namespace: str or None
        :return: Whether the given setting key is in the settings dictionary
        :rtype: bool
        """
        return self.settings.has(name, namespace)

    def getSetting(self, name, default=None, namespace=None):
        """Get value of setting from the settings dictionary of this object

        :param name: Name of the setting
        :type name: str
        :param default: Default value to return
        :type default: object or None
        :param namespace: Namespace of the settings, defaults to the namespace of this object
        :type namespace: str or None
        :return: The value or the default value
        :rtype: object or None
        """
        result = default
        if namespace is None:
            namespace = self.getNameSpace()
        if self.settings.has(name, namespace):
            result = self.settings.get(name, namespace=namespace)
        return result

    def setSetting(self, key, value, namespace=None):
        """
        Set value of setting in the settings dictionary of this object
        :param key: Name of the setting to set (without namespace)
        :type key: str
        :param value: Value to set setting to
        :type value: object
        :param namespace: Namespace of setting, if None use default namespace of object
        :type namespace: str or None
        :return: Reference to self
        :rtype: ConfigurableObject
        """
        if namespace is None:
            namespace = self.getNameSpace()
        self.settings.set(key, value, namespace)
        return self

    def removeSetting(self, key, namespace=None):
        self.settings.remove(key, namespace)
        return self

    def copy(self, memo=None):
        result = type(self)(settings=self.settings)
        # do more
        return result


class ConfigurableObjectException(pcException.PCException):
    """Exception raised by a ConfigurableObject"""

    def __init__(self, msg):
        super(ConfigurableObjectException, self).__init__(msg=msg)
