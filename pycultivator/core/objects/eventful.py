"""Implements a object to which others can subscribe for notification upon a certain event"""

from pycultivator.core.objects import BaseObject
import functools
import six

__all__ = [
    "EventFulObject", "receives", "triggers"
]


class EventfulMeta(type):
    """A meta object"""

    @staticmethod
    def __new__(mcs, name, bases=None, attrs=None):
        if bases is None:
            bases = []
        if attrs is None:
            attrs = {}
        result = super(EventfulMeta, mcs).__new__(mcs, name, bases, attrs)
        return result

    def __init__(cls, name, bases=None, attrs=None):
        class_event_hooks = set()
        class_event_slots = {}
        attributes = filter(lambda a: hasattr(cls, a), dir(cls))
        # find event hooks and slots
        for name in attributes:
            attr = getattr(cls, name)
            if hasattr(attr, "_hook"):
                hook = getattr(attr, "_hook")
                class_event_hooks.add(hook)
            elif hasattr(attr, "_slot"):
                slot = getattr(attr, "_slot")
                if slot not in class_event_slots.keys():
                    class_event_slots[slot] = {attr}
                else:
                    class_event_slots[slot].add(attr)
        cls.class_event_hooks = class_event_hooks
        cls.class_event_slots = class_event_slots
        super(EventfulMeta, cls).__init__(name, bases, attrs)


class EventFulObject(six.with_metaclass(EventfulMeta, BaseObject)):
    """A eventful object facilitates event-driven programming

    NOTE: Inherits from BaseObject (NOT PCObject!) for simplicity

    EventFulObjects allow for one or more methods to be called whenever an event is triggered.
    Implementation of EventFulObjects should call the internal _trigger method with the event name and extra
    arguments to call registered methods and pass the arguments.

    When registering a method to an event, make sure it accepts all passed arguments.

    """

    def __init__(self, *args, **kwargs):
        super(EventFulObject, self).__init__(*args, **kwargs)
        # names of the events being raised by this class
        self._event_hooks = self.class_event_hooks.copy()
        """:type: set[str]"""
        # functions being called when a event is received by this class
        import copy
        # fill event slots with class defined slots
        self._event_slots = copy.deepcopy(self.class_event_slots)
        """:type: dict[str, set[callable | pycultivator.core.objects.eventful.EventFulObject]]"""
        # register observers
        self._event_observers = set()

    def __del__(self):
        # check if we passed __init__
        if hasattr(self, "_event_observers"):
            while len(self._event_observers) > 0:
                observer = next(iter(self._event_observers))
                self.removeObserver(observer)
                if isinstance(observer, EventFulObject):
                    observer.removeObserver(self)

    def getEventHooks(self):
        """List the names of the event hooks provided by this class"""
        return self._event_hooks

    @property
    def event_hooks(self):
        return self.getEventHooks()

    def hasEventHook(self, name):
        return name in self.getEventHooks()

    def getEventSlots(self):
        """List the names of the event slots implemented by this class"""
        return self._event_slots.keys()

    @property
    def event_slots(self):
        return self.getEventSlots()

    def hasEventSlot(self, name):
        return name in self.getEventSlots()

    def addEventSlot(self, name, handler):
        if not callable(handler) and not isinstance(handler, EventFulObject):
            raise TypeError("Expected a callable or pycultivator.core.objects.eventful.EventFulObject")
        if name in self._event_slots.keys():
            self._event_slots[name].add(handler)
        else:
            self._event_slots[name] = {handler}

    def addObserver(self, observer):
        """Register an `observer` to receive events raised by this class

        Whenever the event is triggered by the class, it will call all observers registered to that event
        """
        result = False
        if not callable(observer) and not isinstance(observer, EventFulObject):
            raise TypeError("Expected a callable or pycultivator.core.objects.eventful.EventFulObject")
        if observer not in self._event_observers:
            self._event_observers.add(observer)
            result = True
        return result

    def removeObserver(self, observer):
        """Remove `observer` from being called when `event` is raised by this class"""
        result = False
        if not callable(observer) and not isinstance(observer, EventFulObject):
            raise TypeError("Expected a callable or pycultivator.core.objects.eventful.EventFulObject")
        if observer in self._event_observers:
            self._event_observers.remove(observer)
            result = True
        return result

    def triggerEvent(self, event, data=None):
        """Triggers an event"""
        for observer in self._event_observers:
            if isinstance(observer, EventFulObject):
                observer.receiveEvent(event, self, data)
            elif callable(observer):
                observer(event, self, data)

    def receiveEvent(self, event, source, data=None):
        """Receives an event and relays it to the appropriate slot"""
        if event in self._event_slots.keys():
            handlers = self._event_slots[event]
            for handler in handlers:
                if callable(handler):
                    try:
                        import inspect
                        if 'self' in inspect.getargspec(handler).args:
                            handler(self, event, source, data)
                        else:
                            handler(event, source, data)
                    except TypeError as te:
                        pass

    def copy(self, memo=None):
        result = super(EventFulObject, self).copy(memo=memo)
        import copy
        result._event_hooks = copy.deepcopy(self._event_hooks, memo=memo)
        result._event_slots = copy.deepcopy(self._event_slots, memo=memo)
        result._event_observers = copy.deepcopy(self._event_observers, memo=memo)
        return result


def triggers(name=None, before=False):
    """A decorator for triggering an event after the method was called"""
    def decorator(f):
        hook_name = f.__name__
        if name is not None:
            hook_name = name
        f._hook = hook_name

        @functools.wraps(f)
        def magic(self, *args, **kwargs):
            result = None
            if before:
                self.triggerEvent(hook_name, result)
            try:
                result = f(self, *args, **kwargs)
            finally:
                if not before:
                    self.triggerEvent(hook_name, result)
            return result
        return magic
    return decorator


def receives(name=None):
    """A decorator for connecting a method to an event handler"""
    def decorator(f):
        hook_name = f.__name__
        if name is not None:
            hook_name = name
        f._slot = hook_name

        return f
    return decorator
