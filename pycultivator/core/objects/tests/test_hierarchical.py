"""
Test file to Test pcObject.py
"""

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.core.objects import hierarchical

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class DefaultHierarchicalObject(hierarchical.HierarchicalObject):
    """"""

    namespace = "test"
    default_settings = {
        'a': 1, 'b': 2, 'c': 3,
    }

    def __init__(self, name, parent=None, settings=None, **kwargs):
        super(DefaultHierarchicalObject, self).__init__(name, parent, settings=settings, **kwargs)
        self._children = {}

    @property
    def children(self):
        """Returns all the child of this object

        :rtype: dict[str, pycultivator.fundament.ConfigurableObject.HierarchicalObject]
        """
        return self._children

    def hasChild(self, name):
        """Returns whether a child with the given name is registered to this object"""
        return name in self.getChildren().keys()

    def getChild(self, name):
        """Returns a child from this object"""
        if not self.hasChild(name):
            raise KeyError("Unknown child %s" % name)
        return self.getChildren().get(name)

    def addChild(self, name, child):
        """Adds a child with a name

        :type name: str
        :type child: pycultivator.fundament.ConfigurableObject.HierarchicalObject
        """
        if self.hasChild(name):
            raise KeyError("%s is already used" % name)
        self.getChildren()[name] = child
        child.setParent(self)
        return self

    def removeChild(self, name):
        """Removes a child from this object"""
        if not self.hasChild(name):
            raise KeyError("Unknown child %s" % name)
        child = self.getChildren().pop(name)
        child.setParent("")
        return self


class TestHierarchicalObject(UvaSubjectTestCase):

    def test_getIdentifier(self):
        o = DefaultHierarchicalObject("a")
        self.assertEqual(o.identity, "a")
        self.assertEqual(o.name, "a")

    def test_getParent(self):
        self.fail()

    def test_hasParent(self):
        self.fail()

    def test_setParent(self):
        self.fail()

    def test_getPath(self):
        self.fail()

    def test_getParentPath(self):
        self.fail()

    def test_getAbsolutePath(self):
        self.fail()

    def test_childWithPath(self):
        self.fail()

    def test_objectFromPath(self):
        self.fail()
