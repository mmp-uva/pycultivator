"""Test pcObject from pycultivator.core.objects"""

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.core.objects import PCObject


class TestPCObject(UvaSubjectTestCase):

    _subject_cls = PCObject
    _abstract = False

    def getSubject(self):
        """Subject object

        :return:
        :rtype: pycultivator.core.objects.PCObject
        """
        return super(TestPCObject, self).getSubject()

