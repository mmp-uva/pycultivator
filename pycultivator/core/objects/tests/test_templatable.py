from pycultivator.core.tests import UvaTestCase
from pycultivator.core.objects.templatable import *

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class A(Template):

    values = InheritingAttribute(["a"])


class B(A):

    values = InheritingAttribute(["b"])


class C(A):

    values = InheritingAttribute(["c"], inherits=False)


class D(B):

    values = InheritingAttribute(["d"])


class E(D, C):

    values = InheritingAttribute(["e"])


class F(A):
    values = None


class TestTemplate(UvaTestCase):

    _abstract = False

    def __init__(self, methodName="runTest"):
        super(TestTemplate, self).__init__(methodName=methodName)
        self._data = None

    def test_is_immediate_inheritance(self):
        # self
        self.assertTrue(is_immediate_parent(A, A.__name__, A.mro()))
        # direct inheritance
        self.assertTrue(is_immediate_parent(A, B.__name__, B.mro()))
        self.assertTrue(is_immediate_parent(B, B.__name__, B.mro()))
        self.assertFalse(is_immediate_parent(C, B.__name__, B.mro()))
        # indirect inheritance
        self.assertTrue(is_immediate_parent(B, D.__name__, D.mro()))
        self.assertFalse(is_immediate_parent(A, D.__name__, D.mro()))
        self.assertTrue(is_immediate_parent(D, D.__name__, D.mro()))
        # multiple inheritance
        self.assertTrue(is_immediate_parent(D, E.__name__, E.mro()))
        self.assertTrue(is_immediate_parent(C, E.__name__, E.mro()))
        self.assertTrue(is_immediate_parent(E, E.__name__, E.mro()))
        self.assertFalse(is_immediate_parent(B, E.__name__, E.mro()))
        self.assertFalse(is_immediate_parent(A, E.__name__, E.mro()))

    def test_dict_inheritance(self):
        class A(Template):
            values = InheritingAttribute({"a": 1})

        class B(A):
            values = InheritingAttribute({"a": 2, "b": 1})

        class C(A):
            values = InheritingAttribute({"a": 3, "c": 1}, inherits=False)

        class D(B):
            values = InheritingAttribute({"d": 1})

        class E(D, C):
            values = InheritingAttribute({"e": 1})

        self.assertEqual(A().values, {"a": 1})
        self.assertEqual(B().values, {"a": 2, "b": 1})
        self.assertEqual(C().values, {"a": 3, "c": 1})
        self.assertEqual(D().values, {"a": 2, "b": 1, "d": 1})
        self.assertEqual(E().values, {"a": 3, "b": 1, "d": 1, "c": 1, "e": 1})

    def test_list_inheritance(self):
        self.assertEqual(A().values, ["a"])
        self.assertEqual(B().values, ["a", "b"])
        self.assertEqual(C().values, ["c"])
        self.assertEqual(D().values, ["a", "b", "d"])
        self.assertEqual(E().values, ["a", "b", "d", "c", "e"])
        self.assertNotEqual(E().values, ["a", "b", "c", "d", "e"])
        self.assertIsNone(F().values)

    def test_int_inheritance(self):
        class A(Template):
            value = InheritingAttribute(1)

        class B(A):
            value = InheritingAttribute(2)

        class C(A):
            pass

        class D(A):
            value = None

        self.assertEqual(A().value, 1)
        self.assertEqual(B().value, 2)
        self.assertEqual(C().value, 1)
        self.assertIsNone(D().value)

    def test_independence(self):
        a = A()
        b = B()
        self.assertEqual(a.values, ["a"])
        self.assertEqual(b.values, ["a", "b"])
        a2 = A()
        self.assertEqual(a2.values, ["a"])
        a2.values.append("b")
        self.assertEqual(a2.values, ["a", "b"])
        self.assertEqual(a.values, ["a"])
        self.assertEqual(b.values, ["a", "b"])
