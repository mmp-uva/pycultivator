
from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.core.objects.configurable import ConfigurableObject
from pycultivator.core import pcSettings

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class ObjectA(ConfigurableObject):

    namespace = "test.a"
    default_settings = {
        'a': 1, 'b': 0, 'c': 0, 'd': 0
    }


class ObjectB(ObjectA):

    namespace = "test.b"
    default_settings = {
        'b': 1,
    }


class ObjectB1(ObjectA):

    namespace = "test.b"
    default_settings = {
        'b': 2
    }


class ObjectC(ObjectB):

    namespace = "test.c"
    default_settings = {
        'c': 1, 'd': -1
    }


class ObjectC1(ObjectB1):

    namespace = "test.c"
    inherits_default_settings = False
    default_settings = {
        'c': 2, 'd': -1
    }


class ObjectD(ObjectC, ObjectC1):

    namespace = "test.d"
    inherits_default_settings = True
    default_settings = {
        'd': 1
    }


class ObjectE(ObjectA):

    namespace = "test.e"
    default_settings = {
        'e': {'a': 1, 'b': 2, 'c': 3}
    }


class ObjectF(ObjectD, ObjectA):

    namespace = "test.f"
    default_settings = {
        'a': 2
    }


class TestConfigurableObject(UvaSubjectTestCase):

    _subject_cls = ConfigurableObject
    _abstract = False
    _default_subject = None

    def setUp(self):
        super(TestConfigurableObject, self).setUp()
        self._default_subject = ObjectA()

    def test_direct_bases(self):
        subject = object
        direct_bases = ObjectA.filterDistantBases(subject.__name__, subject.__bases__)
        self.assertListEqual(list(direct_bases), [])
        subject = ObjectA
        direct_bases = ObjectA.filterDistantBases(subject.__name__, subject.__bases__)
        self.assertListEqual(list(direct_bases), [ConfigurableObject])
        subject = ObjectB
        direct_bases = ObjectA.filterDistantBases(subject.__name__, subject.__bases__)
        self.assertListEqual(list(direct_bases), [ObjectA])
        subject = ObjectD
        direct_bases = ObjectA.filterDistantBases(subject.__name__, subject.__bases__)
        self.assertListEqual(list(direct_bases), [ObjectC, ObjectC1])
        subject = ObjectF
        direct_bases = ObjectA.filterDistantBases(subject.__name__, subject.__bases__)
        self.assertListEqual(list(direct_bases), [ObjectD])

    def test_object_init(self):
        # test default init without additional parameters
        self.assertEqual(self._subject.getSettings().to_dict(), {}, msg="Not empty settings dict")
        self.assertEqual(self._subject.getDefaultSettings().to_dict(), {}, msg="Not empty default settings dict")
        self.assertEqual(self._subject.getNameSpace(), "", msg="Not empty namespace str")
        # test the settings parameter
        subject = ObjectA(settings={'a': 2})
        self.assertEqual(subject.getSetting("a"), 2)
        # new settings are allowed as part of the settings dict
        subject = ObjectA(settings={'xyz': 4})
        self.assertTrue(subject.hasSetting("xyz"))
        # test the kwargs parameter
        self._subject = ObjectA(a=3)
        self.assertEqual(self._subject.getSetting("a"), 3)
        self._subject = ObjectA(**{'a': 3})
        self.assertEqual(self._subject.getSetting("a"), 3)
        # new settings are not allowed as keyword arguments
        subject = ObjectA(xyz=4)
        self.assertFalse(subject.hasSetting("xyz"))
        subject = ObjectA(**{'xyz':4})
        self.assertFalse(subject.hasSetting("xyz"))
        # test combination of kwargs and settings -> kwargs should replace
        settings = {'a': 1, 'b': 2}
        self._subject = ObjectA(settings=settings, b=0)
        self.assertEqual(self._subject.getSetting('a'), 1)
        self.assertEqual(self._subject.getSetting('b'), 0)
        self._subject = ObjectA(settings=settings, **{'b': 0})
        self.assertEqual(self._subject.getSetting('a'), 1)
        self.assertEqual(self._subject.getSetting('b'), 0)

    def test_default_settings(self):
        settings = ObjectA.getDefaultSettings().to_dict()
        self.assertEqual(settings, {'test.a.a': 1, 'test.a.b': 0, 'test.a.c': 0, 'test.a.d': 0})
        settings = ObjectB.getDefaultSettings().to_dict()
        self.assertEqual(settings, {'test.b.a': 1, 'test.b.b': 1, 'test.b.c': 0, 'test.b.d': 0})
        settings = ObjectB1.getDefaultSettings().to_dict()
        self.assertEqual(settings, {'test.b.a': 1, 'test.b.b': 2, 'test.b.c': 0, 'test.b.d': 0})
        settings = ObjectC.getDefaultSettings().to_dict()
        self.assertEqual(settings, {'test.c.a': 1, 'test.c.b': 1, 'test.c.c': 1, 'test.c.d': -1})
        settings = ObjectC1.getDefaultSettings().to_dict()
        self.assertEqual(settings, {'test.c.c': 2, 'test.c.d': -1})
        settings = ObjectD.getDefaultSettings().to_dict()
        self.assertEqual(settings, {'test.d.a': 1, 'test.d.b': 1, 'test.d.c': 1, 'test.d.d': 1})
        settings = ObjectF.getDefaultSettings().to_dict()
        self.assertEqual(settings, {'test.f.a': 2, 'test.f.b': 1, 'test.f.c': 1, 'test.f.d': 1})

    def test_getSetting(self):
        self.assertIsInstance(ObjectA().settings, pcSettings.PCSettings)
        self.assertEqual(ObjectA().getSetting('a'), 1)
        self.assertIsNone(ObjectA().getSetting('a', namespace="foo"))
        self.assertIsNone(ObjectA().getSetting('e'))
        self.assertEqual(ObjectA().getSetting('b', default=3), 0)
        self.assertEqual(ObjectA().getSetting('e', default=3), 3)
        self.assertEqual(ObjectA().getSetting('e', default=3), 3)

    def test_setSetting(self):
        self._subject.setSetting("a", 1)
        self.assertEqual(self._subject.getSetting('a'), 1)
        self.assertEqual(self._default_subject.getSetting('a'), 1)
        self._default_subject.setSetting("a", 0)
        self.assertEqual(self._default_subject.getSetting('a'), 0)
        self._default_subject.setSetting("a", 2, namespace="")
        self.assertEqual(self._default_subject.getSetting('a'), 0)
        self.assertEqual(self._default_subject.getSetting('a', namespace=""), 2)

    def test_independence(self):
        a = ObjectA()
        b = ObjectB()
        self.assertEqual(a.getSettings().to_dict(), {'test.a.a': 1, 'test.a.b': 0, 'test.a.c': 0, 'test.a.d': 0})
        self.assertEqual(b.getSettings().to_dict(), {'test.b.a': 1, 'test.b.b': 1, 'test.b.c': 0, 'test.b.d': 0})
        b.setSetting("b", 2)
        self.assertEqual(a.getSettings().to_dict(), {'test.a.a': 1, 'test.a.b': 0, 'test.a.c': 0, 'test.a.d': 0})
        self.assertEqual(b.getSettings().to_dict(), {'test.b.a': 1, 'test.b.b': 2, 'test.b.c': 0, 'test.b.d': 0})
        a = ObjectE()
        b = ObjectE()
        self.assertEqual(a.getSetting("e"), {'a': 1, 'b': 2, 'c': 3})
        self.assertEqual(b.getSetting("e"), {'a': 1, 'b': 2, 'c': 3})
        a.getSetting("e")['b'] = -1
        self.assertEqual(a.getSetting("e"), {'a': 1, 'b': -1, 'c': 3})
        self.assertEqual(b.getSetting("e"), {'a': 1, 'b': 2, 'c': 3})
