
from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.core.objects.eventful import *

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class A(EventFulObject):

    def __init__(self, value=0):
        super(A, self).__init__()
        self._value = value

    @property
    def value(self):
        return self._value

    @receives("add")
    def _event_add(self, event, source, data=None):
        if data is None:
            data = 1
        self.add(data)

    @triggers()
    def add(self, value=1):
        self._value += value
        return self._value

    @triggers()
    def remove(self, value=1):
        self._value -= value
        return self._value

    @receives("reset")
    def _event_reset(self, *args):
        self.reset()

    @triggers()
    def reset(self):
        self._value = 0


class B(A):

    @receives("add")
    def _event_add(self, event, source, data=None):
        if data is None:
            data = 1
        self.remove(data)

    @triggers()
    def extra(self, value=1):
        self._value = value

    @receives("extra")
    def _event_extra(self, event, source, data=None):
        if data is None:
            data = 1
        self.extra(data)


class TestEventFulObject(UvaSubjectTestCase):

    _subject_cls = EventFulObject
    _abstract = False

    def __init__(self, methodName="runTest"):
        super(TestEventFulObject, self).__init__(methodName=methodName)
        self._data = None

    def setUp(self):
        result = super(TestEventFulObject, self).setUp()
        self._data = None
        return result

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.core.objects.eventful.EventFulObject
        """
        return super(TestEventFulObject, self).getSubject()

    def test_event_hook_registration(self):
        subject = A()
        self.assertIn("add", subject.getEventHooks())
        self.assertNotIn("extra", subject.getEventHooks())
        self.assertIn("reset", subject.getEventHooks())
        subject = B()
        self.assertIn("add", subject.getEventHooks())
        self.assertIn("extra", subject.getEventHooks())
        self.assertIn("reset", subject.getEventHooks())

    def test_event_hook_independence(self):
        self.assertEqual(len(list(EventFulObject.class_event_hooks)), 0)
        self.assertEqual(len(list(A.class_event_hooks)), 3)
        self.assertEqual(len(list(B.class_event_hooks)), 4)

    def test_event_slot_registration(self):
        subject = A()
        self.assertIn("add", subject.getEventSlots())
        self.assertIn("reset", subject.getEventSlots())

    def test_event_slot_independence(self):
        self.assertEqual(len(list(EventFulObject.class_event_slots.keys())), 0)
        self.assertEqual(len(list(A.class_event_slots.keys())), 2)
        # in B we replace a trigger
        self.assertEqual(len(list(B.class_event_slots.keys())), 3)

    def test_events(self):
        # test using the same class
        subject_a = A()
        subject_b = A()
        # b will receive events from a
        subject_a.addObserver(subject_b)
        self.assertEqual(subject_a.value, 0)
        self.assertEqual(subject_b.value, subject_a.value)
        # add 1
        subject_a.add()
        self.assertEqual(subject_a.value, 1)
        self.assertEqual(subject_b.value, subject_a.value)
        # test using different classes
        subject_a = A()
        subject_b = B()
        # b will receive events from a
        subject_a.addObserver(subject_b)
        self.assertEqual(subject_a.value, 0)
        self.assertEqual(subject_b.value, subject_a.value)
        # add 1
        subject_a.add()
        self.assertEqual(subject_a.value, 1)
        self.assertEqual(subject_b.value, -1)
