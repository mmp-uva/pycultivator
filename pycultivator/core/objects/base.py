# coding=utf-8
"""Implement most basic classes for pyCultivator

BaseObject: Solves super.__init__(...) calls by accepting *args and **kwargs.
PCObject: Object commonly used in pycultivator, automatically connects to logger

"""

from pycultivator.core import pcLogger

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class BaseObject(object):
    """The very base of every object in pyCultivator

    The BaseObject aims to fix any super().__init__() problems; having a single base object.
    All overloading classes should call super in their init, in order to keep the chain.
    """

    # class wide logger instance
    _log = None

    @classmethod
    def getRootLog(cls):
        """Returns the root logger object used in this script

        :rtype: logging.Logger
        """
        return pcLogger.getLogger()

    @classmethod
    def getLog(cls):
        """Return the logger object of this class

        :return: The logger object connected to this class
        :rtype: logging.Logger
        """
        if cls._log is None:
            name = cls.__name__
            cls._log = pcLogger.createLogger(name)
            cls._log.log(0, "Connected {} to logger".format(name))
        return cls._log

    @property
    def log(self):
        return self.getLog()

    def __init__(self, *args, **kwargs):
        # ignore any argument passed
        pass

    @classmethod
    def dot_path(cls):
        """Return the dot path of this class

        :rtype: str
        """
        return cls.__module__ + "." + cls.__name__

    def copy(self, memo=None):
        """Create a deepcopy of this object"""
        result = type(self)()
        # do some more
        return result

    def __copy__(self):
        return self.copy()

    def __deepcopy__(self, memo=None):
        return self.copy(memo=memo)
