# coding=utf-8
"""Module with Objects implementing the logging module"""
from __future__ import print_function

import sys

import logging
from logging import StreamHandler
from logging.handlers import RotatingFileHandler
from datetime import datetime as dt

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

LEVELS = {
    logging.CRITICAL: 'CRITICAL',
    logging.ERROR: 'ERROR',
    logging.WARNING: 'WARNING',
    logging.INFO: 'INFO',
    logging.DEBUG: 'DEBUG',
    logging.NOTSET: 'NOTSET',
    'CRITICAL': logging.CRITICAL,
    'ERROR': logging.ERROR,
    'WARN': logging.WARNING,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG,
    'NOTSET': logging.NOTSET,
}


def levels(value):
    if isinstance(value, str) and value.upper() in LEVELS:
        value = LEVELS[value.upper()]
    return value


class PCLogger(logging.Logger):
    """A Logger with some added capabilities"""

    def addStreamHandler(self, level=logging.NOTSET, propagate=False):
        """Connect a UVAStreamHandler to this logger.

        :param level: The minimal level of messages that will be accepted by this handler
        :type level: int
        :param propagate: Whether to allow propagation of messages, default False to prevent double messages.
        :type propagate: bool
        :rtype: pycultivator.foundation.pcLogger.PCLogger
        """
        return connectStreamHandler(self, level=level, propagate=propagate)

    def addFileHandler(self, logfile, level=logging.NOTSET, propagate=False):
        """Connect a FileHandler, with appropriate format, to the given logger and write log to given logfile.

        :param logfile: path to file to log to, or use <date>_<logger.name>.log
        :type logfile: str
        :param level: The minimal level of messages that will be accepted by this handler
        :type level: int
        :param propagate: Whether to allow propagation of messages, default False to prevent double messages.
        :type propagate: bool
        :rtype: pycultivator.foundation.pcLogger.PCLogger
        """
        return connectFileHandler(self, logfile, level=level, propagate=propagate)

    def callHandlers(self, record):
        c = self
        found = 0
        while c:
            for hdlr in c.handlers:
                found = found + 1
                if record.levelno >= hdlr.level:
                    hdlr.handle(record)
            if not c.propagate:
                c = None  # break out
            else:
                c = c.parent
        if (found == 0) and record.levelno > 20:
            print(UVALogFormatter().format(record), file=sys.stderr)


class UVAStreamHandler(StreamHandler):
    """A StreamHandler that by default formats it's messages with UVAFormatter"""

    def __init__(self, stream=None, formatter=None, level=logging.NOTSET):
        super(UVAStreamHandler, self).__init__(stream=stream)
        if formatter is None:
            formatter = UVALogFormatter()
        self.setFormatter(formatter)
        self.setLevel(level)


class UVARotatingFileHandler(RotatingFileHandler):
    """A RotatingFileHandler that by default formats it's messages with UVAFormatter"""

    def __init__(self, filename, mode="a", maxBytes=5 * 1024 ** 2, backupCount=5, encoding=None, delay=0,
                 formatter=None, level=logging.NOTSET):
        super(UVARotatingFileHandler, self).__init__(
            filename, mode=mode, maxBytes=maxBytes, backupCount=backupCount, encoding=encoding, delay=delay
        )
        if formatter is None:
            formatter = UVALogFormatter()
        self.setFormatter(formatter)
        self.setLevel(level)


class UVALogFormatter(logging.Formatter):
    """A formatting class; to format log message to string"""

    LOG_FORMAT = '(%(asctime)s) %(name)s [%(levelname)s]: %(message)s'
    LOG_DT_FORMAT = None
    _converter = dt.fromtimestamp

    def __init__(self, fmt=LOG_FORMAT, datefmt=LOG_DT_FORMAT):
        logging.Formatter.__init__(self, fmt=fmt, datefmt=datefmt)

    def formatTime(self, record, fmt=None):
        """A function to format time"""
        ct = self._converter(record.created)
        if fmt:
            s = ct.strftime(fmt)
        else:
            t = ct.strftime("%Y-%m-%d %H:%M:%S")
            s = "%s.%.2d" % (t, record.msecs)
        return s


logging.setLoggerClass(PCLogger)
_loggers = {
    'root': logging.getLogger()
}


def hasLogger(name):
    return name in _loggers.keys()


def getLogger(name=None):
    """Returns a logger that is registered in the logger dictionary

    :param name: Name of the logger to retrieve, if None return root logger
    :type name: None or str
    :return: Object of the logger
    :rtype: pycultivator.core.pcLogger.PCLogger
    """
    if name is None:
        name = 'root'
    if not hasLogger(name):
        raise KeyError("Name - {} - not registered as logger".format(name))
    return _loggers[name]


def _createLogger(name=None, parent=None, level=logging.NOTSET):
    """Connect to a logger, if no parent is given, use root as parent.

    :param name: The name of this logger to register
    :type name: str
    :param parent: The name of the parent to connect to
    :type parent: str
    :return: Reference to logger object
    :rtype: pycultivator.core.pcLogger.PCLogger
    """
    # if no parent set, make child of application
    if parent is None:
        start = sys.argv[0].rfind("/") + 1
        end = sys.argv[0].rfind(".")
        if sys.argv[0].rfind(".") == -1:
            end = len(sys.argv[0])
        parent = sys.argv[0][start:end]
    # add name if set (else set application as root)
    if name is not None and "" not in [name, parent]:
        name = "{parent}.{name}".format(parent=parent, name=name)
    elif name is None or name == "":
        name = parent
    result = logging.getLogger(name)
    result.setLevel(level=level)
    logging.setLoggerClass(PCLogger)
    return result


def createLogger(name=None, parent=None, level=logging.NOTSET):
    """Creates a logger with the given name

    :rtype
    """
    # if no parent set, make child of application
    if parent is None:
        start = sys.argv[0].rfind("/") + 1
        end = sys.argv[0].rfind(".")
        if sys.argv[0].rfind(".") == -1:
            end = len(sys.argv[0])
        parent = sys.argv[0][start:end]
    # add name if set (else set application as root)
    if name is not None and "" not in [name, parent]:
        name = "{parent}.{name}".format(parent=parent, name=name)
    elif name is None or name == "":
        name = parent
    if hasLogger(name):
        result = getLogger(name)
    else:
        result = _createLogger(name=name, parent="", level=level)
        _loggers[name] = result
    return result


def connectHandler(log, handler, level=logging.NOTSET, propagate=False):
    handler.setLevel(level)
    log.addHandler(handler)
    log.propagate = propagate
    return log


def connectStreamHandler(log, handler=None, level=logging.NOTSET, propagate=False):
    if handler is None:
        handler = UVAStreamHandler()
    return connectHandler(log, handler, level=level, propagate=propagate)


def connectFileHandler(log, logfile, handler=None, level=logging.NOTSET, propagate=False):
    if not logfile:
        logfile = "./{}_{}.log".format(dt.now().strftime('%Y%m%d'), log.name)
    if handler is None:
        handler = UVARotatingFileHandler(logfile)
    return connectHandler(log, handler, level=level, propagate=propagate)
