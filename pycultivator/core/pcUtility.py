
import pcLogger
import os

TIME_UNITS = [
    'seconds', 'minutes', 'hours', 'days', 'weeks', 'months', 'years'
]

TIME_UNIT_SIZE = {
    'seconds': 60,
    'minutes': 60,
    'hours': 24,
    'days': 7,
    'weeks': 4,
    'months': 12,
    'years': 1
}

TIME_UNIT_ABBREVIATION = {
    'seconds': 'sec',
    'minutes': 'min',
    'hours': 'hrs',
    'days': 'dys',
    'weeks': 'wks',
    'months': 'mths',
    'years': 'yrs'
}


def seconds_to_str(seconds, short=False, lower=1, upper=None):
    pre = ""
    unit = "seconds"
    for unit in TIME_UNITS:
        unit_size = TIME_UNIT_SIZE[unit]
        if seconds > unit_size:
            if unit_size > 0:
                seconds /= float(unit_size)
        else:
            break
    if short:
        unit = TIME_UNIT_ABBREVIATION[unit]
    if lower is not None and seconds < lower:
        seconds = lower
        pre = "< "
    if upper is not None and seconds > upper:
        seconds = upper
        pre = "> "
    return "{}{:.2f} {}".format(pre, seconds, unit)


def find_files(location, name=None, extension=None):
    """Find all files at any of the given location, matching one of the names and extensions.

    If the location is a list or tuple, all locations will be tried.
    If the name is a list or tuple all names will be tried.
    If the name includes the extension; the extension variable is set to the extension included in the name
    If the extension is a list or tuple all extension will be tried.

    The result yields all files matching any combination of the given criteria.

    :param location: A list of directories to look in.
    :type location: str or tuple[str] or list[str]
    :param name: A list of file names to look for.
    :type name: str or tuple[str] or list[str]
    :param extension: Extension of the file names (if not present).
    :type extension: str or tuple[str] or list[str]
    :rtype: list[str]
    """
    results = []
    # process location argument
    if isinstance(location, (tuple, list)):
        for l in location:
            results.extend(find_files(l, name, extension))
        return results
    # process name argument
    if name is None:
        name = "*"
    if isinstance(name, (tuple, list)):
        if len(name) == 0:
            name = [None]
        for n in name:
            results.extend(find_files(location, n, extension))
        return results
    # process extension argument
    if extension is None:
        extension = "*"
    # check if the name already includes the extension
    name_ext = os.path.splitext(name)[1]
    if name_ext != "":
        extension = ""
    if isinstance(extension, (tuple, list)):
        if len(extension) == 0:
            extension = [None]
        for e in extension:
            results.extend(find_files(location, name, e))
        return results
    if extension not in ("", "*") and not extension.startswith("."):
        extension = ".{}".format(extension)
    if extension == "*" and name == "*":
        extension = ""
    # construct file name pattern
    fp = "{}{}".format(name, extension)
    # create path pattern for matching
    pattern = os.path.join(location, fp)
    # search for matching files
    import glob
    files = glob.glob(pattern)
    # filter so we remove symlinks and directories
    files = filter(lambda f: os.path.isfile(f), files)
    # record
    results.extend(files)
    return results


def import_object_from_str(path, package=None):
    """Import an object from a string path

    :param path: Path to desired object
    :type path: str
    :param package: Path to the package in case the path is relative
    :type package: str
    :raises ImportError: If path could not be imported
    :return: Object that should be imported or None if failed
    :rtype: object | type
    """
    result = None
    module_path = path
    object_name = None
    # parse name
    if isinstance(path, (unicode, str)):
        object_split = path.rfind(".")
        # if path can be decomposed into multiple parts, we remove the object name
        if object_split > 0:
            module_path = path[:object_split]
            object_name = path[(object_split+1):]
    if None not in (module_path, object_name):
        import importlib
        try:
            # loads module
            m = importlib.import_module(module_path, package=package)
            if object_name is None:
                result = m
            elif hasattr(m, object_name):
                result = getattr(m, object_name)
        except AttributeError:
            raise ImportError
    if result is None:
        raise ImportError
    return result


def collect_entry_points(group):
    results = {}
    from pkg_resources import iter_entry_points
    entry_points = iter_entry_points(group=group, name=None)
    import types
    for entry_point in entry_points:
        try:
            items = []
            e = entry_point.load()
            if isinstance(e, types.FunctionType):
                e = e()
            if isinstance(e, (set, tuple, list)):
                items = list(e)
            else:
                items.append(e)
            if entry_point.name in results.keys():
                results[entry_point.name].extend(items)
            else:
                results[entry_point.name] = items
        except ImportError as ie:
            pcLogger.getLogger().warning("Unable to load {}".format(entry_point), exc_info=1)
    return results
