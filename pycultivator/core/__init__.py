"""The foundation module provides core modules and classes"""

from .objects import *
from .pcException import PCException
from .pcLogger import PCLogger
from .pcSettings import PCSettings
from .pcParser import Parser

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    # all standard objects
    "BaseObject", "PCObject", "ConfigurableObject", "HierarchicalObject",
    # all exceptions
    "PCException",
    # the log object
    "PCLogger",
    # settings object,
    "PCSettings",
    # general utilities
    "Parser"
]

