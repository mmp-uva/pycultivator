
from pycultivator.contrib.script import Script
from pycultivator.config.xmlConfig import XMLConfig

import os


class ValidateXMLConfiguration(
    Script
):

    def __init__(self, cwd=None, **kwargs):
        super(ValidateXMLConfiguration, self).__init__(cwd=cwd, **kwargs)
        self._config_path = None

    def getConfigPath(self):
        return self._config_path

    def hasConfigPath(self):
        return self.getConfigPath() is not None

    def setConfigPath(self, v):
        self._config_path = v

    def execute(self):
        if self.getLog().getEffectiveLevel() > 20:
            self.writeln("For detailed validation messages increase log level with the '-vvvv' parameter")
        if self.hasConfigPath():
            fp = self.getConfigPath()
            if os.path.exists(fp):
                self.writeln("Will validate the configuration file at: {}".format(fp))
                result = self.validate(fp)
                if result:
                    self.writeln("The configuration file IS Valid!")
            else:
                self.writeln_error("Please provide a valid path. Unable to open {}".format(fp))
        else:
            self.writeln_error("Please provide a path to the configuration file")

    def validate(self, path, schema=None, **kwargs):
        result = False
        class_name = XMLConfig.extractClassName(path)
        self.getLog().info("XML defines class of configuration source handler: {}".format(class_name))
        config = XMLConfig.extractClass(path)
        if config is None:
            self.write_error("Unable to load the configuration source handler")
        else:
            if schema is None:
                schema = config.loadSchema(path)
                if schema is not None:
                    self.getLog().info("Configuration Source Handler picked schema at: {}".format(schema.file_path))
            # first basic checks (path existence, etc.)
            from pycultivator.core import pcXML
            # create xml from source
            self.getLog().info("Try to load XML Document Object")
            xml = pcXML.XML.createFromSource(path, schema=schema)
            if xml is None:
                self.writeln_error("Unable to create XML object (invalid XML?)")
                return False
            # define which schema to use
            schema = xml.schema
            if schema is not None:
                self.getLog().info("Will use XML Schema at {}".format(xml.schema.file_path))
            # check schema
            self.getLog().info("Validate XML Document against XML Schema")
            if not xml.isValid(schema=schema):
                self.writeln_error("Configuration is not accepted by schema")
                return False
            # third check: compatibility with library
            if schema is not None and not config.isCompatible(schema.getVersion()):
                self.writeln_error("Schema is not compatible with configuration source handler!")
                self.writeln_error("Schema uses version {}".format(schema.getVersion()))
                return False
            result = True
        return result

    def parse_arguments(self, arguments):
        super(ValidateXMLConfiguration, self).parse_arguments(arguments)
        fp = arguments.get("config")
        if fp is not None:
            self.setConfigPath(fp)

    def define_arguments(self, parser=None):
        parser = super(ValidateXMLConfiguration, self).define_arguments(parser)
        parser.description = "Validate XML Configuration File"
        parser.add_argument(
            "config",
        )
        return parser


def main():
    # bootstrap and run
    ValidateXMLConfiguration.bootstrap(cwd=__file__)


if __name__ == "__main__":
    main()