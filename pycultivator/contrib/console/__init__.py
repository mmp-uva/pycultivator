
from .commands import Command
from .arguments import *
from .console import BaseConsole, Console
from .exceptions import ConsoleException, CommandException, ArgumentException

__all__ = [
    "BaseConsole", "Console",
    "Command",
    "CommandArgument", "BooleanArgument", "IntegerArgument", "FloatArgument", "StringArgument", "PathArgument",
    "ConsoleException", "CommandException", "ArgumentException",

    "bootstrap"
]


def bootstrap(klass=None, *args, **kwargs):

    if klass is None:
        klass = Console
    if not issubclass(klass, BaseConsole):
        raise TypeError("Invalid console type, {} does not inherit from BaseConsole".format(klass))

    import os
    history = os.path.join(os.path.expanduser("~"), ".pycultivator_console")

    # configure argument parser
    import argparse
    p = argparse.ArgumentParser(add_help=False)
    p.add_argument(
        'console_args', nargs=argparse.REMAINDER,
        help="Any arguments given are executed as a single command."
    )
    p.add_argument(
        '-h', '--help', action="store_true",
        help="Show this help message and exit"
    )
    p.add_argument(
        '-v', '--version', action="store_true",
        help="Print version information"
    )
    p.add_argument(
        '-r', '--run-list', dest="run_list",
        help="Execute the commands found in the file at the given path."
    )
    p.add_argument(
        "--history", default=history,
        help="Location of console history file"
    )

    # parse arguments
    a = vars(p.parse_args())

    # set-up history
    if a["history"] is not None:
        history = a["history"]
    kwargs['history'] = history

    # if a command was given
    input_line = None
    if isinstance(a['console_args'], (list, set, tuple)) and len(a['console_args']) > 0:
        input_line = " ".join(a['console_args'])

    if a['help']:
        input_line = "help"

    if a['version']:
        input_line = "version"

    input_file = None
    if a['run_list']:
        input_file = a['run_list']

    # configure console
    c = klass(*args, **kwargs)
    """:type: pycultivator.contrib.console.Console"""

    if input_line is not None:
        c.execute_line(input_line)
    elif input_file is not None:
        c.execute_file(input_file)
    else:
        c.start()


def main():
    bootstrap()


if __name__ == "__main__":
    main()
