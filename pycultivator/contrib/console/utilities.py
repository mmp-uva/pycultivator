
import os
import glob


class PathCompleter(object):
    """Helps completing paths"""

    def __init__(self, context):
        self.context = context
        self.matches = None

    @property
    def cwd(self):
        return self.context.getRootDirectory()

    def match_path(self, text):
        search_dir = text
        if not os.path.isdir(search_dir):
            search_dir = os.path.dirname(text) + "/"
        search_text = text.replace(search_dir, "")
        try:
            for entry in os.listdir(search_dir):
                if not entry.startswith("."):
                    if search_text == "" or entry.startswith(search_text):
                        yield os.path.join(search_dir, entry)
        except OSError:
            raise StopIteration

    def complete(self, text, state):
        if self.matches is None:
            search_text = text # os.path.expanduser(text)
            if not os.path.exists(os.path.dirname(search_text)):
                search_text = os.path.expanduser(os.path.join(self.cwd, text))
            self.matches = []
            for match in self.match_path(search_text):
                match = match.replace(search_text, text)
                self.matches.append(match)
            # print self.matches
        try:
            return self.matches.pop(0)
        except IndexError:
            self.matches = None
            return None
