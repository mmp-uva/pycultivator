
from .base import BaseConsole
from pycultivator.contrib.console.exceptions import *
from pycultivator.context import Context

import readline
import sys
import logging


class ConsoleLogFormatter(logging.Formatter):

    LOG_FORMAT = '[%(levelname)s]: %(message)s'

    def __init__(self, fmt=LOG_FORMAT, datefmt=None):
        super(ConsoleLogFormatter, self).__init__(fmt=fmt, datefmt=datefmt)


# noinspection PyCompatibility
class Console(BaseConsole):
    """A console application: executes commands given from the terminal"""

    required_context = Context

    def __init__(self, commands=None, history=None):
        super(Console, self).__init__(commands=commands, history=history)

    # console method

    def prepare(self):
        super(Console, self).prepare()
        self.prepare_managers()
        if self._history is not None:
            self.prepare_history()
        # attach completer
        self.prepare_completer()
        # prevent duplicates by preventing context from propagating messages to root
        self.context.log.propagate = False
        # welcome user
        self.prepare_welcome()

    def prepare_managers(self):
        # attach config manager
        from pycultivator.manager import ConfigManager
        self.context.load_manager("config", ConfigManager)
        from pycultivator.manager import DeviceManager
        self.context.load_manager("device", DeviceManager)
        # TODO: load data manager

    def prepare_log(self):
        super(Console, self).prepare_log()
        # attach context log to console
        from logging import StreamHandler
        handler = StreamHandler(stream=sys.stdout)
        handler.setFormatter(ConsoleLogFormatter())
        handler.setLevel(0)
        self.context.log.addHandler(handler)

    def prepare_history(self):
        try:
            readline.read_history_file(self._history)
            readline.set_history_length(1000)
            self.info("Load history from: {}".format(self._history))
        except IOError:
            pass

    def prepare_completer(self):
        readline.parse_and_bind("tab: complete")
        readline.parse_and_bind("]: complete")
        delims = readline.get_completer_delims()
        delims = delims.replace("/", "")
        delims = delims.replace("\\", "")
        readline.set_completer_delims(delims)
        from pycultivator.contrib.console.utilities import PathCompleter
        self._completer = PathCompleter(self.context)
        readline.set_completer(self._completer.complete)

    def prepare_welcome(self):
        # print welcome
        self.info("Welcome to the pyCultivator Console.")
        self.info("Enter a command or type 'help' for available commands.")
        self.info("Type 'exit' to exit or press Ctrl-D (EOF).")

    def process(self):
        try:
            # wait for input
            line = self.read_input()
            if line != "":
                # process and execute input
                self.execute_line(line)
        except KeyboardInterrupt:
            self.print_message("")
        except EOFError:
            self.print_message("")
            self._exit = True

    def finalize(self):
        super(Console, self).finalize()
        self.info("Bye!")

    def read_input(self, prompt="> "):
        from six.moves import input
        return input(prompt).strip()

    def execute_command(self, command, *args, **kwargs):
        command = super(Console, self).execute_command(command, *args, **kwargs)
        result = command.is_successful()
        if result and command.has_results():
            output = command.pretty_result
            self.print_output(output)
        return command

    def execute_line(self, line):
        result, command = False, None
        try:
            command = self.process_line(line)
            if command is not None:
                command = self.execute_command(command)
        except UnknownCommandException as uce:
            self.error(uce)
            self.error("Type 'help' for information on commands available.")
        except ArgumentException as ae:
            self.error("Invalid argument(s):")
            self.error(ae)
        return result, command

    def execute_lines(self, lines):
        result, idx = True, 0
        for idx, line in enumerate(lines):
            line = line.strip()
            if line != "" and not line.startswith("#"):
                result = self.execute_line(line)
                if not result:
                    break
        if not result:
            self.error(
                "{} commands were not executed. ".format(
                    len(lines) - (idx + 1)
                )
            )
        return result

    def execute_file(self, path):
        import os
        path = os.path.abspath(os.path.expanduser(path))
        if not os.path.exists(path):
            self.error("Cannot open {}".format(path))
            return False
        # read files
        with open(path, 'r') as src:
            lines = src.readlines()
        self.info("Execute {} lines read from {}".format(len(lines), path))
        result = self.execute_lines(lines)
        self.info("Done")
        return result

    def print_output(self, output):
        message = self.prettify_output(output)
        # turn into message
        self.print_message(message)
