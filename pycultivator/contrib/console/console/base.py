from pycultivator.core import BaseObject
from pycultivator.contrib.console.exceptions import *
from pycultivator.context import BaseContext

import sys
import readline
import re
import types

__all__ = [
    "BaseConsole"
]


class BaseConsole(BaseObject):

    required_context = BaseContext

    @classmethod
    def assert_context(cls, context, required_context=None):
        if required_context is None:
            required_context = cls.required_context
        if not issubclass(required_context, BaseContext):
            raise TypeError("Required context must inherit from BaseContext")
        if not issubclass(context, required_context):
            raise TypeError("Provided context class is not supported by this console")
        return True

    def load_context(self, name=0, context=None):
        if context is None:
            context = self.required_context
        self.assert_context(context)
        return context(name, parent=self)

    @property
    def services(self):
        return self.context.services

    def __init__(self, commands=None, history=None):
        super(BaseConsole, self).__init__()
        self._context = self.load_context()
        self._commands = self.load_commands(commands)
        self._completer = None
        self._history = history
        self._is_running = False
        self._exit = False

    @property
    def commands(self):
        """Dictionary of commands known by this console

        :return:
        :rtype: dict[str, pycultivator_lab.console.commands.Command]
        """
        return self._commands

    @property
    def context(self):
        """The context commands should use

        :rtype: pycultivator.context.Context
        """
        return self._context

    # console methods

    def is_running(self):
        return self._is_running is True

    def set_exit(self):
        self._exit = True

    def should_exit(self):
        return self._exit is True

    def start(self, **kwargs):
        """Start the console"""
        self.prepare()
        try:
            self.execute()
        finally:
            self.finalize()

    def prepare(self):
        self.prepare_log()

    def prepare_log(self):
        from pycultivator.core import pcLogger
        # tap log messages
        self.getRootLog().addHandler(
            pcLogger.UVAStreamHandler(level=20)
        )
        self.getRootLog().level = 20

    def execute(self):
        while not self.should_exit():
            self.process()

    def process(self):
        raise NotImplementedError

    def process_line(self, line):
        """Processes a line into an command"""
        # command = None
        # identify best match
        candidate = self.match_command(line)
        if candidate is not None:
            # remove command from input
            line = line.replace(candidate.get_full_command(), "", 1)
            # split line into arguments
            args, kwargs = self.decompose(line)
            # create command with arguments, might raise exceptions!
            command = self.create_command(candidate, args=args, kwargs=kwargs)
        else:
            raise UnknownCommandException(line)
        return command

    def match_commands(self, line):
        """Find all potential matches to the command

        :rtype: list[callable -> pycultivator.contrib.console.commands.Command]
        """
        for cmd in sorted(self.commands.values(), key=lambda c: c.get_full_command(), reverse=True):
            if re.search("^" + re.escape(cmd.get_full_command()) + "(\s|\Z)", line):
                yield cmd

    def match_command(self, line):
        """Match line to best fitting command (i.e longest fit)

        :rtype: callable -> pycultivator.contrib.console.commands.Command | None
        """
        result = None
        for result in self.match_commands(line):
            break
        return result

    @staticmethod
    def split(s, split=" "):
        def replacer(m):
            return m.group(0).replace(split, "\x00")

        parts = re.sub(r"([\"']).+?\1", replacer, s).split()
        parts = [p.replace("\x00", split).strip() for p in parts]
        return parts

    @classmethod
    def decompose(cls, line):
        """Breaks the input line into positional arguments and keyword arguments

        :type line: str
        :rtype: tuple[list[str], dict[str,str]]
        """
        # turn double quotes into single quotes
        # line = line.replace('"', "'")
        # decompose in pieces and remove trailing whitespace
        parts = cls.split(line)
        # remove empty strings
        parts = filter(lambda x: x != "", parts)
        # divide arguments over unnamed and named arguments
        args, kwargs = [], {}
        for part in parts:
            # if not a quoted string and there is an equal sign it is named
            if re.match(r"^[^\"^\']+=", part):
                name, value = [p.strip("'\"") for p in part.split("=", 1)]
                kwargs[name] = value
            else:
                args.append(part.strip("'\""))
        return args, kwargs

    def create_command(self, klass, args=(), kwargs=None):
        """Creates a new command instance from the klass and arguments

        :rtype: None | pycultivator.contrib.console.commands.Command
        :raises pycultivator.contrib.console.exceptions.ArgumentException: If provided arguments do not match
        """
        return klass(context=self.context, console=self, args=args, kwargs=kwargs)

    def execute_command(self, command, *args, **kwargs):
        """Runs a single command """
        try:
            command.start(*args, **kwargs)
        except (ExecutionException, CommandException) as ce:
            self.log.error("Unable to execute command {}: {}".format(
                command.get_full_command(), ce
            ))
        return command

    def execute_commands(self, commands):
        for command in commands:
            yield self.execute_command(command)

    def finalize(self):
        self._exit = True
        # clean workspace
        if self._context is not None:
            self.info("Cleaning workspace....")
            self._context.clean()
        # save history
        if self._history is not None:
            self.info("Saved history to: {}".format(self._history))
            readline.set_history_length(1000)
            readline.write_history_file(self._history)

    def load_commands(self, commands=None):
        if commands is None:
            commands = set(self.find_commands())
        # remove commands not supported by this console
        commands = filter(lambda c: isinstance(self.context, c.required_context), commands)
        # collect commands
        names = [c.get_full_command() for c in commands]
        # collect duplicates
        duplicates = [n for n in names if names.count(n) > 1]
        if len(duplicates) > 0:
            from pycultivator.contrib.console.exceptions import DuplicateCommandException
            raise DuplicateCommandException(duplicates)
        # convert into dictionary
        results = {}
        for command in commands:
            results[command.get_full_command()] = command
        return results

    @classmethod
    def find_commands(cls):
        """Search for entry points defining the commands"""
        # collect commands
        from pycultivator.core.pcUtility import collect_entry_points
        collection = collect_entry_points('pycultivator.console.commands')
        # reduce dictionary of lists
        entries = []
        for c in collection.values():
            entries.extend(c)
        # now iterate over entries
        import types
        from pycultivator.contrib.console.commands import Command
        return filter(lambda e: isinstance(e, (type, types.ClassType)) and issubclass(e, Command), entries)

    # info methods

    @classmethod
    def error(cls, msg, start="", end="\n"):
        cls.print_error(msg, start, end)

    @classmethod
    def info(cls, msg, start="", end="\n"):
        start = "{}## ".format(start)
        cls.print_message(msg, start, end)

    @staticmethod
    def print_message(msg, start="", end="\n"):
        sys.stdout.write("{}{}{}".format(start, msg, end))
        sys.stdout.flush()

    @staticmethod
    def print_error(msg, start="", end="\n"):
        sys.stderr.write("{}{}{}".format(start, msg, end))
        sys.stderr.flush()

    @classmethod
    def prettify_output(cls, output, level=0):
        if isinstance(output, str):
            return output
        if isinstance(output, dict):
            indent = "".join([" " for i in range(4 * level)])
            extra_indent = indent + "".join([" " for i in range(4)])
            lines = []
            start, end = "{}"
            for k in output.keys():
                lines.append("\n{}{} : {}".format(
                    extra_indent,
                    cls.prettify_output(k, level=level+1),
                    cls.prettify_output(output[k], level=level+1)
                ))
            if len(lines) > 0:
                end = "\n{}{}".format(indent, end)
            return "{}{}{}".format(start, ",".join(lines), end)
        if isinstance(output, (list, set, tuple, types.GeneratorType)):
            indent = "".join([" " for _ in range(4 * level)])
            extra_indent = indent + "".join([" " for _ in range(4)])
            lines = []
            start, end = "[]"
            for v in output:
                lines.append("\n{}{}".format(
                    extra_indent, cls.prettify_output(v, level=level+1))
                )
            if len(lines) > 0:
                end = "\n{}{}".format(indent, end)
            return "{}{}{}".format(start, ",".join(lines), end)
        if isinstance(output, BaseObject):
            return "<{}>: {}".format(output.__class__.__name__, str(output))
        return str(output)
