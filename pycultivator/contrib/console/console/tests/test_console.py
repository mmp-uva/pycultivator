from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.contrib.console import Console


class TestConsole(UvaSubjectTestCase):

    _abstract = False
    _subject_cls = Console

    def initSubject(self, *args, **kwargs):
        return self._subject_cls(*args, **kwargs)

    def test_split(self):
        self.assertEqual(
            self._subject_cls.split("path a b"), ["path", "a", "b"]
        )
        self.assertEqual(
            self._subject_cls.split("path a=1 b=2"), ["path", "a=1", "b=2"]
        )
        self.assertEqual(
            self._subject_cls.split("path a='1' b=2"), ["path", "a='1'", "b=2"]
        )
        self.assertEqual(
            self._subject_cls.split("path a=\"1\" b=2"), ["path", "a=\"1\"", "b=2"]
        )
        self.assertEqual(
            self._subject_cls.split('path a="1" b=2'), ["path", "a=\"1\"", "b=2"]
        )

    def test_decompose(self):
        self.assertEqual(
            self._subject_cls.decompose("path a b"), (["path", "a", "b"], {})
        )
        self.assertEqual(
            self._subject_cls.decompose("path a=1 b=1"), (["path"], {'a': '1', 'b': '1'})
        )
        self.assertEqual(
            self._subject_cls.decompose("path 'a' b"), (["path", 'a', 'b'], {})
        )
        self.assertEqual(
            self._subject_cls.decompose("path \"a\" b"), (["path", 'a', 'b'], {})
        )
        self.assertEqual(
            self._subject_cls.decompose('path "a" b'), (["path", 'a', 'b'], {})
        )
        self.assertEqual(
            self._subject_cls.decompose("path a='1' b=1"), (["path"], {'a': '1', 'b': '1'})
        )
        self.assertEqual(
            self._subject_cls.decompose("path a=\"1\" b=1"), (["path"], {'a': '1', 'b': '1'})
        )
        self.assertEqual(
            self._subject_cls.decompose('path a="1" b=1'), (["path"], {'a': '1', 'b': '1'})
        )
        self.assertEqual(
            self._subject_cls.decompose('path a="1 b=1"'), (["path"], {'a': '1 b=1'})
        )
        # NOT SUPPORTED YET
        # self.assertEqual(
        #     self._subject_cls.decompose('path "a=1 b"=1'), (["path"], {'a=1 b': '1'})
        # )
        # CURRENT WRONG (!) IMPLEMENTATION
        self.assertEqual(
            self._subject_cls.decompose('path "a=1 b"=1'), (["path", 'a=1 b"=1'], {})
        )

