
from .base import ExecutionException

__all__ = [
    "ConfigException",
    "UnknownConfigException",
]


class ConfigException(ExecutionException):
    """Exception raised relating to problems with a configuration source"""

    def __init__(self, command, config, msg=None):
        self.config = config
        if msg is None:
            msg = "Unexpected error with configuration source {}".format(command)
        super(ConfigException, self).__init__(command, msg=msg)


class UnknownConfigException(ConfigException):
    """Exception raised when given config name does not exist"""

    def __init__(self, command, config, msg=None):
        if msg is None:
            msg = "Unknown configuration source {}".format(config)
        super(UnknownConfigException, self).__init__(command, config, msg=msg)
