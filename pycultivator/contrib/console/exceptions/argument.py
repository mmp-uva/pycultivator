
from .base import ConsoleException

__all__ = [
    "ArgumentException",
    "UnexpectedArgumentsException",
    "UnknownArgumentException",
    "MissingArgumentException",
    "InvalidArgumentException",
    "InvalidArgumentValueException",
    "InvalidPathArgumentException",
    "InvalidConfigArgumentException",
    "InvalidDeviceArgumentException",
]


class ArgumentException(ConsoleException):
    """Exception raised when an argument could not be parsed"""

    def __init__(self, argument, msg=None):
        self.argument = argument
        if msg is None:
            msg = "Unable to parse argument {}".format(argument)
        super(ArgumentException, self).__init__(msg=msg)


class UnexpectedArgumentsException(ArgumentException):
    """Exception raised when one or more arguments were encountered while not expecting them"""

    def __init__(self, arguments, msg=None):
        if not isinstance(arguments, list):
            arguments = [arguments]
        if msg is None:
            msg = "Encountered {} unexpected argument(s)".format(len(arguments))
        super(UnexpectedArgumentsException, self).__init__(arguments, msg=msg)


class UnknownArgumentException(ArgumentException):
    """Exception raised when encountering a named argument not defined for the command"""

    def __init__(self, argument, msg=None):
        if msg is None:
            msg = "No argument defined with the name '{}'".format(argument)
        super(UnknownArgumentException, self).__init__(argument, msg=msg)


class MissingArgumentException(ArgumentException):
    """Exception raised when missing an argument"""

    def __init__(self, arguments, msg=None):
        if not isinstance(arguments, list):
            arguments = [arguments]
        if msg is None:
            msg = "Missing value for argument(s): {}".format(", ".join(arguments))
        super(MissingArgumentException, self).__init__(arguments, msg=msg)


class InvalidArgumentException(ArgumentException):
    """Exception raised when input does not match to a argument"""

    def __init__(self, argument, msg=None):
        if msg is None:
            msg = "Argument '{}' is not a valid".format(argument)
        super(InvalidArgumentException, self).__init__(argument, msg=msg)


class InvalidArgumentValueException(InvalidArgumentException):
    """Exception raised when the input value is not valid"""

    def __init__(self, argument, value, msg=None):
        if msg is None:
            msg = "Invalid value ({}) for argument {}".format(value, argument)
        super(InvalidArgumentValueException, self).__init__(argument, msg=msg)


class InvalidPathArgumentException(InvalidArgumentValueException):
    """Raised when the given path does not exist"""

    def __init__(self, argument, value, msg=None):
        if msg is None:
            msg = "'{}' is NOT a valid path".format(value, argument)
        super(InvalidPathArgumentException, self).__init__(argument, msg=msg)


class InvalidConfigArgumentException(InvalidArgumentValueException):

    def __init__(self, argument, value, msg=None):
        if msg is None:
            msg = "No configuration source registered with the name '{}'".format(value, argument)
        super(InvalidConfigArgumentException, self).__init__(argument, value, msg=msg)


class InvalidDeviceArgumentException(InvalidArgumentValueException):

    def __init__(self, argument, value, msg=None):
        if msg is None:
            msg = "No device registered with the name '{}'".format(value, argument)
        super(InvalidDeviceArgumentException, self).__init__(argument, value, msg=msg)
