
from .base import ExecutionException

__all__ = [
    "DeviceException",
    "UnknownDeviceException",
    "InvalidDeviceException",
    'NotConnectedException'
]


class DeviceException(ExecutionException):
    """Exception raised relating to problems with a device"""

    def __init__(self, command, device, msg=None):
        self.device = device
        if msg is None:
            msg = "Unexpected error with device {}".format(device)
        super(DeviceException, self).__init__(command, msg=msg)


class UnknownDeviceException(DeviceException):
    """Exception raised when given device name does not exist"""

    def __init__(self, command, device, msg=None):
        if msg is None:
            msg = "Unknown device {}".format(device)
        super(UnknownDeviceException, self).__init__(command, device, msg=msg)


class InvalidDeviceException(DeviceException):
    """Exception raised when given device name yields the wrong device type"""

    def __init__(self, command, device, expected, msg=None):
        self.expected = expected
        if msg is None:
            msg = "Device {} ({}) is not a {}".format(
                device, type(device), expected
            )
        super(InvalidDeviceException, self).__init__(command, device, msg=msg)


class NotConnectedException(DeviceException):
    """Exception raised when a commands need the device to be connected """

    def __init__(self, command, device, msg=None):
        if msg is None:
            msg = "Device {} NOT connected, connect first!".format(device)
        super(NotConnectedException, self).__init__(command, device, msg=msg)
