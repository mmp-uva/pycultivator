
from .base import ConsoleException

__all__ = [
    "CommandException",
    "DuplicateCommandException",
    "InvalidCommandException",
]


class CommandException(ConsoleException):
    """Exception raised while parsing a command from input"""

    def __init__(self, command, msg=None):
        self.command = command
        if msg is None:
            msg = "Unable to execute the command {}".format(command)
        super(CommandException, self).__init__(msg=msg)


class DuplicateCommandException(ConsoleException):

    def __init__(self, commands, msg=None):
        if msg is None:
            msg = "Found {} commands with the same name: ".format(len(commands), ", ".join(commands))
        super(DuplicateCommandException, self).__init__(msg=msg)


class InvalidCommandException(CommandException):
    """Raised when input does not match to the command"""

    def __init__(self, command, msg=None):
        if msg is None:
            msg = "Input does not match to {}".format(command)
        super(InvalidCommandException, self).__init__(command, msg=msg)
