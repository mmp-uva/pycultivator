
from .base import ExecutionException

__all__ = [
    "ManagerException",
    "UnknownManagerException"
]


class ManagerException(ExecutionException):
    """Exception raised relating to problems with a device"""

    def __init__(self, command, msg=None):
        if msg is None:
            msg = "Unexpected error while working with a manager"
        super(ManagerException, self).__init__(command, msg=msg)


class UnknownManagerException(ManagerException):
    """Exception raised when trying to access a unknown manager"""

    def __init__(self, command, manager, msg=None):
        if msg is None:
            msg = "No manager available with the name {}".format(manager)
        super(UnknownManagerException, self).__init__(command, msg=msg)


