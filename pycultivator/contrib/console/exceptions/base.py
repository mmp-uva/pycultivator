
from pycultivator.core import PCException

__all__ = [
    "ConsoleException",
    "UnknownCommandException",
    "ExecutionException",
    "ContextException",
    "InvalidContextException"
]


class ConsoleException(PCException):
    """Exception raised in the console"""

    pass


class UnknownCommandException(ConsoleException):
    """Exception raised if the command """

    def __init__(self, cmd, msg=None):
        self.command = cmd
        if msg is None:
            msg = "Unknown command: {}".format(cmd)
        super(UnknownCommandException, self).__init__(msg=msg)


class ExecutionException(ConsoleException):
    """Exception raised while executing a command"""

    def __init__(self, cmd, msg=None):
        self.command = cmd
        if msg is None:
            msg = "Unable to execute {}".format(cmd)
        super(ExecutionException, self).__init__(msg=msg)


class ContextException(ConsoleException):

    def __init__(self, context, msg=None):
        self.context = context
        super(ContextException, self).__init__(msg=msg)


class InvalidContextException(ContextException):

    def __init__(self, context, expected, msg=None):
        if msg is None:
            msg = "Invalid context of type '{}', expected '{}'".format(type(context), expected)
        super(InvalidContextException, self).__init__(context, msg=msg)
