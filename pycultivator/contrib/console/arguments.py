"""Arguments for commands"""

from pycultivator.core import BaseObject
from pycultivator.contrib.console import exceptions
import re

__all__ = [
    "CommandArgument",
    "BooleanArgument", "FloatArgument", "IntegerArgument",
    "StringArgument", "PathArgument", "DeviceArgument",
    "IntegerRangeArgument", "IndexRangeArgument",
]


class CommandArgument(BaseObject):

    def extract_arg(self, args):
        """Take the argument for this command

        :param args:
        :return:
        """
        return args.pop(0)

    def extract_kwarg(self, kwargs):
        """Take the kwargs matching the name of this command


        :type kwargs: dict[str, object]
        :rtype: object | dict[str, object]
        """
        if self.name in kwargs.keys():
            return kwargs.pop(self.name)
        return None

    def parse_arguments(self, args, kwargs, context):
        value = None
        if len(args) > 0:
            value = self.extract_arg(args)
        if len(kwargs.keys()) > 0:
            kwarg_value = self.extract_kwarg(kwargs)
            if kwarg_value is not None:
                value = kwarg_value
        if value is not None:
            if self.test(value, context):
                self.process(value, context)
        return args, kwargs

    def __init__(self, name, default=None, is_required=True):
        super(CommandArgument, self).__init__()
        self._name = name
        self._required = is_required
        self._type = None
        self._default = default
        self._value = None

    @property
    def name(self):
        return self._name

    @property
    def type(self):
        return self._type

    @property
    def is_required(self):
        return self._required

    @property
    def default(self):
        return self._default

    @property
    def value(self):
        result = self.default
        if self._value is not None:
            result = self._value
        return result

    def is_empty(self):
        return self.value is None

    def is_set(self):
        return not self.is_empty()

    def reset(self):
        """Resets the argument to undefined"""
        self._value = None

    def test(self, value, context):
        """Test if the argument(s) are valid"""
        return True     # overload

    def process(self, value, context):
        """Process the value and store it

        :param context:
        :raises pycultivator.contrib.console.exceptions.ArgumentException: When value is not valid
        """
        self._value = value
        return self._value

    def describe(self):
        value = "" if self.default is None else "={}".format(self.default)
        if not self.is_empty():
            value = "" if self.value is None else "={}".format(self.value)
        type_ = "any" if self.type is None else self.type.__name__
        return "<{}> {}{}".format(type_, self.name, value)

    def __str__(self):
        return self.describe()

    def __eq__(self, other):
        result = False
        if isinstance(other, type(self)):
            result = other.name == self.name and other.type == self.type
        return result

    def __ne__(self, other):
        return not self.__eq__(other)

    def copy(self, memo=None):
        result = type(self)(
            self.name, is_required=self.is_required, default=self.default
        )
        result._type = self.type
        result.reset()
        return result


class BooleanArgument(CommandArgument):

    def __init__(self, name, is_required=True, default=False):
        super(BooleanArgument, self).__init__(name=name, is_required=is_required, default=default)
        self._type = bool

    def test(self, value, context):
        return value.lower() in ["true", "false", "1", "0"]

    def process(self, value, context):
        if value.lower() in ["true", "1"]:
            result = True
        elif value.lower() in ["false", "0"]:
            result = False
        else:
            raise exceptions.InvalidArgumentValueException(self, value=value)
        return super(BooleanArgument, self).process(result, context)

    def copy(self, memo=None):
        result = type(self)(
            self.name, is_required=self.is_required, default=self.default
        )
        result.reset()
        return result


class IntegerArgument(CommandArgument):

    def __init__(self, name, is_required=True, default=None, minimum=None, maximum=None):
        super(IntegerArgument, self).__init__(name=name, is_required=is_required, default=default)
        self._type = int
        self.minimum = minimum
        self.maximum = maximum

    def test(self, value, context):
        try:
            result = self.process(value, None)
        except ValueError:
            raise exceptions.InvalidArgumentValueException(self, value)
        # constrain
        if self.minimum is not None and result < self.minimum:
            raise exceptions.InvalidArgumentValueException(
                self, value, msg="{} is below minimum {} for argument {}".format(result, self.minimum, self)
            )
        if self.maximum is not None and result > self.maximum:
            raise exceptions.InvalidArgumentValueException(
                self, value, msg="{} is above maximum {} for argument {}".format(result, self.minimum, self)
            )
        return True

    def process(self, value, context):
        result = self._type(value)
        return super(IntegerArgument, self).process(result, context)

    def copy(self, memo=None):
        result = type(self)(
            self.name, is_required=self.is_required, default=self.default,
            minimum=self.minimum, maximum=self.maximum
        )
        result.reset()
        return result


class FloatArgument(IntegerArgument):

    def __init__(self, name, is_required=True, default=None, minimum=None, maximum=None):
        super(FloatArgument, self).__init__(
            name=name, is_required=is_required, default=default, minimum=minimum, maximum=maximum
        )
        self._type = float


class StringArgument(CommandArgument):

    def __init__(self, name, is_required=True, default=None, choices=None):
        super(StringArgument, self).__init__(name=name, is_required=is_required, default=default)
        self._type = str
        if choices is None:
            choices = []
        if isinstance(choices, (tuple, set)):
            choices = list(choices)
        if not isinstance(choices, list):
            choices = [choices]
        self._choices = []
        for choice in choices:
            self._choices.append(str(choice))

    @property
    def choices(self):
        return self._choices

    def test(self, value, context):
        if not isinstance(value, str) or value == "":
            raise exceptions.InvalidArgumentValueException(self, value)
        if len(self.choices) > 0:
            if value not in self.choices:
                raise exceptions.InvalidArgumentValueException(self, value)
        return True

    def process(self, value, context):
        """

        :param context:
        :param value:
        :type value: str
        :return:
        :rtype: str
        """
        result = value.strip()
        return super(StringArgument, self).process(result, context)

    def copy(self, memo=None):
        result = type(self)(
            self.name, is_required=self.is_required, default=self.default
        )
        result.reset()
        return result

    def describe(self):
        description = super(StringArgument, self).describe()
        if len(self.choices) > 0:
            description = "%s {%s}" % (description, ", ".join(self.choices))
        return description


class PathArgument(CommandArgument):

    def __init__(self, name, is_required=True, default=None, require_exists=True):
        super(PathArgument, self).__init__(name=name, is_required=is_required, default=default)
        self._type = str
        self._require_exists = require_exists

    def parse_path(self, value):
        import os
        return os.path.abspath(os.path.expanduser(value))

    def test(self, value, context):
        if not isinstance(value, str) or value == "":
            raise exceptions.InvalidArgumentValueException(self, value)
        import os
        path = self.parse_path(value)
        if not os.path.exists(os.path.dirname(path)):
            from .exceptions.argument import InvalidPathArgumentException
            raise InvalidPathArgumentException(self, path)
        if self._require_exists and not os.path.exists(path):
            from .exceptions.argument import InvalidPathArgumentException
            raise InvalidPathArgumentException(self, path)
        return True

    def process(self, value, context):
        result = self.parse_path(value)
        return super(PathArgument, self).process(result, context)

    def copy(self, memo=None):
        result = type(self)(
            self.name, is_required=self.is_required,
            default=self.default, require_exists=self._require_exists
        )
        result.reset()
        return result


class IntegerRangeArgument(CommandArgument):
    """Accepts a range of one ore more integers"""

    range_pattern = re.compile(r"^(\d+)(?:-(\d+))?$")

    def __init__(self, name, is_required=True, default=()):
        super(IntegerRangeArgument, self).__init__(name=name, is_required=is_required, default=default)

    def test(self, value, context):
        if not isinstance(value, str) or value == "":
            raise exceptions.InvalidArgumentValueException(self, value)
        elif self.range_pattern.match(value) is not None:
            pass
        else:
            raise exceptions.InvalidArgumentValueException(self, value)
        return True


class IndexRangeArgument(IntegerRangeArgument):

    def __init__(self, name, is_required=True, default=(), takes_all=True):
        super(IntegerRangeArgument, self).__init__(name=name, is_required=is_required, default=default)
        self._takes_all = takes_all is True

    @property
    def takes_all(self):
        return self._takes_all

    @takes_all.setter
    def takes_all(self, state):
        self._takes_all = state is True

    def test(self, value, context):
        if not isinstance(value, str) or value == "":
            raise exceptions.InvalidArgumentValueException(self, value)
        elif self.range_pattern.match(value) is not None:
            pass
        elif value == "all":
            pass
        else:
            raise exceptions.InvalidArgumentValueException(self, value)
        return True

    def select(self, items, argument=None):
        """Select the items corresponding to the range specified"""
        if argument is None and self.is_empty():
            argument = self.value
        if argument is None:
            argument = []
        results = []
        if self.takes_all and argument.lower() == "all":
            results = items
        match = self.range_pattern.match(argument)
        if match is not None:
            start, end = match.groups()
            indexes = range(start, end)
            for idx in indexes:
                if 0 <= idx < len(items):
                    results.append([idx])
        return results

    def copy(self, memo=None):
        result = type(self)(self.name, is_required=self.is_required, default=self.default, takes_all=self.takes_all)
        result.reset()
        return result

# class FloatRangeArgument(IntegerRangeArgument):
#
#     range_pattern = re.compile(r"^(\d+(?:\.\d+)?)(?:-(\d+(?:\.\d)?))?$")


class ArgsArgument(CommandArgument):
    """Takes all arguments and stores them in a list"""

    def __init__(self, name, nargs=None, is_required=False, default=()):
        super(ArgsArgument, self).__init__(name, is_required=is_required, default=default)
        if nargs == 0:
            raise exceptions.ArgumentException("Setting nargs to 0 leads to a dysfunctional argument!")
        self._nargs = nargs

    def is_empty(self):
        nargs = self._nargs
        if nargs is None:
            nargs = 0
        return len(self.value) < nargs

    def extract_arg(self, args):
        results = []
        nargs = self._nargs
        if nargs is None:
            nargs = len(args)
        while len(results) < nargs and len(args) > 0:
            args.pop(0)
        return results

    def test(self, value, context):
        result = super(ArgsArgument, self).test(value, context)
        if not isinstance(value, list):
            raise exceptions.InvalidArgumentValueException(self, value)
        return result

    def copy(self, memo=None):
        result = type(self)(self.name, nargs=self._nargs, is_required=self.is_required, default=self.default)
        result.reset()
        return result


class KwargsArgument(CommandArgument):
    """Takes all keyword arguments and stores them in a dictionary"""

    def __init__(self, name, is_required=False, default=None):
        if default is None:
            default = {}
        super(KwargsArgument, self).__init__(name, is_required=is_required, default=default)

    def is_empty(self):
        return len(self.value.keys()) == 0

    def extract_kwarg(self, kwargs):
        result = kwargs.copy()
        kwargs.clear()
        return result

    def test(self, value, context):
        result = super(KwargsArgument, self).test(value, context)
        if not isinstance(value, dict):
            raise exceptions.InvalidArgumentValueException(self, value)
        return result


class ImportArgument(StringArgument):
    """Helper to automatically validate and load import strings"""

    def __init__(self, name, is_required=True, default=None):
        super(ImportArgument, self).__init__(name, is_required=is_required, default=default)
        self._import_string = None

    @property
    def import_string(self):
        return self._import_string

    def test(self, value, context):
        result = super(ImportArgument, self).test(value, context=context)
        if result:
            # try to import
            from pycultivator.core.pcUtility import import_object_from_str
            try:
                result = import_object_from_str(value) is not None
            except ImportError as ie:
                raise exceptions.InvalidArgumentValueException(self, value, "Unable to import {}: {}".format(value, ie))
        return result
    
    def process(self, value, context):
        super(ImportArgument, self).process(value, context=context)
        from pycultivator.core.pcUtility import import_object_from_str
        # we can safely assume test has been called before running
        self._import_string = self._value
        self._value = import_object_from_str(self._value)
        return self._value


class ConfigArgument(StringArgument):
    """Helper to automatically validate and load configuration sources by name """

    def __init__(self, name, is_required=True, default=None):
        super(ConfigArgument, self).__init__(name, is_required=is_required, default=default)
        self._config_name = None

    @property
    def config_name(self):
        return self._config_name

    def test(self, value, context):
        result = super(ConfigArgument, self).test(value, context=context)
        value = value.strip()
        if result:
            # test if correct context
            from pycultivator.context import ManagerContext
            if not isinstance(context, ManagerContext):
                raise exceptions.InvalidContextException(context, ManagerContext)
            result = self.get_config(value, context) is not None
        return result

    def process(self, value, context):
        super(ConfigArgument, self).process(value, context)
        self._config_name = self._value
        self._value = self.get_config(self.config_name, context)
        return self._value

    def get_manager(self, name, context):
        # try to load manager
        if not context.has_manager(name):
            raise exceptions.UnknownManagerException(self, name)
        return context.get_manager(name)

    def get_config(self, name, context):
        manager = self.get_manager("config", context)
        if not manager.has(name):
            from .exceptions.config import UnknownConfigException
            raise UnknownConfigException(self, name)
        return manager.get(name)


class DeviceArgument(StringArgument):

    def __init__(self, name, is_required=True, default=None):
        super(DeviceArgument, self).__init__(name, is_required=is_required, default=default)
        self._device_name = None

    @property
    def device_name(self):
        return self._device_name

    def test(self, value, context):
        result = super(DeviceArgument, self).test(value, context)
        value = value.strip()
        if result:
            from pycultivator.context import ManagerContext
            if not isinstance(context, ManagerContext):
                raise exceptions.InvalidContextException(context, ManagerContext)
            result = self.get_device(value, context) is not None
        return result

    def process(self, value, context):
        super(DeviceArgument, self).process(value, context)
        # we can safely assume test was ran before process
        # load device
        self._device_name = self._value
        self._value = self.get_device(self.device_name, context)
        return self._value

    def get_manager(self, name, context):
        # try to load manager
        if not context.has_manager(name):
            from .exceptions.argument import InvalidConfigArgumentException
            raise InvalidConfigArgumentException(self, name)
        return context.get_manager(name)

    def get_device(self, name, context):
        manager = self.get_manager("device", context)
        if not manager.has(name):
            from .exceptions.argument import InvalidDeviceArgumentException
            raise InvalidDeviceArgumentException(self, name)
        return manager.get(name)
