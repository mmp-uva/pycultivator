

from .base import Command
from pycultivator.contrib.console import arguments, exceptions
from pycultivator.context import Context

__all__ = [
    "ListManagersCommand",

    "load_commands"
]


def load_commands():
    return [
        ListManagersCommand
    ]


class ListManagersCommand(Command):

    name = "list managers"
    required_context = Context
    arguments = [
        # arguments.StringArgument("filter", is_required=False)
    ]
    description = "List which managers are loaded"

    def execute(self):
        return self.context.managers.keys()
