
from .base import Command
from pycultivator.contrib.console import arguments, exceptions
from pycultivator.context import Context

__all__ = [
    "load_commands",

    "BaseDeviceCommand", "DeviceCommand",
    "LoadDeviceCommand", "ConnectDeviceCommand", "DisconnectDeviceCommand",
    "ListVariablesCommand", "MeasureCommand", "ControlCommand",
    "ListDeviceCommand", "DeleteDeviceCommand"
]


def load_commands():
    return [
        ListDeviceCommand,
        LoadDeviceCommand,
        ConnectDeviceCommand,
        ListVariablesCommand,
        MeasureCommand,
        ControlCommand,
        DisconnectDeviceCommand,
    ]


class BaseDeviceCommand(Command):

    namespace = "device"
    required_context = Context

    @property
    def device_manager(self):
        """ Reference to the device manager

        :rtype: pycultivator.manager.device.DeviceManager
        """
        return self.get_manager("device")

    def get_manager(self, name, context=None):
        if context is None:
            context = self.context
        # try to load manager
        if not context.has_manager(name):
            from pycultivator.contrib.console.exceptions.manager import UnknownManagerException
            raise UnknownManagerException(self, name)
        return context.get_manager(name)


class DeviceCommand(BaseDeviceCommand):
    """Base class for all command interacting with a device"""

    arguments = [
        arguments.DeviceArgument("device")
    ]

    @property
    def device_name(self):
        return self.device.device_name

    def has_device(self):
        return self.device.value is not None

    def is_connected(self):
        return self.has_device() and self.device.value.isConnected()

    def has_channels(self):
        from pycultivator.device import ComplexDevice
        return isinstance(self.device.value, ComplexDevice)


class ListDeviceCommand(BaseDeviceCommand):

    name = "list"
    arguments = [
        arguments.StringArgument('filter', is_required=False)
    ]
    description = "List all loaded device objects"

    def execute(self):
        manager = self.device_manager
        """:type: pycultivator.manager.device.DeviceManager"""
        return manager.devices

    def prettify(self, result=None):
        devices = super(ListDeviceCommand, self).prettify(result=result)
        result = ["Currently {} device(s) are loaded".format(len(list(devices)))]
        for name in devices:
            result.append("- {}".format(name))
        return "\n".join(result)


class LoadDeviceCommand(BaseDeviceCommand):

    name = "load"
    arguments = [
        arguments.StringArgument("name"),
        arguments.PathArgument("path", is_required=False),
        arguments.BooleanArgument("use_fake", is_required=False, default=False)
    ]
    description = "Load device in memory, optionally from a configuration source directly."

    def execute(self):
        # load parameters
        name = self.get("name")
        use_fake = self.get("use_fake")
        # if a path was provided we should load the config
        if not self.path.is_empty():
            self.load_config(name, self.get("path"))
        # get config manager
        config = self.get_config(name, self.context)
        result = self.load_device(name, config, use_fake=use_fake)
        self.info("Load {}device {}: {}".format(
            "fake " if use_fake else "", name, result
        ))
        return result

    def get_config(self, name, context=None):
        if context is None:
            context = self.context
        manager = self.get_manager("config", context)
        if not manager.has(name):
            from pycultivator.contrib.console.exceptions.config import UnknownConfigException
            raise UnknownConfigException(self, name)
        return manager.get(name)

    def load_config(self, name, path):
        # execute config command
        from .config import LoadConfigCommand
        load_config = self.console.create_command(LoadConfigCommand, args=(name, path))
        return self.console.execute_command(load_config)

    def load_device(self, name, config, use_fake=False):
        return self.device_manager.load(name, config, use_fake=use_fake)


class ConnectDeviceCommand(DeviceCommand):

    name = "connect"
    arguments = [
        arguments.BooleanArgument("use_fake", is_required=False, default=False)
    ]
    description = "Connect to the device"

    def execute(self):
        result = False
        device = self.get("device")
        use_fake = self.get("use_fake")
        if not self.is_connected():
            device.createConnection(fake=use_fake)
            result = device.connect()
        else:
            self.error("Device already connected, disconnect first")
        self.info("Connect to {}{}: {}".format(
            "fake " if use_fake else "", self.device_name, result)
        )
        return result


class ListVariablesCommand(BaseDeviceCommand):

    namespace = ""
    name = "list variables"
    arguments = [
        arguments.StringArgument("path", is_required=False, default="/*"),
        arguments.StringArgument("type", is_required=False, choices=['measure', 'control'])
    ]
    description = "List what variables can measured and controlled"

    def analyse_item(self, item, retrieve_types=None):
        result = {}
        if retrieve_types is None:
            retrieve_types = ["measure", "control"]
        if not isinstance(retrieve_types, list):
            retrieve_types = [retrieve_types]
        if "measure" in retrieve_types and hasattr(item, "measures"):
            result["measure"] = item.measures()
        if "controls" in retrieve_types and hasattr(item, "controls"):
            result["controls"] = item.controls()
        return result

    def execute(self):
        path = self.get("path")
        retrieve_types = self.get("type")
        items = self.device_manager.path.resolve(path)
        for item in items:
            yield {item: self.analyse_item(item, retrieve_types)}


class MeasureCommand(BaseDeviceCommand):

    namespace = ""
    name = "measure"
    arguments = [
        arguments.StringArgument("variable"),
        arguments.StringArgument("path", is_required=False, default=None),
        arguments.KwargsArgument("kwargs"),
    ]
    description = "Measure a parameter on the device"

    def execute(self):
        path = self.get("path")
        variable = self.get("variable")
        kwargs = self.get("kwargs")
        return self.device_manager.measure(variable=variable, path=path, **kwargs)


class ControlCommand(BaseDeviceCommand):

    namespace = ""
    name = "control"
    arguments = [
        arguments.StringArgument("variable"),
        arguments.StringArgument("value"),
        arguments.StringArgument("path", is_required=False, default=None),
        arguments.KwargsArgument('kwargs')
    ]
    description = "Change a parameter on the device"

    def execute(self):
        path = self.get("path")
        variable = self.get("variable")
        value = self.get("value")
        kwargs = self.get("kwargs")
        return self.device_manager.control(variable=variable, value=value, path=path, **kwargs)


class DisconnectDeviceCommand(DeviceCommand):

    name = "disconnect"
    description = "Disconnect the device"

    def execute(self):
        result = True
        device = self.get("device")
        if self.is_connected:
            result = device.disconnect()
        else:
            self.error("Device already disconnected")
        self.info("Disconnect device {}: {}".format(self.device_name, result))
        return result


class DeleteDeviceCommand(DeviceCommand):

    name = "delete"
    description = "Delete a device"

    def execute(self):
        result = True
        name = self.data_name
        device = self.get("device")
        if self.is_connected():
            result = device.disconnect()
        if result:
            self.context.delete_device(name)
        self.info("Removed device '{}' from memory: {}".format(name, result))
        return result

