from base import Command
from console import *


__all__ = [
    "Command",
    "ExitCommand", "VersionCommand", "HelpCommand", "ConsoleCommand"
]


def load_commands():
    """Return a list of command classes"""
    results = [
        ConsoleCommand,
        HelpCommand,
        VersionCommand,
        ExitCommand
    ]
    # add common commands
    from .common import load_commands
    results.extend(load_commands())
    # add configuration commands
    from .config import load_commands
    results.extend(load_commands())
    # add device commands
    from .device import load_commands
    results.extend(load_commands())
    # add manager commands
    from .manager import load_commands
    results.extend(load_commands())
    return results
