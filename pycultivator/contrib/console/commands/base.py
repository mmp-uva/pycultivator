from pycultivator.core import BaseObject

from pycultivator.contrib.console import arguments, exceptions
from pycultivator.context import BaseContext
from pycultivator.manager import BaseManager

import time

__all__ = ["Command"]


class CommandMeta(type):

    @staticmethod
    def load_from_dict(attrs, name, default=None, remove=True):
        """Load a value from a dictionary by key and remove the key if necessary"""
        value = default
        if name in attrs.keys():
            value = attrs.get(name)
            if remove:
                attrs.pop(name)
        return value

    def __init__(cls, name, bases=None, attrs=None):
        super(CommandMeta, cls).__init__(name, bases, attrs)
        if cls.command in [None, ""]:
            cls.command = cls.name
        inherits = cls.load_from_dict(attrs, 'inherits_arguments', default=True)
        cls.inherits_arguments = inherits
        args = cls.load_from_dict(attrs, 'arguments')
        if args is None:
            args = []
        elif isinstance(args, (list, tuple)):
            args = list(args)
        else:
            args = [args]
        cls.arguments = cls._collectArguments(args, bases, inherits)

    def _collectArguments(cls, args, bases, inherits=True):
        results = []
        bases = list(bases)[:]
        # reverse order: furthest away first
        bases.reverse()
        if inherits:
            for base in bases:
                if isinstance(base, CommandMeta):
                    for argument in base.arguments:
                        if argument in results:
                            results.remove(argument)
                        results.append(argument)
        for argument in args:
            if argument in results:
                results.remove(argument)
            results.append(argument)
        return results

    def inheritsArguments(cls):
        return cls.inherits_arguments


class Command(BaseObject):
    """Basic command class"""

    __metaclass__ = CommandMeta

    # declare class properties
    name = ""
    """Name of the command"""
    namespace = ""
    """Additional prefix of the command for grouping"""
    command = None
    """Optional name of the command to start command from console"""
    arguments = []
    """:type: list[pycultivator.contrib.console.commands.CommandArgument]"""
    """List of arguments of the command"""
    inherits_arguments = True
    """Whether the command inherits its arguments from it's parents"""
    description = ""
    """A small message to be displayed in help"""
    required_context = BaseContext
    """What kind of context is required to run this command"""
    required_manager = BaseManager
    """What kind of manager should be loaded to run this command"""

    def __init__(self, context=None, console=None, args=(), kwargs=None):
        super(Command, self).__init__()
        self._context = None
        self.set_context(context)
        self._console = console
        self._values = {}
        # load arguments into values
        for argument in self.arguments:
            self._values[argument.name] = argument.copy()
        self._t_started = None
        self._t_finished = None
        self._success = None
        self._result = None
        # parse arguments into object
        if not isinstance(kwargs, dict):
            kwargs = {}
        self.parse_arguments(*args, **kwargs)

    @classmethod
    def get_full_command(cls):
        """The command with it's namespace prefix

        :rtype: str
        """
        namespace = cls.namespace.strip() if isinstance(cls.namespace, str) else ""
        command = cls.command.strip() if isinstance(cls.command, str) else ""
        if namespace != "":
            command = "{} {}".format(namespace, command)
        return command

    def get_context(self):
        """The context in which this command is running

        :return:
        :rtype: pycultivator.contrib.console.context.BaseContext
        """
        return self._context

    def set_context(self, context, silent=True):
        if self.assert_context(context, silent=silent):
            self._context = context

    def assert_context(self, context, required_context=None, silent=True):
        if required_context is None:
            required_context = BaseContext
        if not issubclass(required_context, BaseContext):
            raise exceptions.CommandException(
                self, msg="The given required context is not valid, must inherit from BaseContext"
            )
        if context is None or isinstance(context, required_context):
            return True
        if not silent:
            raise exceptions.InvalidContextException(context, required_context)
        return False

    def assert_manager(self, manager, required_manager=None, silent=True):
        if required_manager is None:
            required_manager = BaseManager
        if not issubclass(required_manager, BaseManager):
            raise exceptions.CommandException(
                self, msg="The given required manager is not a manager"
            )
        if manager is None or isinstance(manager, required_manager):
            return True
        if not silent:
            raise exceptions.UnknownManagerException(manager, required_manager)
        return False

    @property
    def console(self):
        return self._console

    @property
    def context(self):
        """Context in which we should run the command

        :rtype: pycultivator.context.BaseContext
        """
        return self.get_context()

    @property
    def values(self):
        """The arguments of this console

        :return:
        :rtype: dict[str, pycultivator.contrib.console.arguments.CommandArgument]
        """
        return self._values

    def is_ready(self):
        # check if all required arguments have been set
        required_arguments = filter(lambda a: a.is_required, self.values.values())
        return all([not a.is_empty() for a in required_arguments])

    def is_running(self):
        return self._t_started is not None

    def is_finished(self):
        return self._t_finished is not None

    @property
    def duration(self):
        result = None
        if self.is_finished():
            result = self._t_finished - self._t_started
        return result

    def is_successful(self):
        return self._success

    def has_results(self):
        return self._result is not None

    @property
    def result(self):
        return self._result

    @property
    def pretty_result(self):
        return self.prettify()

    def prettify(self, result=None):
        if result is None:
            result = self.result
        return result

    @classmethod
    def match(cls, line):
        """Check if the line matches to this command"""
        return line.startswith(cls.get_full_command())

    def parse_arguments(self, *args, **kwargs):
        args = list(args)
        # while there are arguments try to place them
        for arg in self.arguments:
            argument = self.values[arg.name]
            argument.parse_arguments(args, kwargs, context=self.context)
        if len(args) > 0:
            # break # ignore
            # still arguments left, report as unexpected
            raise exceptions.UnexpectedArgumentsException(args)
        # done
        return self

    def reset(self, reset_values=True):
        if reset_values:
            self._values = []
        self._t_started = None
        self._t_finished = None
        self._success = False
        self._result = None

    def start(self, context=None, *args, **kwargs):
        """Process input from the console

        :return: Whether the command executed successfully
        :rtype: None | pycultivator.contrib.console.commands.CommandResult
        """
        self.reset(False)
        self._t_started = time.time()
        try:
            # load context
            if context is not None:
                self.set_context(context)
            # check context
            if not isinstance(self.context, BaseContext):
                raise TypeError("A context must be set before running this command")
            # interpret argument given
            self.parse_arguments(*args, **kwargs)
            # check if all arguments are given
            if self.is_ready():
                result = self.execute()
                self._success = result if isinstance(result, bool) else True
                # wrap result into command output
                if not isinstance(result, (bool, type(None))):
                    self._result = result
            else:
                missing = filter(lambda a: a.is_required and a.is_empty(), self.values.values())
                raise exceptions.MissingArgumentException([str(m) for m in missing])
        finally:
            # signal the command was evaluated
            self._t_finished = time.time()
        # return success
        return self._success

    def execute(self):
        """Run the command"""
        return False  # overload

    def has(self, name):
        """Whether this command has an argument with the given name

        :rtype: bool
        """
        return name in self.values.keys()

    def get(self, name, expand=True):
        """Access an argument by it's name

        :param default: default value to return [default: None]
        :param expand: return the argument value instead of the argument object [default: True]
        :type expand: bool
        :return: The argument object or it's value
        :raises KeyError: If no argument is registered under the given name
        """
        result = self.get_argument(name)
        if expand and isinstance(result, arguments.CommandArgument):
            result = result.value
        return result

    def get_argument(self, name):
        if not self.has(name):
            raise KeyError("No argument with name {}".format(name))
        return self.values[name]

    def get_value(self, name, default=None):
        result = default
        try:
            result = self.get_argument(name)
        except KeyError:
            pass
        if isinstance(result, arguments.CommandArgument):
            result = result.value
        return result

    def __getattr__(self, item):
        """Access arguments via `command.item`

        NOTE: Returns the argument object to access it's value use `command.item.value`

        :param item: Name of the argument
        :return: The argument
        :rtype: pycultivator.contrib.console.arguments.CommandArgument
        :raises KeyError: If no argument is registered under the given name
        """
        return self.get(item, expand=False)

    def __getitem__(self, item):
        """Access argument values via `command['item']`

        NOTE: Returns the value of the argument not the argument object

        :param item: Name of the argument
        :return: The argument value
        :raises KeyError: If no argument is registered under the given name
        """
        return self.get(item, expand=True)

    @classmethod
    def describe(cls):
        result = cls.get_full_command()
        if len(cls.arguments) > 0:
            args = [argument.describe() for argument in cls.arguments]
            result += " [{}]".format(", ".join(args))
        return result

    @classmethod
    def help(cls):
        result = cls.describe()
        if cls.description not in (None, ""):
            result += "\n\t\t{}".format(cls.description)
        return result

    def info(self, msg):
        self.context.log.info(msg)

    def warn(self, msg):
        self.context.log.warn(msg)

    def error(self, msg):
        self.context.log.error(msg)

    def __str__(self):
        return self.describe()

    def __json__(self):
        """Returns a json representation of this command"""
        result = {
            'command': self.dot_path(),
            'args': [],
            'kwargs': {}
        }
        # add arguments as named arguments
        for a in self.arguments:
            result['kwargs'][a] = a.value

        # export to json
        import json
        return json.dumps(result)
