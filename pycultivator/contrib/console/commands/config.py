
from .base import Command
from pycultivator.contrib.console import arguments, exceptions
from pycultivator.context import Context

__all__ = [
    "load_commands",

    "BaseConfigCommand",
    "LoadConfigCommand", "ValidateConfigCommand"
]


def load_commands():
    return [
        LoadConfigCommand,
        ValidateConfigCommand,
        ListConfigCommand
    ]


class BaseConfigCommand(Command):

    namespace = "config"
    required_context = Context


class ConfigCommand(BaseConfigCommand):

    arguments = [
        arguments.ConfigArgument("config")
    ]

    @property
    def config_name(self):
        return self.get("config", expand=False).config_name

    def has_config(self):
        return self.config is not None


class LoadConfigCommand(BaseConfigCommand):

    name = "load"
    arguments = [
        arguments.StringArgument("name"),
        arguments.PathArgument("path"),
    ]
    description = "Load a configuration source into memory"

    def execute(self):
        result = False
        name = self.get('name')
        fp = self.get('path')
        try:
            manager = self.context.get_manager("config")
            """:type: pycultivator.manager.config.ConfigManager"""
        except KeyError:
            raise exceptions.CommandException("No configuration manager loaded")
        else:
            config = manager.load(name, fp)
            result = config is not None
        finally:
            self.info("Load configuration {}: {}".format(name, result))
        return result


class SaveConfigCommand(ConfigCommand):

    name = "save"

    arguments = [
        arguments.PathArgument("path", require_exists=False)
    ]
    description = "Save the configuration to the specified file"

    def execute(self):
        fp = self.get('path')
        result = self.save_config(self.config, fp)
        self.info("Save configuration {} to {}: {}".format(self.config_name, fp, result))
        return result

    def save_config(self, config, fp):
        """Save the configuration source

        :param config: The configuration source which should be saved
        :type config: pycultivator.config.baseConfig.Config
        :param fp: path to where configuration source should be saved
        :type fp: str
        :return: Whether the file was saved succesfully
        """
        from pycultivator.config import ConfigException
        result = False
        try:
            result = config.save(fp)
        except ConfigException as ce:
            self.error("Unable to save configuration source: {}".format(ce))
        return result


class ValidateConfigCommand(BaseConfigCommand):

    name = "validate"
    arguments = [
        arguments.PathArgument('path')
    ]
    description = "Check a configuration source on validity"

    def execute(self):
        result = False
        fp = self.get("path")
        if fp is not None:
            self._result = self.validate_config(fp)
            result = True
        return result

    def prettify(self, result=None):
        result = super(ValidateConfigCommand, self).prettify(result=result)
        return "The configuration at {} {} valid".format(
            self.get('path'), "IS" if result else "is NOT"
        )

    def validate_config(self, fp):
        result = False
        try:
            manager = self.context.get_manager("config")
            """:type: pycultivator.manager.config.ConfigManager"""
        except KeyError:
            raise exceptions.CommandException("No configuration manager loaded")
        else:
            result = manager.validate(fp)
        return result


class ListConfigCommand(BaseConfigCommand):

    name = "list"
    description = "List all loaded configuration sources"

    def execute(self):
        try:
            manager = self.context.get_manager("config")
            """:type: pycultivator.manager.config.ConfigManager"""
        except KeyError:
            raise exceptions.CommandException("No configuration manager loaded")
        return manager.configs
        
    def prettify(self, result=None):
        configs = super(ListConfigCommand, self).prettify(result=result)
        result = ["Currently {} Configuration(s) are loaded".format(len(configs))]
        for name in configs.keys():
            result.append("- {}".format(name))
        return "\n".join(result)


class DeleteConfigCommand(ConfigCommand):

    name = "delete"
    description = "Delete a configuration source from memory"

    def execute(self):
        result = True
        name = self.data_name
        try:
            manager = self.context.get_manager("config")
            """:type: pycultivator.manager.config.ConfigManager"""
        except KeyError:
            raise exceptions.CommandException("No configuration manager loaded")
        else:
            result = manager.remove(name)
        finally:
            self.info("Removed configuration source '{}' from memory: {}".format(name, result))
        return result
