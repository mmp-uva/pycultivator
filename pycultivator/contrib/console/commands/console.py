
from .base import Command
from pycultivator.contrib.console import arguments

__all__ = [
    "VersionCommand",
    "HelpCommand",
    "ConsoleCommand",
    "ExitCommand"
]


class VersionCommand(Command):

    name = "version"
    description = "Show pyCultivator version information."

    def execute(self):
        import pkg_resources
        return pkg_resources.get_distribution("pycultivator").version

    def prettify(self, result=None):
        result = super(VersionCommand, self).prettify(result=result)
        return "pyCultivator Console ({version})".format(version=result)


class HelpCommand(Command):

    name = "help"
    arguments = [
        arguments.StringArgument("filter", is_required=False)
    ]
    description = "Show this help message."

    def execute(self):
        commands = sorted(self.context.parent.commands.values(), key=lambda c: c.get_full_command())
        if not self.filter.is_empty():
            filter_name = self.get('filter')
            # filter commands using the given prefix
            commands = filter(lambda c: c.get_full_command().startswith(filter_name), commands)
        return commands

    def prettify(self, result=None):
        commands = super(HelpCommand, self).prettify(result=result)
        msg = [
            "pyCultivator (interactive) Console.",
            "The console provides low-level access to the pyCultivator library",
            ""
        ]
        if not self.filter.is_empty():
            msg.append("Only showing commands starting with '{}'".format(self.filter.value))
        msg.append("These commands are available:")
        for command in commands:
            msg.append("\t{}".format(command.help()))
        return "\n".join(msg)


class ConsoleCommand(Command):

    name = "console"
    description = "Opens a python interactive console"

    def execute(self):
        self.info("Entering interactive console...")
        self.info("Use the 'context' variable to access devices, configurations etc.")
        self.info("press Ctrl-D to exit")
        try:
            import code
            code.interact(local={'context': self.context})
        except SystemExit:
            pass
        except ImportError:
            self.error("Unable to load the code module; are sure this is installed?")
        except Exception as e:
            self.error("Error: {}".format(e))
        self.info("Left the interactive console, welcome back")


class ExitCommand(Command):

    name = "exit"
    description = "Exit this console"

    def execute(self):
        self.console.set_exit()
