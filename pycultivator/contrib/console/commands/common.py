
from .base import Command
from pycultivator.contrib.console import arguments, exceptions

__all__ = [
    "load_commands",
    "ShowCommand",
    "ListServicesCommand",
]


def load_commands():
    return [
        ShowCommand,
        ListServicesCommand,
    ]


class ShowCommand(Command):

    name = "show"
    arguments = [
        arguments.StringArgument("path", is_required=False, default="/"),
        arguments.IntegerArgument("max_level", is_required=False, default=1)
    ]
    description = "Show the structure of the objects in the path"

    @property
    def has_max_level(self):
        return self.get("max_level") > 0

    def list_items(self, items, level=0, max_level=0):
        for item in items:
            if hasattr(item, "children") and (max_level <= 0 or level < max_level):
                yield {
                    'object': item,
                    'children': self.list_items(item.children, level=level + 1, max_level=max_level)
                }
            else:
                yield {
                    'object': item, # 'children': []
                }

    def execute(self):
        path = self.get("path")
        max_level = self.get("max_level")
        items = self.context.path.resolve(path)
        return self.list_items(items, max_level=max_level)


class ListServicesCommand(Command):

    name = "list services"
    description = "List all the services provided by this console"

    def execute(self):
        return self.console.services
