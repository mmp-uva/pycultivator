
from .client import BaseClient

__all__ = [
    "BaseClient",

    "bootstrap"
]


def bootstrap(klass=None, *args, **kwargs):

    if klass is None:
        klass = BaseClient
    if not issubclass(klass, BaseClient):
        raise TypeError("Invalid console type, {} does not inherit from BaseServer".format(klass))

    # configure console
    c = klass(*args, **kwargs)

    # configure argument parser
    import argparse
    p = argparse.ArgumentParser(add_help=True)
    p.add_argument(
        dest="address", nargs="?",
        help="The address to connect to"
    )
    p.add_argument(
        '-v', '--version', action="store_true",
        help="Print version information"
    )

    # parse arguments
    a = vars(p.parse_args())

    address = a['address']

    # cheat in case version is given
    if a['version']:
        for v in c.get_version():
            c.print_message(v)
        exit(0)

    c.start(address=address)


def main():
    bootstrap()


if __name__ == "__main__":
    main()
