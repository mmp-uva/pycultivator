
from pycultivator.contrib.console.commands import Command
from pycultivator.contrib.console import arguments

__all__ = [
    "load_commands",

]


def load_commands():
    return [
        RemoteCommand,
    ]


class RemoteCommand(Command):

    required_context = 

