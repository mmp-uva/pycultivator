
from pycultivator.core import BaseObject
from pycultivator.contrib.console import BaseConsole
from pycultivator.contrib.console.exceptions import *

import zmq

__all__ = [
    "BaseClient"
]


class BaseClient(BaseConsole):

    def __init__(self, address=None, commands=None):
        super(BaseClient, self).__init__(commands=commands)
        self._address = address
        # zeromq context
        context = zmq.Context()
        # for now use synchronous communication
        self._action_socket = context.socket(zmq.ROUTER)

    @property
    def address(self):
        return self._address

    def start(self, address=None, **kwargs):
        if address is not None:
            self._address = address
        super(BaseClient, self).start(**kwargs)

    def prepare(self):
        super(BaseClient, self).prepare()
        self.prepare_connection()

    def prepare_connection(self):
        address = self.address
        if address is None:
            raise ValueError("No address defined to bind to")
        self._action_socket.connect(address)
        self.log.info("Listening on {}".format(address))

    def process(self):
        try:
            # retrieve input
            command = self.read_input()
            if command is not None:
                # send command to server
                self.send_line(command)
                result = self.receive_result()
                self.print_output(result)
        except (KeyboardInterrupt, EOFError):
            self._exit = True

    def read_input(self, prompt="> "):
        from six.moves import input
        return input(prompt).strip()

    def send_line(self, command):
        self.send_message(["CMD", command])

    def send_ping(self):
        self.send_message(["PING"])

    def send_message(self, message):
        result = False
        try:
            self._action_socket.send_multipart(message)
            result = True
        except zmq.ZMQError as e:
            self.log.error(e, exc_info=1)
        return result

    def receive_message(self):
        command, message = None, None
        try:
            data = self._action_socket.recv_multipart()
            if len(data) >= 2:
                command = data[0]
                message = data[1]
        except zmq.ZMQError as e:
            self.log.error(e, exc_info=1)
        return command, message

    def receive_result(self):
        command, message = None, None
        has_response = False
        try:
            while not has_response:
                command, message = self.receive_message()
                if command == "CMD":
                    has_response = True
                    break
        except KeyboardInterrupt:
            pass
        # process
        return command, message

    def print_output(self, output):
        message = self.prettify_output(output)
        # turn into message
        self.print_message(message)

    def get_version(self):
        import pkg_resources
        version = pkg_resources.get_distribution("pycultivator").version
        return [
            "pyCultivator version {}".format(version),
            "Using pyzmq version {}".format(zmq.__version__),
            "Using libzmq version {}".format(zmq.zmq_version())
        ]