
from .server import BaseServer

__all__ = [
    "BaseServer",

    "bootstrap"
]


def bootstrap(klass=None, *args, **kwargs):
    """Helps interfacing the server application as a shell script"""

    if klass is None:
        klass = BaseServer
    if not issubclass(klass, BaseServer):
        raise TypeError("Invalid console type, {} does not inherit from BaseServer".format(klass))

    # configure console
    c = klass(*args, **kwargs)

    # configure argument parser
    import argparse
    p = argparse.ArgumentParser(add_help=True)
    p.add_argument(
        dest="address", nargs="?",
        help="The address to listen for requests at"
    )
    p.add_argument(
        '-v', '--version', action="store_true",
        help="Print version information"
    )

    # parse arguments
    a = vars(p.parse_args())

    address = a['address']

    # cheat in case version is given
    if a['version']:
        for v in c.get_version():
            c.print_message(v)
        exit(0)

    c.start(address=address)


def main():
    """Main entry point"""
    bootstrap()


if __name__ == "__main__":
    main()
