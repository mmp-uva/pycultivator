from pycultivator.contrib.console import BaseConsole
from pycultivator.contrib.console.exceptions import *

import zmq

__all__ = [
    "BaseServer"
]


class BaseServer(BaseConsole):

    req

    def __init__(self, address=None, commands=None):
        super(BaseServer, self).__init__(commands=commands)
        self._address = address
        # zeromq context
        self._action_context = zmq.Context()
        # for now use synchronous communication
        self._action_socket = self._action_context.socket(zmq.REP)

    @property
    def address(self):
        return self._address

    def start(self, address=None, **kwargs):
        if address is not None:
            self._address = address
        super(BaseServer, self).start(**kwargs)

    def prepare(self):
        super(BaseServer, self).prepare()
        self.prepare_connection()

    def prepare_connection(self):
        address = self.address
        if address is None:
            raise ValueError("No address defined to bind to")
        self._action_socket.bind(address)
        self.log.info("Listening on {}".format(address))

    def process(self):
        command = None
        try:
            command = self.read_input()
            if command is None:
                self.log.info("Processing command")
                command = self.execute_command(command)
                self.send_output(command)
        except CommandException as ce:
            self.send_error(ce.command, ce)
        except ConsoleException as e:
            self.send_error(command, e)
        except (KeyboardInterrupt, EOFError):
            self.send_error(None, "Exiting")
            self._exit = True

    def finalize(self):
        self._action_socket.close()
        self._action_context.term()

    def read_input(self):
        result, message = None, None
        # TODO: should be adjusted when using router sockets
        try:
            message = self._action_socket.recv_string()
        except zmq.ZMQError as ze:
            self.log.error(ze, exc_info=1)
        if message:
            # parse message
            result = self.process_line(message)
        return result

    def send_error(self, command, error):
        response = {
            'command': command.dot_path(),
            'success': False,
            'result': str(error),
        }
        self.send_response(response)

    def send_output(self, command):
        """Send command output to the client

        :type command: pycultivator.contrib.console.commands.Command
        """
        response = {
            'command': command.dot_path(),
            'success': command.is_successful(),
            'result': self.prettify_output(command.result),
        }
        self.send_response(response)

    def send_response(self, response):
        """Sends a message encoded as JSON"""
        try:
            self._action_socket.send_json(response)
        except zmq.ZMQError as ze:
            self.log.error(ze, exc_info=1)

    def send_message(self, message):
        result = False
        try:
            self._action_socket.send_multipart(message)
            result = True
        except zmq.ZMQError as e:
            self.log.error(e, exc_info=1)
        return result

    def receive_message(self):
        command, message = None, None
        try:
            data = self._action_socket.recv_multipart()
            if len(data) >= 2:
                command = data[0]
                message = data[1]
        except zmq.ZMQError as e:
            self.log.error(e, exc_info=1)
        return command, message

    def finalize(self):
        self._exit = True
        self._action_socket.close()
        # clean workspace
        if self._context is not None:
            self.log.info("Cleaning workspace....")
            self._context.clean()

    def get_version(self):
        import pkg_resources
        version = pkg_resources.get_distribution("pycultivator").version
        return [
            "Using pyCultivator version {}".format(version),
            "Using pyzmq version {}".format(zmq.__version__),
            "Using libzmq version {}".format(zmq.zmq_version())
        ]
