"""Mix-ins providing logging capabilities"""

from console import ConsoleMixin
import time

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class ConsoleLoggerMixin(ConsoleMixin):

    @classmethod
    def log(cls, msg, start="\r", end="\n"):
        """Write a log message to the console"""
        cls.write(msg, start=start, end=end)

    @classmethod
    def warn(cls, msg, start="\r", end="\n"):
        cls.write_error(msg, start=start, end=end)

    @classmethod
    def log_progress(cls, value, index=None, total=100, msg="Measuring {} ({:.1%})", start="\r", end=""):
        """Writes a log message with a progress indicator (percentage bar)

        :param value:
        :param index:
        :param total:
        :param msg:
        :param start:
        :param end:
        :return:
        """
        if index is None:
            index = value
        cls.log(start + msg.format(value, index / float(total)), end=end)

    @classmethod
    def log_wait(cls, t_wait, t_interval=1, msg="Wait for {} seconds"):
        """Writes a log message while pausing the programing for a t_wait amount of seconds.

        :param t_wait: Number of seconds to wait
        :param t_interval: Update message every t_interval seconds.
        :param msg: Message to display, make sure to add "{}" in the message.
        :return:
        """
        cls.log(msg.format(t_wait), end="")
        while t_wait > 0:
            cls.log(msg.format(t_wait), start="\r", end="")
            if (t_wait - t_interval) < 0:
                t_interval = t_wait
            t_wait -= t_interval
            time.sleep(t_interval)
        cls.log("", start="\r", end="")


# TODO: Implement file logger mixin
# class FileLoggerMixin(object):

