"""Module implementing a dynamic packet"""

from pycultivator.connection import packet, _packet

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class FormPacket(packet.Packet, _packet.BaseForm):
    """ A form packet allows for writing multiple values in the data segment of the packet

    """

    def __init__(self, names=None, fmts=None, values=None):
        packet.Packet.__init__(self)
        _packet.BaseForm.__init__(self, names=names, fmts=fmts, values=values)

    def check(self):
        result = False
        try:
            self.writeFields()
            result = True
        except ValueError:
            pass  #
        except packet.PacketException:
            pass
        return result

    def write(self):
        if not self.check():
            raise FormPacketException("Unable write packet; packet is not valid")
        return self.writeFields()

    def writeFields(self, p=None):
        for name in self.getFieldNames():
            p = self.writeField(name=name, p=p)
        return p

    def writeField(self, name=None, index=None, p=None):
        fmt = self.getFieldFormat(name, index)
        value = self.getFieldValue(name, index)
        p = self.writeValue(fmt, value, p=p)
        return p


class FormPacketException(packet.PacketException, _packet.BaseFormException):
    """Exception raised by a FormPacket"""

    def __init__(self, msg):
        super(FormPacketException, self).__init__(msg)


class SandwichFormPacket(FormPacket, packet.SandwichPacket):
    """A form packet with a fixed header and footer"""

    def __init__(self, names=None, fmts=None, values=None):
        super(SandwichFormPacket, self).__init__(names=names, fmts=fmts, values=values)

    def getDataFormats(self):
        return self.getFieldFormats()

    def getDataValues(self):
        return self.getFieldValues()

    def writeData(self, p=None):
        return self.writeFields(p=p)
