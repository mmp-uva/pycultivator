# coding=utf-8
"""
Test file to Test serial.py
"""

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.connection import connection


class TestConnection(UvaSubjectTestCase):
    """Class to unit test the Connection class"""

    _abstract = True
    _subject_cls = connection.Connection

    def initSubject(self, *args, **kwargs):
        """ Return a new initialized subject class instance

        :return: The instance of the class subject to this testcase
        :rtype: pycultivator.connection.Connection.Connection
        """
        return super(TestConnection, self).initSubject(*args, **kwargs)

    def getSubject(self):
        """Return the currently used subject class instance

        :return: The instance of the class subject to this testcase
        :rtype: pycultivator.connection.Connection.Connection
        """
        return super(TestConnection, self).getSubject()

    def test_isFake(self):
        self.assertFalse(self.subject.isFake())

    def test_isConnected(self):
        self.assertFalse(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().connect())
        self.assertTrue(self.getSubject().isConnected())

    def test_connect(self):
        # test opening a connection without setting a port
        subject = self.initSubject()
        # self.assertFalse(subject.connect())
        # test with the object created in setUp
        self.assertFalse(self.getSubject().isConnected(), msg="test_connect #1: port already open")
        self.assertTrue(self.getSubject().connect())
        self.assertTrue(self.getSubject().isConnected())
        # try to reopen without closing
        with self.assertRaises(connection.ConnectionException):
            self.getSubject().connect()
        self.assertTrue(self._subject.isConnected())

    def test_disconnect(self):
        self.assertFalse(self.getSubject().isConnected(), msg="test_close #1: port already open")
        with self.assertRaises(
                connection.ConnectionException, msg="test_close #2: success when closing a closed port"
        ):
            self.getSubject().disconnect()
        self.assertTrue(self.getSubject().connect(), msg="test_close #3: failed to open port")
        self.assertTrue(self.getSubject().isConnected(), msg="test_close #4: port did not open")
        self.assertTrue(self.getSubject().disconnect(), msg="test_close #5: failed to close port")
        self.assertFalse(self.getSubject().isConnected(), msg="test_close #6: port did not close")


class TestFakeConnection(TestConnection):

    _subject_cls = connection.FakeConnection

    def test_isFake(self):
        return self.assertTrue(self.subject.isFake())
