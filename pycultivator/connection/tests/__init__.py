""""
Base test class
"""

import struct, os
from pycultivator.connection import packet
from pycultivator.core.tests import UvaSubjectTestCase

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


def createByteArray(packetFields, crc=None):
    """Create a byte array from a list of tuples. Each tuple specifies the format and content.

    :param packetFields: List of tuples, with format and contnet
    :type packetFields: list[tuple[str, str]]
    :param crc: Crc field, Disable crc with False or auto calculate crc when set to None
    :type crc: bool or int or None
    :return: The bytearray
    :rtype: bytearray
    """
    p = bytearray()
    for f in packetFields:
        # strings should be parsed as 1, combinations of bytes as seperate
        if len(f[0]) > 1 and f[0][-1] != packet.Packet.FORMAT_STRING:
            p.extend(struct.pack(f[0], *f[1]))
        else:
            p.extend(struct.pack(f[0], f[1]))
    # recalculate crc unless crc is explicitly set to False
    if crc is not False:
        if crc is None:
            crc = packet.CRCPacket.crc(p)
        p.extend(struct.pack(packet.CRCPacket.FORMAT_UBYTE, crc))
    return p


class SerialSubjectTestCase(UvaSubjectTestCase):
    """	"""

    _subject = None
    ''':type: pycultivator.serial.Serial.Serial'''

    _serial = None
    _serial_cls = None
    _port = None

    @classmethod
    def setUpClass(cls):
        super(SerialSubjectTestCase, cls).setUpClass()
        cls._port = os.getenv("SERIAL_PORT", None)
        # cls._serial_cls = Serial.Serial
        if cls._port is None:
            # cls._serial_cls = Serial.FakeSerial
            cls._port = "/dev/null"

    def initSerial(self, *args, **kwargs):
        self.setSerial(self.getSerialClass()(*args, **kwargs))

    def getSerial(self):
        return self._serial

    def setSerial(self, s):
        """Sets the serial object in this test case

        :rtype: pycultivator.connection.serial.Serial.Serial
        """
        self._serial = s

    def clearSerial(self):
        self.setSerial(None)

    @classmethod
    def getSerialClass(cls):
        """Returns the serial class of used in this test case

        :rtype: pycultivator.connection.serial.Serial.Serial[]
        """
        return cls._serial_cls
