# coding=utf-8
"""Test cases for the template module"""

from pycultivator.connection import template
from test_Packet import PacketTest
import struct

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TemplateTest(PacketTest):

    _subject_cls = template.Template
    _abstract = False

    def test_read(self):
        self.skipTest("Not implemented yet")
