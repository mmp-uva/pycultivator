# coding=utf-8
"""Test cases for the FormPacket module"""

from pycultivator.connection import formPacket
from test_Packet import PacketTest
import struct

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class FormPacketTest(PacketTest):

    _subject_cls = formPacket.FormPacket
    _abstract = False

    _field_names = ["a", "b"]
    _field_fmts = ["c", "B"]
    _field_values = ["a", 1]
    _fields = {}
    for idx, name in enumerate(_field_names):
        _fields[name] = _field_fmts[idx], _field_values[idx]

    @classmethod
    def getSubjectClass(cls):
        """

        :rtype: type[pycultivator.connection.FormPacket.FormPacket]
        """
        return super(FormPacketTest, cls).getSubjectClass()

    def getSubject(self):
        """

        :rtype: pycultivator.connection.FormPacket.FormPacket
        """
        return super(FormPacketTest, self).getSubject()

    @property
    def subject(self):
        return self.getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(
            self._field_names,
            self._field_fmts,
            self._field_values
        )

    def test_init(self):
        self.assertEqual(self.subject._indexes, self._field_names)
        self.assertEqual(self.subject._data, self._fields)

    def test_getFields(self):
        data = {}
        for idx, name in enumerate(self._field_names):
            data[name] = self._field_fmts[idx], self._field_values[idx]
        self.assertEqual(self.subject.getFields(), self._fields)

    def test_getFieldNames(self):
        self.assertEqual(self.subject.getFieldNames(), self._field_names)

    def test_getFieldFormats(self):
        self.assertEqual(self.subject.getFieldFormats(), self._field_fmts)

    def test_getFieldValues(self):
        self.assertEqual(self.subject.getFieldValues(), self._field_values)

    def test_getField(self):
        self.assertEqual(self.subject.getField("a"), self._fields["a"])
        self.assertEqual(self.subject.getField(index=1), self._fields["b"])
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getField()
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getField(index=100)
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getField("abcd")

    def test_getFieldFormat(self):
        self.assertEqual(self.subject.getFieldFormat("a"), self._fields["a"][0])
        self.assertEqual(self.subject.getFieldFormat(index=1), self._field_fmts[1])
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getFieldFormat()
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getFieldFormat(index=100)
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getFieldFormat("abcd")

    def test_getFieldValue(self):
        self.assertEqual(self.subject.getFieldValue("a"), self._fields["a"][1])
        self.assertEqual(self.subject.getFieldValue(index=1), self._field_values[1])
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getFieldValue()
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getFieldValue(index=100)
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.getFieldValue("abcd")

    def test_addField(self):
        self.assertEqual(len(self.subject.getFields()), 2)
        self.subject.addField("c", "H", (1 << 8)-1)
        self.assertEqual(len(self.subject.getFields()), 3)
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.addField("a", "c", "a")
        self.subject.addField("d", "B")
        self.assertEqual(self.subject.getField("d"), ("B", None))
        self.subject.addField("e", "B", index=2)
        self.assertEqual(self.subject.getField(index=2), ("B", None))

    def test_setField(self):
        with self.assertRaises(formPacket._packet.BaseFormException):
            self.subject.setField("q", "c", "a")

    def test_writeFields(self):
        self.assertIsNotNone(self.subject.writeFields())
        ba = bytearray()
        for name in self._field_names:
            ba.extend(
                struct.pack(self._fields[name][0], self._fields[name][1])
            )
        self.assertEqual(self.subject.writeFields(), ba)

    def test_write(self):
        self.assertIsNotNone(self.subject.write())
        ba = bytearray()
        for name in self._field_names:
            ba.extend(
                struct.pack(self._fields[name][0], self._fields[name][1])
            )
        self.assertEqual(self.subject.write(), ba)


class SandwichFormPacketTest(FormPacketTest):

    _subject_cls = formPacket.SandwichFormPacket
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """

        :rtype: calllable[pycultivator.connection.FormPacket.SandwichFormPacket]
        """
        return super(SandwichFormPacketTest, cls).getSubjectClass()

    def getSubject(self):
        """

        :rtype: pycultivator.connection.FormPacket.SandwichFormPacket
        """
        return super(SandwichFormPacketTest, self).getSubject()
