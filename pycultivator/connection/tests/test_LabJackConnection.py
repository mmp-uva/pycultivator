"""
Module to test the LabJackConnection module
"""

import u6
from pycultivator.connection.tests.test_Connection import TestConnection, TestFakeConnection
from unittest import SkipTest
from pycultivator.connection import labjackConnection

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestLabJackConnection(TestConnection):
    """Test the LabJack Connection class"""

    _abstract = False
    _subject_cls = labjackConnection.LabJackConnection
    _test_flashes = False

    @classmethod
    def setUpClass(cls):
        try:
            u6.U6()
        except:
            raise SkipTest("No LabJack connected, skip testing live class")
        return super(TestLabJackConnection, cls).setUpClass()

    def setUp(self):
        self.setSubject(self.initSubject(autoConnect=True))

    def tearDown(self):
        if self.getSubject().isConnected():
            self.getSubject().disconnect()

    def initSubject(self, *args, **kwargs):
        """ Return a new instance of the subject class

        :rtype: pycultivator_lcp.connection.LabJackConnection.LabJackConnection
        """
        return super(TestLabJackConnection, self).initSubject(*args, **kwargs)

    def getSubject(self):
        """ Return the subject class instance, subject to this test

        :return: The instance of the class subject to this testcase
        :rtype: pycultivator_lcp.connection.LabJackConnection.LabJackConnection
        """
        return super(TestLabJackConnection, self).getSubject()

    def test_connectWithSerialNr(self):
        # test connection
        self.assertFalse(self.getSubject().isConnected())
        # test connecting without connection
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        serialNr = self.getSubject().getSerialNumber()
        self.assertIsNotNone(serialNr)
        self.assertTrue(self.getSubject().disconnect())
        self.assertTrue(self.getSubject().connectWithSerialNr(serialNr=serialNr))
        self.assertTrue(self.getSubject().disconnect())
        self.assertFalse(self.getSubject().connectWithSerialNr(serialNr=serialNr + 1))

    def test_connectWithLocalID(self):
        # test connection
        self.assertFalse(self.getSubject().isConnected())
        # test connecting without connection
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        localID = self.getSubject().getLocalID()
        self.assertIsNotNone(localID)
        self.assertTrue(self.getSubject().disconnect())
        self.assertTrue(self.getSubject().connectWithLocalID(localID=localID))
        self.assertTrue(self.getSubject().disconnect())
        self.assertFalse(self.getSubject().connectWithLocalID(localID=localID+1))

    def test_connect(self):
        super(TestLabJackConnection, self).test_connect()
        # we know that test_connect will leave the subject connected; check state and close it.
        self.assertTrue(self.getSubject().isConnected())
        self.getSubject().disconnect()
        # test state
        self.assertFalse(self.getSubject().isConnected())
        # test connecting without connection
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        # check connection status
        self.assertTrue(self.getSubject().isConnected())
        # try to connect with connection
        with self.assertRaises(labjackConnection.LabJackException):
            self.getSubject().connect(autoConnect=True)
        serialNr = self.getSubject().getSerialNumber()
        localID = self.getSubject().getLocalID()
        # test connecting with serial Nr
        self.assertTrue(self.getSubject().disconnect())
        self.assertTrue(self.getSubject().connect(serialNr=serialNr))
        self.assertEqual(self.getSubject().getSerialNumber(), serialNr)
        self.assertTrue(self.getSubject().isConnected())
        # test connecting with localID
        self.assertTrue(self.getSubject().disconnect())
        self.assertTrue(self.getSubject().connect(localID=localID))
        self.assertEqual(self.getSubject().getLocalID(), localID)
        self.assertTrue(self.getSubject().isConnected())

    def test_disconnect(self):
        super(TestLabJackConnection, self).test_disconnect()
        # we know that test_disconnect returns with subject disconnected, check state
        self.assertFalse(self.getSubject().isConnected())
        # test disconnecting without connection
        self.assertFalse(self.getSubject().disconnect())
        # connect
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        # test if connecting worked
        self.assertTrue(self.getSubject().isConnected())
        # test disconnecting with connection
        self.assertTrue(self.getSubject().disconnect())
        self.assertFalse(self.getSubject().isConnected())

    def test_ping(self):
        # self.assertFalse(self.getSubject().ping())
        # self.assertTrue(self.getSubject().connect())
        # self.assertTrue(self.getSubject().ping())
        pass

    def test_identify(self):
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        if self._test_flashes:
            self.getSubject().identify()

    def test_jingle(self):
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        if self._test_flashes:
            self.getSubject().jingle()

    def test_setLED(self):
        self.assertFalse(self.getSubject().setLED(True))
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        self.assertTrue(self.getSubject().setLED(True))
        self.assertTrue(self.getSubject().setLED(False))

    def test_readAIN(self):
        self.assertEqual(self.getSubject().readAIN(1, 12, 1), 0.0)
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        self.assertNotEqual(self.getSubject().readAIN(1, 12, 1), 0.0)

    def test_measureTemperature(self):
        self.assertEqual(self.getSubject().measureTemperature(), 0.0)
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        self.assertAlmostEqual(self.getSubject().measureTemperature(), 21 + 273.15, delta=5)
        self.assertAlmostEqual(self.getSubject().measureTemperature(units="celsius"), 21, delta=5)
        self.assertAlmostEqual(
            self.getSubject().measureTemperature(),
            self.getSubject().measureTemperature(units="celsius") + 273.15,
            delta=1
        )

    def test_readInfo(self):
        self.assertEqual(self.getSubject().readInfo(), {})
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        self.assertIsNotNone(self.getSubject().readInfo().get("LocalID", None))
        old_id = self.getSubject().readInfo().get("LocalID", 1)
        if old_id is not None:
            new_id = 9
            self.getSubject()._setLocalID(new_id, write=True)
            self.assertEqual(self.getSubject().readInfo().get("LocalID"), new_id)
        self.getSubject()._setLocalID(old_id, write=True)
        self.assertEqual(self.getSubject().readInfo().get("LocalID"), old_id)
        self.getSubject()._setLocalID(new_id, write=False)
        self.assertEqual(self.getSubject().readInfo().get("LocalID"), old_id)


class TestFakeLabJackConnection(TestFakeConnection, TestLabJackConnection):
    """Unit test for the FakeLabJackConnection class"""

    _abstract = False
    _subject_cls = labjackConnection.FakeLabJackConnection
    _test_flashes = False

    @classmethod
    def setUpClass(cls):
        return super(TestLabJackConnection, cls).setUpClass()

    def test_auto_connect(self):
        self.assertFalse(self.getSubject().isConnected())
        with self.assertRaises(labjackConnection.ConnectionException):
            self.subject.ping()
        # set auto connect to True
        self.subject.settings.set('auto_connect', True)

    def test_connectWithSerialNr(self):
        # test connection
        self.assertFalse(self.getSubject().isConnected())
        # test connecting without connection
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        serialNr = self.getSubject().getSerialNumber()
        self.assertIsNotNone(serialNr)
        self.assertTrue(self.getSubject().disconnect())
        self.assertTrue(self.getSubject().connectWithSerialNr(serialNr=serialNr))
        self.assertTrue(self.getSubject().disconnect())
        # remember we fake the connections; so giving an invalid serialNr will succeed!
        self.assertTrue(self.getSubject().connectWithSerialNr(serialNr=serialNr + 1))

    def test_connectWithLocalID(self):
        # test connection
        self.assertFalse(self.getSubject().isConnected())
        # test connecting without connection
        self.assertTrue(self.getSubject().connect(autoConnect=True))
        localID = self.getSubject().getLocalID()
        self.assertIsNotNone(localID)
        self.assertTrue(self.getSubject().disconnect())
        self.assertTrue(self.getSubject().connectWithLocalID(localID=localID))
        self.assertTrue(self.getSubject().disconnect())
        # remember we fake the connections; so giving an invalid serialNr will succeed!
        self.assertTrue(self.getSubject().connectWithLocalID(localID=localID+1))
