# coding=utf-8
"""
Test file to Test serial.py
"""

import serial, os, struct
from collections import deque
from pycultivator.connection import serialConnection, packet, formPacket
from test_Connection import TestConnection, TestFakeConnection
from unittest import SkipTest

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


def createByteArray(packetFields, crc=None):
    """Create a byte array from a list of tuples. Each tuple specifies the format and content.

    :param packetFields: List of tuples, with format and contnet
    :type packetFields: list[tuple[str, str]]
    :param crc: Crc field, Disable crc with False or auto calculate crc when set to None
    :type crc: bool or int or None
    :return: The bytearray
    :rtype: bytearray
    """
    p = bytearray()
    for f in packetFields:
        # strings should be parsed as 1, combinations of bytes as seperate
        p.extend(struct.pack(f[0], f[1]))
    # recalculate crc unless crc is explicitly set to False
    if crc is not False:
        if crc is None:
            crc = packet.CRCPacket.crc(p)
        p.extend(struct.pack(packet.Packet.FORMAT_UBYTE, crc))
    return p


class TestSerialConnection(TestConnection):
    """
    Tests the serial library, e.g ability to connect, read and send messages.
    """

    _abstract = False
    _subject_cls = serialConnection.SerialConnection
    _port = None

    # default ping package for psi
    _ba = createByteArray([
        ('4s', b'FMTP'),    # fmt
        ('H', 1),           # seq
        ('H', 2),           # len
        ('H', 4),           # cmd
    ], 8)
    _template = {
        "names": ['header', "sequence", "length", "command"],
        "fmts": ['4s', 'H', 'H', 'H']
    }
    _packet = formPacket.FormPacket(**_template)

    @classmethod
    def setUpClass(cls):
        result = super(TestSerialConnection, cls).setUpClass()
        cls._port = os.getenv("SERIAL_PORT", None)
        if cls._port is None:
            raise SkipTest("No port given to run unit tests against")
        return result

    def tearDown(self):
        if self.getSubject().isConnected():
            self.getSubject().disconnect()
        return super(TestSerialConnection, self).tearDown()

    def setUp(self):
        self.setSubject(self.initSubject(port=self.getPort()))

    def getSubject(self):
        """ Return the subject class instance, subject to this test

        :return: The instance of the class subject to this testcase
        :rtype: pycultivator.connection.SerialConnection.SerialConnection
        """
        return super(TestSerialConnection, self).getSubject()

    @classmethod
    def getPort(cls):
        """Returns the port that is used to test against"""
        return cls._port

    def test_init(self):
        self.assertFalse(self._subject.isConnected())
        name = serialConnection.SerialConnection.OS_UNKNOWN
        if os.name == 'posix':
            name = serialConnection.SerialConnection.OS_LINUX
        if os.name == 'nt':
            name = serialConnection.SerialConnection.OS_WINDOWS
        default_settings = {
            # "port": None,
            "rate": 115200,
            'os': name,
            'readAttempts': 3,
            'readTimeout': 0.5,
            'writeTimeout': None,
            'byteSize': serial.EIGHTBITS,
            'parity': serial.PARITY_NONE,
            'stopBits': serial.STOPBITS_ONE,
            'rtscts': False,
            'dsrdtr': False,
            'xonxoff': False,
        }
        for setting in default_settings.keys():
            if not self.subject.getSettings().has(setting):
                raise AssertionError("{} not in actual settings".format(setting))
            self.assertEqual(
                self._subject.getSettings()[setting],
                default_settings[setting],
                msg="Default Value of {} not equal to actual".format(setting)
            )

    def test_configure(self):
        settings = {'port': '/dev/usb1'}
        with self.assertRaises(serialConnection.SerialException):
            self.getSubject()._configure(settings)
        self.getSubject()._createDevice()
        self.getSubject()._configure(settings)
        self.assertFalse(self.getSubject().isConnected())
        self.assertEqual(self.getSubject().getSettings()["port"], '/dev/usb1')
        self.assertEqual(self.getSubject().getPort(), '/dev/usb1')
        self.getSubject()._configure(settings, port="/dev/usb2")
        self.assertEqual(self.getSubject().getSettings()["port"], '/dev/usb2')
        self.assertEqual(self.getSubject().getPort(), '/dev/usb2')
        self.assertFalse(self.getSubject().isConnected())
        self.assertTrue(self.getSubject().connect())
        with self.assertRaises(serialConnection.SerialException):
            self.getSubject()._configure(settings, port="/dev/usb2")

    def test__write(self):
        # test writing: do naive we only try to send something
        # write without connection
        with self.assertRaises(serialConnection.SerialException, msg="test_write #1: can write to closed port"):
            self.getSubject()._write(self._ba)
        self.assertTrue(self.getSubject().connect(), msg="test_write #2: failed to open port")
        self.assertEqual(
            self.getSubject()._write(self._ba), len(self._ba),
            msg="test_write #3: msg length not matching"
        )

    def test__read(self):
        self.skipTest("No read test implemented yet")

    def test_send(self):
        with self.assertRaises(serialConnection.SerialException, msg="test_write #1: can write to closed port"):
            self.getSubject().send(self._packet)
        self.assertTrue(self.getSubject().connect(), msg="test_write #2: failed to open port")
        self.assertTrue(self.getSubject().send(self._packet), msg="test_write #3: msg length not matching")

    def test_receive(self):
        self.skipTest("No receive test implemented yet")

    def test_send_receive(self):
        pass


class TestFakeSerialConnection(TestFakeConnection, TestSerialConnection):

    _abstract = False
    _subject_cls = serialConnection.FakeSerialConnection
    _port = "/dev/null"

    @classmethod
    def setUpClass(cls):
        return super(TestSerialConnection, cls).setUpClass()

    def getSubject(self):
        """ Return the subject class instance, subject to this test

        :return: The instance of the class subject to this testcase
        :rtype: pycultivator.connection.SerialConnection.FakeSerialConnection
        """
        return super(TestFakeSerialConnection, self).getSubject()

    def tearDown(self):
        self.getSubject().clearReadQueue()
        self.getSubject().clearWriteQueue()
        super(TestFakeSerialConnection, self).tearDown()

    def test_readFromQueue(self):
        queue = deque()
        # nothing in queue so we read nothing
        self.assertEqual(self.getSubject()._readFromQueue(queue), bytearray())
        queue.extend('abcd')
        self.assertEqual(self.getSubject()._readFromQueue(queue), 'abcd')
        self.assertEqual(len(queue), 0)
        queue.extend('abcd')
        self.assertEqual(self.getSubject()._readFromQueue(queue, 2), 'ab')
        self.assertEqual(len(queue), 2)
        queue.extend('abcd')
        self.assertEqual(self.getSubject()._readFromQueue(queue, 6), 'cdabcd')
        queue = deque(maxlen=3)
        queue.extend('abcd')
        self.assertEqual(self.getSubject()._readFromQueue(queue, 4), 'bcd')

    def test_setQueueSize(self):
        queue = deque()
        self.assertIsNone(queue.maxlen)
        queue = self.getSubject()._setQueueSize(queue, 10)
        self.assertEqual(queue.maxlen, 10)
        queue.extend('abcdabcdabcd')
        self.assertEqual(len(queue), 10)
        queue = self.getSubject()._setQueueSize(queue, None)
        self.assertIsNone(queue.maxlen)

    def test_addToQueue(self):
        queue = deque()
        self.getSubject()._addToQueue(queue, 'abcd')
        self.assertEqual(len(queue), 4)
        queue.clear()
        queue = deque(maxlen=3)
        self.getSubject()._addToQueue(queue, 'abcd')
        self.assertEqual(len(queue), 3)

    def test_clearQueue(self):
        queue = deque()
        queue.extend('abcd')
        self.assertEqual(len(queue), 4)
        self.getSubject()._clearQueue(queue)
        self.assertEqual(len(queue), 0)

    def test_inWaiting(self):
        with self.assertRaises(serialConnection.SerialException):
            self.getSubject().inWaiting()
        self.assertTrue(self.getSubject().connect())
        self.assertEqual(self.getSubject().inWaiting(), 0)
        self.getSubject().addToReadQueue('abcd')
        self.assertEqual(self.getSubject().inWaiting(), 4)
        self.getSubject().readFromReadQueue(4)
        self.assertEqual(self.getSubject().inWaiting(), 0)
        self.getSubject().addToReadQueue('abcd')
        self.getSubject().readFromReadQueue(2)
        self.assertEqual(self.getSubject().inWaiting(), 2)

    def test__read(self):
        with self.assertRaises(serialConnection.SerialException):
            self.getSubject()._read()
        self.assertTrue(self.getSubject().connect())
        self.assertEqual(self.getSubject()._read(), bytearray())
        self.getSubject().addToReadQueue('abcd')
        self.assertEqual(self.getSubject()._read(4), bytes(bytearray('abcd')))

    def test__write(self):
        with self.assertRaises(serialConnection.SerialException):
            self.getSubject()._write(bytearray('abcd'))
        self.assertTrue(self.getSubject().connect())
        self.assertTrue(self.getSubject()._write(bytearray('abcd')))
        self.assertEqual(self.getSubject().readFromWriteQueue(4), bytearray('abcd'))
        self.getSubject().setWriteQueueSize(3)
        self.getSubject()._write(bytearray('abcd'))
        self.assertEqual(self.getSubject().readFromWriteQueue(4), bytearray('bcd'))

    def test_flushInput(self):
        with self.assertRaises(serialConnection.SerialException):
            self.getSubject().flushInput()
        self.assertTrue(self.getSubject().connect())
        self.getSubject().flushInput()
        self.assertEqual(self.getSubject().inWaiting(), 0)
        self.getSubject().addToReadQueue('abcd')
        self.getSubject().addToWriteQueue('abcd')
        self.assertEqual(self.getSubject().inWaiting(), 4)
        self.assertEqual(self.getSubject().getWriteQueueSize(), 4)
        self.getSubject().flushInput()
        self.assertEqual(self.getSubject().inWaiting(), 0)
        self.assertEqual(self.getSubject().getWriteQueueSize(), 4)

    def test_flushOutput(self):
        with self.assertRaises(serialConnection.SerialException):
            self.getSubject().flushOutput()
        self.assertTrue(self.getSubject().connect())
        self.getSubject().flushOutput()
        self.assertEqual(self._subject.getWriteQueueSize(), 0)
        self.getSubject().addToWriteQueue('abcd')
        self.getSubject().addToReadQueue('abcd')
        self.assertEqual(self.getSubject().inWaiting(), 4)
        self.assertEqual(self.getSubject().getWriteQueueSize(), 4)
        self.getSubject().flushOutput()
        self.assertEqual(self.getSubject().inWaiting(), 4)
        self.assertEqual(self.getSubject().getWriteQueueSize(), 0)
