# coding=utf-8
"""Test cases for the packet module"""

from pycultivator.connection import _packet, packet
from pycultivator.core.tests import UvaSubjectTestCase
import struct

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class _PacketTest(UvaSubjectTestCase):

    _subject_cls = _packet.BasePacket
    _abstract = True

    @classmethod
    def getSubjectClass(cls):
        """

        :rtype: pycultivator.connection.Packet.BasePacket
        """
        return super(_PacketTest, cls).getSubjectClass()

    def getSubject(self):
        """

        :rtype: pycultivator.connection.Packet.BasePacket
        """
        return super(_PacketTest, self).getSubject()

    def test_isFormat(self):
        f = lambda x: self.getSubjectClass().isFormat(x)
        for fmt in self.getSubjectClass().FORMATS:
            with self.subTest(fmt):
                self.assertTrue(f(fmt))
            with self.subTest(fmt):
                self.assertTrue(f("2{}".format(fmt)))
            with self.subTest(fmt):
                self.assertTrue(f("22{}".format(fmt)))
            with self.subTest(fmt):
                self.assertTrue(f("{}2".format(fmt)))
        self.assertFalse(f("D"))

    def test_sizeOfFormat(self):
        f = lambda x: self.getSubjectClass().sizeOfFormat(x)
        self.assertEqual(f("B"), 1)
        self.assertEqual(f("H"), 2)
        self.assertEqual(f("2B"), 2)
        self.assertEqual(f("2H"), 4)
        self.assertEqual(f(""), 0)
        with self.assertRaises(ValueError):
            f("2d")

    def test_isValid(self):
        f = lambda x,y: self.getSubjectClass().isValid(x, y)
        g = lambda x,y: bytearray(struct.pack(x, *y))
        self.assertTrue(f("BB", g("BB", [1, 2])))
        self.assertFalse(f("BB", g("BBB", [1, 2, 3])))
        self.assertTrue(f("3B", g("BBB", [1, 2, 3])))

    def test_byteArrayToHex(self):
        f = lambda x: self.getSubjectClass().byteArrayToHex(x)
        g = lambda x, y: bytearray(struct.pack(x, *y))
        self.assertEqual(f(g("BBB", [255, 0, 255])), "ff 00 ff")

    def test_byteArrayToInt(self):
        f = lambda x: self.getSubjectClass().byteArrayToInt(x)
        g = lambda x, y: bytearray(struct.pack(x, *y))
        self.assertEqual(f(g("BBB", [255, 0, 255])), "255 00 255")

    def test_byteArrayToString(self):
        f = lambda x: self.getSubjectClass().byteArrayToString(x)
        g = lambda x, y: bytearray(struct.pack(x, *y))
        self.assertEqual(f(g("BBB", [97, 98, 99])), "a  b  c")


class BasePacketTest(_PacketTest):

    _subject_cls = _packet.BasePacket
    _abstract = True


class PacketTest(BasePacketTest):

    _subject_cls = packet.Packet
    _abstract = False


class BaseCRCTest(UvaSubjectTestCase):

    _subject_cls = _packet.BaseCRC
    _abstract = False

    def test_crc(self):
        data = bytearray()
        self.assertEqual(
            self.subject.crc(data), 0
        )
        data.append(1)
        self.assertEqual(
            self.subject.crc(data), 0 ^ 1
        )
        data.append(2)
        self.assertEqual(
            self.subject.crc(data), 0 ^ 1 ^ 2
        )


class DefaultCRCPacket(packet.CRCPacket):

    def getFormats(self):
        return ["B"]

    def getValues(self):
        return [1]


class CRCPacketTest(BasePacketTest):

    _subject_cls = DefaultCRCPacket
    _abstract = False

    def test_write(self):
        ba = bytearray()
        ba.append(1)
        ba.append(0 ^ 1)

        self.assertEqual(
            self.subject.write(), ba
        )
