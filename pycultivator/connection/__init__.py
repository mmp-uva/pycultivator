# coding=utf-8
"""
The Connection package contains the IO drivers for communication with Devices.

A registry is used to account for all known
"""

from pycultivator.core import pcRegistry
from connection import Connection, FakeConnection, ConnectionException

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

_all__ = [
    "Connection", "FakeConnection", "ConnectionException"
]

# load registry class
registry = pcRegistry.URLRegistry(
    # pre-fill registry
    {
        #"psimc": "pycultivator.connection.serial.PsiController.PsiController",
        #"fake-psimc": "pycultivator.serial.PsiController.FakePsiController",
        #"reglo": "pycultivator.serial.RegloController.RegloController",
        #"fake-reglo": "pycultivator.serial.RegloController.FakeRegloController",
    },
    Connection
)
