"""The LabJack Connection module implements a pyCultivator connection to a LabJack U6 Device"""

import u6
from .connection import *
from time import sleep

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    "LabJackConnection",
    "FakeLabJackConnection",
    "LabJackException"
]


class LabJackConnection(Connection):
    """A connection driver to a labjack device"""

    namespace = "connection.labjack"
    default_settings ={
            "serialNr": None,
            "localID": None,
            'autoConnect': False,
        }

    FLASH_TIME_UNIT = 0.25
    TEMPERATURE_KELVIN = "kelvin"
    TEMPERATURE_CELSIUS = "celsius"
    TEMPERATURE_FAHRENHEIT = "fahrenheit"

    def __init__(self, settings=None, **kwargs):
        super(LabJackConnection, self).__init__(settings=settings, **kwargs)

    @classmethod
    def getFake(cls):
        return FakeLabJackConnection

    def getSerialNumber(self):
        """Returns the serial number of the (to be) connected device"""
        return self.getSetting("serialNr")

    def _setSerialNumber(self, serialNr):
        """Sets the serial number of the connected device"""
        self.setSetting("serialNr", serialNr)
        return self.getSerialNumber()

    def getLocalID(self):
        """Returns the local identification flag of the (to be) connected device"""
        return self.getSetting("localID")

    def _setLocalID(self, localID, write=False):
        """Sets the local identification flag of the (to be) connected device

        When a device is connected AND write is True, write local identifier to memory

        :param localID: The new localId of the connected device (or the Id that will be searched for)
        :type localID: int
        :param write: If a device is connected, write the new localId to the memory of the device
        :type write: bool
        """
        self.setSetting("localID", localID)
        if self.isConnected() and write:
            config = self.getDevice().configU6(localID)
            nlocalID = config.get("LocalID", None)
            if nlocalID != localID:
                self.getLog().error("Unable to set localID: {} != {}".format(nlocalID, localID))
        return self.getLocalID()

    def getDevice(self):
        """Returns the device to which this connection goes

        :return: The LabJack ComplexDevice
        :rtype: u6.U6
        """
        return super(LabJackConnection, self).getDevice()

    def willAutoConnect(self):
        """Returns whether this object will search available labjacks, when trying to connect

        :rtype: bool
        """
        return self.getSetting("autoConnect") is True

    def setAutoConnect(self, state):
        """Sets whether this object will search available labjacks

        :type state: bool
        :rtype: bool
        """
        self.setSetting("autoConnect", state is True)
        return self.willAutoConnect()

    def _createDevice(self):
        """Creates a new connection handle"""
        self._setDevice(u6.U6(autoOpen=False))
        return self.getDevice()

    def disconnect(self):
        result = False
        if not self.hasDevice():
            raise LabJackException("No device connected, so nothing to disconnect")
        if self.isConnected():
            self.getDevice().close()
            # update information
            self._isOpen = False
            self._setSerialNumber(None)
            self._setLocalID(None, write=False)
            result = not self.isConnected()
        return result

    def connectWithLocalID(self, localID=None):
        """Connect to a LabJack ComplexDevice using a localID, will fail if localID is not found"""
        if localID is None:
            localID = self.getLocalID()
        return self._connect(localID=localID)

    def connectWithSerialNr(self, serialNr=None):
        """Connect to a LabJack ComplexDevice using a serialNr, will fail if serialNr is not found"""
        if serialNr is None:
            serialNr = self.getSerialNumber()
        return self._connect(serialNr=serialNr)

    def connect(self, localID=None, serialNr=None, autoConnect=None):
        """Create a connection, will fill the settings automatically.

        It will first try to use the serialNumber (if available), if this failed it will try to use the localID (if
        set). If autoConnect is allowed and still no connection has been made, let driver find a connected LabJack.

        :param localID: The localID of the LabJack
        :param serialNr: The serial Number of the LabJack that should be connected
        :param autoConnect: Whether to allow the driver to detect any connected LabJack and connect to that one.
        :rtype: bool
        """
        result = False
        if localID is None:
            localID = self.getLocalID()
        if serialNr is None:
            serialNr = self.getSerialNumber()
        if autoConnect is None:
            autoConnect = self.willAutoConnect()
        # force autoConnect on; if both localID and serialID are missing
        if localID is None and serialNr is None:
            autoConnect = True
        # if not yet connected and serial number is available, try to connect
        if not result and serialNr is not None:
            result = self._connect(serialNr=serialNr)
        # if not yet connected and local ID is available, try to connect
        if not result and localID is not None:
            result = self._connect(localID=localID)
        # if not yet connect, let driver recognize labjack
        if not result and autoConnect:
            result = self._connect()
        return result

    def _connect(self, localID=None, serialNr=None):
        """Connect to a labjack

        :param localID: If localID is set, try to connect to this specific LabJack (cannot be set with serialNr)
        :param serialNr: If serialNr is set, try to conncet to this specific LabJack (cannot be set with localID)

        :rtype: bool
        """
        result = False
        if self.isConnected():
            raise LabJackException("Already connected, disconnect first")
        if localID is not None and serialNr is not None:
            raise LabJackException("LocalID and serialNr cannot be set at the same time")
        if not self.hasDevice():
            self._createDevice()
        try:
            self.getDevice().open(localId=localID, serial=serialNr)
            self.getDevice().getCalibrationData()
            self._isOpen = True
            result = self.isConnected()
            self.readInfo()
        except u6.LabJackException as lje:
            self.getLog().warning("LabJack Exception when trying to connect to LabJack:\n{}".format(lje))
        return result

    def readInfo(self):
        """Reads some basic configuration information of the u6"""
        result = {}
        if self.isConnected():
            result = self.getDevice().configU6()
            # update serial number
            self._setSerialNumber(result.get("SerialNumber", None))
            # update local ID
            self._setLocalID(result.get("LocalID", None), write=False)
        return result

    def identify(self):
        """Flickers the Status LED with 3 short flashes"""
        for i in range(3):
            self._flashShort()

    def jingle(self):
        """Send HELLO in MORSE code the Status LED"""
        self._flashPauseShort()
        [self._flashShort() for i in range(4)]  # H
        self._flashPauseLong()
        self._flashShort()  # E
        self._flashShort(); self._flashLong(); self._flashShort(); self._flashShort()   # L
        self._flashShort(); self._flashLong(); self._flashShort(); self._flashShort()   # L
        [self._flashLong() for i in range(3)]   # O

    def _flashPause(self, units=1):
        self.setLED(False)
        sleep(units * self.FLASH_TIME_UNIT)

    def _flashPauseShort(self):
        """Intra letter pause in MORSE"""
        self._flashPause(1)

    def _flashPauseMedium(self):
        """Intra word pause in MORSE"""
        self._flashPause(3)

    def _flashPauseLong(self):
        """Inter word pause in MORSE"""
        self._flashPause(7)

    # commands

    def ping(self):
        """Send a ping packet to the labjack"""
        self.assert_connected()
        result = False
        try:
            self.getDevice().ping()
            result = True
        except u6.LabJackException as lje:
            self.getLog().warning("Unable to send ping:\n>>>>\n{}\n<<<<<".format(lje))
        return result

    def readAIN(self, channel, resolutionIndex=12, gainIndex=1):
        """Reads the AIN Value of a channel on the labjack

        :param channel: The channel to measure
        :param resolutionIndex: ADC-resolution, 1 ~ 16-bit resolution, 12 ~ 22-bit resolution.
        :param gainIndex: Amplification of Signal. More amplification means smaller ranges = more precise.
        :rtype: float
        """
        self.assert_connected()
        result = 0.0
        try:
            result = self.getDevice().getAIN(channel, resolutionIndex=resolutionIndex, gainIndex=gainIndex)
        except u6.LabJackException as lje:
            self.getLog().error("Unable to measure AIN in {}:\n>>>>\n{}\n<<<<<".format(channel, lje))
        return result

    def setLED(self, state):
        self.assert_connected()
        result = False
        try:
            self.getDevice().setLED(state is True)
            result = True
        except Exception as e:
            self.getLog().error("Unable to set status LED:\n {}".format(e))
        # always return True
        return result

    def measureTemperature(self, units=TEMPERATURE_KELVIN):
        """Measures the internal temperature of the labjack"""
        self.assert_connected()
        result = 0.0
        try:
            result = self.getDevice().getTemperature()
            if units == self.TEMPERATURE_CELSIUS:
                result = self._kelvinToCelsius(result)
            if units == self.TEMPERATURE_FAHRENHEIT:
                result = self._kelvinToFahrenheit(result)
        except Exception as e:
            self.getLog().error("Unable to measure temperature:\n {}".format(e))
        return result

    def setDIOState(self, ioNum, state=1):
        self.assert_connected()
        result = 0.0
        try:
            result = self.getDevice().setDIOState(ioNum, state=state)
        except Exception as e:
            self.getLog().error("Unable to set DIO State:\n {}".format(e))
        return result

    def setDOState(self, ioNum, state=1):
        self.assert_connected()
        result = 0.0
        try:
            self.getDevice().setDOState(ioNum, state=state)
            result = True
        except Exception as e:
            self.getLog().error("Unable to set DO State:\n {}".format(e))
        return result

    def _kelvinToCelsius(self, kelvin):
        """Transforms kelvin into celsius"""
        return kelvin - 273.15

    def celciusToKelvin(self, celsius):
        """Transforms celsius to kelvin"""
        return celsius + 273.15

    def _kelvinToFahrenheit(self, kelvin):
        """Transforms kelvin to fahrenheit"""
        return kelvin * (9/5) + 459.67

    def fahrenheitToKelvin(self, fahrenheit):
        """Transforms fahrenheit to kelvin"""
        return (fahrenheit + 459.67) * (5/9)

    def _celsiusToFahrenheit(self, celsius):
        """Transforms celsius to fahrenheit"""
        return celsius * (9/5) + 32

    def _fahrenheitToCelsius(self, fahrenheit):
        """Transforms fahrenheit to celsius"""
        return (fahrenheit - 32) * (5/9)

    # def read(self, channel):
    #     """Read from a specific channel on the labjack"""
    #     raise NotImplementedError
    #
    # def write(self, channel, msg):
    #     """Write to a channel on the labjack"""
    #     raise NotImplementedError


class FakeLabJackConnection(LabJackConnection, FakeConnection):
    """Implements a fake LabJackConnection: Simulates a real connection"""

    def __init__(self, settings=None, **kwargs):
        LabJackConnection.__init__(self, settings=settings, **kwargs)
        # set default fake values
        self._fake_serialNr = 123
        self._fake_localID = 999
        self._fake_devNr = 1

    def getFakeSerialNumber(self):
        return self._fake_serialNr

    def _setFakeSerialNumber(self, serialNr):
        self._fake_serialNr = serialNr
        return self.getFakeSerialNumber()

    def getFakeLocalID(self):
        return self._fake_localID

    def _setFakeLocalID(self, localID):
        self._fake_localID = localID
        return self.getFakeLocalID()

    def getFakeDevNr(self):
        return self._fake_devNr

    def _connect(self, localID=None, serialNr=None):
        """Connect to a labjack

        :param localID: If localID is set, try to connect to this specific LabJack (cannot be set with serialNr)
        :param serialNr: If serialNr is set, try to conncet to this specific LabJack (cannot be set with
        localID)

        :rtype: bool
        """
        result = False
        if self.isConnected():
            raise LabJackException("Already connected, disconnect first")
        if localID is not None and serialNr is not None:
            raise LabJackException("LocalID and serialNr cannot be set at the same time")
        if not self.hasDevice():
            self._createDevice()
        self._isOpen = True
        # set localID and serialNr to given numbers, as to look like we have connected to right device
        if localID is not None:
            self._setFakeLocalID(localID)
        if serialNr is not None:
            self._setFakeSerialNumber(serialNr)
        self.readInfo()
        return self._isOpen

    def disconnect(self):
        result = False
        if not self.hasDevice():
            raise LabJackException("No device connected, so nothing to disconnect")
        if self.isConnected():
            # self.getDevice().close()
            # update information
            self._isOpen = False
            self._setSerialNumber(None)
            self._setLocalID(None, write=False)
            result = not self.isConnected()
        return result

    def readInfo(self):
        """Reads some basic configuration information of the u6"""
        result = {}
        if self.isConnected():
            # skip reading info
            # result = self.getDevice().configU6()
            result = {
                'SerialNumber': self.getFakeSerialNumber(),
                'LocalID': self.getFakeLocalID()
            }
            # update serial number
            self._setSerialNumber(result.get("SerialNumber", None))
            # update local ID
            self._setLocalID(result.get("LocalID", None), write=False)
        return result

    def _setLocalID(self, localID, write=False):
        """Sets the local identification flag of the (to be) connected device

        When a device is connected AND write is True, write local identifier to memory

        :param localID: The new localId of the connected device (or the Id that will be searched for)
        :type localID: int
        :param write: If a device is connected, write the new localId to the memory of the device
        :type write: bool
        """
        self.setSetting("localID", localID)
        if self.isConnected() and write:
            # config = self.getDevice().configU6(localID)
            self._setFakeLocalID(localID)
            # nlocalID = config.get("LocalID", None)
            nlocalID = self.getFakeLocalID()
            if nlocalID != localID:
                self.getLog().error("Unable to set localID: {} != {}".format(nlocalID, localID))
        return self.getLocalID()

    def ping(self):
        """Send a ping packet to the labjack"""
        self.assert_connected()
        return True

    def readAIN(self, channel, resolutionIndex=12, gainIndex=1):
        """Reads the AIN Value of a channel on the labjack

        :rtype: float
        """
        self.assert_connected()
        return 0.66667

    def setLED(self, state):
        self.assert_connected()
        return True

    def measureTemperature(self, units=LabJackConnection.TEMPERATURE_KELVIN):
        """Measures the internal temperature of the labjack"""
        self.assert_connected()
        result = 0.0
        try:
            result = self.celciusToKelvin(21)
            # result = self.getDevice().getTemperature()
            if units == self.TEMPERATURE_CELSIUS:
                result = self._kelvinToCelsius(result)
            if units == self.TEMPERATURE_FAHRENHEIT:
                result = self._kelvinToFahrenheit(result)
        except Exception as e:
            self.getLog().warning("Unable to measure temperature:\n {}".format(e))
        return result

    def setDIOState(self, ioNum, state=1):
        self.assert_connected()
        return True

    def setDOState(self, ioNum, state=1):
        self.assert_connected()
        return True


class LabJackException(ConnectionException):
    """An exception raised by a labjack connection"""

    pass
