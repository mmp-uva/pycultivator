import re
import struct

from pycultivator.core import BaseObject, PCException

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class _Packet(BaseObject):
    """ABSTRACT Packet class: interface defining what a basic class looks like"""

    # Constants
    FORMAT_BYTE = "b"
    FORMAT_UBYTE = "B"
    FORMAT_SHORT = "h"
    FORMAT_USHORT = "H"
    FORMAT_INT = "i"
    FORMAT_UINT = "I"
    FORMAT_FLOAT = "f"
    FORMAT_STRING = "s"
    FORMAT_CHAR = "c"
    FORMATS = [FORMAT_BYTE, FORMAT_UBYTE, FORMAT_SHORT, FORMAT_USHORT,
               FORMAT_INT, FORMAT_UINT, FORMAT_FLOAT, FORMAT_STRING,
               FORMAT_CHAR]

    FORMAT_TEST = re.compile("[><!=]?[0-9]*[{}]+".format(''.join(FORMATS)))

    def __init__(self):
        super(_Packet, self).__init__()
        self._data = None

    def getData(self):
        return self._data

    @property
    def data(self):
        return self.getData()

    @classmethod
    def isFormat(cls, fmt):
        """Returns whether the given format consists of numbers and format characters

        :param fmt: format specifier
        :type fmt: str
        :return: Whether the format is known
        :rtype: bool
        """
        return cls.FORMAT_TEST.match(fmt) is not None

    @classmethod
    def sizeOfFormat(cls, fmt):
        """Calculates the number of bytes required by the format

        :param fmt: The format string
        :type fmt: str
        :return: The format length in bytes
        :rtype: int
        """
        if not fmt == "" and not cls.isFormat(fmt):
            raise ValueError('Unknown format - %s -' % fmt)
        return struct.calcsize(fmt)

    @classmethod
    def isValid(cls, fmt, data):
        """Checks if the given data is valid according to the format used"""
        return isinstance(data, bytearray) and cls.sizeOfFormat(fmt) == len(data)

    @staticmethod
    def byteArrayToHex(data):
        """Converts each byte in the given bytearray to a 2 digit hex value in a nice string format.

        :param data: data to convert
        :type data: bytearray
        :return: Hex translation of bytes
        :rtype: str
        """
        return " ".join("%02x" % i for i in data)

    @staticmethod
    def byteArrayToInt(data):
        """Converts each byte of the given bytearry to a 2 digit int value in a nice string format.

        :param data: data to convert
        :type data: bytearray
        :return: Int translation of bytes
        :rtype: str
        """
        return " ".join("%02i" % i for i in data)

    @staticmethod
    def byteArrayToBit(data):
        """Converts the given bytearray to a bit sequenc"""
        return " ".join("{:#8b}".format(i) for i in data)

    @staticmethod
    def byteArrayToString(data):
        """Converts each byte to readable character or int, and formats it in a nice string.

        :param data: data to convert
        :type data: bytearray
        :return: Character translation of bytes
        :rtype: str
                """
        s = ""
        for b in data:
            if int(b) >= 30:
                s += "{: <2c} ".format(b)  # OK, translate!
            else:
                s += "{:02d} ".format(b)  # probably an integer, keep it
        return s.strip(" ")


class BasePacket(_Packet):
    """Module implementing a packet"""

    def __init__(self):
        super(BasePacket, self).__init__()
        self._data = bytearray()

    def write(self):
        """Writes the data in the packet to a ByteArray"""
        return self.getData()

    def __eq__(self, other):
        result = False
        if isinstance(other, BaseTemplate):
            result = other.getData() == self.getData()
        if isinstance(other, BasePacket):
            other = other.write()
        if isinstance(other, bytearray):
            result = self.write() == other
        return result


class BaseTemplate(_Packet):
    """Module implementing a template"""

    def __init__(self):
        super(BaseTemplate, self).__init__()

    def read(self, data):
        """Reads the data into the packet"""
        self._data = data

    def __eq__(self, other):
        result = self.getData() == other
        if isinstance(other, BasePacket):
            result = other.getData() == self.getData()
        if isinstance(other, BaseTemplate):
            result = self.getData() == other.getData()
        return result


class BasePacketException(PCException):
    """An exception raised by BasePacket instances"""

    def __init__(self, msg):
        super(BasePacketException, self).__init__(msg)


class BaseCRC(BaseObject):
    """Class implementing crc calculation"""

    @classmethod
    def crc(cls, data):
        """Calculates the crc value of a bytearray

        :param data: The data to calculate the crc of
        :type data: bytearray
        :rtype: int
        """
        crc = 0
        try:
            for i in data:
                crc ^= i
        except TypeError:
            cls.getLog().error("Unable to calculate crc from given array- %s" % data)
        return crc


class BaseForm(BaseObject):
    """Class implementing a form structure using fields"""

    def __init__(self, names=None, fmts=None, values=None):
        super(BaseForm, self).__init__()
        self._data = {}
        self._indexes = []
        if names is None:
            names = []
        if fmts is None:
            fmts = []
        if values is None:
            values = []
        if len(names) != len(fmts):
            raise BaseFormException("Names list is not equal to Formats length")
        for idx, name in enumerate(names):
            fmt = fmts[idx]
            value = None
            if idx < len(values):
                value = values[idx]
            if not self.hasField(name):
                self.addField(name, fmt, value)
        # end of init

    def getFields(self):
        return self._data

    def getFieldNames(self):
        return self._indexes

    def getFieldFormats(self):
        return [self.getFieldFormat(name) for name in self.getFieldNames()]

    def getFormats(self):
        return self.getFieldFormats()

    def getFieldValues(self):
        return [self.getFieldValue(name) for name in self.getFieldNames()]

    def getValues(self):
        return self.getFieldValues()

    def getField(self, name=None, index=None):
        result = (None, None)
        if name is None and index is None:
            raise BaseFormException("Need at least a name or index")
        if index is not None:
            if not (-len(self.getFieldNames()) <= index < len(self.getFieldNames())):
                raise BaseFormException("Invalid index: {}".format(index))
            name = self.getFieldNames()[index]
        if name is not None:
            if not self.hasField(name):
                raise BaseFormException("Unknown name: {}".format(name))
            result = self.getFields()[name]
        return result

    def getFieldFormat(self, name=None, index=None):
        return self.getField(name=name, index=index)[0]

    def getFieldValue(self, name=None, index=None):
        return self.getField(name=name, index=index)[1]

    def addField(self, name, fmt, value=None, index=None):
        if self.hasField(name):
            raise BaseFormException("Name already defined")
        self.getFields()[name] = (fmt, value)
        if index is not None:
            self.getFieldNames().insert(index, name)
        else:
            self.getFieldNames().append(name)

    def setField(self, name, fmt=None, value=None):
        if not self.hasField(name):
            raise BaseFormException("Unknown name: {}".format(name))
        if fmt is None:
            fmt = self.getFieldFormat(name)
        if value is None:
            value = self.getFieldValue(name)
        self.getFields()[name] = (fmt, value)

    def hasField(self, name):
        return name in self.getFieldNames()


class BaseFormException(BasePacketException):

    def __init__(self, msg):
        super(BaseFormException, self).__init__(msg)
