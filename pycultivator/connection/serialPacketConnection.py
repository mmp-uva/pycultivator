"""Module implements a packet based reading of a serial connection"""

from pycultivator.connection import serialConnection

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class SerialPacketConnection(serialConnection.SerialConnection):
    """A class capable of reading the serial connection on a packet basis"""

    default_settings = {
        "packet.separator": ""
    }

    def __init__(self, settings=None, **kwargs):
        super(SerialPacketConnection, self).__init__(settings=settings, **kwargs)

    @classmethod
    def getFake(cls):
        return FakeSerialPacketConnection

    def getSeparator(self):
        return self.getSetting("packet.separator")

    def setSeparator(self, sep):
        self.setSetting("packet.separator", sep)

    def receive(self, template, attempts=None):
        """Tests each received packet"""
        if attempts is None:
            attempts = self.getReadAttempts()
        template.clear()
        finished = False
        while not finished and attempts > 0:
            packets = self._read_packets()
            for p in packets:
                if template.check(p):
                    template.read(p)
                    finished = True
            attempts -= 1
        self.getDevice().reset_input_buffer()
        return template

    def _read_packets(self, sep=None):
        packets = []
        if sep is None:
            sep = self.getSeparator()
        data = self._read_all()
        self.logData(data, "Received data:")
        if data is not None and data != "":
            packets = self._split_packets(data, sep)
        return packets

    def _split_packets(self, data, sep):
        """Reads all data from the serial link and splits it into separate packets

        :type data: bytearray
        :type sep: bytearray
        :return: list of bytearrays
        :rtype: list[bytearray]
        """
        results = []
        if sep is None or sep == "":
            results = [data]
        else:
            packets = data.split(sep)
            # prepend separator to each split
            for packet in packets:
                if len(packet) > 0:
                    results.append(sep + packet)
        return results


class FakeSerialPacketConnection(serialConnection.FakeSerialConnection, SerialPacketConnection):
    """Implementing a fake serial connection"""

    def __init__(self, settings=None, **kwargs):
        super(FakeSerialPacketConnection, self).__init__(settings=settings, **kwargs)

    def receive(self, template, attempts=None):
        return SerialPacketConnection.receive(self, template, attempts)


class SerialPacketConnectionException(serialConnection.SerialException):
    """Exception raised by SerialPacketConnection classes"""

    def __init__(self, msg):
        super(SerialPacketConnectionException, self).__init__(msg)
