"""The Serial Connection module implements a pyCultivator connection interface to serial devices"""

from connection import *
import packet, serial, os
from time import sleep
from collections import deque
import termios

from pycultivator.core import pcLogger

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    "SerialConnection",
    "FakeSerialConnection",
    "SerialException"
]


class SerialConnection(Connection):
    """A driver class implementing a standardized API to Serial Devices"""

    OS_UNKNOWN = 0
    OS_WINDOWS = 1
    OS_LINUX = 2

    namespace = "connection.serial"
    default_settings = {
            "port": None,
            "rate": 115200,
            'os': OS_UNKNOWN,
            'readAttempts': 3,
            'readTimeout': 0.5,
            'writeTimeout': None,
            'byteSize': serial.EIGHTBITS,
            'parity': serial.PARITY_NONE,
            'stopBits': serial.STOPBITS_ONE,
            'rtscts': False,
            'dsrdtr': False,
            'xonxoff': False,
        }

    def __init__(self, settings=None, **kwargs):
        super(SerialConnection, self).__init__(settings=settings, **kwargs)
        name = self.OS_UNKNOWN
        if os.name == 'posix':
            name = self.OS_LINUX
        if os.name == 'nt':
            name = self.OS_WINDOWS
        self.setSetting("os", name)

    @classmethod
    def getFake(cls):
        return FakeSerialConnection

    def getPort(self):
        """Returns the port used by this connection driver"""
        return self.getSetting("port")

    def _setPort(self, port):
        """Sets the port"""
        self.setSetting("port", port)
        return self.getPort()

    def getBaudRate(self):
        return self.getSetting("rate")

    def setBaudRate(self, rate):
        self.setSetting("rate", rate)
        return self.getBaudRate()

    def getTimeout(self):
        return self.getReadTimeout()

    def getReadTimeout(self):
        return self.getSetting("readTimeout")

    def getWriteTimeout(self):
        return self.getSetting("writeTimeout")

    def getReadAttempts(self):
        return self.getSetting("readAttempts")

    def getReadPause(self):
        return self.getSetting("readPause")

    def getByteSize(self):
        return self.getSetting("byteSize")

    def getParity(self):
        return self.getSetting("parity")

    def getStopBits(self):
        return self.getSetting("stopBits")

    def getRTSCTS(self):
        return self.getSetting("rtscts")

    def getDSRDTR(self):
        return self.getSetting("dsrdtr")

    def getXonXoff(self):
        return self.getSetting("xonxoff")

    def getDevice(self):
        """Returns the device to which this connection goes

        :return: The Serial Connection
        :rtype: serial.Serial
        """
        return super(SerialConnection, self).getDevice()

    def _createDevice(self):
        """Creates a new device instance"""
        self._setDevice(serial.Serial())
        return self.getDevice()

    def disconnect(self):
        """Close the serial connection managed by this object"""
        result = False
        if self.isConnected():
            self.getDevice().close()
            # update information
            self._isOpen = False
            result = not self.isConnected()
        return result

    def connect(self, port=None):
        """Open a serial connection to a ComplexDevice

        :rtype: bool
        """
        result = False
        if port is None:
            port = self.getPort()
        if not result and port is not None:
            result = self._connect(port=port)
        # Allow for other connection options
        return result

    def _connect(self, port=None):
        """Creates the actual connection

        :rtype: bool
        """
        # first check if we are connected
        if self.isConnected():
            raise SerialException("Port is already open, disconnect first")
        # if port is set, store it as setting
        if port is not None:
            self._setPort(port)
        # check if a port was set at all
        if self.getPort() is None:
            raise SerialException("No port set, cannot open connection.")
        if not self.hasDevice():
            self._createDevice()
        try:
            # first configure the port with all the settings
            self._configure()
            # then open
            self.getDevice().open()
            self.getLog().debug("Opened serial connection to {}".format(self.getPort()))
            self._isOpen = True
        except OSError as os:
            self.getLog().critical("OSError: {}".format(os))
        except serial.SerialException as se:
            self.getLog().critical("Unable to open port: {}".format(se))
        except ValueError as ve:
            self.getLog().critical("Unable to open port with these options {}".format(ve))
        return self.isConnected()

    def _configure(self, settings=None, **kwargs):
        """Configure the port based on supplied arguments or default values

        :param settings: Dictionary with settings for the serial device
        :type settings: dict or None
        :return: Reference to this instance
        :rtype: pycultivator.connection.SerialConnection.SerialConnection
        """
        if self.isConnected():
            raise SerialException("Unable to configure an open port")
        if not self.hasDevice():
            raise SerialException("No device connected, nothing to be configured")
        if settings is None:
            settings = {}
        # now reduce settings
        settings = self.settings.reduce_dict(settings, namespace=self.settings.namespace)
        # overwrite with kwargs
        settings.update(kwargs)
        # merge with internal settings
        self._settings = self.settings.update(settings)
        # now merge/override with kwargs
        self._settings = self.settings.update(settings)
        self.getDevice().port = self.getPort()
        self.getDevice().baudrate = self.getBaudRate()
        self.getDevice().timeout = self.getTimeout()
        self.getDevice().writeTimeout = self.getWriteTimeout()
        self.getDevice().bytesize = self.getByteSize()
        self.getDevice().parity = self.getParity()
        self.getDevice().stopbits = self.getStopBits()
        self.getDevice().rtscts = self.getRTSCTS()
        self.getDevice().dsrdtr = self.getDSRDTR()
        self.getDevice().xonxoff = self.getXonXoff()
        return self

    def reconfigure(self, settings=None, autoOpen=True, **kwargs):
        """Reconfigures the port; closes the connection first (if open) and open a new connection

        :param settings: PCSettings to be applied to the serial connection
        :type settings: dict[str, object]
        :param autoOpen: Whether the open the connection after configuring
        :type autoOpen: bool
        :param kwargs: Extra arguments passed to the serial connection
        :rtype: bool
        """
        if self.isConnected():
            self.disconnect()
        self._configure(settings=settings, **kwargs)
        if autoOpen is True:
            self._connect()
        # report success if autoOpen is false or if reconnect
        return not autoOpen or self.isConnected()

    def _logData(self, data, msg=None, level=pcLogger.LEVELS["DEBUG"]):
        """
        Print a log message with the bytearray when debugging is on
        :param data: Data received or sent
        :type data: bytearray
        :param msg: Optional message to print
        :type msg: str
        """
        # check if we should invest energy in logging
        if self.log.getEffectiveLevel() == level:
            if msg is None:
                msg = ""
            msg = "{}\n\t{}".format(msg, packet.Packet.byteArrayToHex(data))
            msg = "{}\n\t{}".format(msg, packet.Packet.byteArrayToInt(data))
            msg = "{}\n\t{}".format(msg, packet.Packet.byteArrayToString(data))
            self.log.log(msg=msg, level=level)  # write to log

    def __str__(self):
        """
        Return string representation of object
        :return: String representation
        :rtype: str
        """
        return (
            "Serial [{state}]:\n <port={port}, rate={rate}, byteSize={byteSize}, parity={parity}, "
            "stopBits={stopBits}, timeout={timeout}, writeTimeout={writeTimeout}, readTimeout={readTimeout}, "
            "rtscts={rtscts}, dsrdtr={dsrdtr}, xonxoff={xonxoff}".format(
                state="OPEN" if self.isConnected() else "CLOSED",
                port=self.getPort(), rate=self.getBaudRate(),
                byteSize=self.getByteSize(), parity=self.getParity(),
                stopBits=self.getStopBits(), timeout=self.getTimeout(),
                writeTimeout=self.getWriteTimeout(), readTimeout=self.getReadTimeout(),
                rtscts=self.getRTSCTS(), dsrdtr=self.getDSRDTR(),
                xonxoff=self.getXonXoff()
            )
        )

    #
    # Commands
    #

    def send(self, packet):
        """Write data on the serial connection to the device.

        :param packet: Packet to be written on the serial connection.
        :type packet: pycultivator.connection.Packet.Packet
        :return: Whether all the data was written
        :rtype: bool
        """
        if not packet.check():
            raise SerialException("Packet is not valid, unable to send it")
        return self._write(packet.write())

    def _write(self, data):
        """Internal method for writing a bytearray on the serial connection

        :raises: SerialException

        :type data: bytearray
        :rtype: bool
        """
        self.assert_connected()
        length = 0
        try:
            length = self.getDevice().write(data)
        except IOError:
            raise SerialException("IOError when writing to {}".format(self.getPort()))
        except termios.error:
            raise SerialException("Termios Error when writing to {}".format(self.getPort()))
        self.logData(data, msg="Sent message:")
        return length == len(data)

    def receive(self, template, attempts=None):
        """Read data from the serial connection.

        :type template: pycultivator.connection.Template.Template
        :rtype: pycultivator.connection.Template.Template
        """
        if attempts is None:
            attempts = self.getReadAttempts()
        template.clear()
        data = bytearray()
        while template.isEmpty() and attempts > 0:
            data.extend(self._read(len(template)))
            if template.check(data):
                template.read(data)
            attempts -= 1
        self.getDevice().reset_input_buffer()
        self.logData(data, "Received data:")
        return template

    def _read(self, length=-1):
        """Read data from the serial connection

        :param length: maximum number of bytes to read
        :type length: int
        :return: The bytes read or an empty bytearray if nothing was received
        :rtype: bytearray
        """
        self.assert_connected()
        if not (length == -1 or length > 0):
            raise ValueError("Read length must be -1 or a positive number")
        return bytearray(self.getDevice().read(length))

    def _read_all(self):
        """Read data from the serial connection, until readTimeout is reached

        :return: The bytes read or an empty bytearray if nothing was received
        :rtype: bytearray
        """
        self.assert_connected()
        result = bytearray()
        attempt, no_data = 0, 0
        while attempt < 100 and no_data < 5:
            n = self._in_waiting()
            if n > 0:
                data = self._read(n)
                result.extend(data)
                no_data = 0
            else:
                no_data += 1
            attempt += 1
            sleep(0.01)
        return result

    def _read_until(self, stop):
        """Read all data until readTimeout is reached or stop char is found"""
        self.assert_connected()
        result = bytearray()
        attempt, no_data, batch = 0, 0, bytearray()
        while attempt < 100 and no_data < 5:
            waiting = self._in_waiting()
            if waiting > 0:
                batch = self.getDevice().read(waiting)
                result.extend(batch)
                no_data = 0
            else:
                no_data += 1
            if batch.endswith(stop):
                break
            attempt += 1
            sleep(0.01)
        result = result.rstrip(stop)
        return result

    def _in_waiting(self):
        self.assert_connected()
        result = 0
        try:
            result = self.getDevice().in_waiting
        except IOError:
            pass
        except termios.error:
            pass
        return result

    def send_receive(self, packet, template, pause=0, attempts=None):
        """Send a packet and than try to receive the response using the template

        :rtype: pycultivator.connection.Template.Template
        """
        result = False
        try:
            result = self.send(packet=packet)
        except SerialException:
            pass
        if result:
            if pause > 0:
                sleep(pause)
            try:
                self.receive(template=template, attempts=attempts)
            except SerialException:
                pass
        return template

    def logData(self, data, msg, level=1):
        msg = "{}\nHEX:\t{}".format(msg, packet.Packet.byteArrayToHex(data))
        msg = "{}\nINT:\t{}".format(msg, packet.Packet.byteArrayToInt(data))
        msg = "{}\nSTR:\t{}".format(msg, packet.Packet.byteArrayToString(data))
        self.getLog().log(level=level, msg=msg)


class FakeSerialConnection(SerialConnection, FakeConnection):
    """A fake serial connection"""

    def __init__(self, *args, **kwargs):
        SerialConnection.__init__(self, *args, **kwargs)
        self._read_queue = deque()
        self._write_queue = deque()

    def _connect(self, port=None):
        # first check if we are connected
        if self.isConnected():
            raise SerialException("Port is already open, disconnect first")
        # if port is set, store it as setting
        if port is not None:
            self._setPort(port)
        # check if a port was set at all
        if self.getPort() is None:
            raise SerialException("No port set, cannot open connection.")
        if not self.hasDevice():
            self._createDevice()
        # self.clearReadQueue()
        # self.clearWriteQueue()
        # self._serial.open()
        self.log.debug("Opened serial connection to {}".format(self.getPort()))
        self._isOpen = True
        return self.isConnected()

    def disconnect(self):
        """Close the serial connection managed by this object"""
        result = False
        if not self.hasDevice():
            raise SerialException("Unable to disconnect: no connection to close")
        if self.isConnected():
            # self.getDevice().close()
            # update information
            self._isOpen = False
            #self._setPort(None)
            result = not self.isConnected()
        return result

    @staticmethod
    def _readFromQueue(queue, length=None):
        """Reads `length` number of bytes from the queue"""
        result = bytearray()
        if length is None or length > len(queue) or length < 0:
            length = len(queue)
        while length > 0:
            result.append(queue.popleft())
            length -= 1
        return result

    @staticmethod
    def _setQueueSize(queue, size=None):
        """Sets the maximum size (in bytes) of the queue"""
        queue = deque(queue, maxlen=size)
        return queue

    @staticmethod
    def _getQueueSize(queue):
        """Returns the actual size (in bytes) of the queue"""
        return len(queue)

    @staticmethod
    def _clearQueue(queue):
        """Clears the queue and returns the new queue size"""
        queue.clear()
        return len(queue)

    @staticmethod
    def _addToQueue(queue, data):
        """Adds data to the queue"""
        queue.extend(bytearray(data))
        return len(queue)

    def setReadQueueSize(self, size=None):
        """Sets the maximum size of the read queue (for testing purposes)"""
        self._read_queue = self._setQueueSize(self._read_queue, size)
        return self._read_queue

    def readFromReadQueue(self, length=None):
        """Reads from the read queue (for testing purposes)"""
        return self._readFromQueue(self._read_queue, length=length)

    def addToReadQueue(self, data):
        """Writes data to the read queue (for testing purposes)"""
        return self._addToQueue(self._read_queue, data)

    def clearReadQueue(self):
        """Clears the read queue (for testing purposes)"""
        return self._clearQueue(self._read_queue)

    def getReadQueueSize(self):
        """Returns the actual size of the read queue"""
        return self._getQueueSize(self._read_queue)

    def setWriteQueueSize(self, size=None):
        """Sets the maximum size of the write queue (for testing purposes)"""
        self._write_queue = self._setQueueSize(self._write_queue, size)
        return self._write_queue

    def readFromWriteQueue(self, length=None):
        """Reads from the write queue (for testing purposes)"""
        return self._readFromQueue(self._write_queue, length=length)

    def addToWriteQueue(self, data):
        """Writes data to the write queue (for testing purposes)"""
        return self._addToQueue(self._write_queue, data)

    def clearWriteQueue(self):
        """Clears the write queue (for testing purposes)"""
        return self._clearQueue(self._write_queue)

    def getWriteQueueSize(self):
        """Returns the actual size of the write queue"""
        return self._getQueueSize(self._write_queue)

    def inWaiting(self):
        """Returns the number of bytes waiting to be read """
        if not self.isConnected():
            raise SerialException("Connection is closed, unable to measure buffer size")
        return self.getReadQueueSize()

    def flushInput(self):
        """Clears the input buffer (i.e. the read queue)"""
        if not self.isConnected():
            raise SerialException("Connection is closed, unable to flush input")
        self.clearReadQueue()

    def flushOutput(self):
        """Clears the output buffer (i.e. the write queue)"""
        if not self.isConnected():
            raise SerialException("Connection is closed, unable to flush output")
        self.clearWriteQueue()

    def _read(self, length=-1):
        """Reads from the serial connection"""
        if not self.isConnected():
            raise SerialException("Unable to read, no connection")
        return self.readFromReadQueue(length=length)

    def _read_all(self):
        """Reads all data until readTimeout"""
        result = bytearray()
        if not self.isConnected():
            raise SerialException("Unable to read, no connection")
        attempt, no_data = 0, 0
        while attempt < 100 and no_data < 10:
            if self.getReadQueueSize() > 0:
                result.extend(self.readFromReadQueue(self.getReadQueueSize()))
                no_data = 0
            else:
                no_data += 1
            attempt += 1
        return result

    def _write(self, data):
        """Writes data on the serial connection"""
        if not self.isConnected():
            raise SerialException("Connection is closed, unable to write")
        self.addToWriteQueue(data=data)
        return True


class SerialException(ConnectionException):
    """A Exception raised by the SerialException classes"""

    pass
