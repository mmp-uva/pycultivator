"""Module implementing a dynamic template"""

from pycultivator.connection import template, _packet

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class FormTemplate(template.Template, _packet.BaseForm):
    """A template using fields"""

    def __init__(self, names=None, fmts=None):
        super(FormTemplate, self).__init__(names=names, fmts=fmts)

    def readFields(self, data, offset=0):
        results = {}
        for name in self.getFieldNames():
            value, offset = self.readField(data, name=name, offset=offset)
            results[name] = value
        return results, offset

    def readField(self, data, name, offset=0):
        fmt = self.getFieldFormat(name)
        value, offset = self.readValue(data, fmt, offset=offset)
        self.setField(name, value=value)
        return value, offset


class FormTemplateException(template.TemplateException, _packet.BaseFormException):
    """Exception raised by a FormTemplate"""

    def __init__(self, msg):
        super(FormTemplateException, self).__init__(msg)


class SandwichFormTemplate(FormTemplate, template.SandwichTemplate):
    """A form packet with a fixed header and footer"""

    def __init__(self, names=None, fmts=None):
        super(SandwichFormTemplate, self).__init__(names=names, fmts=fmts)

    def getDataFormats(self):
        return self.getFieldFormats()

    def getDataValues(self):
        return self.getFieldValues()

    def readData(self, data, offset=0):
        return self.readFields(data, offset=offset)
