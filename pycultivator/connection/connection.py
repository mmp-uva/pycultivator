"""
The Connection module defines the the basic infrastructure for the connection API.
"""

from pycultivator.core import ConfigurableObject, PCException

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ =[
    "Connection",
    "FakeConnection",
    "ConnectionException"
]


class Connection(ConfigurableObject):
    """
    The Connection class provides the bare definition for a connection between a device and the software

    A Connection implements:
    - connect
    - disconnect
    - isConnected
    """

    namespace = "connection"
    default_settings = {
        # add default settings for this class
        "auto_connect": False
    }

    # fake = FakeConnection

    def __init__(self, settings=None, **kwargs):
        super(Connection, self).__init__(settings=settings, **kwargs)
        self._isOpen = False
        self._device = None

    def __del__(self):
        """Close connection properly"""
        if self.isConnected():
            self.getLog().warning("Disconnect device before destroying object")
            self.disconnect()

    @classmethod
    def getFake(cls):
        """Returns the fake connection counter part of this connection"""
        return FakeConnection

    @classmethod
    def isFake(cls):
        return issubclass(cls, FakeConnection)

    @classmethod
    def loadFake(cls, *args, **kwargs):
        return cls.getFake()(*args, **kwargs)

    def isConnected(self):
        """Returns whether there is a connection between the software and an external device"""
        return self.hasDevice() and self._isOpen

    def getDevice(self):
        """ Returns the handle to the device used by this connection

        :return: The device that is reached with this connection
        :rtype: object
        """
        return self._device

    def hasDevice(self):
        """Whether this connection has a device handle

        :return: Whether this connection has a device
        :rtype: bool
        """
        return self.getDevice() is not None

    def _setDevice(self, device):
        """Sets the handle of the device to which the computer is connected"""
        self._device = device
        return self.getDevice()

    def assert_connected(self):
        """Test if we are connected"""
        if not self.isConnected():
            if self.settings.get('auto_connect', default=False):
                self.connect()
        if not self.isConnected():
            raise ConnectionException("Not connected to device, while a established connection is required")
        return True

    def connect(self):
        """Estabilishes a connection between the software and an external device"""
        raise NotImplementedError

    def disconnect(self):
        """Destroys the connection between the software and an external device"""
        raise NotImplementedError

    def copy(self, memo=None):
        result = super(Connection, self).copy(memo=memo)
        if self.hasDevice():
            import copy
            result._device = copy.deepcopy(self.getDevice())
        return result


class FakeConnection(Connection):
    """A fake connection object"""

    def __init__(self, settings=None, **kwargs):
        super(FakeConnection, self).__init__(settings=settings, **kwargs)

    def connect(self):
        """Establishes a connection between the software and an external device"""
        raise NotImplementedError

    def disconnect(self):
        """Destroys the connection between the software and an external device"""
        raise NotImplementedError


class ConnectionException(PCException):
    """An exception raised by an Connection class"""

    def __init__(self, msg):
        super(ConnectionException, self).__init__(msg=msg)
