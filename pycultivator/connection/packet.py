# coding=utf-8
"""The Packet Module provides classes that encapsulate information and transforms it into a bytearray.

These classes are used for sending packets over (serial) connections.

The following classes are defined:

- BasePacket: Abstract class defining common base methods for all packet classes.
- Packet: Class, for writing static packets.
- Template: Class, for reading static packets.

- BaseCRC: Extension for packets and templates to implement CRC support
- BaseForm: Extension for packets and template to implement Dynamic packets
"""

import struct
from pycultivator.connection import _packet
from pycultivator.core.pcParser import assert_range

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Packet(_packet.BasePacket):
    """"""

    def __init__(self):
        super(Packet, self).__init__()

    def getFormats(self):
        """ Return a list of the formats in this packet

        :return: List of format specification
        :rtype: list[str]
        """
        return []

    def getPacketFormat(self):
        """Return the sequence of formats of this packet

        :rtype: str
        """
        return "".join(self.getFormats())

    def getValues(self):
        """ Return a list of values in this packet

        :return: list of values
        :rtype: list[str | int | float]
        """
        return []

    def getData(self):
        return self.getValues()

    def check(self):
        result = False
        try:
            self.writeValues(self.getFormats(), self.getValues())
            result = True
        except ValueError:
            pass #
        except struct.error:
            pass
        return result

    def write(self):
        """Writes the packet"""
        if not self.check():
            raise PacketException("Unable write packet; packet is not valid")
        return self.writeValues(self.getFormats(), self.getValues())

    @classmethod
    def writeValues(cls, formats, values, p=None):
        if p is None:
            p = bytearray()
        if len(formats) != len(values):
            raise ValueError("Number of formats is not equal to number of values")
        for idx, fmt in enumerate(formats):
            cls.writeValue(fmt, values[idx], p)
        return p

    @classmethod
    def writeValue(cls, fmt, value, p=None):
        if p is None:
            p = bytearray()
        fmt_type = fmt[-1]
        try:
            if cls.sizeOfFormat(fmt) > cls.sizeOfFormat(fmt_type) and fmt_type != cls.FORMAT_STRING:
                p.extend(struct.pack(fmt, *value))
            else:
                p.extend(struct.pack(fmt, value))
        except struct.error:
            cls.getLog().error("Unable to pack {} into {}".format(value, fmt))
        return p

    @classmethod
    def writeByte(cls, value, p=None):
        assert_range(value, 0, (1 << 8)-1, name="value", include=True)
        return cls.writeValue('B', value, p)

    @classmethod
    def writeShort(cls, value, p=None):
        assert_range(value, 0, (1 << 16)-1, name="value", include=True)
        return cls.writeValue('H', value, p)

    @classmethod
    def writeChar(cls, value, p=None):
        if not isinstance(value, str):
            value = str(value)
        if not len(value) == 1:
            raise ValueError("Cannot pack a value of size larger than 1")
        return cls.writeValue('c', value, p)

    def __str__(self):
        result = "INVALID PACKET: [{}]".format(",".join("{}".format(i) for i in self.getValues()))
        if self.check():
            result = self.byteArrayToHex(self.write())
        return result

    def __len__(self):
        return self.sizeOfFormat(self.getPacketFormat())


class CRCPacket(Packet, _packet.BaseCRC):
    """Implementing a CRC writing packet"""

    def write(self):
        result = super(CRCPacket, self).write()
        return self.writeCRC(result)

    @classmethod
    def writeCRC(cls, p=None):
        if p is None:
            p = bytearray()
        p = cls.writeByte(cls.crc(p), p)
        return p


class SandwichPacket(Packet):
    """Implementing a sandwich structure: Header, Data, Footer"""

    def getFormats(self):
        header = self.getHeaderFormats()
        data = self.getDataFormats()
        footer = self.getFooterFormats()
        return header + data + footer

    def getValues(self):
        header = self.getHeaderValues()
        data = self.getDataValues()
        footer = self.getFooterValues()
        return header + data + footer

    def write(self):
        p = bytearray()
        self.writeHeader(p)
        self.writeData(p)
        self.writeFooter(p)
        return p

    def getHeaderFormats(self):
        return []

    def getHeaderValues(self):
        return []

    def writeHeader(self, p=None):
        return self.writeValues(
            self.getHeaderFormats(), self.getHeaderValues(), p
        )

    def getDataFormats(self):
        return []

    def getDataValues(self):
        return []

    def writeData(self, p=None):
        return self.writeValues(
            self.getDataFormats(), self.getDataValues(), p
        )

    def getFooterFormats(self):
        return []

    def getFooterValues(self):
        return []

    def writeFooter(self, p=None):
        return self.writeValues(
            self.getFooterFormats(),self.getFooterValues(), p
        )


class PacketException(_packet.BasePacketException):
    """An exception raised by Packet instances"""

    def __init__(self, msg):
        super(PacketException, self).__init__(msg)
