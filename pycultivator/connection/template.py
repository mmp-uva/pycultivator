# coding=utf-8
"""The template module provides classes that can be used to interpret and check incoming bytearrays"""

import struct
from pycultivator.connection import _packet

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class Template(_packet.BaseTemplate):
    """A packet used to check and interpret data received as byte arrays"""

    def __init__(self):
        super(Template, self).__init__()
        self._data = []

    def getFormats(self):
        """Return a list of format specifications

        :return: Format specification of which this template consists
        :rtype: list[str]
        """
        return []

    def getTemplateFormat(self):
        return "".join(self.getFormats()[:])

    def check(self, data):
        return self.isValid(self.getTemplateFormat(), data)

    def read(self, data):
        if not self.check(data):
            raise TemplateException("Unable to read data; data is not valid")
        self.clear()
        try:
            self._data, _ = self.readData(data)
        except Exception as e:
            # TODO: narrow exception
            pass
        return self.data is not None

    def clear(self):
        self._data = []

    def isEmpty(self):
        return self.data is None or len(self.data) == 0

    def hasData(self):
        return not self.isEmpty()

    def readData(self, data, offset=0):
        return self.readValues(
            data, self.getFormats(), offset=offset
        )

    @classmethod
    def readValues(cls, data, fmts, offset=0):
        """Reads multiple values from a bytearray

        :type data: bytearray
        :type fmts: list[str]
        :type offset: int
        :return: List with values and new offset
        :rtype: tuple[list[None or str | int | float, int]
        """
        values = []
        for fmt in fmts:
            value, offset = cls.readValue(data, fmt, offset=offset)
            if value is not None:
                values.append(value)
        return values, offset

    @classmethod
    def readValue(cls, data, fmt, offset=0):
        """Reads a value at a offset from the bytearray

        :type data: bytearray
        :type fmt: str
        :type offset: int
        :return: Value and new offset
        :rtype: tuple[None or str | int | float, int]
        """
        value = None
        if len(data) - offset >= struct.calcsize(fmt):
            try:
                values = struct.unpack_from(fmt, data, offset)
                if fmt[-1] in (cls.FORMAT_CHAR, cls.FORMAT_STRING):
                    value = "".join(values)
                else:
                    value = values[0]
                offset += struct.calcsize(fmt)
            except struct.error:
                cls.getLog().warning("Unable to read bytearray in {}".format(fmt))
        return value, offset

    @classmethod
    def readByte(cls, data, offset=0):
        """Reads a byte at a offset from the bytearray

        :rtype: tuple[int, int]
        """
        return cls.readValue(data, "B", offset=offset)

    @classmethod
    def readShort(cls, data, offset=0):
        """Reads a short value at a offset from the bytearray

        :rtype: tuple[int, int]
        """
        return cls.readValue(data, "H", offset=offset)

    @classmethod
    def readChar(cls, data, offset=0):
        """Reads a character value at a offset from the bytearray

        :rtype: tuple[str, int]
        """
        return cls.readValue(data, "c", offset=offset)

    def __len__(self):
        return self.sizeOfFormat(self.getTemplateFormat())

    def __eq__(self, other):
        result = super(Template, self).__eq__(other)
        if isinstance(other, bytearray):
            result = self.data == self.readData(other)[0]
        return result


class CRCTemplate(Template, _packet.BaseCRC):
    """Implementing a CRC checking template"""

    def getTemplateFormat(self):
        return super(CRCTemplate, self).getTemplateFormat() + "B"

    @classmethod
    def isValid(cls, fmt, data):
        return super(CRCTemplate, cls).isValid(fmt, data) and cls.crc(data) == 0

    def readCRC(self, data, offset=0):
        return self.readByte(data, offset=offset)


class SandwichTemplate(Template):
    """Implementing a template with a fixed header, footer etc."""

    def __init__(self):
        super(SandwichTemplate, self).__init__()
        self._header = []
        self._footer = []

    def getFormats(self):
        header = self.getHeaderFormats()
        data = self.getDataFormats()
        footer = self.getFooterFormats()
        return header + data + footer

    def getValues(self):
        """Returns all values read"""
        header = self.getHeader()
        data = self.getData()
        footer = self.getFooter()
        return header + data + footer

    def read(self, data):
        # read header
        values, offset = self.readHeader(data)
        self._header = values
        # read data
        values, offset = self.readData(data, offset=offset)
        self._data = values
        # read footer
        values, offset = self.readFooter(data, offset=offset)
        self._footer = values
        return self._data

    def getHeaderFormats(self):
        return []

    def getHeader(self):
        return self._header

    def hasHeader(self):
        return len(self.getHeader()) == len(self.getHeaderFormats())

    def readHeader(self, data, offset=0):
        return self.readValues(
            data, self.getHeaderFormats(), offset=offset
        )

    def getDataFormats(self):
        return []

    def readData(self, data, offset=0):
        return self.readValues(
            data, self.getDataFormats(), offset=offset
        )

    def getFooterFormats(self):
        return []

    def getFooter(self):
        return self._footer

    def hasFooter(self):
        return len(self.getFooter()) == len(self.getFooterFormats())

    def readFooter(self, data, offset=0):
        return self.readValues(
            data, self.getFooterFormats(), offset=offset
        )


class TemplateException(_packet.BasePacketException):
    """Exception raised by a Template instance"""

    def __init__(self, msg):
        super(TemplateException, self).__init__(msg)
