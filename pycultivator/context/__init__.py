
from .base import BaseContext
from .manager import ManagerContext

__all__ = [
    "BaseContext",
    "ManagerContext",
    "Context"
]


class Context(ManagerContext):
    """Wrapper class to abstract the default context"""

    pass


