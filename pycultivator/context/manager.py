
from .base import BaseContext

__all__ = [
    "ManagerContext"
]


class ManagerContext(BaseContext):
    """Keeps track of loaded managers"""

    services = ["managers"]

    def __init__(self, *args, **kwargs):
        super(ManagerContext, self).__init__(*args, **kwargs)
        self._managers = {}

    @property
    def children(self):
        results = super(ManagerContext, self).children
        results.extend(self._managers.values())
        return results

    @property
    def managers(self):
        return self._managers

    def has_manager(self, name):
        return name in self._managers.keys()

    def get_manager(self, name):
        """Return the manager registered under the given name

        :param name: The name under which the desired manager is registered
        :rtype: pycultivator.manager.Device
        :return: the manager registered under the given name
        """
        if not self.has_manager(name):
            raise KeyError("No manager registered with the name: '{}'".format(name))
        return self._managers[name]

    def load_manager(self, name, klass, **kwargs):
        if self.has_manager(name):
            raise KeyError("Another manager was already registered with the same name: '{}'".format(name))
        from pycultivator.manager import BaseManager
        if not isinstance(klass, type):
            raise TypeError("Can only load managers classes")
        if not issubclass(klass, BaseManager):
            raise TypeError("Can only load managers of the type BaseManager not: '{}'".format(type(klass)))
        # create new class
        manager = klass(name, self, **kwargs)
        # register
        self._managers[manager.name] = manager
        return manager

    def add_manager(self, name, manager):
        if self.has_manager(name):
            raise KeyError("Another manager was already registered with the same name: '{}'".format(name))
        from pycultivator.manager import BaseManager
        if not isinstance(manager, BaseManager):
            raise TypeError("Can only register managers of the type Manager not: '{}'".format(type(manager)))
        # overwrite the name of the manager
        manager.name = name
        # register this context as the owner
        manager.setParent(self)
        # store by name not identity
        self._managers[manager.name] = manager

    def remove_manager(self, name):
        manager = self._managers.pop(name)
        # remove context as owner
        if manager.parent is self:
            manager.setParent(None)

    def clear_managers(self):
        while len(self._managers.keys()) > 0:
            name = self._managers.keys()[0]
            self.remove_manager(name)

    def clean(self):
        """Clean the context"""
        result = super(ManagerContext, self).clean()
        self.clear_managers()
        return result
