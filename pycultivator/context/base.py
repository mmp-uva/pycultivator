
import six
import os
from pycultivator.core import HierarchicalObject, ConfigurableObject
from pycultivator.core.objects.configurable import ConfigurableMeta

__all__ = [
    "BaseContext"
]


class MetaContext(ConfigurableMeta):

    def __init__(cls, name, bases=None, attrs=None):
        super(MetaContext, cls).__init__(name, bases, attrs)
        inherits = cls.load_from_dict(attrs, 'inherits_services', default=True)
        cls.inherits_services = inherits
        services = cls.load_from_dict(attrs, 'services')
        if services is None:
            services = set([])
        elif isinstance(services, (list, tuple)):
            services = set(services)
        else:
            services = {services}
        cls.services = cls._collectServices(services, bases, inherits)

    def _collectServices(cls, services, bases, inherits=True):
        results = set()
        bases = list(bases)[:]
        # reverse order: furthest away first
        bases.reverse()
        if inherits:
            for base in bases:
                if isinstance(base, MetaContext):
                    results.update(base.services)
        results.update(services)
        return results

    def inheritsServices(cls):
        return cls.inherits_services


class BaseContext(
    six.with_metaclass(MetaContext, HierarchicalObject, ConfigurableObject)
):

    namespace = "context"
    default_settings = {
        'root.dir': os.getcwd(),
    }
    services = []

    def __init__(self, *args, **kwargs):
        super(BaseContext, self).__init__(*args, **kwargs)

    def getRootDirectory(self):
        return self.settings.get("root.dir")

    def setRootDirectory(self, path):
        if not os.path.exists(path):
            raise ValueError("Root directory must exist.")
        return self.settings.set("root.dir", path)

    def clean(self):
        return True
