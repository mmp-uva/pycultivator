from .base import BaseDevice, BaseDeviceException

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "InstrumentDevice", "InstrumentDeviceException"
]


class InstrumentDevice(BaseDevice):
    """A device with instruments"""

    namespace = "device"

    INSTRUMENTS = {
        # TYPE: LIMITS
    }

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(InstrumentDevice, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._instruments = {
            # id : instrument
        }

    # Instrument methods

    def getInstruments(self):
        """Returns the currently used dictionary with instruments

        :return:
        :rtype: dict[str, pycultivator.instrument.instrument.Instrument]
        """
        return self._instruments

    @property
    def instruments(self):
        return self.getInstruments()

    def getInstrumentTypes(self, instruments=None):
        """Returns the types of instruments connected to this device

        :type instruments: None or dict[str, pycultivator.instrument.instrument.Instrument]
        :rtype: set[str]
        """
        results = set([])
        if instruments is None:
            instruments = self.getInstruments()
        for i in instruments.values():
            results = results.union(i.getTypeSynonyms())
        return results

    def getInstrumentClasses(self, instruments=None):
        """Returns the different classes of instruments connected to this device

        :type instruments: None or dict[str, pycultivator.instrument.instrument.Instrument]
        :rtype: list[type]
        """
        results = []
        if instruments is None:
            instruments = self.getInstruments()
        for i in instruments.values():
            if type(i) not in results:
                results.append(type(i))
        return results

    def countInstruments(self):
        """Returns the number of instruments registered to this device"""
        return len(self.getInstruments().keys())

    def hasInstrument(self, key):
        return key in self.getInstruments().keys()

    def hasInstrumentOf(self, key=None, klass=None, t=None):
        """Returns whether this channel has an instrument of the given key and type

        Returns whether this channel has instruments that match the given criteria

        :param key: The required key. If None; match all keys.
        :type key: int or str
        :param t: The required type. If None; match all types.
        :return: Number of instruments that meet the criteria
        :rtype: int
        """
        return self.countInstrumentsOf(key=key, klass=klass, t=t) > 0

    def hasInstrumentOfClass(self, klass):
        """Returns whether this channel has an instrument of the given type

        :type klass: type
        :rtype: bool
        """
        return self.countInstrumentsOfClass(klass) > 0

    def hasInstrumentOfType(self, t):
        """Returns whether this channel has at least on instrument of the given type

        A type is the name under which the instrument identifies itself (light, od, temperature, etc.)

        :type t: str
        :rtype: bool
        """
        return self.countInstrumentsOfType(t) > 0

    def getInstrument(self, key):
        """Returns the instrument that is registered under key to this channel

        :rtype: pycultivator.instrument.instrument.Instrument
        """
        if not self.hasInstrument(key):
            raise KeyError("Invalid key - %s - given." % key)
        return self.getInstruments()[key]

    def addInstrument(self, i):
        """Registers an instrument under it's own name to the device

        :type i: pycultivator.instrument.instrument.Instrument
        :rtype: pycultivator.device.Device.InstrumentDevice
        """
        name = i.getName()
        if not self.hasInstrumentLimit(i):
            raise ValueError("Invalid instrument: {} not allowed".format(type(i).__name__))
        if name is None:
            raise ValueError("Invalid instrument: No name set")
        count = self.countInstrumentsOfClass(type(i))
        limit = self.getInstrumentLimit(i)
        if limit is not None and count >= limit:
            raise InstrumentDeviceException("Cannot add instrument: reached maximum ({})".format(limit))
        self.getInstruments()[name] = i
        i.setParent(self)
        return self

    def removeInstrument(self, i):
        """Removes an instrument from the device

        :type i: pycultivator.instrument.instrument.Instrument
        :rtype: pycultivator.device.Device.InstrumentDevice
        """
        name = i.getName()
        if name is not None:
            if not self.hasInstrument(name):
                raise InstrumentDeviceException(
                    "Unable to remove instrument: {} not known".format(name)
                )
            # remove instrument from registry
            self.getInstruments().pop(name)
            # remove this object as parent
            i.setParent(None)
        return self

    def setInstrument(self, key, i):
        """Registers an instrument under key to the channel.

        An Instrument that is already registered under the same name will be removed.

        :type key: str
        :type i: pycultivator.instrument.instrument.Instrument
        :rtype: pycultivator.device.device._InstrumentDevice
        """
        if not self.hasInstrumentLimit(i):
            raise ValueError("Invalid instrument: {} not allowed".format(type(i).__name__))
        if i.name is None:
            raise ValueError("Invalid instrument: name can not be None")
        count = self.countInstrumentsOfClass(type(i))
        # reduce count by one if current instrument is of same type
        if self.hasInstrument(key) and isinstance(i, type(self.getInstrument(key))):
            count -= 1
        limit = self.getInstrumentLimit(i)
        if limit is not None and count >= limit:
            raise InstrumentDeviceException("Cannot add instrument: reached maximum ({})".format(limit))
        if self.hasInstrument(key):
            self.removeInstrument(self.getInstrument(key))
        self.getInstruments()[key] = i
        i.setParent(self)
        return self

    def getInstrumentsOf(self, key=None, klass=None, t=None, instruments=None):
        """Returns a list of all instruments that match the given criteria

        :type key: None or str or int
        :type klass: None or type
        :type t: None or str
        :type instruments: None or dict[str|int, pycultivator.instrument.instrument.Instrument]
        :rtype: dict[str|int, pycultivator.instrument.instrument.Instrument]
        """
        if instruments is None:
            instruments = self.getInstruments()
        # first filter by klass
        if klass is not None:
            instruments = self.getInstrumentsOfClass(klass, instruments=instruments)
        # filter by type
        if t is not None:
            instruments = self.getInstrumentsOfType(t, instruments=instruments)
        # filter by key
        if key is not None:
            if key in instruments.keys():
                instruments = [instruments[key]]
            else:
                instruments = []
        return instruments

    def countInstrumentsOf(self, key=None, klass=None, t=None, instruments=None):
        return len(self.getInstrumentsOf(key=key, klass=klass, t=t, instruments=instruments))

    def getInstrumentsOfClass(self, klass=None, instruments=None):
        """Returns all instruments of the given class

        :type klass: type
        :type instruments: None or dict[str|int, pycultivator.instrument.instrument.Instrument]
        :rtype: dict[str|int, pycultivator.instrument.instrument.Instrument]
        """
        results = {}
        if instruments is None:
            instruments = self.getInstruments()
        if klass is not None:
            for key, item in instruments.items():
                if isinstance(item, klass):
                    results[key] = item
        else:
            results = instruments
        return results

    def countInstrumentsOfClass(self, t, instruments=None):
        return len(self.getInstrumentsOfClass(t, instruments=instruments).keys())

    def getInstrumentsOfType(self, t=None, instruments=None):
        """Returns all Instruments of the given type

        A type is the name under which the instrument identifies itself (light, od, temperature, etc.)

        :type t: str
        :type instruments: None or dict[str|int, pycultivator.instrument.instrument.Instrument]
        :rtype: dict[str|int, pycultivator.instrument.instrument.Instrument]
        """
        results = {}
        if instruments is None:
            instruments = self.getInstruments()
        if isinstance(t, str) and t.lower() == "all":
            t = None
        if t is not None:
            for key, item in instruments.items():
                if item.isOfType(t):
                    results[key] = item
        else:
            results = instruments
        return results

    def countInstrumentsOfType(self, t, instruments=None):
        return len(self.getInstrumentsOfType(t, instruments=instruments).keys())

    def getInstrumentLimits(self):
        """The current maximum number of allowed instruments for this device

        :rtype: dict[pycultivator.instrument.instrument.Instrument, int]
        """
        return self.INSTRUMENTS

    def getInstrumentLimit(self, i):
        """The maximum number of this class of instruments allowed to be connected to this device

        :type i: pycultivator.instrument.instrument.Instrument
        :rtype: int
        """
        result = None
        if len(self.getInstrumentLimits().keys()) > 0:
            if isinstance(i, object):
                i = type(i)
            if i in self.getInstrumentLimits().keys():
                result = self.getInstrumentLimits()[i]
        return result

    def hasInstrumentLimit(self, i):
        """Whether a limit was defined for the instrument

        :type i: pycultivator.instrument.instrument.Instrument
        :rtype: int
        """
        result = True
        if isinstance(i, object):
            i = type(i)
        if len(self.getInstrumentLimits().keys()) > 0:
            result = i in self.getInstrumentLimits().keys()
        return result

    def getInstrumentTypeLimit(self, t):
        """The maximum number of this type of instruments allowed to be connected to this device

        :type t: str
        :rtype: int
        """
        result = None
        if len(self.getInstrumentLimits().keys()) > 0:
            result = 0
            for i in self.getInstrumentLimits().keys():
                if i.isOfType(t):
                    result += self.getInstrumentLimits()[i]
        return result

    # Behaviour

    def measure(self, variable, instruments=None, **kwargs):
        """Will measure all instruments in each channel of this device.

        If a more specific approach is required talk the instrument directly. This function may lead to doing
        measurements in only those instruments that are considered important by the specific implementation (
        i.e. a some device implementations will do measurements on one type of instruments).

        :param instruments: A list of instruments to measure the variable for
        :type instruments: None | list[pycultivator.instrument.Instrument]
        :param variable: The variable to be measure.
        :param kwargs: Extra arguments passed.
        :rtype: list[pycultivator.data.DataModel.Record]
        """
        from pycultivator.core import Parser
        instruments = Parser.to_list(instruments, default=self.instruments.values())
        # iterate over instruments
        for i in instruments:
            if i.canMeasure(variable):
                yield i.measure(variable, **kwargs)

    def measures(self, instruments=None):
        """Returns a list of all variables that can be measured by instruments of this channel"""
        results = set([])
        from pycultivator.core import Parser
        instruments = Parser.to_list(instruments, default=self.instruments.values())
        # iterate over instruments
        for i in instruments:
            results.update(i.measures())
        return results

    def control(self, variable, value, instruments=None, **kwargs):
        """Control a variable to the given value

        :param variable: The variable to control.
        :param value: The value to set the variable to.
        :param instruments: A list of instruments to control the variable for
        :type instruments: None | list[pycultivator.instrument.Instrument]
        :param kwargs: Extra arguments passed
        :rtype: list[bool]
        """
        from pycultivator.core import Parser
        instruments = Parser.to_list(instruments, default=self.instruments.values())
        # iterate over instruments
        for i in instruments:
            if i.canControl(variable):
                yield i.control(variable, value, **kwargs)

    def controls(self, instruments=None):
        """Returns a list of all variables that can be controlled by instruments of this channel"""
        results = set([])
        # collect instruments
        from pycultivator.core import Parser
        instruments = Parser.to_list(instruments, default=self.instruments.values())
        # iterate over instruments
        for i in instruments:
            results.update(i.controls())
        return results

    @property
    def children(self):
        return self.getInstruments().values()

    # Copy

    def copy(self, memo=None):
        """Create a new deepcopy of this device

        :rtype: pycultivator.device._device.InstrumentDevice
        """
        result = super(InstrumentDevice, self).copy(memo)
        if isinstance(result, InstrumentDevice):
            # add instruments
            import copy
            for instrument in self.getInstruments().keys():
                i = copy.deepcopy(self.getInstrument(instrument), memo)
                result.addInstrument(i)
        return result


class InstrumentDeviceException(BaseDeviceException):
    """Exception raised by InstrumentDevice"""

    def __init__(self, msg):
        super(InstrumentDeviceException, self).__init__(msg)
