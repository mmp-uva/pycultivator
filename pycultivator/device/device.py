"""
Experiment devices are the external machines, connected to the computer.

A Devices consist of one or more channels, with each channel consisting of one our more instruments.

A device can be measured
"""

from .connected import *
from .instrumented import *


__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    "Device", "DeviceException",
]


class Device(InstrumentDevice, ConnectedDevice):
    """A simple device with no channels only instruments"""

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Device, self).__init__(identity, parent=parent, settings=settings, **kwargs)


class DeviceException(InstrumentDeviceException, ConnectedDeviceException):
    """An exception raised by device"""

    def __init__(self, msg):
        super(DeviceException, self).__init__(msg)


