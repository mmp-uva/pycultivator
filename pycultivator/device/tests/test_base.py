
from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.device.base import BaseDevice


class TestBaseDevice(UvaSubjectTestCase):

    _abstract = True
    _subject_cls = BaseDevice

    def getSubject(self):
        """

        :rtype: pycultivator.device.device.BaseDevice
        """
        return super(TestBaseDevice, self).getSubject()

    def test_getParent(self):
        self.assertIsNone(self.getSubject().getParent())
        self.assertEqual(self.getSubject().getParent(), self.getSubject().parent)

    def test_hasParent(self):
        self.assertFalse(self.getSubject().hasParent())

    def test_setParent(self):
        self.fail()

    def test_measure(self):
        self.fail()
        # self.assertEqual(self.getSubject().measure("light", "state"), [])

    def test_adjust(self):
        self.fail()
