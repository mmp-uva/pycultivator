
from pycultivator.device.complex import *
from test_base import TestBaseDevice


class TestComplexDevice(TestBaseDevice):

    _abstract = True
    _subject_cls = ComplexDevice

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(name="0", parent=None)

    def test_getConnection(self):
        self.fail()

    def test_setConnection(self):
        self.fail()

    def test_getChannels(self):
        self.fail()

    def test_getChannelCount(self):
        self.fail()

    def test_hasChannel(self):
        self.fail()

    def test_getChannel(self):
        self.fail()

    def test_setChannel(self):
        self.fail()

    def test_addChannel(self):
        self.fail()

    def test_adjust(self):
        self.fail()
