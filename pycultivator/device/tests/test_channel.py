"""

"""

from pycultivator.device.channel import Channel
from test_base import TestBaseDevice


class TestDeviceChannel(TestBaseDevice):

    _abstract = False
    _subject_cls = Channel

    def getSubject(self):
        """

        :rtype: pycultivator.device.device.DeviceChannel
        """
        return super(TestDeviceChannel, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()("0", parent=None)

    def test_getDevice(self):
        self.assertIsNone(self.getSubject().device)

    def test_adjust(self):
        self.fail()

    def test_getInstruments(self):
        self.fail()

    def test_getInstrumentCount(self):
        self.fail()

    def test_hasInstrument(self):
        self.fail()

    def test_getInstrument(self):
        self.fail()

    def test_setInstrument(self):
        self.fail()

    def test_getInstrumentsOf(self):
        self.assertEqual(
            self.getSubject().getInstrumentsOf(),
            self.getSubject().getInstruments()
        )
        self.assertEqual(self.getSubject().getInstrumentsOf(key="bla"), {})
        self.assertEqual(self.getSubject().getInstrumentsOf(t=bool), {})
        self.assertEqual(self.getSubject().getInstrumentsOf(t=bool, key="bla"), {})

    def test_getInstrumentsOfType(self):
        self.assertEqual(
            self.getSubject().getInstrumentsOfType(bool), {}
        )

    def test_eq(self):
        self.assertEqual(self.subject, self.subject)
        self.assertEqual(self.subject, "0")
        self.assertEqual(self.subject, 0)
