
from .connected import *

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "ComplexDevice", "ComplexDeviceException"
]


class ComplexDevice(ConnectedDevice):
    """A basic device class with one or multiple channels"""

    MAX_CHANNELS = None

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(ComplexDevice, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._channels = {}

    # path methods

    @property
    def children(self):
        return self.channels.values()

    # channel methods

    def getChannels(self):
        """Returns a dictionary of registered channels to this device

        :rtype: dict[int, pycultivator.device.channel.Channel]
        """
        return self._channels

    @property
    def channels(self):
        return self.getChannels()

    def getChannelKeys(self):
        """Returns all list with the key of each known channel

        :rtype: list[int]
        """
        return self.getChannels().keys()

    def countChannels(self):
        """Returns the number of channels in this device

        :rtype: int
        """
        return len(self.getChannelKeys())

    def hasChannel(self, idx):
        """ Returns whether the given index is a valid index

        :param idx: Index to test
        :type idx: int
        :return: Whether the index is valid
        :rtype: bool
        """
        return idx in self.getChannelKeys()

    def hasChannelName(self, name):
        result = False
        for c in self.getChannels().values():
            if c.getName() == name:
                result = True
                break
        return result

    def getChannel(self, idx):
        """ Returns the channel object at the given index

        :param idx: Index of the desired channel object
        :type idx: int
        :return: The desired channel object
        :rtype: pycultivator.device.channel.Channel
        """
        if not self.hasChannel(idx):
            raise IndexError("Invalid index - %s - received" % idx)
        return self.getChannels()[idx]

    def getChannelByName(self, name):
        if not self.hasChannelName(name):
            raise KeyError("Invalid name - {} ".format(name))
        c = None
        for c in self.getChannels().values():
            if c.getName() == name:
                break
        return c

    def setChannel(self, c, idx):
        """ Sets the channel object at the given index

        :param c: The new channel that should be stored at the given index
        :type c: pycultivator.device.channel.Channel
        :param idx: Index of the object that has to be replaced
        :type idx: int
        :return: Reference to this object
        :rtype: pycultivator.device.device.ComplexDevice
        """
        if not self.hasChannel(idx):
            raise IndexError("Invalid index - %s - received" % idx)
        # first remove old channel
        self.removeChannel(idx)
        # and register new channel at the given key
        self.getChannels()[idx] = c
        c.setParent(self)
        c.setIndex(idx)
        return self

    def addChannel(self, c, idx=None):
        """ Adds a new channel to the list of channels

        :param idx: Index of the channel
        :type idx: int
        :param c: Channel object to add
        :type c: pycultivator.device.channel.Channel
        :return: Referene to this object
        :rtype: pycultivator.device.device.ComplexDevice
        """
        if idx is None:
            idx = max(self.getChannelKeys()) + 1 if self.countChannels() > 0 else 0
        if self.hasChannel(idx):
            raise ComplexDeviceException("Channel index - %s - already exists" % idx)
        if self.MAX_CHANNELS is not None and (self.countChannels() >= self.MAX_CHANNELS or idx >= self.MAX_CHANNELS):
            raise ComplexDeviceException(
                "Cannot have more than {} channel(s), will not add new channel".format(self.MAX_CHANNELS)
            )
        # register channel
        self.getChannels()[idx] = c
        # register the channel index
        c.setIndex(idx)
        c.setParent(self)
        return self

    def removeChannel(self, idx):
        """Removes a channel from the device and resets the channel parent and index references"""
        if not self.hasChannel(idx):
            raise IndexError("Invalid index- %s - received" % idx)
        c = self.getChannels().pop(idx)
        c.setParent(None)
        c.setIndex(None)
        return c

    def clearChannels(self):
        """Removes all channels from the device"""
        for channel in self.getChannelKeys():
            self.removeChannel(channel)
        return self

    # Instrument methods

    def getInstrumentTypes(self, instruments=None):
        results = set()
        for c in self.getChannels().values():
            results.update(c.getInstrumentTypes(instruments=instruments))
        return results

    def getInstrumentsClasses(self, instruments=None):
        results = set([])
        for c in self.getChannels().values():
            results.update(c.getInstrumentClasses(instruments=instruments))
        return results

    # Behaviour methods

    def measure(self, variable, instruments=None, **kwargs):
        """Will measure all instruments in each channel of this device.

        If a more specific approach is required talk the instrument directly. This function may lead to doing
        measurements in only those instruments that are considered important by the specific implementation (
        i.e. a some device implementations will do measurements on one type of instruments).

        :param variable: The variable to measure.
        :param instruments: A list of instruments to measure the variable of
        :param kwargs: Extra arguments passed.
        :rtype: list[pycultivator.data.DataModel.Record]
        """
        for c in self.getChannels().values():
            for result in c.measure(variable, instruments=instruments, **kwargs):
                yield result

    def measures(self, instruments=None):
        results = set([])
        for c in self.getChannels().values():
            results.update(c.measures(instruments))
        return results

    def control(self, variable, value, instruments=None, **kwargs):
        """Controls the variable

        :param variable: The variable to control.
        :param value: The value to set the variable to.
        :param instruments: A list of instruments to control the variable in
        :param kwargs: Extra arguments passed.
        :rtype: list[bool]
        """
        for c in self.getChannels().values():
            for result in c.control(variable, value, instruments=instruments, **kwargs):
                yield result

    def controls(self, instruments=None):
        results = set([])
        for c in self.getChannels().values():
            results.update(c.controls(instruments))
        return results

    def copy(self, memo=None):
        """Create a new deepcopy of this device

        :return: Copy of this object
        :rtype: pycultivator.device.device.ComplexDevice
        """
        result = super(ComplexDevice, self).copy(memo)
        if isinstance(result, ComplexDevice):
            # copy channels
            import copy
            for idx in self.getChannelKeys():
                c = copy.deepcopy(self.getChannel(idx))
                result.addChannel(c)
        return result


class ComplexDeviceException(ConnectedDeviceException):
    """An exception raised by device classes"""

    def __init__(self, msg):
        super(ComplexDeviceException, self).__init__(msg)