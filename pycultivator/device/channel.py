"""Implements a channel in a device"""

from pycultivator.core import Parser
from .instrumented import *

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "Channel", "ChannelException"
]


class Channel(InstrumentDevice):
    """The channel class defines the specific instance of one channel in a device."""

    namespace = "device.channel"
    default_settings = {}

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Channel, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._index = None

    def makeIdentity(self, name):
        name = Parser.to_int(name)
        if name is None:
            raise ChannelException(
                "Invalid name, {} is not an integer or cannot parsed to integer".format(name)
            )
        return name

    def getIndex(self):
        """Numeric Index of the channel in the device"""
        return self._index

    @property
    def index(self):
        return self.getIndex()

    def hasIndex(self):
        return self.getIndex() is not None

    def setIndex(self, idx):
        v = idx
        if idx is not None:
            v = Parser.to_int(idx)
            if v is None:
                raise ValueError("Unable to cast {} into a integer".format(idx))
        self._index = v

    @index.setter
    def index(self, i):
        self.setIndex(i)

    def getDevice(self):
        """Returns the parent of this channel

        :rtype: pycultivator.device.device.ComplexDevice
        """
        return self.getParent()

    @property
    def device(self):
        return self.getDevice()

    def copy(self, memo=None):
        """Copy this Channel into another Channel

        :type other: None or pycultivator.device.device.DeviceChannel
        :rtype: pycultivator.device.device.DeviceChannel
        """
        result = super(Channel, self).copy(memo=memo)
        """:type: pycultivator.device.ComplexDevice.DeviceChannel"""
        if isinstance(result, Channel):
            # set channel index
            result.setIndex(self.getIndex())
        return result

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return other.index == self.index and other.parent == self.parent
        return self.index == Parser.to_int(other)

    def __ne__(self, other):
        return not self == other


class ChannelException(InstrumentDeviceException):
    """Exception raised by channels"""
    
    def __init__(self, msg=None):
        super(ChannelException, self).__init__(msg)
