
from .base import *
from pycultivator.connection import Connection

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "ConnectedDevice", "ConnectedDeviceException"
]


class ConnectedDevice(BaseDevice):
    """A device with instruments"""

    default_connection_class = None
    required_connection = Connection

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(ConnectedDevice, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._connection = None

    def __del__(self):
        if self.isConnected():
            self.disconnect()

    # connection methods

    def _getConnection(self):
        return self._connection

    def hasConnection(self):
        return self.connection is not None

    def getConnection(self):
        """Returns the connection used to connect to the device

        :return: The connection being used.
        :rtype: None | pycultivator.connection.connection.Connection
        """
        return self._connection

    @property
    def connection(self):
        return self.getConnection()

    @connection.setter
    def connection(self, c):
        self.setConnection(c)

    def setConnection(self, c):
        """Sets the connection to be used to connect to the device

        :param c:
        :type c: None or pycultivator.connection.Connection.Connection
        :return:
        :rtype: None or pycultivator.device.device.ComplexDevice
        """
        self.assertValidConnection(c)
        self._connection = c
        return self

    def createConnection(self, class_=None, fake=False, settings=None, pre_load=True, **kwargs):
        # check if the connection class is a valid class for this object
        if class_ is None:
            class_ = self.default_connection_class
        # fall back to required connection if not default is set
        if class_ is None:
            class_ = self.required_connection
        self.assertValidConnectionClass(class_)
        from pycultivator.core import PCSettings
        new_settings = PCSettings()
        # if connection exists pre-load with current settings
        if pre_load and self.hasConnection():
            new_settings.update(self.getConnection().settings, namespace="")
        # overwrite with provided settings
        if isinstance(settings, (dict, PCSettings)):
            new_settings.update(settings)
        # overwrite with kwargs
        new_settings.update(kwargs)
        # check if we should make a fake connection
        if fake and not class_.isFake():
            class_ = class_.getFake()
        # create connection and register it
        connection = class_(settings=settings)
        self.setConnection(connection)
        return connection

    def assertValidConnection(self, o, expected=None):
        return self.assertValidConnectionClass(type(o), expected)

    @classmethod
    def assertValidConnectionClass(cls, c, expected=None):
        if not cls.checkConnectionClass(c, expected):
            raise ConnectedDeviceException("This device requires a {} to work, not a {}".format(
                expected, c
            ))
        return True

    @classmethod
    def checkConnectionClass(cls, c, expected=None):
        if expected is None:
            expected = cls.required_connection
        if expected is None:
            expected = Connection
        return issubclass(c, expected)

    def isConnected(self):
        """Return whether this device has a connection object and a open connection is registered

        :rtype: bool
        """
        return self.hasConnection() and self.getConnection().isConnected()

    # behaviour methods

    def connect(self):
        """Make a connection to the device"""
        result = False
        if self.hasConnection():
            result = self.getConnection().connect()
        return result

    def disconnect(self):
        """Close connection to the device"""
        result = False
        if self.isConnected():
            result = self.getConnection().disconnect()
        return result

    def measure(self, variable, instruments=None, **kwargs):
        """Will measure all instruments in each channel of this device.

        :param instrument: The instrument to measure.
        :type instrument: str
        :param variable: The variable to measure.
        :param kwargs: Extra arguments passed.
        :rtype: list[pycultivator.data.DataModel.Record]
        """
        raise NotImplementedError

    def measures(self, instruments=None):
        raise NotImplementedError

    def control(self, variable, value, instruments=None, **kwargs):
        """Controls the variable

        :param variable: The variable to control.
        :param value: The value to set the variable to.
        :param kwargs: Extra arguments passed.
        """
        raise NotImplementedError

    def controls(self, instruments=None):
        raise NotImplementedError

    def copy(self, memo=None):
        result = super(ConnectedDevice, self).copy(memo=memo)
        if isinstance(result, self.__class__):
            # copy connection
            # TODO: decide whether we need to make a copy of the connection object
            if self.hasConnection():
                result.setConnection(self.getConnection())
        return result


class ConnectedDeviceException(BaseDeviceException):
    """Exception raised by ConnectedDevice"""

    def __init__(self, msg):
        super(ConnectedDeviceException, self).__init__(msg)