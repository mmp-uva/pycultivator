"""Abstract device classes"""

from pycultivator.core import HierarchicalObject, ConfigurableObject, PCException

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"
__all__ = [
    "BaseDevice", "BaseDeviceException"
]


class BaseDevice(HierarchicalObject, ConfigurableObject):
    """The basic device class defines the basic API for a ComplexDevice

    A device consists of:
    - one connection (serial, ethernet, etc.)
    - >= 1 instruments (light, od, etc.)
    """

    namespace = "device"
    default_settings = {
        # add default settings for this class
    }

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(BaseDevice, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._state = False

    def isActive(self):
        """Return the state of this device"""
        return self._state is True

    @property
    def is_active(self):
        return self.isActive()

    def setActive(self, state=False):
        """Set the state of this device"""
        self._state = state is True

    @is_active.setter
    def is_active(self, state):
        self.setActive(state)

    # Instrument behaviour methods

    def measure(self, variable, instruments=None, **kwargs):
        """Measures the given variable, using this device and all's it's children.

        :param variable: The variable to measure
        :type variable: str
        :param instruments: A list of instruments to measure the variable for
        :type instruments: None | list[pycultivator.instrument.Instrument]
        :param kwargs: Extra arguments passed
        :rtype: dict[str, int | float | str | bool]
        """
        raise NotImplementedError

    def measures(self, instruments=None):
        """Returns a set of variables that can be measured by this device (or it's children).

        :param instruments: A list of instruments to collect measurable variables from
        :type instruments: None | list[pycultivator.instrument.Instrument]
        :rtype: set[str]
        """
        raise NotImplementedError

    def can_measure(self, variable, instruments=None):
        """Returns whether this device is capable of measuring this variable.

        CAUTION: This check is also performed on instrument level in the measure method,
            therefore it's faster to directly call measure without checking can_measure first.

        NOTE: When multiple instruments are checked,
            this method returns True if at least one instrument can measure the variable

        :param variable: The variable to check for
        :param instruments: A list of instruments to test the availability of the variable for
        :type instruments: None | list[pycultivator.instrument.Instrument]
        :type variable: str
        :rtype: bool
        """
        return variable in self.measures(instruments=instruments)

    def control(self, variable, value, instruments=None, **kwargs):
        """Controls the given variable to the given value.

        :param variable: The variable to control.
        :type variable: str
        :param value: The value to set the variable to.
        :param instruments: A list of instruments to control the variable for
        :type instruments: None | list[pycultivator.instrument.Instrument]
        :param kwargs: Extra arguments passed.
        :type value: int | float
        """
        raise NotImplementedError

    def controls(self, instruments=None):
        """Returns a set of variables that can be controlled by this device (or it's children).

        :param instruments: A list of instruments to collect controllable variables from
        :type instruments: None | list[pycultivator.instrument.Instrument]
        :rtype: set[str]
        """
        raise NotImplementedError

    def can_control(self, variable, instruments=None):
        """Checks whether this device is capable of controlling this variable

        CAUTION: This check is also performed on instrument level in the control method,
            therefore it's faster to directly call control without checking can_control first.

        NOTE: When multiple instruments are checked,
            this method returns True if at least one instrument can control the variable

        :param variable: The variable to check for
        :type variable: str
        :param instruments: A list of instruments to test availability of the variable for
        :type instruments: None | list[pycultivator.instrument.Instrument]
        :rtype: bool
        """
        return variable in self.controls(instruments=instruments)

    # Copy methods

    def copy(self, memo=None):
        result = super(BaseDevice, self).copy(memo=memo)
        if isinstance(result, self.__class__):
            result.setActive(self.isActive())
        return result


class BaseDeviceException(PCException):
    """Exception raised by BaseDevices"""

    def __init__(self, msg):
        super(BaseDeviceException, self).__init__(msg)
