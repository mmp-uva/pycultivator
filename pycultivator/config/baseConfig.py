"""Framework for the configuration of objects from external sources

This Framework consists of two parts:

- Config object: Handles connection to the external source
- <class>Config Helpers: Handles the configuration of specific object classes

Object Configuration
~~~~~~~~~~~~~~~~~~~~

A typical flow to load any object(s) from the configuration source

1. call load (for one object) load_all (for all objects)
2. load will try to find all valid definitions and runs parse on each
3. parse has the task to obtain a "template" object and pass it to read, it does one of the following things:
3a. check if a template is passed, if so; check if the template is valid.
3b. if no template is received; try to create a new object using the definition: it calls create_obj
3b1. create_obj uses the definition to load the specified custom class.
3b2. if no valid class is found in the definition, it will use the default class of the helper
3b3. it calls initialize to create an object from the class.
3c. Once a valid object is obtained, the configuration definitions is read using read
4. read will collect the information from the definition (properties, settings, etc.) and store them in the object

A typical flow to save an object to the configuration source

1. call save with the object to save
2. save will search for a definition of the given object
3. if no definition is found, it will create a new definition
4. Once a definition is established, write is called to write the information of the object to the definition

A typical flow to delete an object from the configuration source

1. call delete with the object to delete
2. delete will search for the definition of the given object
3. if a definition is found call delete_definition to delete the definition

Overloading Config Helper
~~~~~~~~~~~~~~~~~~~~~~~~~

A specific custom helper should be created for each class of object that can be loaded from the configuration
and for each type of configuration. There are two levels of customisation involved:

- Configuration Source (for each type of configuration source used (XML, MySQL, etc.))
- Configuration Helper (for each type of object created from the configuration source)

At the configuration source level at least the following methods MUST be implemented:

- assert_definition -> to check validity of the definition
- select -> to find valid definitions
- find -> to find definitions based on an object
- read_class -> to extract class information from the definition
- create_definition -> to create definitions based on an object
- delete_definition -> to delete definitions from the configuration source

At the configuration helper level at least the following methods MUST be implemented:

- read_properties -> to extract property information from the definition
- write_properties -> to write property information to the definition

Other methods that may need to be overload, for specific helpers:

- initialize -> to initialize objects using a definition

Helpers may implemented extra read and write methods to interface with extra information contained in the objects.

"""

from pycultivator.core.objects import PCObject, ConfigurableObject, HierarchicalObject
from pycultivator.core import PCException, PCSettings, Parser
from pycultivator.connection import connection
from pycultivator.device import Device, ComplexDevice, Channel
from pycultivator import instrument
from pycultivator.relation import polynomial, calibration

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class BaseObjectConfig(ConfigurableObject):
    """ABSTRACT class handling the creation of pycultivator objects from a configuration source"""

    namespace = "config.object"
    default_settings = {
        # default settings for this class
    }

    _configures = PCObject
    # whether this class inherits from parent classes
    _inherit_configure_types = True
    # what types this class configures
    _configures_types = {"object"}
    # local copy
    _configures_types_cache = None
    required_source = "pycultivator.config.Config"
    # The configuration source the require

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the object configuration object

        :param source: The Configuration Source that should be used to read from the source
        :type source: pycultivator.config.baseConfig.Config
        :param settings: Additional settings for this configuration object
        :param kwargs: Extra arguments (passed as settings)
        """
        self.assert_source(source)
        super(BaseObjectConfig, self).__init__(settings, **kwargs)
        self._source = source

    # Getters/Setters

    @property
    def source(self):
        return self.getSource()

    def getSource(self):
        """Return the configuration source handler

        :return: The configuration source handler
        :rtype: pycultivator.config.baseConfig.Config
        """
        return self._source

    @classmethod
    def configures(cls):
        """Returns the class that can be configured by this Config object

        :rtype: T <= [pycultivator.core.object.configurable.ConfigurableObject]
        """
        return cls._configures

    @property
    def default_class(self):
        """The default class created by this helper"""
        result = self.configures()
        if isinstance(result, (tuple, list, set)):
            if len(result) > 0:
                result = result[0]
            else:
                result = None
        return result

    @classmethod
    def inheritsConfigureTypes(cls):
        return cls._inherit_configure_types

    @classmethod
    def _configuresTypes(cls):
        if cls._configures_types_cache is None:
            result = set()
            for base in cls.mro():
                if issubclass(base, BaseObjectConfig):
                    result.update(base._configures_types)
                    if not base.inheritsConfigureTypes():
                        break
            cls._configures_types_cache = result
        return cls._configures_types_cache

    @classmethod
    def configuresTypes(cls):
        """Returns the name of the object that this object can configure

        :rtype: set[str]
        """
        return cls._configuresTypes()

    @classmethod
    def canConfigure(cls, desired_class):
        """Returns whether this class can help with configuring the given class name, class or object

        :type desired_class: object | type | str
        :rtype: bool
        """
        result = False
        if isinstance(desired_class, str):
            types = cls.configuresTypes()
            if isinstance(types, str):
                types = [types]
            result = desired_class.lower() in [t.lower() for t in types]
        elif isinstance(desired_class, type):
            result = issubclass(desired_class, cls.configures())
        else:
            result = isinstance(desired_class, cls.configures())
        return result

    # assertion methods

    @classmethod
    def assert_definition(cls, definition, expected=None):
        """Tests whether the definition is as expected, if not raises an Exception.

        :param definition: Definition to test
        :param expected: What is expected (if None load default)
        :raises: ConfigException
        """
        pass        # overload

    @classmethod
    def assert_template(cls, template, expected=None):
        """Tests whether the template is of the correct type, if not raises an Exception.

        :param template: The template object to test
        :type template: object
        :param expected: The required type
        :type expected: type
        :raises: ConfigException
        """
        if expected is None:
            expected = cls.configures()
        if not isinstance(template, expected):
            raise ConfigException(
                "Invalid template type, expected {}, received - {}".format(
                    expected.__name__, type(template)
                )
            )

    @classmethod
    def assert_source(cls, source, expected=None):
        """Tests whether the source is of expected type

        :raises pycultivator.config.ConfigException: If source does not meet requirements
        """
        if source is None:
            raise ConfigException("Source cannot be None")
        if expected is None:
            expected = cls.required_source
        if isinstance(expected, str):
            from pycultivator.core.pcUtility import import_object_from_str
            try:
                expected = import_object_from_str(expected)
            except ImportError:
                import warnings
                warnings.warn("Unable to import {}".format(expected))
                expected = None
        if expected is None:
            expected = Config
        # ultimately fallback
        if not issubclass(expected, Config):
            raise ConfigException("Required source must inherit from Config not {}".format(expected))
        if not isinstance(source, expected):
            raise ConfigException("Expected a {} as source, not {}".format(
                expected.dot_path(), source.dot_path()
            ))

    @classmethod
    def assert_class(cls, class_):
        """Checks if the given klass can be loaded by this config

        :raises pycultivator.config.baseConfig.ConfigException: If class is not accepted
        """
        if class_ is None:
            raise ConfigException("Class cannot be None")
        if not cls.canConfigure(class_):
            raise ConfigException("Class {} cannot be configured by this {}, use {} instead".format(
                class_.__name__, cls.__name__, cls.configures().__name__
            ))

    @classmethod
    def assert_object(cls, obj, allow_none=True):
        """Checks if the given object can be handled by this configuration source helper

        :
        """
        if not allow_none and obj is None:
            raise ConfigException("Object is not allowed to be None")
        if not cls.canConfigure(obj):
            raise ConfigException("Object of class {} cannot be configured by {}, use {} instead".format(
                type(obj).__name__, cls.__name__, cls.configures().__name__
            ))

    def is_valid_class(self, class_):
        result = False
        try:
            self.assert_class(class_)
            result = True
        except ConfigException:
            pass
        return result

    # Behaviour methods

    def select(self, root=None):
        """Select all definitions under root that can be loaded by this class

        :param root: The root definition to search under
        :return: A list of valid definitions
        :rtype: list
        """
        return []       # overload

    def select_first(self, root=None):
        """Select all definitions under root that can be loaded by this class and return first

        :param root: The root definition to search under
        :return: A valid definition or None
        """
        result = None
        definitions = self.select(root=root)
        if len(definitions) > 0:
            result = definitions[0]
        return result

    def find(self, o, root=None, **kwargs):
        """Find all definitions that could have created the given object `o` in the configuration source.

        :param o: The object to search the definition of
        :param root: The root definition to search under
        :rtype: list[]
        """
        self.assert_object(o, allow_none=False)
        return None

    def find_first(self, o, root=None, **kwargs):
        result = None
        definitions = self.find(o, root=root, **kwargs)
        if len(definitions) > 0:
            result = definitions[0]
        return result

    # load methods: responsible for finding and then creating

    def load(self, root=None, template=None, **kwargs):
        """Load the first valid definition from the configuration source into a object

        :param root: Root definition in source to search from
        :param template: The template object to extend
        :type template: None or pycultivator.core.objects.configurable.ConfigurableObject
        :param kwargs: Extra argument passed to the parse function
        :return: The first valid definition from the configuration that is a child of root
        :rtype: None or pycultivator.core.objects.configurable.ConfigurableObject
        """
        result = None
        # search for definitions under root
        definitions = self.select(root)
        # read all of them, but break as soon as we successfully parsed a definition
        while result is None and len(definitions) > 0:
            # select first
            definition = definitions[0]
            # try to parse
            result = self.parse(definition, template=template, **kwargs)
            # if did not work, remove from list
            if result is None:
                definitions.remove(definition)
        return result

    def load_all(self, root=None, template=None, **kwargs):
        """Load all valid definitions from the configuration source into objects

        :param root: Root definition in source to search from
        :param template: The template object to extend
        :type template: None or pycultivator.core.objects.configurable.ConfigurableObject
        :return: All valid definitions from the configuration that is a child of root.
        :rtype: list[pycultivator.core.objects.configurable.ConfigurableObject]
        """
        # search for definitions
        results = []
        # search for definitions under root
        definitions = self.select(root)
        # read all definitions and
        for definition in definitions:
            result = self.parse(definition, template=template, **kwargs)
            if result is not None:
                results.append(result)
        return results

    # parse methods: responsible for the process of creating and reading

    def parse(self, definition, template=None, **kwargs):
        """Parse a definition into a object or template

        :param definition: The definition to parse
        :param template: The template object to use as basis
        :return: An object with the same information as stored in the definition
        :rtype: None | object
        """
        result = None
        # make sure we can read the definition
        self.assert_definition(definition)
        # assume this helper can do the work
        helper = self
        class_ = self.default_class
        # read class attribute to get custom class
        class_name = self.read_class(definition)
        if class_name is not None:
            class_ = self.load_class(class_name)
            if class_ is not None:
                helper = self.source.getHelperFor(class_)
        # we found some
        if None not in [class_, helper]:
            # create object
            result = helper.create_obj(definition, class_=class_, template=template, **kwargs)
        # check if we support the object
        if isinstance(result, helper.configures()):
            # load information into object
            result = helper.read(definition, result, **kwargs)
        return result

    # create methods: responsible for creating the object from the definition then reading

    def create_obj(self, definition, class_, template=None, **kwargs):
        """Create a new object from the definition"""
        # check template
        if template is None:
            template = self.read_template(definition, class_, template=template, **kwargs)
        if template is not None:
            # use template
            self.assert_template(template)
            import copy
            result = copy.deepcopy(template)
        else:
            result = self.initialize(definition, class_, **kwargs)
        return result

    def initialize(self, definition, class_, **kwargs):
        """Initialize a new object using the definition and the given class"""
        return class_(**kwargs)

    # read methods: responsible for loading data into the created object

    def read_template(self, definition, class_, template=None, **kwargs):
        return template

    def read_class(self, definition):
        """Returns the class information from the definition"""
        return None     # overload

    def load_class(self, class_):
        result = None
        from pycultivator.core.pcUtility import import_object_from_str
        try:
            result = import_object_from_str(class_)
        except ImportError:
            pass
        if result is None:
            self.getLog().warning("Unable to load class: {}".format(class_))
        return result

    def read(self, definition, obj, **kwargs):
        """Read information from the definition and store in the object

        Will try to read the given definition into a object, using the default as template.
        Returns None if parsing fails.

        :param definition: The definition to read
        :param obj: The template object to expand / overwrite
        :type obj: pycultivator.core.objects.BaseObject
        :return: The parsed element or None if parsing failed
        :rtype: None or pycultivator.core.objects.BaseObject
        """
        # make sure the object is supported
        self.assert_template(obj)
        # register this configuration object as being responsible for this object
        if isinstance(obj, PCObject):
            setattr(obj, 'config', self)
        # read attributes
        obj = self.read_properties(definition, obj)
        return obj

    def read_properties(self, definition, obj):
        """Read object properties into the object

        :param definition: The definition to read
        :param obj: The template object to expand / overwrite
        :type obj: pycultivator.core.objects.BaseObject
        :return: The parsed element or None if parsing failed
        :rtype: None or pycultivator.core.objects.BaseObject
        """
        return obj      # overload

    # update methods

    def update(self, obj, root=None):
        """Refresh the information in the object with information from the configuration source

        :param obj: Object to update in configuration
        :type obj: pycultivator.core.objects.BaseObject
        :param root: The root definition to search under
        :return: The updated object
        :rtype: pycultivator.core.objects.BaseObject
        """
        element = self.find_first(obj, root=root)
        if element is None:
            raise ConfigException("Unable to find object in configuration source")
        return self.read(element, obj)

    # save methods

    def save(self, obj, root=None, **kwargs):
        """Save the given object to the configuration source

        :param obj: The object to save
        :type obj: pycultivator.core.objects.configurable.BaseObject
        :param root: The root definition to search under
        :return: Whether the object was saved successfully
        :rtype: bool
        """
        result = False
        definition = self.find_first(obj, root=root)
        if definition is None:
            definition = self.create_definition(obj, root=root)
        # now we have the element change all the parameters
        if definition is not None:
            result = self.write(obj, definition)
        return result

    # generate: creates a new definition

    def create_definition(self, obj, root=None):
        """ Creates a new definition in the configuration source

        :param obj: The object to create the definition for
        :param root: The root definition to the create the definition under
        :return: A new definition object
        :rtype: None or object
        """
        return None     # overload

    # write methods: responsible for writing the object into a definition

    def write(self, obj, definition):
        """Writes the information from the object into the definition

        :param obj: The object to write
        :type obj: pycultivator.core.objects.BaseObject
        :param definition: The definition to write the information to
        :return: Whether the object was written successfully
        :rtype: bool
        """
        self.assert_definition(definition)
        # register this configuration object as being responsible for this object
        if isinstance(obj, PCObject):
            obj.config = self
        # write properties
        return self.write_properties(obj, definition)

    def write_properties(self, obj, definition):
        """Write the object properties to the definition

        :param obj: The object containing the settings
        :type obj: pycultivator.core.objects.BaseObject
        :param definition: The definition to write the information to
        :rtype: bool
        """
        return False    # overload

    # delete methods: responsible for remove an object from the configuration source

    def delete(self, obj, root=None):
        """Delete an object from the configuration source

        :param obj: The object to delete
        :param root: The root definition to search under
        :rtype: bool
        """
        result = False
        definition = self.find_first(obj, root=root)
        if definition is not None:
            result = self.delete_definition(definition)
        return result

    def delete_definition(self, definition):
        """Remove the definition from the configuration source

        :param definition: The definition to delete
        :return: Whether this was successful
        :rtype: bool
        """
        return False     # overload


class ObjectConfig(BaseObjectConfig):

    _configures = ConfigurableObject

    def read(self, definition, obj, **kwargs):
        obj = super(ObjectConfig, self).read(definition, obj, **kwargs)
        # read settings
        obj = self.read_settings(definition, obj)
        return obj

    def read_settings(self, definition, obj, template=None):
        """Read settings into the object"""
        helper = self.source.getHelperFor("settings")
        if helper is not None:
            settings = helper.load(definition, template)
            """:type: dict[str, object]"""
            obj.settings.update(settings)
        return obj

    def write(self, obj, definition):
        """Write the object information to the configuration source

        :param obj: The object containing the settings
        :type obj: pycultivator.core.objects.configurable.ConfigurableObject
        :param definition: The definition to write the information to
        :rtype: bool
        """
        result = super(ObjectConfig, self).write(obj, definition)
        if result:
            # write settings
            result = self.write_settings(obj, definition)
        return result

    def write_settings(self, obj, definition):
        """Write the settings information to the configuration source

        :param obj: The object containing the settings
        :type obj: pycultivator.core.objects.configurable.ConfigurableObject
        :param definition: The definition to write the information to
        :rtype: bool
        """
        result = False
        helper = self.source.getHelperFor("settings")
        """:type: pycultivator.config.baseConfig.SettingsConfig"""
        if helper is not None:
            settings = obj.settings.reduce(obj.getNameSpace())
            result = helper.save(settings, root=definition, save_parent=False)
        return result


class PartConfig(ObjectConfig):
    """ABSTRACT class detailing how pycultivator parts should be loaded from a configuration source

    PartConfig Objects are an enhanced version of ObjectConfig Objects,
    specialized in creating hierarchical objects (such as ComplexDevice, Instruments etc.)
    """

    namespace = "config.part"
    default_settings = {
        # default settings for this class
    }

    _configures = HierarchicalObject
    _configures_types = {"part"}

    # find methods

    def find(self, part, root=None, **kwargs):
        """Find all definitions that could have created the given object `o`

        1. If no root is given and part has parent find parent and set as root
        2. Search for part under root

        :param part: Part to look for it's definition
        :type part: pycultivator.core.objects.hierarchical.HierarchicalObject
        :param root: Root definition to search under
        :return:
        """
        self.assert_object(part, allow_none=False)
        if root is None and part.hasParent():
            parent = part.parent
            helper = self.getSource().getHelperFor(parent)
            if helper is not None:
                root = helper.find_first(parent, root=root)
        return super(PartConfig, self).find(part, root=root, **kwargs)

    # parse methods

    def parse(self, definition, template=None, parent=None, **kwargs):
        return super(PartConfig, self).parse(
            definition, template=template, parent=parent, **kwargs
        )

    # read methods

    def read(self, definition, obj, parent=None, **kwargs):
        """Read information from the definition and stores it in the object

        :param definition: The definition to read
        :type definition: object
        :param obj: The object to store the information into
        :type obj: pycultivator.core.objects.hierarchical.HierarchicalObject
        :param parent: The parent object of the new object
        :return: The parsed element or None if parsing failed
        :rtype: None or pycultivator.core.objects.hierarchical.HierarchicalObject
        """
        obj = super(PartConfig, self).read(definition, obj, **kwargs)
        """:type: pycultivator.core.objects.hierarchical.HierarchicalObject"""
        obj.setParent(parent)
        return obj

    # write methods

    def write_children(self, children, definition, helper=None):
        result = True
        for child in children:
            if helper is None or not helper.canConfigure(child):
                helper = self.source.getHelperFor(child)
            if helper is not None:
                result = helper.save(child, root=definition, save_parent=False) and result
        return result

    # save methods

    def save(self, part, root=None, save_parent=True):
        """Save the configuration of this part to the configuration source

        Follows the following steps:

        1. if hasParent
         1.1 find parent element and use as root
         1.2 if no parent element save parent
        2. find definition
         2.1 if none found: create element
        3. if definition is given
         3.1 write information


        :param part: The part to save
        :type part: pycultivator.core.pcObject.HierarchicalObject
        :param root: The root definition to save this definition under
        :return: Whether the part was saved succesfully
        :rtype: bool
        """
        if save_parent and part.hasParent():
            parent = part.getParent()
            helper = self.source.getHelperFor(parent)
            if helper is not None:
                root = helper.find_first(parent, root=root)
                if root is None:
                    helper.save(parent, root=root)
                    root = helper.find_first(parent, root=root)
        return super(PartConfig, self).save(part, root=root)


class BaseDeviceConfig(PartConfig):
    """ABSTRACT Config serving as base for Device and ComplexDevice"""

    namespace = "config.device"
    default_settings = {
        "pre.fill": True
    }

    @property
    def pre_fill(self):
        """Returns whether all channel indexes are set to the default object"""
        return self.getSetting("pre.fill") is True

    @pre_fill.setter
    def pre_fill(self, state):
        """Sets whether all channels in this device are initialised with default objects."""
        self.setSetting("pre.fill", state is True)

    def initialize(self, definition, class_, **kwargs):
        """Initialize a new object using the definition and the given class"""
        return class_(0, **kwargs)


class ConnectedDeviceConfig(BaseDeviceConfig):
    """ABSTRACT Config serving as for connected devices"""

    def read(self, definition, obj, parent=None, **kwargs):
        obj = super(ConnectedDeviceConfig, self).read(definition, obj, parent=parent, **kwargs)
        # load connection
        obj = self.read_connection(definition, obj)
        return obj

    def read_connection(self, definition, obj, template=None):
        """Loads the connection for this element into the device object"""
        # load connection
        helper = self.getSource().getHelperFor("connection")
        """:type: pycultivator.config.XMLConfig.XMLConnectionConfig"""
        if helper is not None:
            c = helper.load(definition, template=template)
            if c is not None:
                obj.setConnection(c)
        return obj

    def write(self, obj, definition):
        """Write the device information to the configuration source

        :type obj: pycultivator.device._device.ConnectedDevice
        :type definition: object
        :rtype: bool
        """
        result = super(ConnectedDeviceConfig, self).write(obj, definition)
        if result:
            result = self.write_connection(obj, definition)
        return result

    def write_connection(self, obj, definition):
        """Write connection information to the definition"""
        # return true if there was no connection to save in the first place
        result = not obj.hasConnection()
        # double negate to get true if there is a connection
        if not result:
            c = obj.getConnection()
            # write connection
            self.write_children([c], definition)
        return result


class InstrumentDeviceConfig(BaseDeviceConfig):
    """ABSTRACT Config class to serialize device classes from/to configuration source"""

    def read(self, definition, obj, parent=None, **kwargs):
        """Read the information from the definition into the

        :param definition: The definition to read from
        :param obj: The device to read into
        :param parent: Parent of the device
        :return: The updated device
        :rtype: pycultivator.device.device.Device
        """
        obj = super(InstrumentDeviceConfig, self).read(definition, obj, parent)
        """:type: pycultivator.device._device.InstrumentDevice"""
        obj = self.read_instruments(definition, obj)
        return obj

    def read_default_instrument(self, definition, obj, _type="instrument"):
        """Loads the default channel definition of this device"""
        result = None
        # loads the channel config helper
        helper = self.getSource().getHelperFor(_type)
        """:type: pycultivator.config.xmlConfig.XMLInstrumentConfig"""
        if helper is not None:
            # make a new template class
            template = helper.default_class("0", parent=obj)
            # read information contained in the device object (calibrations etc.)
            result = helper.read(definition, template, parent=obj)
        return result

    def read_instruments(self, definition, obj, template=None):
        """ Load instrument objects from the configuration source in to the device

        :param definition: The definition to read from
        :type definition: object
        :param obj: The device object to load into
        :type obj: pycultivator.device._device.InstrumentDevice
        :param template: Template object used as template to create new instruments from
        :return: The updated device
        :rtype: pycultivator.device._device.InstrumentDevice
        """
        # pre fill with instruments
        if self.pre_fill:
            obj = self.pre_fill_instruments(definition, obj)
        helper = self.source.getHelperFor("instrument")
        """:type: pycultivator.config.baseConfig.InstrumentConfig"""
        instruments = []
        if helper is not None:
            # load all instruments
            instruments = helper.load_all(root=definition, parent=obj, template=template)
            """:type: list[pycultivator.instrument.Instrument.Instrument]"""
        for i in instruments:
            obj.setInstrument(i.getName(), i)
        return obj

    def pre_fill_instruments(self, definition, obj,):
        limits = obj.getInstrumentLimits()
        # iterate over limits
        for t, limit in limits.items():
            # add instruments of this type
            template = self.read_default_instrument(definition, obj, _type=t)
            import copy
            if template is not None:
                # add all allowed instruments
                for idx in range(limit):
                    i = copy.deepcopy(template)
                    # load default instrument
                    i.setName(str(idx))
                    # add to device
                    obj.addInstrument(i)
        return obj

    def write(self, obj, definition):
        """Write the device information to the configuration source

        :type obj: pycultivator.device._device.InstrumentDevice
        :type definition: object
        :rtype: bool
        """
        result = super(InstrumentDeviceConfig, self).write(obj, definition)
        if result:
            result = self.write_instruments(obj, definition)
        return result

    def write_instruments(self, obj, definition):
        """Write instrument information to the configuration source

        :type obj: pycultivator.device._device.InstrumentDevice
        :type definition: object
        :rtype: bool
        """
        instruments = obj.getInstruments().values()
        return self.write_children(instruments, definition)


class DeviceConfig(ConnectedDeviceConfig, InstrumentDeviceConfig):
    """ABSTRACT Config class to serialise device classes from/to configuration sources"""

    _configures = Device
    _configures_types = {"device"}


class ComplexDeviceConfig(ConnectedDeviceConfig):
    """ABSTRACT Config class to create/save/update the device objects from/to the configuration source"""

    _configures = ComplexDevice
    _configures_types = {'device', 'complex_device'}

    # read

    def read(self, definition, obj, parent=None, **kwargs):
        """Read the information from the definition into the

        :param definition: The definition to read from
        :param obj: The device to read into
        :param parent: Parent of the device
        :return: The updated device
        :rtype: pycultivator.device.device.Device
        """
        obj = super(ComplexDeviceConfig, self).read(definition, obj, parent)
        """:type: pycultivator.device.device.ComplexDevice"""
        # read channels into the object
        obj = self.read_channels(definition, obj)
        return obj

    def read_default_channel(self, definition, obj):
        """Loads the default channel definition of this device"""
        # loads the channel config helper
        helper = self.getSource().getHelperFor("channel")
        """:type: pycultivator.config.xmlConfig.XMLChannelConfig"""
        template = helper.default_class("0", parent=obj)
        return helper.read(definition, template, parent=obj)

    def read_channels(self, definition, obj, template=None):
        """Loads the channel definitions under this definition into the device object"""
        channels = {}
        if self.pre_fill and obj.MAX_CHANNELS is not None:
            # fill with None
            for idx in range(obj.MAX_CHANNELS):
                channels[idx] = None
        if template is None:
            # load default channel as template
            template = self.read_default_channel(definition, obj)
        # load channel config helper
        helper = self.source.getHelperFor("channel")
        """:type: pycultivator.config.baseConfig.ChannelConfig"""
        if helper is not None:
            # load all channels
            results = helper.load_all(root=definition, template=template, parent=obj)
            """:type: list[pycultivator.device.channel.Channel]"""
            for result in results:
                channels[result.name] = result
        # add them to the device
        import copy
        for idx, c in channels.items():
            if c is None:
                c = copy.deepcopy(template)
                c.name = idx
            name = Parser.to_int(c.name, default=idx)
            if obj.hasChannel(name):
                obj.setChannel(c, name)
            else:
                obj.addChannel(c, name)
        return obj

    def write(self, obj, definition):
        """Write device information to the configuration source

        :type obj: pycultivator.device.device.ComplexDevice
        :type definition: object
        :rtype: bool
        """
        result = super(ComplexDeviceConfig, self).write(obj, definition)
        # write channel information
        if result:
            self.write_channels(obj, definition)
        return result

    def write_channels(self, obj, definition):
        """Write channel information to the configuration source

        :type obj: pycultivator.device.device.ComplexDevice
        :type definition: object
        :rtype: bool
        """
        channels = obj.getChannels().values()
        return self.write_children(channels, definition)


class ConnectionConfig(ObjectConfig):
    """ABSTRACT Config class to handle connection configuration from a configuration source"""

    _configures = connection.Connection
    _configures_types = {"connection"}

    namespace = "config.connection"
    default_settings = {
        "fake.connection": False
    }

    @property
    def use_fake(self):
        return self.getSetting("fake.connection") is True

    @use_fake.setter
    def use_fake(self, state):
        self.setSetting("fake.connection", state is True)

    def initialize(self, definition, class_, **kwargs):
        if self.use_fake:
            class_ = class_.getFake(**kwargs)
        return class_(**kwargs)


class ChannelConfig(InstrumentDeviceConfig):
    """ABSTRACT Config class to handle the channel configuration from the configuration source"""

    _configures = Channel
    _configures_types = {"channel"}


class InstrumentConfig(PartConfig):
    """ABSTRACT Config class to handle the instrument configuration from the configuration source"""

    _configures = instrument.Instrument
    _configures_types = {"instrument"}

    def initialize(self, definition, class_, **kwargs):
        """Create a new Instrument object with default settings (hard-coded)"""
        return class_(0, **kwargs)

    def read(self, definition, obj, parent=None, **kwargs):
        obj = super(InstrumentConfig, self).read(definition, obj, parent=parent, **kwargs)
        """:type: pycultivator.instrument.base.Instrument"""
        obj = self.read_calibrations(definition, obj)
        return obj

    def read_calibrations(self, definition, obj, template=None):
        # create and load calibration curves; load config helper
        helper = self.source.getHelperFor(calibration.Calibration)
        """:type: pycultivator.config.baseConfig.CalibrationConfig"""
        # load cTemplate
        calibrations = []
        if helper is not None:
            calibrations = helper.load_all(root=definition, template=template, parent=obj)
        for c in calibrations:
            obj.setCalibration(c)
        return obj

    def write(self, obj, definition):
        """Write instrument information to the configuration source

        :type obj: pycultivator.instrument.Instrument
        :type definition: object
        :rtype: bool
        """
        result = super(InstrumentConfig, self).write(obj, definition)
        if result:
            self.write_calibrations(obj, definition)
        return result

    def write_calibrations(self, obj, definition):
        """Write calibration information to the configuration source

        :type obj: pycultivator.instrument.Instrument
        :type definition: object
        :rtype: bool
        """
        calibrations = obj.getCalibrations().values()
        return self.write_children(calibrations, definition)


class CalibrationConfig(PartConfig):
    """ABSTRACT Config class to handle calibration configuration from a configuration source"""

    _configures = calibration.Calibration
    _configures_types = {"calibration"}

    def initialize(self, definition, class_, **kwargs):
        return class_(0, **kwargs)

    def read(self, definition, obj, parent=None, **kwargs):
        obj = super(CalibrationConfig, self).read(definition, obj, parent=parent, **kwargs)
        """:type: pycultivator.relation.calibration.Calibration"""
        # read polynomials
        obj = self.read_polynomials(definition, obj)
        return obj

    def read_polynomials(self, definition, obj, template=None):
        # load polynomial config
        helper = self.source.getHelperFor(polynomial.PolynomialModel)
        """:type: pycultivator.config.baseConfig.PolynomialConfig"""
        polynomials = []
        if helper is not None:
            polynomials = helper.load_all(root=definition, template=template, parent=obj)
        for p in polynomials:
            # overwrite existing polynomials
            obj.setPolynomial(p)
        return obj

    def write(self, obj, definition):
        """Write calibration information to the configuration source

        :type obj: pycultivator.relation.calibration.Calibration
        :type definition: object
        :rtype: bool
        """
        result = super(CalibrationConfig, self).write(obj, definition)
        if result:
            result = self.write_polynomials(obj, definition)
        return result

    def write_polynomials(self, obj, definition):
        """Write polynomial information to the configuration source

        :type obj: pycultivator.relation.calibration.Calibration
        :type definition: object
        :rtype: bool
        """
        result = True
        # write polynomials
        default = obj.getDefaultPolynomial()
        polynomials = filter(lambda p: p != default, obj.getPolynomials())
        return self.write_children(polynomials, definition)


class PolynomialConfig(PartConfig):
    """ABSTRACT Config class to handle polynomial configuration from a configuration source"""

    _configures = polynomial.PolynomialModel
    _configures_types = {"polynomial"}

    def initialize(self, definition, class_, **kwargs):
        return class_(0, **kwargs)

    def read(self, definition, obj, parent=None, **kwargs):
        obj = super(PolynomialConfig, self).read(definition, obj, parent=parent, **kwargs)
        """:type: pycultivator.relation.polynomial.Polynomial"""
        # read the coefficients
        self.read_coefficients(definition, obj)
        return obj

    # Read Children

    def read_coefficients(self, definition, obj, template=None):
        raise NotImplementedError

    def read_coefficient(self, definition, obj, template=None):
        raise NotImplementedError

    def write(self, obj, definition):
        """Write polynomial information to the configuration source

        :type obj: pycultivator.relation.polynomial.Polynomial
        :type definition: object
        :rtype: bool
        """
        result = super(PolynomialConfig, self).write(obj, definition)
        if result:
            result = self.write_coefficients(definition, obj)
        return result

    def write_coefficients(self, definition, obj, template=None):
        result = True
        for order, v in enumerate(obj.getCoefficients()):
            result = self.write_coefficient(definition, order, v) and result
        return result

    def write_coefficient(self, definition, order, value):
        raise NotImplementedError


class SettingsConfig(BaseObjectConfig):
    """ABSTRACT Config class to handle settings configuration from a configuration source"""

    # by default we will use pcSettings.PCSettings as default class in create_class
    # if necessary consider changing the default_class property
    _configures = (PCSettings, dict)
    _configures_types = {"settings"}

    def read(self, definition, obj, **kwargs):
        obj = super(SettingsConfig, self).read(definition, obj, **kwargs)
        # read individual settings
        return obj


class Config(ConfigurableObject):
    """ The config class provides the API to configure a pycultivator object."""

    # a list of available helper classes
    _known_helpers = [
        # ObjectConfig,
        # PartConfig,
        ComplexDeviceConfig,
        ConnectionConfig,
        ChannelConfig,
        InstrumentConfig,
        CalibrationConfig,
        PolynomialConfig,
        SettingsConfig,
    ]

    namespace = "config"
    default_settings = {
        # Add default settings for this class
    }

    def __init__(self, settings=None, **kwargs):
        super(Config, self).__init__(settings=settings, **kwargs)
        self._helpers = []

    @property
    def location(self):
        return self.getLocation()

    def getLocation(self):
        return None         # overload

    @classmethod
    def load(cls, location, settings=None, **kwargs):
        """ABSTRACT Loads the source into memory and creates the config object

        :param location: Location from which the configuration will be loaded
        :type location: str
        :param settings: Settings to be applied to the configuration source handler
        :type settings: pycultivator.core.pcSettings.Settings
        :param kwargs: Extra arguments using for loading the configuration source
        :type kwargs: dict
        :rtype: None or pycultivator.config.baseConfig.Config
        """
        return None         # overload

    @classmethod
    def extractClassName(cls, location):
        """Extracts the name of the class capable of loading the configuration

        :rtype: str
        """
        return None         # overload

    @classmethod
    def extractClass(cls, source):
        """Returns a reference to the Config Helper Class for this config file

        Each xml config file provides the fully qualified name of the class capable of loading the file.

        :param source: Configuration source to use
        :return: The Class at the specified location or None if failed
        :rtype: None or Callable[..., pycultivator.config.xmlConfig.XMLConfig]
        """
        result = None
        import_path = cls.extractClassName(source)
        if import_path is not None:
            from pycultivator.core import pcUtility
            try:
                result = pcUtility.import_object_from_str(import_path)
            except ImportError:
                pass
        return result

    @classmethod
    def assert_config(cls, location, **kwargs):
        """Assert a valid configuration source at the location

        :raises: ConfigException
        """
        raise ConfigException("Invalid source")

    @classmethod
    def check(cls, location, silent=True, **kwargs):
        """ABSTRACT Checks whether the source can be loaded into the configuration object

        :param location: Location that has to be checked
        :type location: str
        :param silent: Whether to raise a ConfigException as part of the checking process
        :type silent: bool
        :rtype: bool
        """
        result = False
        try:
            cls.assert_config(location, **kwargs)
            result = True
        except ConfigException as ce:
            if silent:
                cls.getLog().warning(
                    "Invalid Configuration Source at {}:\n{}".format(location, ce)
                )
            else:
                raise
        return result

    def save(self, location=None):
        """Save the configuration to the given location"""
        return False

    @classmethod
    def getKnownHelpers(cls):
        """Returns a list of all known helpers that can be loaded by this source

        :rtype: list[Callable[..., pycultivator.config.Config.ObjectConfig]]
        """
        return cls._known_helpers

    @classmethod
    def knowsHelper(cls, helper_class):
        """Returns whether the given class is known by this source

        :type helper_class: Callable[..., pycultivator.config.Config.ObjectConfig]
        :rtype: bool
        """
        return helper_class in cls._known_helpers

    def getHelpers(self):
        """Returns all helpers associated to this configuration source

        :return: All helpers associated with this configuration source
        :rtype: list[pycultivator.config.baseConfig.ObjectConfig]
        """
        return self._helpers

    def hasHelperOfClass(self, helper_class):
        """Returns whether there this source has a helper of the given helper class

        :type helper_class: Callable[..., pycultivator.config.Config.ObjectConfig]
        :rtype: bool
        """
        return len(self._getHelpersOfClass(helper_class)) > 0

    def hasHelperFor(self, desired_class):
        """Returns whether the configuration source has a helper for the desired class

        :type desired_class: Callable[..., pycultivator.config.Config.ObjectConfig]
        :rtype: bool
        """
        return self._getHelpersFor(desired_class) is not None

    def _getHelpersOfClass(self, helper_class):
        results = []
        for helper in self.getHelpers():
            if isinstance(helper, helper_class) and helper not in results:
                results.append(helper)
        return results

    def _getHelperOfClass(self, helper_class):
        result = None
        results = self._getHelpersOfClass(helper_class)
        if len(results) > 0:
            result = results[0]
        return result

    def getHelpersOfClass(self, helper_class):
        """Returns all helpers of the given helper class

        :type helper_class: Callable[..., pycultivator.config.Config.ObjectConfig]
        :rtype: list[pycultivator.config.baseConfig.ObjectConfig]
        """
        return self._getHelpersOfClass(helper_class)

    def getHelperOfClass(self, helper_class, settings=None, **kwargs):
        """Returns the first helper of the given helper class.

        :type helper_class: Callable[..., pycultivator.config.Config.ObjectConfig]
        :type settings: None or dict or pycultivator.core.pcSettings.Settings
        :rtype: pycultivator.config.baseConfig.ObjectConfig
        """
        result = self._getHelperOfClass(helper_class)
        if result is None:
            result = self._createHelper(helper_class, settings=settings, **kwargs)
        return result

    def _getHelpersFor(self, desired_class):
        results = []
        for helper in self.getHelpers():
            if helper.canConfigure(desired_class) and helper not in results:
                results.append(helper)
        return results

    def getHelpersFor(self, desired_class, settings=None, **kwargs):
        """Returns all helpers that can help creating the desired class.

        :type desired_class: type | object | str
        :rtype: list[pycultivator.config.baseConfig.ObjectConfig]
        """
        results = self._getHelpersFor(desired_class)
        if len(results) == 0:
            results.append(self._createHelperFor(desired_class, settings=settings, **kwargs))
        return results

    def getHelperFor(self, desired_class, settings=None, **kwargs):
        """Returns first helper that can help creating the desired class.

        :type desired_class: type | object | str
        :rtype: pycultivator.config.baseConfig.ObjectConfig
        """
        results = self.getHelpersFor(desired_class=desired_class, settings=settings, **kwargs)
        if len(results) > 0:
            result = results[0]
        else:
            result = self._createHelperFor(desired_class, settings=settings, **kwargs)
        return result

    def _createHelper(self, helper_class, settings=None, **kwargs):
        # automatically load settings from this configuration file into the helper
        _settings = self.getSettings().copy().reduce(self.getNameSpace())
        _settings.update(settings, namespace="")
        result = helper_class(self, _settings, **kwargs)
        self._registerHelper(result)
        return result

    def _createHelperFor(self, desired_class, settings=None, silent=True, **kwargs):
        """Create a new helper for the desired class

        :param desired_class: Callable[..., pycultivator.config.Config.ObjectConfig]
        :param settings:
        :param kwargs:
        :return:
        """
        desired_name = type(desired_class).__name__
        if isinstance(desired_class, str):
            desired_name = desired_class
        if isinstance(desired_class, type):
            desired_name = desired_class.__name__
        result = None
        for helper_class in self.getKnownHelpers():
            if helper_class.canConfigure(desired_class):
                result = self._createHelper(helper_class, settings=settings, **kwargs)
                break
        if not silent and result is None:
            raise ConfigException("No helper that can handle - {}.".format(desired_name))
        return result

    def _registerHelper(self, helper):
        """Registers the helper to this configuration source"""
        helper_class = helper.__class__
        helper_name = helper_class.__name__
        if self.hasHelperOfClass(helper.__class__):
            raise ConfigException("Same type of helper - {} - is already registered".format(helper_name))
        # register
        self.getHelpers().append(helper)
        return self

    def _unregisterHelper(self, helper):
        """Removes the helper from the registry"""
        helper_class = helper.__class__
        helper_name = helper_class.__name__
        if helper not in self.getHelpers():
            raise ConfigException("Cannot unregister - {} -, this helper is not registered".format(helper_name))
        self.getHelpers().pop(helper)
        return self


class ConfigException(PCException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)

