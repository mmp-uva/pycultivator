"""
Module for testing the pycultivator Configuration class
"""

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.config import baseConfig

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class TestConfig(UvaSubjectTestCase):
    """Class for testing the pycultivator Config class"""

    _abstract = True
    _subject_cls = baseConfig.Config
    config_location = None

    @classmethod
    def getConfigLocation(cls):
        return cls.config_location

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass().load(self.getConfigLocation())

    def test_getHelperFor(self):
        # test using a string
        self.assertIsInstance(
            self.subject.getHelperFor("device"),
            baseConfig.DeviceConfig
        )
        # test with invalid string
        # should just return None
        self.assertIsNone(self.subject.getHelperFor("foo"))
        # make it fail loud
        with self.assertRaises(baseConfig.ConfigException):
            self.subject.getHelperFor("foo", silent=False)
        # test with class
        self.assertIsInstance(
            self.subject.getHelperFor(baseConfig.PCSettings),
            baseConfig.SettingsConfig
        )
        # test with str
        self.assertIsInstance(
            self.subject.getHelperFor("settings"),
            baseConfig.SettingsConfig
        )


class TestObjectConfig(UvaSubjectTestCase):
    """Class for testing the pycultivator classConfig class"""

    _abstract = True
    _subject_cls = baseConfig.ObjectConfig
    _config_cls = baseConfig.Config

    config_location = None

    def __init__(self, methodName='runTest'):
        super(TestObjectConfig, self).__init__(methodName=methodName)
        self._config = None

    def getDefaultObject(self):
        return self.getSubject().default_class()

    @classmethod
    def getConfigLocation(cls):
        return cls.config_location

    def getConfigClass(self):
        """Returns the used config class"""
        return self._config_cls

    def getConfig(self):
        return self._config

    def initConfig(self):
        return self.getConfigClass().load(self.getConfigLocation())

    def setUp(self):
        super(TestObjectConfig, self).setUp()
        self._config = self.initConfig()


class TestPartConfig(TestObjectConfig):
    """Class for testing the pycultivator PartConfig class"""

    _abstract = True
    _subject_cls = baseConfig.PartConfig


class TestBaseDeviceConfig(TestPartConfig):
    """Test the pycultivator BaseDeviceConfig class"""

    _abstract = True
    _subject_cls = baseConfig.BaseDeviceConfig


class TestDeviceConfig(TestBaseDeviceConfig):
    """Class for testing the pycultivator ComplexDeviceConfig class"""

    _abstract = True
    _subject_cls = baseConfig.DeviceConfig

    def getDefaultObject(self):
        return self.subject.default_class(0, None)


class TestComplexDeviceConfig(TestBaseDeviceConfig):
    """Class for testing the pycultivator ComplexDeviceConfig class"""

    _abstract = True
    _subject_cls = baseConfig.ComplexDeviceConfig


class TestConnectionConfig(TestPartConfig):
    """Class for testing the pycultivator ConnectionConfig class"""

    _abstract = True
    _subject_cls = baseConfig.ConnectionConfig


class TestChannelConfig(TestPartConfig):
    """Class for testing the pycultivator ChannelConfig class"""

    _abstract = True
    _subject_cls = baseConfig.ChannelConfig


class TestInstrumentConfig(TestPartConfig):
    """Class for testing the pycultivator InstrumentConfig class"""

    _abstract = True
    _subject_cls = baseConfig.InstrumentConfig

    def getDefaultObject(self):
        return self.subject.default_class(0, None)


class TestCalibrationConfig(TestObjectConfig):
    """Class for testing the CalibrationConfig class"""

    _abstract = True
    _subject_cls = baseConfig.CalibrationConfig

    def getDefaultObject(self):
        return self.subject.default_class(0, None)


class TestPolynomialConfig(TestObjectConfig):
    """Class for testing the PolynomialConfig class"""

    _abstract = True
    _subject_cls = baseConfig.PolynomialConfig

    def getDefaultObject(self):
        return self.subject.default_class(0, None)


class TestSettingsConfig(TestObjectConfig):
    """Class for testing the SettingsConfig class"""

    _abstract = True
    _subject_cls = baseConfig.SettingsConfig
