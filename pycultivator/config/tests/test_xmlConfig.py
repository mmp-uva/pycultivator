"""
Module for testing the pyCultivator XML Configuration Objects
"""

import test_baseConfig
from pycultivator.config import xmlConfig
from pycultivator.core import pcXML
from unittest2 import SkipTest
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

SCHEMA_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "schema")
CONFIG_PATH = os.path.join(SCHEMA_DIR, "configuration.xml")
SCHEMA_PATH = os.path.join(SCHEMA_DIR, "pycultivator.xsd")


class TestXMLConfig(test_baseConfig.TestConfig):
    """Class for testing the lcp configuration class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLConfig

    schema_path = SCHEMA_PATH
    config_location = CONFIG_PATH

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass().load(self.getConfigLocation())

    def getSubject(self):
        """Return reference to the instance of the class subjected to this test

        :rtype: pycultivator.config.xmlConfig.XMLConfig
        """
        return test_baseConfig.TestConfig.getSubject(self)

    def getConfigPath(self):
        return self.config_location

    def getSchemaPath(self):
        return self.schema_path

    def test_load(self):
        """Test opening a XMLConfiguration file"""
        self.assertIsNotNone(
            self.getSubjectClass().load(self.getConfigLocation())
        )
        self.assertIsNone(
            self.getSubjectClass().load("bla.xml")
        )

    def test_check(self):
        """Test checking a XMLConfiguration file"""
        self.assertTrue(self.getSubjectClass().check(self.getConfigLocation()))

    def test_extractClassName(self):
        self.assertEqual(
            self.subject.extractClassName(self.getConfigLocation()),
            ".".join([self.subject.__module__, self.subject.__class__.__name__])
        )
        self.assertIsNone(self.subject.extractClassName("bla.xml"))

    def test_extractClass(self):
        self.assertIs(
            self.subject.extractClass(self.getConfigLocation()),
            self.getSubjectClass()
        )
        # first create xml object to manipulate
        xml = pcXML.XML.createFromSource(self.getConfigLocation())
        # now change class to bogus package
        xml.setElementAttribute(xml.getRoot(), "class", "pycultivator_false.config.xmlConfig.XMLConfig")
        self.assertIsNone(self.subject.extractClass(xml))
        # to non existent sub package
        xml.setElementAttribute(xml.getRoot(), "class", "pycultivator.false.xmlConfig.XMLConfig")
        self.assertIsNone(self.subject.extractClass(xml))
        # to non-existent module
        xml.setElementAttribute(xml.getRoot(), "class", "pycultivator.config.falseConfig.XMLConfig")
        self.assertIsNone(self.subject.extractClass(xml))
        # to non-existent class
        xml.setElementAttribute(xml.getRoot(), "class", "pycultivator.config.xmlConfig.FalseConfig")
        self.assertIsNone(self.subject.extractClass(xml))
        # try to load a class without package
        xml.setElementAttribute(xml.getRoot(), "class", "TestXMLObjectConfig")
        self.assertIsNone(self.subject.extractClass(xml))


class TestXMLObjectConfig(test_baseConfig.TestObjectConfig):
    """Class for testing XMLObjectConfig Object"""

    _abstract = True
    _subject_cls = xmlConfig.XMLObjectConfig
    _config_cls = xmlConfig.XMLConfig

    schema_path = SCHEMA_PATH
    config_location = CONFIG_PATH

    @classmethod
    def getConfiguration(cls):
        from pycultivator.config import load_config
        result = load_config(cls.getConfigLocation())
        if result is None:
            raise ValueError("Unable to load configuration")
        return result

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.config.xmlConfig.XMLObjectConfig
        """
        return super(TestXMLObjectConfig, self).getSubject()

    def initSubject(self, *args, **kwargs):
        c = self.getConfiguration()
        return self.getSubjectClass()(c)

    def test_clean_tag(self):
        self.assertEqual(self.getSubject().clean_tag("a"), "a")
        self.assertEqual(self.getSubject().clean_tag("<a>a"), "<a>a")
        self.assertEqual(self.getSubject().clean_tag("{a}a"), "a")
        self.assertEqual(self.getSubject().clean_tag("{Ab2}a"), "a")
        self.assertEqual(self.getSubject().clean_tag("{}a"), "a")
        self.assertEqual(self.getSubject().clean_tag(" {A2b}a "), "a")
        self.assertEqual(self.getSubject().clean_tag(" {A2b}A "), "a")

    def test_select(self):
        raise SkipTest("Not implemented yet")

    def test_find(self):
        raise SkipTest("Not implemented yet")

    def test_parse(self):
        raise SkipTest("Not implemented yet")

    def test_save(self):
        raise SkipTest("Not implemented yet")

    def test_write(self):
        raise SkipTest("Not implemented yet")

    def test_create_definition(self):
        raise SkipTest("Not implemented yet")

class TestXMLPartConfig(TestXMLObjectConfig, test_baseConfig.TestPartConfig):
    """Class for testing the XMLPartConfig class"""

    _abstract = True
    _subject_cls = xmlConfig.XMLPartConfig


class TestXMLBaseDeviceConfig(TestXMLPartConfig, test_baseConfig.TestBaseDeviceConfig):
    """Test the pycultivator BaseDeviceConfig class"""

    _abstract = True
    _subject_cls = xmlConfig.XMLBaseDeviceConfig

    def test_select(self):
        """:type: pycultivator.config.xmlConfig.XMLComplexDeviceConfig"""
        self.assertEqual(len(self.subject.select()), 1)
        self.assertEqual(len(self.subject.select(scope="/")), 0)
        self.assertEqual(len(self.subject.select(scope="//")), 1)
        self.assertEqual(len(self.subject.select(scope="./")), 1)
        self.assertEqual(len(self.subject.select(name="foo")), 0)
        self.assertEqual(len(self.subject.select(name="0")), 1)

    def test_is_valid_tag(self):
        e = pcXML.et.XML("<device></device>")
        self.assertTrue(self.getSubject().is_valid_tag(e))
        e = pcXML.et.XML("<bla></bla>")
        self.assertFalse(self.getSubject().is_valid_tag(e))
        devices = self.subject.find_elements(self.subject.XML_ELEMENT_TAG)
        self.assertGreaterEqual(len(devices), 1)
        self.assertTrue(self.getSubject().is_valid_tag(devices[0]))


class TestXMLConnectionConfig(TestXMLBaseDeviceConfig, test_baseConfig.TestConnectionConfig):
    """"""

    _abstract = True
    _subject_cls = xmlConfig.XMLConnectionConfig

    def test_select(self):
        self.assertEqual(len(self.subject.select()), 1)
        self.assertEqual(len(self.subject.select(scope="/")), 0)
        self.assertEqual(len(self.subject.select(scope="//")), 1)
        self.assertEqual(len(self.subject.select(scope="./")), 1)

    def test_load_fake(self):
        raise SkipTest("Unable to test on abstract class")


class TestXMLDeviceConfig(TestXMLBaseDeviceConfig, test_baseConfig.TestDeviceConfig):
    """Class for testing the XMLComplexDeviceConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLDeviceConfig
    _xml = """
        <device id='0'>
            <settings />
            <instruments>
                <instrument>
                </instrument>
            </instruments>
        </device>
        """


class TestXMLComplexDeviceConfig(TestXMLBaseDeviceConfig, test_baseConfig.TestComplexDeviceConfig):
    """Class for testing the XMLComplexDeviceConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLComplexDeviceConfig
    _xml = """
        <device id='0'>
            <settings />
            <connection>
    
            </connection>
        </device>
        """

class TestXMLChannelConfig(TestXMLPartConfig, test_baseConfig.TestChannelConfig):
    """Class for testing the XMLChannelConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLChannelConfig

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.config.xmlConfig.XMLChannelConfig
        """
        return super(TestXMLChannelConfig, self).getSubject()

    def test_select(self):
        # test default
        self.assertEqual(len(self.subject.select()), 1)
        # search for channel 1
        self.assertEqual(len(self.subject.select(name="1")), 1)
        self.assertEqual(len(self.subject.select(name=1)), 1)
        self.assertEqual(len(self.subject.select(name=1)), 1)
        # search for channel 9
        self.assertEqual(len(self.subject.select(name="9")), 0)
        self.assertEqual(len(self.subject.select(name=9)), 0)
        # search global
        self.assertEqual(len(self.subject.select(scope="//")), 1)
        # search direct under root
        self.assertEqual(len(self.subject.select(scope="/")), 0)

    def test_read(self):
        raise SkipTest("Cannot test on abstract class")


class TestXMLInstrumentConfig(TestXMLPartConfig, test_baseConfig.TestInstrumentConfig):
    """Class for testing the XMLInstrumentConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLInstrumentConfig

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator.config.xmlConfig.XMLInstrumentConfig
        """
        return super(TestXMLInstrumentConfig, self).getSubject()

    def test_select(self):
        self.assertEqual(len(self.subject.select()), 4)
        self.assertEqual(len(self.subject.select(scope="//")), 4)

    def test_read(self):
        raise SkipTest("Cannot test on abstract class")


class TestXMLCalibrationConfig(TestXMLObjectConfig, test_baseConfig.TestCalibrationConfig):
    """Class for testing the XMLCalibrationConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLCalibrationConfig

    def test_select(self):
        self.assertEqual(len(self.subject.select()), 0)
        self.assertEqual(len(self.subject.select(scope="//")), 2)

    def test_parse(self):
        elements = self.subject.select(scope="//")
        self.assertEqual(len(elements), 2)
        c = self.subject.parse(elements[0])
        self.assertIsNotNone(c)
        self.assertEqual(c.identity, 0)
        self.assertFalse(c.is_active)

    def test_write(self):
        calibration = self.getDefaultObject()
        calibration.add(calibration.createPolynomialModel("0", [0, 1]))
        # test write to element with wrong tag
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <settings></settings></a>""")
        elements = self.getSubject().getXML().getElementsByXPath("//pc:settings", root=xml, prefix="pc")
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        with self.assertRaises(xmlConfig.XMLConfigException):
            self.getSubject().write(calibration, element)
        # test write to existing element
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <calibration></calibration></a>""")
        elements = self.getSubject().select(xml, scope="//")
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        self.getSubject().write(calibration, element)
        # now check if we can find the polynomial
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:polynomial", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 0)
        calibration.clear()
        calibration.add(calibration.createPolynomialModel("0", [0, 2]))
        self.getSubject().write(calibration, element)
        # now check if we can find the polynomial
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:polynomial", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 1)

    def test_save(self):
        calibration = self.getDefaultObject()
        calibration.add(calibration.createPolynomialModel("0", [0, 1]))
        # test save to existing
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:instruments/pc:instrument[@id=$iid]/pc:calibration[@id=$cid]", prefix="pc",
            iid="0", cid="0"
        )
        self.assertEqual(len(elements), 1)
        self.assertTrue(self.getSubject().getXML().isValid())
        element = elements[0]
        self.getSubject().save(calibration, root=element)
        self.assertTrue(self.getSubject().getXML().isValid())
        # test save to new
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:channel[@id=$cid]/pc:instruments/pc:instrument[@id=$iid]", prefix="pc",
            cid="1", iid="0"
        )
        self.assertEqual(len(elements), 1)
        element = elements[0]
        self.getSubject().save(calibration, root=element)
        self.assertTrue(self.getSubject().getXML().isValid())


class TestXMLPolynomialConfig(TestXMLObjectConfig, test_baseConfig.TestPolynomialConfig):
    """Class for testing the XMLCalibrationConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLPolynomialConfig

    def test_select(self):
        # local
        self.assertEqual(len(self.subject.select()), 0)
        # global scope
        self.assertEqual(len(self.subject.select(scope="//")), 2)
        # custom xml
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <polynomial></polynomial></a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 1)

    def test_read(self):
        elements = self.getSubject().select(scope="//")
        self.assertGreaterEqual(len(elements), 2)
        element = elements[0]
        p = self.getSubject().read(element, self.getDefaultObject())
        self.assertIsNotNone(p)
        self.assertEqual(p.identity, 0)

    def test_write(self):
        polynomial = self.getDefaultObject()
        # test write to existing element
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <polynomial></polynomial></a>""")
        elements = self.getSubject().select(xml)
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        self.getSubject().write(polynomial, element)
        # now check if we can find the polynomial
        elements = self.getSubject().getXML().getElementsByXPath(
            "./pc:polynomial", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 1)
        # check if we can find a coefficients
        elements = self.getSubject().getXML().getElementsByXPath(
            ".//pc:coefficient", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 2)
        # test a more complex polynomial
        polynomial = self.getDefaultObject()
        polynomial.setDomain("[1,2.5)")
        polynomial.setCoefficients([-1, 0.5, -3.5527136788e-15])
        # test write to existing element
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <polynomial></polynomial></a>""")
        self.assertTrue(self.subject.getXML().isValid())
        elements = self.getSubject().select(xml)
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        self.getSubject().write(polynomial, element)
        # now check if we can find the polynomial
        elements = self.getSubject().getXML().getElementsByXPath(
            "./pc:polynomial", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 1)
        element = elements[0]
        # check domain
        self.assertEqual(
            self.subject.getXML().getElementAttribute(element, "domain"),
            "[1.0,2.5)"
        )
        # check if we can find a coefficients
        elements = self.getSubject().getXML().getElementsByXPath(
            ".//pc:coefficient", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 3)
        self.assertEqual(float(elements[0].text), -1.0)
        self.assertEqual(float(elements[1].text), 0.5)
        self.assertEqual(float(elements[2].text), -3.5527136788e-15)


class TestXMLSettingsConfig(TestXMLObjectConfig, test_baseConfig.TestSettingsConfig):
    """Class for testing the XMLSettingsConfig class"""

    _abstract = False
    _subject_cls = xmlConfig.XMLSettingsConfig

    SAMPLE_SETTINGS = {'0': 1.0, '1': [1, 2, 3], '2': {'1': 1, 'b': 2, 'bla': 3}}

    def test_select(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <settings></settings></a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 1)
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 0)

    def test_parse(self):
        elements = self.getSubject().select()
        self.assertGreaterEqual(len(elements), 0)
        elements = self.getSubject().select(scope=self.subject.XML_LOCAL_DEEP_SCOPE)
        self.assertGreaterEqual(len(elements), 0)
        element = elements[0]
        s = self.getSubject().parse(element)
        self.assertEqual(s.to_dict(), self.SAMPLE_SETTINGS)

    def test_write(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <settings></settings></a>""")
        elements = self.getSubject().select(xml)
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        self.getSubject().write(self.SAMPLE_SETTINGS, element)
        # now check if we can find the settings class
        elements = self.getSubject().getXML().getElementsByXPath(
            "./pc:settings", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 1)
        # check if we can find a setting element
        elements = self.getSubject().getXML().getElementsByXPath(
            ".//pc:setting", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 3)
        elements = self.getSubject().select(xml)
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        self.assertEqual(
            self.getSubject().parse(element).to_dict(), self.SAMPLE_SETTINGS
        )

    def test_save(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <settings></settings></a>""")
        self.getSubject().save(self.SAMPLE_SETTINGS, root=xml)
        elements = self.getSubject().getXML().getElementsByXPath(
            "./pc:settings/pc:setting", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 3)
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        self.getSubject().save(self.SAMPLE_SETTINGS, root=xml)
        # now check if we can find the settings class
        elements = self.getSubject().getXML().getElementsByXPath(
            "./pc:settings/pc:setting", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 3)
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <settings><setting name="2"><value>1</value></setting></settings>
                <b>
                </b>
            </a>""")
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:a/pc:settings/pc:setting", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 1)
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:b", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 1)
        self.getSubject().save(self.SAMPLE_SETTINGS, root=elements[0])
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:b/pc:settings/pc:setting", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 3)
        elements = self.getSubject().getXML().getElementsByXPath(
            "//pc:a/pc:settings/pc:setting", prefix="pc", root=xml
        )
        self.assertEqual(len(elements), 1)
        devices = self.getSubject().getXML().getElementsByXPath(
            "//pc:device", prefix="pc"
        )
        self.assertEqual(len(devices), 1)
        self.getSubject().save(self.SAMPLE_SETTINGS, devices[0])
        elements = self.getSubject().select(root=devices[0])
        self.assertGreaterEqual(len(elements), 1)
        element = elements[0]
        self.assertEqual(
            self.getSubject().parse(element).to_dict(), self.SAMPLE_SETTINGS
        )
