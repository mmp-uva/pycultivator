"""Test the package"""

import os

from pycultivator.core.tests import UvaTestCase
from pycultivator import config
from pycultivator.config.xmlConfig import XMLConfig

SCHEMA_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "schema")
CONFIG_PATH = os.path.join(SCHEMA_DIR, "configuration.xml")
SCHEMA_PATH = os.path.join(SCHEMA_DIR, "pycultivator.xsd")


class ConfigTestCase(UvaTestCase):

    def test_load_config(self):
        fp = CONFIG_PATH
        self.assertIsInstance(
            config.load_config(fp), XMLConfig
        )

