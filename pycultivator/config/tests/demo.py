
from pycultivator.config.xmlConfig import XMLConfig

import os

SCHEMA_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "schema")
CONFIG_PATH = os.path.join(SCHEMA_DIR, "configuration.xml")
SCHEMA_PATH = os.path.join(SCHEMA_DIR, "pycultivator.xsd")

if __name__ == "__main__":
    device = None
    # load configuration handler
    print("Load configuration file...")
    config = XMLConfig.load(CONFIG_PATH)
    print("Load device from configuration...")
    if config is not None:
        # load device
        xdc = config.getHelperFor("device")
        device = xdc.load()
    print("Done loading...")
    print(device is not None)
    print("Number of channels: {}".format(
        len(device.getChannels())
    ))
