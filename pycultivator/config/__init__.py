# coding=utf-8
"""
Configuration package
"""

from pycultivator.core.pcRegistry import Registry
from baseConfig import *


__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    "load_config", "load_device",

    "Config", "ConfigException",
    "ObjectConfig",
    "PartConfig",
    "DeviceConfig",
    "ConnectionConfig",
    "ChannelConfig",
    "InstrumentConfig",
    "CalibrationConfig",
    "PolynomialConfig",
    "SettingsConfig"
]


class ConfigRegistry(Registry):

    def match_by_source(self, source, settings=None, **kwargs):
        # TODO: we can speed this up using the extension/format of the source
        config = None
        for config_handler in self._registry.values():
            config = config_handler.load(source, settings=settings, **kwargs)
            if config is not None:
                break
        return config


# create registry
config_handlers = ConfigRegistry().load_entry_points("pycultivator.config")


def load_config(source, settings=None, **kwargs):
    """Returns configuration object for the source"""
    return config_handlers.match_by_source(source, settings=settings, **kwargs)


def load_device(config, settings=None, use_fake=False, **kwargs):
    """Returns a device instance by loading it from the configuration source object

    :type config: pycultivator.config.Config | str
    :type settings: dict[str, object] | None
    :type use_fake: bool
    :param kwargs: Extra arguments parsed into settings
    :raises pycultivator.core.PCException: If loading the device failed
    :rtype: pycultivator.device.BaseDevice
    """
    if not isinstance(config, Config):
        # treat as configuration source
        config = load_config(config, settings=settings, **kwargs)
    if not isinstance(config, Config):
        raise TypeError("Invalid Configuration Source object and unable to load into Configuration Source object")
    # set fake connection setting
    config.setSetting("fake.connection", use_fake, namespace="")
    xdc = config.getHelperFor("device", settings={"fake.connection": use_fake}, **kwargs)
    return xdc.load()
