"""This module implements configuration of pyCultivator using XML files"""

import baseConfig
from pycultivator.core import pcXML, Parser
import pkg_resources, os, re

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class XMLBaseObjectConfig(baseConfig.BaseObjectConfig):
    """"""

    XML_LOCAL_SCOPE = pcXML.XML.XPATH_LOCAL_SCOPE
    XML_LOCAL_DEEP_SCOPE = pcXML.XML.XPATH_LOCAL_DEEP_SCOPE
    XML_GLOBAL_SCOPE = pcXML.XML.XPATH_GLOBAL_SCOPE
    XML_GLOBAL_DEEP_SCOPE = pcXML.XML.XPATH_GLOBAL_DEEP_SCOPE

    XML_ELEMENT_TAG = "object"
    XML_ELEMENT_SCOPE = XML_LOCAL_SCOPE
    required_source = "pycultivator.config.xmlConfig.XMLConfig"

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the part configuration

        :type source: pycultivator.config.xmlConfig.XMLConfig
        """
        super(XMLBaseObjectConfig, self).__init__(source=source, settings=settings, **kwargs)

    def getSource(self):
        """Returns the object representing the whole configuration source

        :rtype: pycultivator.config.xmlConfig.XMLConfig
        """
        return super(XMLBaseObjectConfig, self).getSource()

    def getXML(self):
        """Returns the XML Object of the connected source

        :rtype: pycultivator.core.pcXML.XML
        """
        return self.source.getXML()

    @property
    def xml(self):
        return self.getXML()

    @staticmethod
    def clean_tag(tag):
        """Returns the stripped and lowercase tag of a element

        :type tag: str
        :rtype: str
        """
        import re
        return re.sub(r"{[a-zA-Z0-9]*}", '', tag).strip().lower()

    @classmethod
    def compare_tag(cls, one_tag, other_tag):
        return cls.clean_tag(one_tag) == cls.clean_tag(other_tag)

    @classmethod
    def is_valid_tag(cls, element, valid_tag=None):
        """Returns whether the given element can be parsed by this Config Element (based on tag name)

        :type element: lxml.etree._Element._Element
        :type valid_tag: None or str
        :rtype: bool
        """
        if valid_tag is None:
            valid_tag = cls.XML_ELEMENT_TAG
        return cls.compare_tag(element.tag, valid_tag)

    @classmethod
    def assert_definition(cls, element, expected=None):
        """Tests if the given element can be parsed by the Config, and raises an Exception if not.

        :type element: lxml.etree._Element._Element
        :type expected: None or str
        :rtype: None
        """
        if expected is None:
            expected = cls.XML_ELEMENT_TAG
        if not cls.is_valid_tag(element=element, valid_tag=expected):
            raise XMLConfigException(
                "Expected an element with `{}` tag, but received an element with `` tag".format(
                    expected, element
                )
            )

    def find_elements(self, tag, scope=None, identifier=None, name=None, root=None):
        """Finds all elements (within a scope, relative to root) by tag and id (name)

        :param tag: The tag of the desired element
        :type  tag: str
        :param scope: Where to search for the elements ('./' relative to root, './/' all descending from root, etc.)
        :type scope: str
        :param name: The value of the name attribute of the desired element; accepts all (and missing) values if None
        :type name: str or None
        :param identifier: optional value of 'id' attribute to select elements by
        :type identifier: str
        :param root: The root element to search from; search from document root if None
        :type root: lxml.etree._Element._Element or None
        :return: All elements that match the criteria
        :rtype: list[lxml.etree._Element._Element]
        """
        if root is None:
            root = self.getXML().getRoot()
        if scope is None:
            scope = self.XML_ELEMENT_SCOPE
        _variables = {}
        predicate = ""
        xpath = "{}pc:{}{}".format(scope, tag, predicate)
        elements = self.getXML().getElementsByXPath(
            xpath, root=root, prefix="pc", namespace=self.getXML().getXMLNameSpace(), **_variables
        )
        if identifier is not None:
            elements = self.getXML().filterElementsById(elements, identifier)
        if name is not None:
            elements = self.getXML().filterElementsByName(elements, name)
        return elements

    def find_element(self, tag, scope=None, identifier=None, name=None, root=None):
        """Finds the first element (within a scope, relative to root) by tag and id (name)

        :param tag: The tag of the desired element
        :type  tag: str
        :param scope: Where to search for the elements ('./' relative to root, './/' all descending from root, etc.)
        :type scope: str
        :param name: The value of the name attribute of the desired element; accepts all (and missing) values if None
        :type name: str or None
        :param identifier: optional value of 'id' attribute to select elements by
        :type identifier: str
        :param root: The root element to search from; search from document root if None
        :type root: lxml.etree._Element._Element or None
        :return: First element that matches to the criteria or None if no element was found
        :rtype: lxml.etree._Element._Element or None
        """
        result = None
        elements = self.find_elements(tag, scope=scope, identifier=identifier, name=name, root=root)
        if len(elements) > 0:
            result = elements[0]
        return result

    def select(self, root=None, scope=None):
        """Select all elements under root within the given scope that have the correct tag

        :param root: The root element to search from; search from document root if None
        :type root: lxml.etree._Element._Element or None
        :param scope: Scope of search. If None, use element defined scope
        :type scope: None or str
        :rtype: list[lxml.etree._Element._Element]
        """
        return self.find_elements(self.XML_ELEMENT_TAG, scope=scope, root=root)

    def find(self, o, root=None, scope=None, **kwargs):
        """Find all elements that might hold the configuration of the given object"""
        # use standard implementation to check object type, we do not use it's result
        super(XMLBaseObjectConfig, self).find(o, root=root)
        if scope is None:
            scope = self.XML_ELEMENT_SCOPE
        # if root is None use document root
        if root is None:
            root = self.getXML().getRoot()
        # find all eligible elements
        elements = self.find_elements(tag=self.XML_ELEMENT_TAG, scope=scope, root=root)
        # check if root = element we are looking for
        if self.is_valid_tag(root) and root not in elements:
            elements.insert(0, root)
        return elements

    def find_first(self, obj, root=None, scope=None, **kwargs):
        """Find the first element that holds the configuration of the given object"""
        return super(XMLBaseObjectConfig, self).find_first(obj, root=root, scope=scope, **kwargs)

    def read_class(self, definition):
        """Read the class attribute in the definition"""
        return definition.get("class")

    def create_definition(self, obj, root=None):
        """Creates a new definition for the element"""
        if root is None:
            root = self.getXML().getRoot()
        return self.getXML().addElement(root, self.XML_ELEMENT_TAG)

    def write_properties(self, obj, definition):
        """Write the information from the part into the element"""
        return True  # overload

    def delete_definition(self, definition):
        """Remove the definition from the configuration source

        :type definition: lxml.etree._Element._Element
        :rtype: bool
        """
        result = False
        parent = definition.getparent()
        """:type: lxml._Element._Element"""
        if parent is not None:
            parent.remove(definition)
            result = True
        return result


class XMLObjectConfig(XMLBaseObjectConfig, baseConfig.ObjectConfig):
    """A XML Config class to handle the configuration of pycultivator object from a configuration source"""

    XML_ELEMENT_TAG = "object"
    XML_ELEMENT_SCOPE = XMLBaseObjectConfig.XML_LOCAL_SCOPE


class XMLPartConfig(XMLObjectConfig, baseConfig.PartConfig):
    """A XML Config class to handle the configuration of pycultivator Parts from a configuration source"""

    XML_ELEMENT_TAG = "part"
    XML_ELEMENT_SCOPE = XMLBaseObjectConfig.XML_LOCAL_DEEP_SCOPE

    def __init__(self, source, settings=None, **kwargs):
        """Initialise the part configuration

        :type source: pycultivator.config.xmlConfig.XMLConfig
        """
        super(XMLPartConfig, self).__init__(source=source, settings=settings, **kwargs)

    def select(self, root=None, scope=None, name=None):
        """Select all elements under root within the given scope that have the correct tag and ID

        :param root: The root definition to search from
        :param name: The name (identifier) of the definition to find
        :rtype: list
        """
        elements = super(XMLPartConfig, self).select(root=root, scope=scope)
        if name is not None:
            elements = self.getXML().filterElementsById(elements, name)
        return elements

    def find(self, part, root=None, scope=None, **kwargs):
        """Finds the element that holds the configuration of the given object

        :type part: pycultivator.core.pcObject.HierarchicalObject
        :type root: None or lxml.etree._Element._Element
        :rtype: list[lxml.etree._Element._Element]
        """
        results = super(XMLPartConfig, self).find(part, root=root, scope=scope, **kwargs)
        # filter by id
        results = self.getXML().filterElementsById(results, part.name)
        return results

    def initialize(self, definition, class_, **kwargs):
        """Initialize a new object using the definition and the given class"""
        id_ = self.xml.getElementId(definition, _type=str, default="0")
        return class_(id_, **kwargs)

    def read_properties(self, definition, obj):
        """Read property information from the definition into the obj"""
        obj = super(XMLPartConfig, self).read_properties(definition, obj)
        """:type: pycultivator.core.objects.hierarchical.HierarchicalObject"""
        # gather information
        id_ = self.xml.getElementId(definition, _type=str, default="0")
        # apply information
        obj.setIdentity(id_)
        return obj

    # Load methods

    def write_properties(self, obj, definition):
        """ Write the information from the part into the element

        :type obj: pycultivator.core.objects.hierarchical.HierarchicalObject
        """
        # checks validity of element
        result = super(XMLPartConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementAttribute(definition, "id", str(obj.name))
        return result


class XMLBaseDeviceConfig(XMLPartConfig, baseConfig.BaseDeviceConfig):
    """Config class for handling the configuration of BaseDevice classes to/from XML"""

    XML_ELEMENT_TAG = "device"
    XML_ELEMENT_SCOPE = XMLBaseObjectConfig.XML_GLOBAL_DEEP_SCOPE

    def read_properties(self, definition, obj):
        obj = super(XMLBaseDeviceConfig, self).read_properties(definition, obj)
        """:type: pycultivator.device.device.Device"""
        state = self.xml.getElementActive(definition)
        obj.setActive(state)
        return obj

    def write_properties(self, obj, definition):
        """Write object properties to XML

        :param obj: The object to write
        :type obj: pycultivator.device.device.Device
        :param definition:
        :return: Whether it was successful
        :rtype: bool
        """
        result = super(XMLBaseDeviceConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementActive(definition, obj.is_active)
        return result


class XMLConnectedDeviceConfig(XMLBaseDeviceConfig, baseConfig.ConnectedDeviceConfig):
    """Config class for handling the configuration of ConnectedDevice classes to/from XML"""

    pass


class XMLInstrumentDeviceConfig(XMLBaseDeviceConfig, baseConfig.InstrumentDeviceConfig):
    """Config class for handling the configuration of InstrumentDevice classes to/from XML"""

    def read_instruments(self, definition, obj, template=None):
        """ Load instrument objects from the configuration source in to the device

        :param definition: The definition to read from
        :type definition: lxml.etree._Element._Element
        :param obj: The device object to load into
        :type obj: pycultivator.device.device.Device
        :return: The updated device
        :rtype: pycultivator.device.device.Device
        """
        root = self.find_element("instruments", root=definition)
        if root is not None:
            # only call super if there is an instruments element
            obj = super(XMLInstrumentDeviceConfig, self).read_instruments(root, obj, template=template)
        return obj

    def write(self, obj, definition):
        super(XMLInstrumentDeviceConfig, self).write(obj, definition)

    def write_instruments(self, obj, definition):
        """Write instrument information to the configuration source

        :type obj: pycultivator.device._device.InstrumentDevice
        :type definition: lxml.etree._Element._Element
        :rtype: bool
        """
        # check if there is an instruments element
        root = self.find_element("instruments", root=definition)
        if root is None:
            # create new instruments element
            root = self.getXML().addElement(definition, "instruments")
        return super(XMLInstrumentDeviceConfig, self).write_instruments(obj, root)


class XMLDeviceConfig(XMLConnectedDeviceConfig, XMLInstrumentDeviceConfig, baseConfig.DeviceConfig):
    """Config class handling configuration of devices"""

    pass


class XMLComplexDeviceConfig(XMLConnectedDeviceConfig, baseConfig.ComplexDeviceConfig):
    """Config class to handle the plate configuration from the configuration source"""

    def read_channels(self, definition, obj, template=None):
        root = self.find_element("channels", root=definition)
        if root is not None:
            # only call super if there is an instruments element
            obj = super(XMLComplexDeviceConfig, self).read_channels(root, obj, template=template)
        return obj

    def write_channels(self, obj, definition):
        """Write channel information to the configuration source

        :type obj: pycultivator.device.device.ComplexDevice
        :type definition: lxml.etree._Element._Element
        :rtype: bool
        """
        # check if there is an channels element
        root = self.find_element("channels", root=definition)
        if root is None:
            # create new instruments element
            root = self.getXML().addElement(definition, "channels")
        return super(XMLComplexDeviceConfig, self).write_channels(obj, root)


class XMLConnectionConfig(XMLObjectConfig, baseConfig.ConnectionConfig):
    """Config class for handling connection configuration from the configuration source"""

    XML_ELEMENT_TAG = "connection"
    XML_ELEMENT_SCOPE = XMLBaseObjectConfig.XML_LOCAL_DEEP_SCOPE


class XMLChannelConfig(XMLInstrumentDeviceConfig, baseConfig.ChannelConfig):
    """Config class to handle the Channel configuration from the configuration source"""

    XML_ELEMENT_TAG = "channel"
    XML_ELEMENT_SCOPE = XMLBaseObjectConfig.XML_LOCAL_DEEP_SCOPE

    def create_definition(self, obj, root=None):
        if root is None:
            root = self.getXML().getRoot()
        # check if we are in a channels element
        if not self.is_valid_tag(root, "channels"):
            element = self.find_element("channels", root=root)
            if element is None:
                # create channels element
                element = self.getXML().addElement(root, "channels")
            root = element
        return super(XMLChannelConfig, self).create_definition(obj, root)


class XMLInstrumentConfig(XMLPartConfig, baseConfig.InstrumentConfig):
    """Config class to handle the channel configuration from the configuration source"""

    XML_ELEMENT_TAG = "instrument"
    XML_ELEMENT_SCOPE = XMLBaseObjectConfig.XML_LOCAL_DEEP_SCOPE

    def read_template(self, definition, class_, template=None, parent=None):
        id_ = self.getXML().getElementId(definition, _type=str, default="0")
        # now retrieve default instrument from the channel
        if parent is not None:
            templates = parent.getInstrumentsOf(key=id_, klass=self.configures())
            if len(templates) > 0:
                template = templates[0]
        return template
    
    # def read_calibrations(self, definition, obj, template=None):
    #     # root = self.find_element("calibrations", root=definition)
    #     # if root is not None:
    #         # only call super if there is an instruments element
    #         # obj = super(XMLInstrumentConfig, self).read_channels(root, obj)
    #     # return obj
    #     return super(XMLInstrumentConfig, self).read_calibrations(definition, obj, template=template)
    #
    # def write_calibrations(self, obj, definition):
    #     """Write calibration information to the configuration source
    #
    #     :type obj: pycultivator.instrument.Instrument
    #     :type definition: lxml.etree._Element._Element
    #     :rtype: bool
    #     """
    #     # check if there is an calibrations element
    #     # root = self.find_element("calibrations", root=definition)
    #     # if root is None:
    #     #     # create new instruments element
    #     #     root = self.getXML().addElement(definition, "calibrations")
    #     return super(XMLInstrumentConfig, self).write_calibrations(obj, definition)

    def create_definition(self, obj, root=None):
        if root is None:
            root = self.getXML().getRoot()
        # check if we are in a instruments element
        if not self.is_valid_tag(root, "instruments"):
            # check if there is a instruments directly under it
            element = self.find_element("instruments", root=root)
            if element is None:
                # create instruments element
                element = self.getXML().addElement(root, "instruments")
            root = element
        return super(XMLInstrumentConfig, self).create_definition(obj, root)


class XMLCalibrationConfig(XMLPartConfig, baseConfig.CalibrationConfig):
    """Config class to handle calibration configuration from the configuration source"""

    XML_ELEMENT_TAG = "calibration"
    XML_ELEMENT_SCOPE = XMLBaseObjectConfig.XML_LOCAL_SCOPE

    def read_template(self, definition, class_, template=None, parent=None):
        id_ = self.xml.getElementId(definition, _type=str, default="0")
        # now retrieve default instrument from the channel
        if parent is not None and parent.hasCalibration(id_):
            template = parent.getCalibration(id_)
        return template

    def read_properties(self, definition, obj):
        """Read property information from the definition into the obj"""
        obj = super(XMLPartConfig, self).read_properties(definition, obj)
        # gather information
        state = self.getXML().getElementActive(definition)
        # apply information
        obj.setActive(state)
        return obj

    def write_properties(self, obj, definition):
        result = super(XMLCalibrationConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementActive(definition, obj.is_active)
        return result

    def create_definition(self, obj, root=None):
        # if root is None:
        #     root = self.getXML().getRoot()
        # # check if we are in a calibrations element
        # if not self.is_valid_tag(root, "calibrations"):
        #     # check if there is a calibrations directly under it
        #     element = self.find_element("calibrations", root=root)
        #     if element is None:
        #         # create calibrations element
        #         element = self.getXML().addElement(root, "calibrations")
        #     root = element
        return super(XMLCalibrationConfig, self).create_definition(obj, root)


class XMLPolynomialConfig(XMLPartConfig, baseConfig.PolynomialConfig):
    """Config class to handle polynomial configuration from the configuration source"""

    XML_ELEMENT_TAG = "polynomial"
    XML_ELEMENT_SCOPE = XMLBaseObjectConfig.XML_LOCAL_SCOPE

    def read_template(self, definition, class_, template=None, parent=None, **kwargs):
        # now retrieve default instrument from the channel
        name = self.getXML().getElementId(definition, _type=int, default=0)
        if parent is not None and parent.has(name):
            template = parent.getPolynomial(name)
        return template

    def read_properties(self, definition, obj):
        obj = super(XMLPolynomialConfig, self).read_properties(definition, obj)
        # read state
        state = self.xml.getElementActive(definition)
        obj.setActive(state)
        # read domain
        domain = self.getXML().getElementAttribute(definition, "domain", default="(,)")
        obj.setDomain(domain)
        return obj

    def read_coefficients(self, definition, obj, template=None):
        # find coefficients
        coefficients = self.find_elements("coefficient", root=definition)
        for coefficient in coefficients:
            self.read_coefficient(coefficient, obj)
        return obj

    def read_coefficient(self, definition, obj, template=None):
        """Reads one coefficient into the polynomial object

        :param definition: The XML Element from the XML Configuration File
        :type definition: lxml.etree._Element._Element
        :return: The coefficient pair
        :rtype: tuple[int, float]
        """
        # test if element can be parsed, if not raise Exception
        self.assert_definition(definition, expected="coefficient")
        order = self.getXML().getElementAttribute(definition, "order", 0, _type=int)
        value = Parser.to_float(definition.text.strip(), default=0.0)
        # set coefficient in polynomial
        obj.setCoefficient(order, value)
        return obj

    def write(self, obj, definition):
        """ Writes information to the configuration

        :param polynomial: pycultivator.relation.Polynomial.Polynomial
        :param element: lxml.etree._Element._Element
        :return: bool
        """
        # first remove all information from element
        definition.clear()
        # check tag, write name, write settings
        return super(XMLPolynomialConfig, self).write(obj, definition)

    def write_properties(self, obj, definition):
        result = super(XMLPolynomialConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementActive(definition, obj.is_active)
            self.getXML().setElementAttribute(definition, "domain", obj.domain)
        return result

    def write_coefficient(self, definition, order, value):
        v_e = self.getXML().addElement(definition, "coefficient")
        self.getXML().setElementAttribute(v_e, "order", order)
        v_e.text = str(float(value))
        return True


class XMLSettingsConfig(XMLBaseObjectConfig, baseConfig.SettingsConfig):
    """Config class to handle settings configuration from configuration source"""

    XML_ELEMENT_TAG = "settings"
    XML_LOCAL_SCOPE = XMLBaseObjectConfig.XML_LOCAL_SCOPE

    TYPE_REGEX = re.compile(r"(dict|list)?\[?([a-z]+)\]?")

    def read(self, definition, obj, **kwargs):
        """Parse element into a settings object

        :param definition: The XML Element from the XML Configuration File
        :type definition: lxml.etree._Element._Element
        :param obj: Object to read into
        :type obj: pycultivator.core.pcSettings.PCSettings
        :return: A dictionary with all (readable) the settings
        :rtype: pycultivator.core.pcSettings.PCSettings
        """
        obj = super(XMLSettingsConfig, self).read(definition, obj, **kwargs)
        """:type: pycultivator.core.pcSettings.PCSettings"""
        # collect all settings elements
        elements = self.find_elements("setting", root=definition)
        for element in elements:
            obj = self.read_setting(element, obj)
        return obj

    def read_setting(self, definition, obj):
        """Parse a setting element into a key-value pair

        :param definition: The XML Setting Element to read
        :type definition: lxml.etree._Element._Element
        :param obj: Settings Object to add setting to
        :type obj: pycultivator.core.pcSettings.PCSettings
        :return: A dictionary of 1 item with key
        :rtype: pycultivator.core.pcSettings.PCSettings
        """
        # test if element can be parsed, if not raise Exception
        self.assert_definition(definition, expected="setting")
        # get setting name
        key = self.getXML().getElementAttribute(definition, "name")
        _type = self.getXML().getElementAttribute(definition, "type")
        values = self.read_values(definition, _type=_type)
        if key != "" and values is not None:
            if not isinstance(values, (tuple, list, dict)) or len(values) > 0:
                obj.set(key, values)
        return obj

    def read_values(self, element, default=None, _type=None):
        """Read all values set in this settings"""
        result = None
        values_e = self.find_elements("value", root=element)
        # check if there are multiple keys
        if len(values_e) > 1:
            value_e = values_e[0]
            # check if first value has a key defined
            first_key = self.getXML().getElementAttribute(value_e, "key", default="")
            if first_key != "":
                result = self.read_values_dict(element, default=default, _type=_type)
            else:
                result = self.read_values_list(element, default=default, _type=_type)
        elif len(values_e) == 1:
            v = self.read_value(values_e[0], default=default, _type=_type).values()
            if len(v) > 0:
                result = v[0]
        return result

    def read_values_list(self, element, default=None, _type=None):
        """Read all values as list"""
        values = []
        values_e = self.find_elements("value", root=element)
        for value_e in values_e:
            v = self.read_value(value_e, default=default, _type=_type).values()
            if len(v) > 0:
                values.append(v[0])
        return values

    def read_values_dict(self, element, default=None, _type=None):
        """Read all values as dictionary key-pairs"""
        values = {}
        values_e = self.find_elements("value", root=element)
        for value_e in values_e:
            # update dictionary with dictionary returned by readValue
            values.update(self.read_value(value_e, default=default, _type=_type))
        return values

    def read_value(self, element, default=None, _type=None):
        """Reads one value from a value element

        :param element:
        :type element: lxml.etree._Element._Element
        :param default:
        :return:
        :rtype: dict[str, str]
        """
        result = {}
        value = element.text.strip()
        if _type is None:
            _type = str
        value = Parser.parse(value, default=default, _type=_type)
        if value is not None:
            key = self.getXML().getElementAttribute(element, "key", default="", _type=str)
            result = {key: value}
        return result

    def write(self, settings, element):
        """ Writes information to the configuration

        :param settings: dict[str, object]
        :param element: lxml.etree._Element._Element
        :return: bool
        """
        result = True
        for key, value in settings.items():
            if value is not None:
                elements = self.find_elements("setting", root=element, name=key)
                if len(elements) > 0:
                    result = self.write_setting(elements[0], key, value) and result
                else:
                    result = self.create_setting(element, key, value) and result
        return result

    def create_setting(self, root, setting, value):
        # place setting always at beginning
        setting_e = self.getXML().addElement(root, "setting", position=0)
        self.write_setting(setting_e, setting, value)
        return True

    def write_setting(self, element, setting, value):
        """Change the information of the setting element

        :type element: lxml.etree._Element._Element
        :type setting: dict | list | str | int | float
        """
        # clear element
        element.clear()
        # set name
        element.set("name", setting)
        # now write the values
        if isinstance(value, (tuple, list)) and len(value) > 0:
            v = value[0]
            element.set("type", type(v).__name__)
            self.write_values_list(element, value)
        elif isinstance(value, dict) and len(value.values()) > 0:
            v = value.values()[0]
            element.set("type", type(v).__name__)
            self.write_values_dict(element, value)
        else:
            element.set("type", type(value).__name__)
            self.write_value(element, value)
        return True

    def write_values_list(self, element, values):
        for v in values:
            self.write_value(element, v)
        return True

    def write_values_dict(self, element, values):
        for k, v in values.items():
            self.write_value(element, v, key=k)
        return True

    def write_value(self, element, value, key=None):
        v_e = self.getXML().addElement(element, "value")
        if key not in ("", None):
            self.getXML().setElementAttribute(v_e, "key", key)
        # convert to string
        from pycultivator.core import Parser
        v_e.text = Parser.to_string(value)
        return True

    def create_definition(self, obj, root=None):
        if root is None:
            root = self.getXML().getRoot()
        # check if we are in a settings element
        if not self.is_valid_tag(root, "settings"):
            # check if there is a settings directly under it
            element = self.find_element("settings", root=root)
            if element is None:
                # create calibrations element
                element = self.getXML().addElement(root, "settings")
            root = element
        return root


class XMLConfig(baseConfig.Config):
    """ The config class provides the API to configure a light calibration plate object.

    Provides:
    - Loading of XML data source
    - Data checking
    - Element access

    """

    namespace = "config"
    # a list of available helper classes
    _known_helpers = [
        # XMLObjectConfig,
        # XMLPartConfig,
        XMLDeviceConfig,
        XMLComplexDeviceConfig,
        XMLConnectionConfig,
        XMLChannelConfig,
        XMLInstrumentConfig,
        XMLCalibrationConfig,
        XMLPolynomialConfig,
        XMLSettingsConfig,
    ]

    default_settings = {
    }

    XML_SCHEMA_COMPATIBILITY = {
        '3.1': 2,
        '3.0': 0,
        '2.0': 0,
        # version: level
        # level 2: full support
        # level 1: deprecated
        # level 0: not supported
    }
    XML_SCHEMA_PATH = os.path.join("schema", "pycultivator_device.xsd")

    def __init__(self, path, xml, settings=None, **kwargs):
        """Initialise XMLConfig, should never be called directly, but via open()"""
        super(XMLConfig, self).__init__(settings, **kwargs)
        self._path = path
        self._xml = xml

    def getLocation(self):
        return self.getFilePath()

    def getFilePath(self):
        """Returns the path to the XML File"""
        return self._path

    def getXML(self):
        """Returns the XML Object representing the XML file

        :rtype: pycultivator.core.pcXML.XML
        """
        return self._xml

    @classmethod
    def isCompatible(cls, schema_version):
        """Checks whether the given schema version is compatible with this file.

        :param schema_version: Version of the schema file that is used by the XML Document
        :type schema_version: str
        :return: Whether this library is compatible with the given schema version
        :rtype: bool
        """
        level = cls.XML_SCHEMA_COMPATIBILITY.get(schema_version, 0)
        if level == 1:
            cls.getLog().warning("Version {} is deprecated, consider upgrading!".format(schema_version))
        return level > 0

    @classmethod
    def load(cls, path, settings=None, schema=None, search_dirs=None, **kwargs):
        """Open the XML Configuration File, check it and load into a Configuration object

        :param path: Path to the XML File
        :type path: str
        :param schema: XML Schema object
        :type schema: None or pycultivator.foundation.pcXML.XMLSchema
        :param settings: Dictionary with settings to be passed to Configuration object
        :type settings: dict[str, object]
        :param kwargs: Extra keyword arguments to be incorporated in the settings
        :type kwargs: dict[str, object]
        :rtype: None or pycultivator.config.xmlConfig.XMLConfig
        """
        result = None
        if os.path.exists(path):
            # load class
            config = cls.extractClass(path)
            if config is None:
                raise XMLConfigException("Unable to load configuration class")
            # check path and compatibility
            if config.check(path, schema=schema):
                # create xml object
                xml = pcXML.XML.createFromSource(path)
                # create XMLConfig Object
                result = config(path, xml, settings=settings, **kwargs)
        elif search_dirs is not False:
            for candidate in cls.locate(path, search_dirs):
                result = cls.load(candidate, settings=settings, schema=schema, search_dirs=False, **kwargs)
        return result

    @classmethod
    def locate(cls, path, search_dirs=None):
        if search_dirs is None:
            search_dirs = []
        if isinstance(search_dirs, (tuple, set)):
            search_dirs = list(search_dirs)
        if not isinstance(search_dirs, list):
            search_dirs = [search_dirs]
        # add current location as candidate
        if '' not in search_dirs:
            search_dirs.insert(0, '')
        # add
        # try at multiple locations
        from pycultivator.core.pcUtility import find_files
        return find_files(search_dirs, path)

    @classmethod
    def loadSchema(cls, path=None):
        """

        :rtype: pycultivator.core.pcXML.XMLSchema or None
        """
        result = None
        if path is not None:
            result = cls.loadFileSchema(path)
        if result is None:
            result = cls.loadPackageSchema()
        return result

    @classmethod
    def loadPackageSchema(cls):
        # schema_dir = os.path.dirname(cls.XML_SCHEMA_PATH)
        # schema_file = pkg_resources.resource_filename(cls.__module__, schema_dir)
        schema_path = pkg_resources.resource_filename(cls.__module__, cls.XML_SCHEMA_PATH)
        return pcXML.XMLSchema.createFromSource(schema_path)

    @classmethod
    def loadFileSchema(cls, path):
        schema = None
        if not os.path.exists(path):
            raise XMLConfigException("Unable to open {}".format(path))
        if pcXML.XMLSchema.isSourceSchema(path):
            schema = pcXML.XMLSchema.createFromSource(path)
        if schema is None:
            # if xml extract schema from xml
            xml = pcXML.XML.createFromSource(path)
            if xml is not None:
                schema = xml.getSchema()
        return schema

    @classmethod
    def extractClassName(cls, source):
        """Returns the name of the Config Helper Class compatible with this config file

        Each xml config file provides the fully qualified name of the class capable of loading the file.

        :param source: Configuration Source (XML, Path, etc.)
        :type source: str
        :rtype: str
        """
        result = None
        try:
            xml = pcXML.XML.createFromSource(source)
            if xml is None:
                raise XMLConfigException("Unable to load configuration source into XML")
            # search for class attribute in root element
            result = xml.getElementAttribute(xml.getRoot(), "class", _type=str)
        except XMLConfigException as xce:
            cls.getLog().warning("Unable to parse XML Document: \n\t%s" % xce)
        return result

    @classmethod
    def assert_config(cls, path, schema=None, **kwargs):
        """Check XML Configuration file on existence, validity and compatilibity

        :raises: XMLConfigException

        """
        result = False
        # extract class information
        config = cls.extractClass(path)
        if cls is not config:
            if config is None:
                raise XMLConfigException("Unable to load configuration class")
            else:
                result = config.assert_config(path, schema, **kwargs)
        else:
            if schema is None:
                schema = cls.loadSchema(path)
            xml = pcXML.XML.createFromSource(path, schema=schema)
            if xml is None:
                raise XMLConfigException("Unable to read XML Document")
            # second check: xml validity
            if not xml.isValid(schema=schema):
                raise XMLConfigException("Schema rejected XML Document")
            # third check: compatibility with library
            if schema is not None and not cls.isCompatible(schema.getVersion()):
                raise XMLConfigException("Incompatible XML Document")
            result = True
        return result

    def save(self, location=None):
        """Save the configuration source"""
        if location is None:
            location = self.getFilePath()
        if not os.path.exists(os.path.dirname(location)):
            raise XMLConfigException("Unable to open {}".format(os.path.dirname(location)))
        # now we write the xml
        return self.getXML().save(location)

    def write(self, location=None):
        """Writes the Configuration to the source, if path is not set overwrite current source"""
        return self.save(location)


class XMLConfigException(baseConfig.ConfigException):
    """An exception raised by the pycultivator.config.XMLConfig.XMLConfig object"""

    def __init__(self, msg):
        super(XMLConfigException, self).__init__(msg)
