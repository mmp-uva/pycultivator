"""A simple time measuring instrument"""

from base import Instrument, InstrumentVariable, InstrumentException

from datetime import datetime as dt

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Time(Instrument):
    """An instrument that returns the time in a certain unit when measured

    Resets every day at midnight or 12 (if hours

    """

    INSTRUMENT_TYPE_SYNONYMS = {"time"}

    TIME_UNIT_HOUR = "hour"
    TIME_UNIT_MINUTE = "minute"
    TIME_UNIT_SECOND = "second"
    DEFAULT_TIME_UNIT = TIME_UNIT_HOUR
    TIME_ORDER = [TIME_UNIT_HOUR, TIME_UNIT_MINUTE, TIME_UNIT_SECOND] # 60^0, 60^1, 60^2

    namespace = "instrument.time"
    default_settings = {
        "time.unit": DEFAULT_TIME_UNIT,
    }

    _time = InstrumentVariable(None, private=True)
    _hour = InstrumentVariable(None, private=True)
    _minute = InstrumentVariable(None, private=True)
    _second = InstrumentVariable(None, private=True)

    @classmethod
    def formatFromUnit(cls, unit):
        if unit not in cls.TIME_ORDER:
            raise TimeInstrumentException("Invalid unit - {}".format(unit))
        return None

    def secondsSinceStart(self, time_now):
        """Calculates the total seconds since the start of today

        :type time_now: datetime.time
        """
        return time_now.second + time_now.minute * 60 + time_now.hour * 60**2

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Time, self).__init__(identity, parent=parent, settings=settings, **kwargs)

    def getTimeUnit(self):
        return self.getSetting("time.unit")

    def hasTimeUnit(self, unit):
        return unit in self.TIME_ORDER


class TimeInstrumentException(InstrumentException):
    """Exception raised by the TimeInstrument"""

    pass

