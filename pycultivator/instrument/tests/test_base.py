
from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.core.tests.test_pcVariable import TrackingVariableTest
from pycultivator.instrument import Instrument, InstrumentVariable
from testfixtures import LogCapture  # , log_capture


class InstrumentVariableTest(TrackingVariableTest):

    _subject_cls = InstrumentVariable
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """Return reference to the subject class

        :return:
        :rtype: (...) -> pycultivator.instrument.InstrumentVariable
        """
        return super(InstrumentVariableTest, cls).getSubjectClass()

    def getSubject(self):
        """Return instance of the subject

        :return:
        :rtype: pycultivator.instrument.InstrumentVariable
        """
        return super(InstrumentVariableTest, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(self.default_value, False, False)

    def test_can_measure(self):
        self.assertFalse(self.subject.can_measure)

    def test_can_control(self):
        self.assertFalse(self.getSubject().can_control)


class ExampleInstrument(Instrument):

    _od = InstrumentVariable(0.0)
    _intensity = InstrumentVariable(None, can_measure=True, can_control=True, private=True)
    _state = InstrumentVariable(False, can_measure=False, can_control=True, private=True)

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(ExampleInstrument, self).__init__(identity, parent=parent, settings=settings, **kwargs)

    @property
    def state(self):
        return self._state.get()

    @property
    def intensity(self):
        result = self._intensity.get()
        if result is not None:
            result += 1
        return result

    def _measure(self, source, **kwargs):
        if source is self._od:
            self.measure_od()
        if source is self._intensity:
            self.measure_intensity()

    def measure_od(self):
        self._od.set(10.0)

    def measure_intensity(self):
        self._intensity.set(90)

    def _control(self, source, value, **kwargs):
        if source is self._state:
            self.control_state(value)

    def control_state(self, state):
        self.getLog().info("New state: %s" % state)
        self._state.set(state)
        return True


class InstrumentTest(UvaSubjectTestCase):

    _subject_cls = ExampleInstrument
    _abstract = False

    @classmethod
    def getSubjectClass(cls):
        """Return reference to the settings class

        :return:
        :rtype: (...) -> ExampleInstrument
        """
        return super(InstrumentTest, cls).getSubjectClass()

    def getSubject(self):
        """

        :return:
        :rtype: ExampleInstrument
        """
        return super(InstrumentTest, self).getSubject()

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()(1, None, od=100.0)

    def test_init(self):
        s = self.getSubjectClass()(2, None)
        self.assertIsNone(s.intensity)
        self.assertEqual(self.getSubject().od, 100.0)
        s.variables["intensity"].set(100)
        self.assertEqual(s.intensity, 101)

    def test_measures(self):
        self.assertItemsEqual(
            self.getSubject().measures(), ["od", "intensity"]
        )

    def test_measure(self):
        self.assertEqual(self.getSubject().od, 100.0)
        m = self.getSubject().measure("od")
        self.assertTrue(m['success'])
        self.assertEqual(m['value'], 10)
        m = self.getSubject().measure("OD")
        self.assertFalse(m['success'])
        self.assertIsNone(m['value'])
        m = self.getSubject().measure("bar")
        self.assertFalse(m['success'])
        self.assertIsNone(m['value'])
        m = self.getSubject().measure("intensity")
        self.assertTrue(m['success'])
        self.assertEqual(m['value'], 90)

    def test_controls(self):
        self.assertItemsEqual(
            self.getSubject().controls(), ["intensity", "state"]
        )

    def test_control(self):
        self.assertFalse(self.getSubject().state)
        r = self.getSubject().control("state", True)
        self.assertTrue(r['success'])
        self.assertTrue(self.getSubject().state)
        r = self.getSubject().control("State", True)
        self.assertFalse(r['success'])
        log_name = self.getSubject().getLog().name
        with LogCapture(log_name) as l:
            r = self.getSubject().control("state", False)
        l.check((log_name, "INFO", "New state: False"))

    def test_independence(self):
        i1 = self.getSubjectClass()(1, None, od=1)
        i2 = self.getSubjectClass()(2, None, od=2)
        self.assertNotEqual(i1.od, i2.od)
        self.assertNotEqual(i1._od.get(), i2._od.get())
        i2.od = 4
        self.assertEqual(i2._od.get(), 4)
        self.assertNotEqual(i1._od.get(), i2._od.get())
        # self.assertNotEqual(i1.state, i2.state)
        # self.assertNotEqual(i1._name.get(), i2._name.get())

    def test_overload(self):
        s = self.getSubjectClass()(2, None, intensity=100.0, state=True)
        self.assertTrue(s.variables["state"].get())
        self.assertTrue(s.state)
        self.assertEqual(s.variables["intensity"].get(), 100.0)
        self.assertEqual(s.intensity, 101)

