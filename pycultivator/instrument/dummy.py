"""A dummy instrument returning the same value"""

from base import Instrument, InstrumentException, InstrumentVariable

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Dummy(Instrument):
    """An dummy instrument returning the same value every time it is measured"""

    INSTRUMENT_TYPE_SYNONYMS = ["dummy"]
    default_settings = {
        # default settings
    }

    value = InstrumentVariable(0.0, can_measure=True, can_control=True)

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Dummy, self).__init__(identity, parent=parent, settings=settings, **kwargs)

    def _measure(self, source, **kwargs):
        # Do nothing
        pass

    def _control(self, source, value, **kwargs):
        if source is self.value:
            # set new value to the given value
            self.value.set(value)


class DummyInstrumentException(InstrumentException):
    """Exception raised by the DummyInstrument"""

    pass


