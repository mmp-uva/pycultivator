"""Base Instrument API"""
from __future__ import absolute_import

from pycultivator.core import pcObject, pcTemplate, pcVariable, pcException
from pycultivator.core.objects.eventful import EventfulMeta, receives

import six
import time

__all__ = [
    "InstrumentVariable", "ConstrainedInstrumentVariable",
    "Instrument", "InstrumentException"
]


class InstrumentVariable(pcVariable.TrackingVariable):
    """Instrument Variable: Manages the access to the value of a variable in an instrument

    An instrument variable can be measured and or controlled; these calls

    Hooks
    -----

    * measure: Called to retrieve the actual value from the device (should update the variable)
    * control: Called to apply a new value to the device (should also set the variable)

    """

    def __init__(self, value=None, private=False, can_measure=True, can_control=False, **kwargs):
        super(InstrumentVariable, self).__init__(value=value, private=private, **kwargs)
        self._can_measure = can_measure
        self._can_control = can_control

    @property
    def can_measure(self):
        return self._can_measure

    @property
    def can_control(self):
        return self._can_control

    def measure(self, **kwargs):
        """Retrieves the actual value for the variable

        If a method is registered to the `measure` hook:
        * Call that method
        * commit to the current value

        :return: New value of the variable
        :rtype: object
        """
        # trigger measure event
        self.triggerEvent("measure", data=kwargs)
        # commit updated value
        self.commit()
        # return updated value
        return self.get()

    def control(self, value, **kwargs):
        """Set the variable to the given value

        If a method is registered to the `control` hook:
        * Call that method and obtain success flag

        If success or no method was registered:
        * commit to the current value

        """
        kwargs["value"] = value
        # trigger control event
        self.triggerEvent("control", kwargs)
        # commit updated value
        self.commit()
        # return whether the new value equals the control value
        return self.get() == value

    def copy(self, memo=None):
        result = super(InstrumentVariable, self).copy(memo=memo)
        if isinstance(result, InstrumentVariable):
            result._can_measure = self.can_measure
            result._can_control = self.can_control
        return result


class ConstrainedInstrumentVariable(pcVariable.ConstrainedVariable, InstrumentVariable):
    """A constrained variable to be used in instruments"""

    def __init__(self, value=None, private=False, can_measure=True, can_control=False, **kwargs):
        """Initialize a Constrained Instrument Variable

        arguments
        ---------

        * value: Default value.
        * private: Whether to public property descriptors.
        * can_measure: Whether the instrument can measure this variable.
        * can_control: Whether the instrument can control this variable.
        * minimum: The minimum value tolerated.
        * maximum: The maximum value tolerated.
        * includes: Whether allowed value domain includes the minmimum and maximum values.

        """
        super(ConstrainedInstrumentVariable, self).__init__(
            value=value, private=private, can_measure=can_measure, can_control=can_control, **kwargs
        )


class InstrumentMeta(
    pcTemplate.ConfigurableTemplateMeta, EventfulMeta
):

    def __init__(cls, name, bases=None, attrs=None):
        super(InstrumentMeta, cls).__init__(name, bases, attrs)


class Instrument(
    six.with_metaclass(
        InstrumentMeta,
        pcObject.HierarchicalObject, pcObject.EventFulObject, pcTemplate.ConfigurableTrackingTemplate
    )
):
    """
    The Instrument class implements the basic API for an Instrument model object.

    An Instrument consists of:
    - parent, the device that has this instrument
    - measurements, previous measurements
    - calibration, the calibration which is used to calibrate the obtained values
    - relations, the relationships of a parameter of this instrument with other instruments

    An Instrument is capable of:
    - measure, which runs a measurement and returns the result
    - adjust, which adjusts a parameter of the instrument using a relationship with some other

    Type advertising
    ----------------

    Each instrument will advertise it's type; specifying under what name it can be retrieved. Besides one main type,
    it also declares a number of synonyms. The latter are inherited and shared between ancestors. The
    `getTypeSynonyms` will create a list of the synonyms declared by this class and it's parents, using mro.

    Variables
    ---------

    Each instrument can measure and/or control multiple variables in the form of class attributes:

    >>> class LightInstrument(Instrument):
    ...     light = InstrumentVariable(0.0)
    ...     state = InstrumentVariable(False)

    The Instrument will convert these attributes into Variables per initialized object and create public property
    descriptors to the values.

    If one needs control over the getting and/or setting of the variables (as in most cases), private Variables are
    preferred:

    >>> class LightInstrument(Instrument):
    ...     _light = InstrumentVariable(0.0, private=True)
    ...     _state = InstrumentVariable(False, private=True)

    This will prevent Instrument from creating the public property descriptors, so you can implement them yourself.
    In those cases one needs to access the value of the variables via the variables property of the class: Remember
    that you are accessing the variable object, and not the value, so you need to use `get()` and `set(...)` to
    access the value contained in the variable.

    >>> li = LightInstrument(0, None, light=5)
    ... li.variables["light"].get()
    ... li._light.get()

    Variables use the event system to inform the instrument about the actions performed to it.
    For example, if the variable is asked to measure it's current value, it will trigger the 'measure' event.
    The instrument containing this variable will automatically receive this event and relay it to the `_measure` method.

    1. A call to measure the light is received by the instrument:
        `instrument.measure('light')`
    2. The call is relayed to the corresponding variable:
        `instrument.variables["light"].measure()`
    3. The variable will then inform the instrument it needs to be measured:
        `instrument._measure(instrument.variables["light"])`
    4. The instrument will measure the value and set the variable:
        `instrument.variable["light"].set()`
    5. The variable will inform it's observers that it has been updated.
    """

    _variable_type = InstrumentVariable

    INSTRUMENT_TYPE = "instrument"
    INSTRUMENT_TYPE_SYNONYMS = set()
    INSTRUMENT_TYPE_INHERITANCE = True

    INSTRUMENT_TYPE_FLUORESCENCE = "fluorescence"
    INSTRUMENT_TYPE_PUMP = "pump"
    INSTRUMENT_TYPE_OD = "od"
    INSTRUMENT_TYPE_LIGHT = "light"
    INSTRUMENT_TYPE_TIME = "time"
    INSTRUMENT_TYPE_PHOTO_DIODE = "photodiode"

    namespace = "instrument"
    default_settings = {
        # declare default settings for this class
    }

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        """ Initialise an instrument object

        The __init__ method is a good place to bind internal methods to the variable hooks.

        :param identity: Name of the instrument
        :type identity: str
        :param parent: Parent object that has this instrument
        :type parent: pycultivator.device.ComplexDevice.Channel
        :param settings: PCSettings for the object
        :type settings: None or dict
        :param kwargs: Extra keyword arguments for the variables
        :type kwargs: dict
        """
        super(Instrument, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        # bind variables to object
        for variable in self.variables.values():
            # bind events
            self.addObserver(variable)
            variable.addObserver(self)
        # self._measurements = []
        self._calibrations = {}

    @classmethod
    def getType(cls):
        """Returns the main type of this instrument

        :return: str
        """
        return cls.INSTRUMENT_TYPE

    @classmethod
    def inheritsTypeSynonyms(cls):
        return cls.INSTRUMENT_TYPE_INHERITANCE is True

    @classmethod
    def _getTypeSynonyms(cls):
        """Resolves the type synonyms of for this instrument"""
        result = set()
        # iterate over base classes
        for base in cls.mro():
            if issubclass(base, Instrument):
                result.update(base.INSTRUMENT_TYPE_SYNONYMS)
                if not base.inheritsTypeSynonyms():
                    break
        return result

    @classmethod
    def getTypeSynonyms(cls):
        """Returns all type synonyms that this instrument has

        :return: The type of instrument
        :rtype: set[str]
        """
        return cls._getTypeSynonyms()

    @classmethod
    def isOfType(cls, t):
        """Returns if this instrument is of the given type

        :type t: str
        :rtype: bool
        """
        return isinstance(t, str) and (t.lower() == cls.getType() or t.lower() in cls.getTypeSynonyms())

    @property
    def instrument_type(self):
        return self.INSTRUMENT_TYPE

    @property
    def instrument_types(self):
        results = self.getTypeSynonyms()
        results.add(self.instrument_type)
        return results

    # path methods

    @property
    def children(self):
        """Returns the child element or None if not found"""
        return self.getCalibrations().values()

    # Behaviour methods

    def calibrate(self, value, inverse=False):
        """Calculate the real value from a input value

        :param inverse: Whether to inverse (i.e. calculate input from real value)
        """
        result = value
        calibrations = self.getCalibrations(only_active=True).values()
        if len(calibrations) > 0:
            result = calibrations[0].calculate(value, inverse=inverse)
        if len(calibrations) > 1:
            self.getLog().warning("Found multiple calibrations in {} {}, using the first".format(
                type(self), self.name
            ))
        return result

    def calibrate_inverse(self, value):
        """Calculate the input value from a real value"""
        result = value
        calibrations = self.getCalibrations(only_active=True).values()
        if len(calibrations) > 0:
            result = calibrations[0].calculate_inverse(value)
        if len(calibrations) > 1:
            self.getLog().warning("Found multiple calibrations in {} {}, using the first".format(
                type(self), self.name
            ))
        return result

    @receives("measure")
    def _event_measure(self, event, source, data=None):
        if not isinstance(data, dict):
            data = {}
        self._measure(source, **data)

    def _measure(self, source, **kwargs):
        """INTERNAL Measurement method: should call device communication methods

        This method will receive all `measure` events from all variables connected to this Instrument.
        Check the source to determine how to handle the call.
        """
        pass        # Overload

    def measure(self, variable, **kwargs):
        """The measure function performs a measurement and returns a measurement object

        Note: `measure` will update the variable value and commit to the new value

        :param variable: The variable to be controlled, should be in measures()
        :type variable: str
        :param kwargs: Extra arguments passed
        :return: The measurement that was obtained after measuring
        :rtype: dict[str, object]
        """
        result = {
            'source': self, 'timestamp': time.time(),
            'variable': variable, 'value': None, 'success': False,
        }

        result.update(kwargs)
        v = self.variables.get(variable, None)
        if isinstance(v, InstrumentVariable):
            if v.can_measure:
                value = v.measure(**kwargs)
                result['success'] = True
                result['value'] = value
        return result

    @classmethod
    def measures(cls):
        """Returns all the variables that can be measured using this instrument

        :rtype: set[str]
        """
        result = set([])
        for name, variable in cls.class_fields.items():
            if isinstance(variable, InstrumentVariable):
                if variable.can_measure:
                    result.add(name)
        return result

    @classmethod
    def canMeasure(cls, variable):
        """Checks whether this instrument is able to measure this variable

        :type variable: str
        :rtype: bool
        """
        return isinstance(variable, str) and variable.lower() in cls.measures()

    @receives("control")
    def _event_control(self, event, source, data=None):
        if not isinstance(data, dict):
            data = {}
        value = None
        if "value" in data.keys():
            value = data.pop("value")
        if value is not None:
            self._control(source, value, **data)

    def _control(self, source, value, **kwargs):
        """INTERNAL control method: should call device communication methods

        This method will receive all `control` events from all variables connected to this Instrument.
        Check the source to determine which method to call.
        """
        pass        # overload

    def control(self, variable, value, **kwargs):
        """Change the value of the variable to the given value and apply to the device.

        Note: `control` will change the variable value and commit to the new value

        :param variable: The variable to be controlled, should be in controls()
        :param value: The value to control the variable to.
        :param kwargs: Extra arguments to be used by control
        :return: Whether controlling the variable was successful
        :rtype: dict[str, object]
        """
        result = {
            'source': self, 'timestamp': time.time(),
            'variable': variable, 'value': value, 'success': False,
        }

        result.update(kwargs)
        v = self.variables.get(variable, None)
        if isinstance(v, InstrumentVariable):
            if v.can_control:
                result['success'] = v.control(value, **kwargs)
        return result

    @classmethod
    def controls(cls):
        """Returns a list of all variables that can be controlled with this instrument

        :rtype: set[str]
        """
        result = set([])
        for name, variable in cls.class_fields.items():
            if isinstance(variable, InstrumentVariable):
                if variable.can_control:
                    result.add(name)
        return result

    @classmethod
    def canControl(cls, variable):
        """Checks whether this instrument is able to control this variable

        :type variable: str
        :rtype: bool
        """
        return isinstance(variable, str) and variable.lower() in cls.controls()

    def apply(self):
        """Apply the internal settings to the device

        Calls `control` on al variables that are controllable
        Note: `control` will change the variable value and commit to the new value

        :return: Return whether all variables were set correctly
        :rtype: bool
        """
        result = True
        for variable in self.variables.values():
            if isinstance(variable, InstrumentVariable):
                if variable.can_control:
                    result = variable.control(variable.value) and result
        return result

    def update(self):
        """Update the internal settings with settings from the device

        Calls `measure` on al variables that are measurable.
        Note: `measure` will update the variable value and commit to the new value
        """
        for variable in self.variables.values():
            if isinstance(variable, InstrumentVariable):
                if variable.can_measure:
                    variable.measure()
        return self

    # Calibration methods

    def getCalibrations(self, only_active=False):
        """Return the dictionary with calibrations and their names

        :param only_active: Whether to only return active calibrations
            (Will copy the dictionary if True)
        :type only_active: bool
        :return: Dictionary with calibrations and their names
        :rtype: dict[str, pycultivator.relation.calibration.Calibration]
        """
        results = self._calibrations
        if only_active:
            results = {k: v for k, v in results.items() if v.is_active}
        return results

    @property
    def calibrations(self):
        """Return the dictionary with calibrations and their names

        :return: Dictionary with calibrations and their names
        :rtype: dict[str, pycultivator.relation.calibration.Calibration]
        """
        return self.getCalibrations()

    def hasCalibrations(self):
        return len(self._calibrations.keys()) > 0

    def hasCalibration(self, name):
        return name in self.getCalibrations().keys()

    def getCalibration(self, name):
        if not self.hasCalibration(name):
            raise InstrumentException("Unknown calibration name - {}".format(name))
        return self.getCalibrations()[name]

    def setCalibration(self, calibration):
        """Replaces the calibration registered to this instrument under the given name

        :param calibration: The new calibation
        :type calibration: pycultivator.relation.Calibration.Calibration
        :return:
        :rtype:
        """
        name = calibration.name
        if self.hasCalibration(name):
            self.removeCalibration(calibration)
        self._calibrations[calibration.getName()] = calibration
        calibration.setParent(self)
        return self.getCalibrations()[calibration.getName()]

    def addCalibration(self, calibration):
        """Adds a new calibration to the instrument and registers it under the given name

        :param calibration: The calibration to add
        :type calibration: pycultivator.relation.Calibration.Calibration
        :return:
        :rtype:
        """
        name = calibration.name
        calibration.setParent(self)
        if self.hasCalibration(name):
            raise InstrumentException("{} - is already registered".format(calibration.getName()))
        return self.setCalibration(calibration)

    def removeCalibration(self, calibration):
        """Remove an existing calibration from the instrument"""
        name = calibration.name
        if not self.hasCalibration(name):
            raise InstrumentException("Unable to remove calibration, do not know: {}".format(name))
        self.getCalibrations().pop(name)
        # remove myself as parent
        calibration.setParent(None)
        return not self.hasCalibration(name)

    def clearCalibrations(self):
        while len(self.getCalibrations()) > 0:
            c = self.getCalibrations().values()[0]
            self.removeCalibration(c)
        return len(self.getCalibrations()) == 0

    # Copy methods

    def copy(self, memo=None):
        """Create a new deepcopy of this instrument

        :rtype: pycultivator.instrument.instrument.Instrument
        """
        result = super(Instrument, self).copy(memo=memo)
        # import calibrations
        if isinstance(result, Instrument):
            import copy
            for calibration in self.getCalibrations().values():
                c = copy.deepcopy(calibration, memo)
                result.addCalibration(c)
        return result


class InstrumentException(pcException.PCException):
    """Exception raised by an instrument"""

    def __init__(self, msg):
        super(InstrumentException, self).__init__(msg)
