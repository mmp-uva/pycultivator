"""A general LED instrument"""

from base import Instrument, InstrumentException, InstrumentVariable, ConstrainedInstrumentVariable

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class Light(Instrument):
    """A general Light instrument

    - measure: Light Intensity (in percentage and uE/m2/s) and state
    - control: Light Intensity (in percentage and uE/m2/s) and state

    Properties: will get/set local/intenal state and intensity values
    Measure/Control: will get/set actual state and intensity values in device
    Getter/Setters: Use the `update` argument to control interaction with device

    """

    namespace = "instrument.led"
    default_settings = {
        # declare defaults for this class
    }

    INSTRUMENT_TYPE_SYNONYMS = {
        "light",
    }

    _raw_intensity = ConstrainedInstrumentVariable(
        0.0, can_measure=True, can_control=True, private=True, minimum=0, maximum=None
    )
    _intensity = ConstrainedInstrumentVariable(
        0.0, can_measure=True, can_control=True, private=True, minimum=0, maximum=None
    )
    # intensity will update raw intensity
    _state = InstrumentVariable(
        False, can_measure=True, can_control=True, private=True
    )

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Light, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._color = None
        self._wavelength = -1

    # Getters / Setters

    def getColor(self):
        """Return the color that this LED emits

        :rtype: str
        """
        return self._color

    @property
    def color(self):
        return self.getColor()

    def setColor(self, color):
        """Set the color that this LED emits

        :type color: str
        """
        self._color = color

    @color.setter
    def color(self, color):
        self.setColor(color)

    def emitsColor(self, color):
        """Checks whether this LED emits the given color

        :param color: The color to compare to
        :type color: str
        :rtype: bool
        """
        return isinstance(color, str) and color.lower() == self.color.lower()

    def getWavelength(self):
        """Return at which wavelength this LED emit

        :rtype: float
        """
        return self._wavelength

    @property
    def wavelength(self):
        return self.getWavelength()

    def setWavelength(self, nm):
        """Set the wavelength at which this LED emits in nanometers

        :param nm: Wavelength emitted by this LED in nanometers
        :type nm: float
        """
        self._wavelength = nm

    @wavelength.setter
    def wavelength(self, nm):
        self.setWavelength(nm)

    def emitsWavelength(self, nm):
        """Check whether this LED emits at the given wavelength"""
        return self.wavelength == nm

    def getState(self, update=False):
        if update:
            state = self.read_led_state()
            self._state.set(state)
            self._state.commit()
        return self._state.get()

    @property
    def state(self):
        return self.getState()

    def setState(self, state, update=False):
        result = True
        self._state.set(state)
        if update:
            result = self.write_led_state(state)
            if result:
                self._state.commit()
        return result

    @state.setter
    def state(self, value):
        self.setState(value)

    def getRawIntensity(self, update=False):
        """Return the raw intensity value

        :param update: Whether to update th internal state to the actual state of the device
        :type update: bool
        :return: The raw intensity value according to the object
        :rtype: float
        """
        if update:
            value = self.read_led_intensity()
            self._raw_intensity.set(value)
            self._raw_intensity.commit()
        return self._raw_intensity.get()

    @property
    def raw_intensity(self):
        return self.getRawIntensity()

    def getIntensity(self, update=False):
        """Return the absolute intensity in uE/m2/s according to internal stored value

        :param update: Whether to update th internal state to the actual state of the device
        :type update: bool
        :return: The intensity (in uE/m2/s) according to the object
        :rtype: float
        """
        value = self.getRawIntensity(update)
        # convert
        intensity = self.calibrate(value)
        if intensity < 0:
            intensity = 0
        self._intensity.set(intensity)
        self._intensity.commit()
        return self._intensity.get()

    @property
    def intensity(self):
        return self.getIntensity()

    def setRawIntensity(self, value, update=False):
        """Set the raw intensity value

        :param update: Whether to apply this change to the device
        :type update: bool
        :rtype: bool
        """
        result = True
        # constrain value to be within bounds
        value = self._raw_intensity.constrain(value)
        self._raw_intensity.set(value)
        if update:
            result = self.write_led_intensity(value)
            if result:
                self._raw_intensity.commit()
        return result

    @raw_intensity.setter
    def raw_intensity(self, v):
        self.setRawIntensity(v)

    def setIntensity(self, intensity, update=False):
        # constrain value to be within bounds
        intensity = self._intensity.constrain(intensity)
        value = self.calibrate_inverse(intensity)
        # if desired intensity is 0 but predicted input value is non-zero, set to 0
        if intensity == 0 and value > 0:
            value = 0
        # apply
        result = self.setRawIntensity(value, update=update)
        # if success store intensity as new intensity
        if result:
            self._intensity.set(intensity)
            # if we applied it to the instrument, commit to it
            if update:
                self._intensity.commit()
        return result

    @intensity.setter
    def intensity(self, v):
        self.setIntensity(v)

    def testRawIntensity(self, raw_intensity):
        result = False
        try:
            self._raw_intensity.validate(raw_intensity)
            result = True
        except ValueError:
            pass
        return result

    def testIntensity(self, intensity):
        """Checks if the intensity can be applied to the machine"""
        result = False
        if intensity > 0:
            value = self.calibrate_inverse(intensity)
            result = self.testRawIntensity(value)
        return result

    # Communication hooks

    def _measure(self, source, **kwargs):
        if source is self._raw_intensity:
            self.getRawIntensity(True)
        if source is self._intensity:
            self.getIntensity(True)
        if source is self._state:
            self.getState(True)

    def _control(self, source, value, **kwargs):
        if source is self._raw_intensity:
            self.setRawIntensity(value, True)
        if source is self._intensity:
            self.setIntensity(value, True)
        if source is self._state:
            self.setState(value, True)

    def read_led_state(self):
        """Obtains the state of the LED from the device

        :rtype: bool
        """
        raise NotImplementedError

    def write_led_state(self, state):
        """Applies the given state to the LED on the device

        :type state: bool
        :rtype: bool
        """
        raise NotImplementedError

    def read_led_intensity(self):
        """Obtains the intensity (in percentage) of the LED from the device

        :rtype: float
        """
        raise NotImplementedError

    def write_led_intensity(self, value):
        """Applies the intensity (in percentage) to the LED on the device

        :type value: float
        :rtype: bool
        """
        raise NotImplementedError

    def copy(self, memo=None):
        result = super(Light, self).copy(memo=memo)
        result.setWavelength(self.getWavelength())
        result.setColor(self.getColor())
        result.setRawIntensity(self.getRawIntensity(False), False)
        result.setIntensity(self.getIntensity(False), False)
        result.setState(self.getState(False), False)
        return result


class LightException(InstrumentException):
    """Exception raised by a Light Instrument"""

    pass
