"""A general diode instrument"""

from base import Instrument, InstrumentVariable, InstrumentException

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class Diode(Instrument):
    """A general diode instrument

    Interact with a PhotoDiode:

    - measure: Intensity in voltage and Intensity in micro einstein

    """

    namespace = "instrument.diode"

    default_settings = {
        "replicates": 1,
    }

    INSTRUMENT_TYPE_SYNONYMS = [
        "photodiode", "diode"
    ]

    _raw_intensity = InstrumentVariable(None, private=True)
    _intensity = InstrumentVariable(None, private=True)

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Diode, self).__init__(identity, parent=parent, settings=settings, **kwargs)

    # Getters / Setters

    def getReplicates(self):
        """Returns the number of replicate readings

        :return: Number of readings to take
        :rtype: int
        """
        return self.getSetting("replicates")

    @property
    def replicates(self):
        return self.getReplicates()

    def _measure(self, source, **kwargs):
        if source is self._raw_intensity:
            self.getRawIntensity(True)
        if source is self._intensity:
            self.getIntensity(True)

    def getRawIntensity(self, update=False):
        """Return the raw intensity value

        :param update: Whether to update th internal state to the actual state of the device
        :type update: bool
        :return: The raw intensity value according to the object
        :rtype: float
        """
        if update:
            value = self.read_diode_raw()
            self._raw_intensity.set(value)
            self._raw_intensity.commit()
        return self._raw_intensity.get()

    @property
    def raw_intensity(self):
        return self.getRawIntensity()

    def getIntensity(self, update=False):
        """Return the absolute intensity in uE/m2/s

        :param update: Whether to update th internal state to the actual state of the device
        :type update: bool
        :return: The intensity (in uE/m2/s) according to the object
        :rtype: float
        """
        value = self.getRawIntensity(update)
        # convert
        if value is not None:
            intensity = self.calibrate(value)
            intensity = self.validate_intensity(intensity)
            self._intensity.set(intensity)
            self._intensity.commit()
        else:
            self._intensity.set(None)
        return self._intensity.get()

    @property
    def intensity(self):
        return self.getIntensity()

    # Behaviour methods

    def validate_intensity(self, intensity):
        if intensity < 0:
            self.getLog().warning("Intensity cannot be negative: set to 0 instead.")
            intensity = 0
        return intensity

    def read_diode(self):
        """Obtains a single reading from the photo diode"""
        raise NotImplementedError

    def read_diode_raw(self, replicates=None):
        """Read the intensity in VOLTAGE from the photo diode

        This method measures the intensity on the diode and will not update the intensity variables.
        Use the properties or getters instead!

        :param replicates: Number of replicate readings to collect and average
            If None; use replicate setting of object
        :type replicates: int | None
        :return: Intensity in voltage
        :rtype: float
        """
        result = 0.0
        if replicates is None:
            replicates = self.getReplicates()
        for __ in range(replicates):
            v = self.read_diode()
            if v is not None:
                result += v
        if replicates > 0:
            result /= float(replicates)
        return result

    def read_diode_intensity(self, replicates=None):
        """Read the intensity in uE/m2/s from the photo diode

        This method measures the intensity on the diode and will not update the intensity variables.
        Use the properties or getters instead!

        :param replicates: Number of replicate readings to collect and average
            If None; use replicate setting of object
        :type replicates: int | None
        :return: Intensity in micro Einstein per squared meter per second
        :rtype: float
        """
        voltage = self.read_diode_raw(replicates=replicates)
        return self.calibrate(voltage)


class DiodeInstrumentException(InstrumentException):
    """Exception raised by a PlateDiode Instrument"""

    pass
