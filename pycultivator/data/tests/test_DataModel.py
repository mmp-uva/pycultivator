# coding=utf-8
"""
Test file to Test uvaSerial.py
"""

from datetime import datetime as dt
from unittest import TestCase

from pycultivator.data import DataModel

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SchemaRecord(DataModel.Record):
    _schema = {
        "a": "b"
    }


class TestRecord(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._subject_cls = DataModel.Record

    def setUp(self):
        self._subject = self._subject_cls()
        self._schema_subject = SchemaRecord()

    def test_init(self):
        self.assertEqual(self._subject.getProperties(), self._subject_cls.getSchema())
        self.assertIsNone(self._subject.getProperty("a"))
        self.assertEqual(self._schema_subject.getProperties(), {"a": "b"})

    def test_ParseProperties(self):
        self.assertEqual(self._subject.getSchema(), {})
        self.assertEqual(self._subject.parse(**{"a": "b"}), {"a": "b"})
        self.assertEqual(self._schema_subject.parse(**{"c": "d"}), {"a": "b"})


class TestODRecord(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._subject_cls = DataModel.ODRecord

    def setUp(self):
        self._subject = self._subject_cls(OD=0)

    def test_init(self):
        self.assertEqual(self._subject.getProperties(), self._subject_cls.getSchema())

    # TODO: extend


class TestLightRecord(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._subject_cls = DataModel.LightRecord

    def setUp(self):
        self._subject = self._subject_cls(0)

    def test_init(self):
        self.assertEqual(self._subject.getSchema(), self._subject_cls.getSchema())

    # TODO: extend


class TestTimeRecord(TestCase):
    @classmethod
    def setUpClass(cls):
        cls._subject_cls = DataModel.TimeRecord

    def setUp(self):
        t_now = dt.now()
        self._subject = self._subject_cls(t_now)
        self._default_schema = self._subject_cls.mergeSchema(
            {"time.point": t_now}
        )

    def test_init(self):
        self.assertEqual(self._subject.getSchema(), self._default_schema)

    # TODO: extend
