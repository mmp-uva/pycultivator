
from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.relation.domain import Domain


class TestDomain(UvaSubjectTestCase):

    _abstract = False
    _subject_cls = Domain

    def setUp(self):
        self._empty = Domain()
        self._default = Domain(1, True, 10, True)

    def test_createFromString(self):
        d = self.getSubjectClass().createFromString("(,2)")
        """:type: pycultivator.relation.Polynomial.Domain"""
        self.assertEqual(d.getStartValue(), None)
        self.assertFalse(d.includesStart())
        self.assertEqual(d.getEndValue(), 2)
        self.assertFalse(d.includesEnd())
        d = self.getSubjectClass().createFromString("[2.32e-5,)")
        self.assertEqual(d.getStartValue(), 2.32e-5)
        self.assertTrue(d.includesStart())
        self.assertEqual(d.getEndValue(), None)
        self.assertFalse(d.includesEnd())
        d = self.getSubjectClass().createFromString("[-2.32e-5,)")
        self.assertEqual(d.getStartValue(), -2.32e-5)
        with self.assertRaises(ValueError):
            self.getSubjectClass().createFromString("<1,2>")
        with self.assertRaises(ValueError):
            self.getSubjectClass().createFromString("(1, 2)")
        with self.assertRaises(ValueError):
            self.getSubjectClass().createFromString("(-1.1.1,2)")

    def test_getStartValue(self):
        d = self.getSubjectClass()(1, True, 10, True)
        """:type: pycultivator.relation.Polynomial.Domain"""
        self.assertEqual(d.getStartValue(), 1)
        d = self.getSubjectClass()(None, True, 10, True)
        self.assertEqual(d.getStartValue(), None)
        d = self.getSubjectClass().createFromString("[1,10]")
        self.assertEqual(d.getStartValue(), 1)

    def test_includesStart(self):
        d = self.getSubjectClass()(1, True, 10, True)
        """:type: pycultivator.relation.Polynomial.Domain"""
        self.assertTrue(d.includesStart())
        d = self.getSubjectClass()(None, False, 10, True)
        self.assertFalse(d.includesStart())
        d = self.getSubjectClass().createFromString("[1,10]")
        self.assertTrue(d.includesStart())
        d = self.getSubjectClass().createFromString("(1,10]")
        self.assertFalse(d.includesStart())

    def test_getEndValue(self):
        d = self.getSubjectClass()(1, True, 10, True)
        """:type: pycultivator.relation.Polynomial.Domain"""
        self.assertEqual(d.getEndValue(), 10)
        d = self.getSubjectClass()(None, True, None, True)
        self.assertEqual(d.getEndValue(), None)
        d = self.getSubjectClass().createFromString("[1,10]")
        self.assertEqual(d.getEndValue(), 10)

    def test_includesEnd(self):
        d = self.getSubjectClass()(1, True, 10, True)
        """:type: pycultivator.relation.Polynomial.Domain"""
        self.assertTrue(d.includesEnd())
        d = self.getSubjectClass()(None, False, 10, False)
        self.assertFalse(d.includesEnd())
        d = self.getSubjectClass().createFromString("[1,10]")
        self.assertTrue(d.includesEnd())
        d = self.getSubjectClass().createFromString("(1,10)")
        self.assertFalse(d.includesEnd())

    def test_contains(self):
        d = self.getSubjectClass()(1, True, 10, True)
        """:type: pycultivator.relation.Polynomial.Domain"""
        self.assertFalse(d.contains(0))
        self.assertTrue(d.contains(1))
        self.assertTrue(d.contains(5))
        self.assertTrue(d.contains(10))
        self.assertFalse(d.contains(11))
        d = self.getSubjectClass()(1, False, 10, False)
        """:type: pycultivator.relation.Polynomial.Domain"""
        self.assertFalse(d.contains(0))
        self.assertFalse(d.contains(1))
        self.assertTrue(d.contains(5))
        self.assertFalse(d.contains(10))
        self.assertFalse(d.contains(11))

    def test_domainToString(self):
        s = self.getSubjectClass().domainToString(1, True, 10, False)
        self.assertEqual(s, "[1,10)")
        d = self.getSubjectClass()(-2.32e-5, False, 10, True)
        """:type: pycultivator.relation.Polynomial.Domain"""
        self.assertEqual(str(d), "(-2.32e-05,10]")

    def test__eq(self):
        d1 = self.getSubjectClass()(-2.32e-5, False, 10, True)
        d2 = self.getSubjectClass()(-2.32e-5, False, 10, True)
        self.assertTrue(d1 == d2)
        d1 = self.getSubjectClass()(1, False, 10, False)
        d2 = self.getSubjectClass()(1, False, 10, False)
        self.assertTrue(d1 == d2)
        d1 = self.getSubjectClass()(-2.32e-5, False, 10, True)
        d2 = self.getSubjectClass()(1, False, 10, False)
        self.assertFalse(d1 == d2)