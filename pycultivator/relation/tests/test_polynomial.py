"""Test the Polynomial module"""

from pycultivator.core.tests import UvaSubjectTestCase
from pycultivator.relation import polynomial


class TestPolynomialModel(UvaSubjectTestCase):

    _abstract = False
    _subject_cls = polynomial.PolynomialModel

    def initSubject(self, *args, **kwargs):
        return self.getSubjectClass()("0", coefficients=[0, 2])

    def getSubject(self):
        """Return the default object subjected to tests

        :return:
        :rtype: pycultivator.relation.Polynomial.Polynomial
        """
        return super(TestPolynomialModel, self).getSubject()

    def test_coefficientsToString(self):
        s = self.getSubjectClass().coefficientsToString([-1, 2.32e-5, 500])
        self.assertEqual(s, "500.0x^2+0.0x-1.0")
        # increase precision
        s = self.getSubjectClass().coefficientsToString([-1, 2.32e-5, 500], precision=6)
        self.assertEqual(s, "500.0x^2+2.3e-05x-1.0")
        s = self.getSubjectClass().coefficientsToString([])
        self.assertEqual(s, "")
        s = self.getSubjectClass().coefficientsToString([0, 0, 0, 0, ])
        self.assertEqual(s, "")
        s = self.getSubjectClass().coefficientsToString([0, 1])
        self.assertEqual(s, "x")
        s = self.getSubjectClass().coefficientsToString([-1, 0])
        self.assertEqual(s, "-1.0")
        s = self.getSubjectClass().coefficientsToString([0, 1, -1, 1])
        self.assertEqual(s, "x^3-x^2+x")

    def test_init(self):
        self.assertEqual(str(self.getSubject().getDomain()), "(,)")
        self.assertEqual(self.getSubject().getCoefficients(), [0, 2])
        self.assertEqual(self.getSubject().isActive(), True)

    def test_getIdentity(self):
        self.assertEqual(self.subject.identity, 0)

    def test_getName(self):
        self.assertEqual(self.subject.name, "0")

    def test_calculate(self):
        # default subject is of form y=2x
        self.assertEqual(self.subject.calculate(1), 2)
        self.assertEqual(self.subject.calculate(100), 200)
        self.assertEqual(self.subject.calculate(99999), 99999*2)
        self.assertEqual(self.subject.calculate(1, True), 0.5)
        self.assertEqual(self.subject.calculate(200, True), 100)
        s = self.getSubjectClass()("0", coefficients=[0, 1, 1], domain="(-10,10]")
        self.assertEqual(s.calculate(1), 1 ** 2 + 1)
        self.assertEqual(s.calculate(1 ** 2 + 1, True), 1)
        self.assertEqual(s.calculate(2), 2 ** 2 + 2)
        self.assertEqual(s.calculate(2 ** 2 + 2, True), 2)
        s = self.getSubjectClass()("0", coefficients=[0, 1, -2], domain="(0,]")
        self.assertEqual(s.calculate(-1, True), s.calculate_inverse(-1))

    def test_calculate_inverse(self):
        # default subject is of form y=2x
        self.assertEqual(self.subject.calculate_inverse(1), 0.5)
        self.assertEqual(self.subject.calculate_inverse(200), 100)
        s = self.getSubjectClass()("0", coefficients=[0, 1, 1], domain="(-10,10]")
        self.assertEqual(s.calculate_inverse(1 ** 2 + 1), 1)
        self.assertEqual(s.calculate_inverse(2 ** 2 + 2), 2)
        s = self.getSubjectClass()("0", coefficients=[0, 1, -2], domain="(0,]")
        self.assertEqual(s.calculate_inverse(-1), 1)

    def test_contains(self):
        # default subject is active on (,)
        self.assertTrue(self.subject.contains(-9999999))
        self.assertTrue(self.subject.contains(1))
        self.assertTrue(self.subject.contains(9999999))
        # make subject active on (-10, 10]
        s = self.getSubjectClass()("0", coefficients=[0, 1], domain="(-10,10]")
        self.assertFalse(s.contains(-9999999))
        self.assertFalse(s.contains(-10))
        self.assertTrue(s.contains(1))
        self.assertTrue(s.contains(10))
        self.assertFalse(s.contains(9999999))


class TestPolynomialDefinition(UvaSubjectTestCase):

    _abstract = False
    _subject_cls = polynomial.PolynomialRelation

    def initSubject(self, *args, **kwargs):
        result = self.getSubjectClass()("0")
        p = self.getSubjectClass().createPolynomialModel("0", [0, 2])
        result.add(p)
        return result

    def test_createPolynomial(self):
        p = self.getSubjectClass().createPolynomialModel("0", [0, 1], "(-10,10]")
        self.assertEqual(p, polynomial.PolynomialModel("0", coefficients=[0, 1], domain="(-10,10]"))

    def test_getPolynomials(self):
        p = self.getSubjectClass().createPolynomialModel("0", [0, 2])
        self.assertEqual(self.subject.getPolynomials(), [p])

    def test_clear(self):
        self.subject.clear()
        self.assertEqual(len(self.subject.getPolynomials()), 0)

    def test_hasValidPolynomial(self):
        self.assertTrue(self.subject.hasValidPolynomial(-9999999))
        self.assertTrue(self.subject.hasValidPolynomial(1))
        self.assertTrue(self.subject.hasValidPolynomial(9999999))
        self.subject.clear()
        p = self.getSubjectClass().createPolynomialModel("0", [0, 1], "(-10,10]")
        self.subject.add(p)
        self.assertFalse(self.subject.hasValidPolynomial(-9999999))
        self.assertFalse(self.subject.hasValidPolynomial(-10))
        self.assertTrue(self.subject.hasValidPolynomial(1))
        self.assertTrue(self.subject.hasValidPolynomial(10))
        self.assertFalse(self.subject.hasValidPolynomial(9999999))

    def test_calculate(self):
        # default subject is of form y=2x
        self.assertEqual(self.subject.calculate(1), 2)
        self.assertEqual(self.subject.calculate(100), 200)
        self.subject.clear()
        p = self.getSubjectClass().createPolynomialModel("0", [0, 2], "(,5]")
        self.subject.add(p)
        p = self.getSubjectClass().createPolynomialModel("1", [1, 2], "(5,]")
        self.subject.add(p)
        self.assertEqual(self.subject.calculate(1), 2)
        self.assertEqual(self.subject.calculate(4), 8)
        self.assertEqual(self.subject.calculate(5), 10)
        self.assertEqual(self.subject.calculate(6), 13)
        self.assertEqual(self.subject.calculate(100), 201)

    def test_calculate_inverse(self):
        # default subject is of form y=2x
        self.assertEqual(self.subject.calculate_inverse(1), 0.5)
        self.assertEqual(self.subject.calculate_inverse(200), 100)
        # a simple equation
        self.subject.clear()
        p = self.getSubjectClass().createPolynomialModel("0", [0, 2], "(,5]")
        self.subject.add(p)
        p = self.getSubjectClass().createPolynomialModel("1", [1, 2], "(5,]")
        self.subject.add(p)
        self.assertEqual(self.subject.calculate_inverse(2), 1)
        self.assertEqual(self.subject.calculate_inverse(8), 4)
        self.assertEqual(self.subject.calculate_inverse(9), 4.5)
        self.assertEqual(self.subject.calculate_inverse(10), 5)
        self.assertEqual(self.subject.calculate_inverse(13), 6)
        self.subject.clear()
        p = self.getSubjectClass().createPolynomialModel("0", [0, 1, 1], "(,1]")
        self.subject.add(p)
        p = self.getSubjectClass().createPolynomialModel("1", [1, 2], "(1,]")
        self.subject.add(p)
        self.assertEqual(self.subject.calculate_inverse(1 ** 2 + 1), 1)
        self.assertEqual(self.subject.calculate_inverse(3.2), 1.1)
