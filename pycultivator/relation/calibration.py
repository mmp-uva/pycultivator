"""
The Calibration class is a relation definition that uses polynomials to translate a value into a calibrated value
"""

from pycultivator.relation import polynomial

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Calibration(polynomial.PolynomialRelation):
    """Defines a calibration relation in the form:

    real_value = f(input_value)

    Calculate real value from input value:

    ..:: python

        Calibration.calibrate(input_value) == real_value
        Calibration.calculate(input_value) == real_value

    Deduce input value from a real value:

    ..:: python

        Calibration.calibrate(real_value, inverse=True) == input_value
        Calibration.calculate(real_value, inverse=True) == input_value
    """

    namespace = "calibration"
    default_settings = {
        # add default settings for this class
    }

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Calibration, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._state = True

    def calibrate(self, value, inverse=True):
        """Calculate the real_value from a given input_value

        If inverse is true, it will calculate the input value from a real value

        :type value: float
        :rtype: float
        """
        return self.calculate(value, inverse=inverse)

    def copy(self, memo=None):
        """Copy this Calibration into another Calibration

        :param other: The other object to copy to, if None copy to new instance
        :type other: pycultivator.relation.calibration.Calibration
        :return: The other object as a copy of this
        :rtype: pycultivator.relation.calibration.Calibration
        """
        other = super(Calibration, self).copy(memo=memo)
        # do some extra modifications
        return other


class CalibrationException(polynomial.PolynomialRelationException):
    """An exception raised by a Calibration object"""

    pass
