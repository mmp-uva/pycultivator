
from pycultivator.core import BaseObject, Parser
import re


class Domain(BaseObject):
    """A Domain Object"""

    DOMAIN_TEST = re.compile("([(\[])(-?\d*\.?\d+(?:e-?\d+)?)?,(-?\d*\.?\d+(?:e-?\d+)?)?([)\]])")

    @staticmethod
    def domainToString(start=None, includes_start=False, end=None, includes_end=False):
        result = "[" if includes_start else "("
        if start is not None:
            result += str(start)
        result += ","
        if end is not None:
            result += str(end)
        result += "]" if includes_end else ")"
        return result

    def __init__(self, start=None, includes_start=False, end=None, includes_end=False):
        super(Domain, self).__init__()
        self._start = start
        self._includes_start = includes_start is True
        self._end = end
        self._includes_end = includes_end is True

    @classmethod
    def createFromString(cls, domain):
        """Creates a domain object from a string, using the format:

        * 1 opening bracket: [ or (. [ means include start point
        * optional a number (0.0 or .0)
        * a comma
        * optional a number (0.8 or .8)
        * 1 closing bracket: ] or ). ) means don't include end point

        :param domain: Domain string
        :type domain: str
        :return: The desired object
        :raises ValueError: If given domain string is invalid
        :rtype: None or pycultivator.relation.polynomial.Domain
        """
        match = cls.DOMAIN_TEST.match(domain)
        if match is None:
            raise ValueError("Invalid domain - %s - given, no matches." % domain)
        m = match.groups()
        if len(m) < 4:
            raise ValueError("Invalid domain - %s - given, not enough captures." % domain)
        includes_start = m[0] == "["
        start = None
        value = Parser.to_float(m[1], default=None)
        if value is not None:
            start = value
        end = None
        value = Parser.to_float(m[2], default=None)
        if value is not None:
            end = value
        includes_end = m[3] == "]"
        return cls(start=start, includes_start=includes_start, end=end, includes_end=includes_end)

    def getStartValue(self):
        """Returns the start value of the domain, None if Infinite"""
        return self._start

    @property
    def start(self):
        return self.getStartValue()

    def includesStart(self):
        """Returns whether the start value is included in the domain"""
        return self._includes_start is True

    @property
    def includes_start(self):
        return self.includesStart()

    def getEndValue(self):
        """Returns the end value of the domain, None if Infinite"""
        return self._end

    @property
    def end(self):
        return self.getEndValue()

    def includesEnd(self):
        """Returns whether the end value is included in the domain"""
        return self._includes_end is True

    @property
    def includes_end(self):
        return self.includesEnd()

    def contains(self, x):
        """Returns whether the x-value is contained within this domain

        :type x: int
        :rtype: bool
        """
        # domain start is None -> infinite
        start = self.getStartValue()
        includes_start = self.includesStart()
        after_start = start is None or (includes_start and start <= x) or (not includes_start and start < x)
        # domain end is None -> infinite
        end = self.getEndValue()
        includes_end = self.includesEnd()
        before_end = end is None or (includes_end and end >= x) or (not includes_end and end > x)
        return after_start and before_end

    def copy(self, memo=None):
        result = type(self)(
            start=self.getStartValue(), includes_start=self.includesStart(),
            end=self.getEndValue(), includes_end=self.includesEnd()
        )
        return result

    def __str__(self):
        """Returns a string representation of this object"""
        return self.domainToString(self.getStartValue(), self.includesStart(), self.getEndValue(), self.includesEnd())

    def __eq__(self, other):
        """Compares this object to an `other` object

        :type other: object
        :rtype: bool
        """
        result = False
        if isinstance(other, Domain):
            result = self.includesStart() is other.includesStart()
            result = result and self.getStartValue() == other.getStartValue()
            result = result and self.includesEnd() is other.includesEnd()
            result = result and self.getEndValue() == other.getEndValue()
        return result

    def __ne__(self, other):
        return not self == other

    def __contains__(self, item):
        """Whether the value is withing the domain"""
        return self.contains(item)