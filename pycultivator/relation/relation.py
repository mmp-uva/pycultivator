""""""

from pycultivator.core import ConfigurableObject, HierarchicalObject, PCException

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Relation(HierarchicalObject, ConfigurableObject):
    """The basic relation class"""

    namespace = "relation"
    default_settings = {
        # add default settings for this class
    }

    def __init__(self, identity, parent=None, active=False, settings=None, **kwargs):
        super(Relation, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._is_active = active is True
        self._source = None
        self._target = None
        self._models = {}

    # source methods

    def isActive(self):
        return self._is_active is True

    @property
    def is_active(self):
        return self.isActive()

    def setActive(self, state):
        self._is_active = state is True

    @is_active.setter
    def is_active(self, state):
        self.setActive(state)

    @property
    def source(self):
        return self._source

    def getSource(self):
        return self.source

    def hasSource(self):
        return self.getSource() is not None

    @source.setter
    def source(self, s):
        if isinstance(s, str):
            from pycultivator.core.pcPath import PathException
            try:
                s = self.path.resolve(s)
            except (TypeError, PathException):
                self.getLog().warning("Unable to resolve string: {}".format(s))
                t = None
        if s is not None:
            self._source = s

    def setSource(self, s):
        self.source = s

    # target methods

    @property
    def target(self):
        return self._target

    def getTarget(self):
        return self.target

    def hasTarget(self):
        return self.getTarget() is not None

    @target.setter
    def target(self, t):
        if isinstance(t, str):
            from pycultivator.core.pcPath import PathException
            try:
                t = self.path.resolve(t)
            except (TypeError, PathException):
                self.getLog().warning("Unable to resolve string: {}".format(t))
                t = None
        if t is not None:
            self._target = t

    def setTarget(self, t):
        self.target = t

    # definition methods

    def copy(self, memo=None):
        import copy
        result = type(self)(
            self.name, self.parent, active=self.is_active,
            source=self.getSource(), target=self.getTarget(),
            settings=copy.deepcopy(self.getSettings(), memo)
        )
        # do customization
        return result


class RelationException(PCException):
    
    def __init__(self, msg):
        super(RelationException, self).__init__(msg=msg)
