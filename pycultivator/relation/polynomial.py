"""Implement Polynomial Relations"""

from pycultivator.core import Parser
import model
import relation
import numpy as np

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class PolynomialModel(model.Model):
    """A Polynomial"""

    @staticmethod
    def coefficientsToString(coefficients, precision=4):
        """Writes a set of coefficients as an equation using the form y= coefficients[i] * x^i"""
        result = ""
        idx = len(coefficients) - 1  # start with highest index
        while idx >= 0:
            coefficient = coefficients[idx]
            if coefficient != 0:
                plus_sign = ""
                if result != "":
                    plus_sign = "+"
                if coefficient < 0:
                    plus_sign = "-"
                coefficient = abs(coefficient)
                if idx > 0 and coefficient == 1:
                    coefficient = ""
                else:
                    coefficient = round(float(coefficient), precision)
                result = "{result}{plus}{value}{x}{order}".format(
                    result=result,
                    plus=plus_sign,
                    value=coefficient,
                    x="x" if idx > 0 else "",
                    order="^{}".format(idx) if idx > 1 else "",
                )
            idx -= 1
        return result

    def __init__(self, identity, parent=None, coefficients=None, domain=None, active=True, settings=None, **kwargs):
        """ Initialise the polynomial

        :param domain: The domain definition for this polynomial
        :type domain: None or str or pycultivator.relation.polynomial.Domain
        :param coefficients: A list with coefficients ascending in order (0 .. N)
        :type coefficients: None or list[float]
        """
        super(PolynomialModel, self).__init__(
            identity, parent=parent, domain=domain, active=active, settings=settings, **kwargs
        )
        self._coefficients = [0, 0]
        if coefficients is not None:
            self._coefficients = coefficients

    def makeIdentity(self, name):
        name = Parser.to_int(name)
        if name is None:
            raise PolynomialModelException(
                "Invalid name, {} is not an integer or cannot parsed to integer".format(name)
            )
        return name

    @property
    def coefficients(self):
        return self._coefficients

    def getCoefficients(self):
        return self.coefficients

    @coefficients.setter
    def coefficients(self, coefficients):
        if isinstance(coefficients, (int, float)):
            coefficients = [coefficients]
        if not isinstance(coefficients, list):
            raise PolynomialModelException(
                "Expected a list, received - {}".format(type(coefficients))
            )
        self._coefficients = coefficients

    def setCoefficients(self, coefficients):
        self.coefficients = coefficients

    def count(self):
        return len(self.getCoefficients())

    def countCoefficients(self):
        return self.count()

    def clear(self):
        self.setCoefficients([])
        return self

    def clearCoefficients(self):
        return self.clear()

    def hasCoefficientIndex(self, index):
        return self.has(index)

    def has(self, index):
        """Returns whether this polynomial has a coefficient at the given position

        :type index: int
        :rtype: bool
        """
        return -self.count() <= index < self.count()

    def getCoefficient(self, order):
        return self.get(order)

    def get(self, order):
        """Returns the coefficient of the given order

        :param order: The order of the coefficient
        :type order: int
        :return:
        :rtype: float | int
        """
        if not self.has(order):
            raise PolynomialModelException(
                "Invalid order - {}".format(order)
            )
        return self.getCoefficients()[order]

    def setCoefficient(self, order, value):
        return self.set(order, value)

    def set(self, order, value):
        """Sets the value of a coefficient at the given order in the polynomial

        :type order: int
        :type value: int or float
        :rtype: float
        """
        if order >= len(self.getCoefficients()):
            for idx in range(len(self.getCoefficients()) - 1, order):
                self.addCoefficient(0.0)
        if isinstance(value, int):
            value = float(value)
        if not isinstance(value, float):
            raise PolynomialModelException(
                "Invalid value type, expected float but received - {}".format(value)
            )
        self.getCoefficients()[order] = value
        return self.getCoefficient(order)

    def addCoefficient(self, value):
        self.add(value)

    def add(self, value):
        """Adds a value to the list of coefficient,
            increasing the order of the polynomial and at highest order level.

        :type value: int or float
        :rtype: float
        """
        if isinstance(value, int):
            value = float(value)
        if not isinstance(value, float):
            raise PolynomialModelException(
                "Invalid value type, expected float but received - {}".format(value)
            )
        self.getCoefficients().append(value)
        return self.getCoefficient(-1)

    def remove(self, order):
        if not self.has(order):
            raise PolynomialModelException(
                "Invalid order - {}".format(order)
            )
        self._coefficients.pop(order)
        return self

    def calculate(self, value, inverse=False):
        """Solves the polynomial for the given value

        Method will NOT check if the given value is within the domain of the polynomial.
        Use `contains(value)` to check if the returned value is valid.

        >>> PolynomialModel("0", [0, 2]).calculate(3.0)
        6.0

        >>> PolynomialModel("0",[0, 0, 1]).calculate(3.0)
        9.0

        :param value: Value to solve for: f(value) = result | f(result) = value
        :type value: float
        :param inverse: Whether to solve for x (inverse == False) or y (inverse == True)
        :type inverse: bool
        :return: The predicted value
        :rtype: float
        """
        if inverse:
            result = self.calculate_inverse(value)
        else:
            result = 0.0
            for order in range(self.countCoefficients()):
                result += float(self.getCoefficient(order)) * (value**order)
        return result

    def calculate_inverse(self, value):
        """Solves the polynomial for the given y value

        Method will NOT check if the given result is within the domain of the polynomial.
        Use `contains(result)` to check if the returned value is valid.

        >>> p = PolynomialModel("0", [0, 2])
        >>> p.calculate_inverse(6)
        3.0

        >>> v = p.calculate(3)
        >>> v
        6.0

        >>> p.calculate_inverse(v)
        3.0

        >>> p = PolynomialModel("0", [0, -5, 1])
        >>> p.calculate_inverse(0)
        5.0

        >>> p.calculate_inverse(-6)
        2.0

        >>> v = p.calculate(2)
        >>> v
        -6.0

        >>> p.calculate_inverse(v)
        2.0

        :param value: Value to solve for using: f(result) = value
        :type value: float
        :return: Returns smallest positive solution or 0.0
        :rtype: float
        """
        p = self.getCoefficients()[:]
        p.reverse()
        # subtract y from intercept; f(x) = y <=> f(x) - y = 0
        p[-1] -= value
        # solve for zero
        roots = filter(lambda x: x > 0, list(np.roots(p)))
        result = None
        if len(roots) > 0:
            result = round(float(min(roots)), 8)
        if result is None:
            # if we could not solve, we set result to 0
            result = 0.0
        return result

    def __str__(self):
        return "%s : %s" % (self.getDomain(), self.coefficientsToString(self.getCoefficients()))

    def __eq__(self, other):
        """Compares the domain of both

        :type other: object
        :rtype: bool
        """
        if isinstance(other, PolynomialModel):
            return self.getDomain() == other.getDomain() and self.getCoefficients() == other.getCoefficients()
        return super(PolynomialModel, self).__eq__(other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def copy(self, memo=None):
        """Create a new deepcopy of this object

        :rtype: pycultivator.relation.polynomial.Polynomial
        """
        result = super(PolynomialModel, self).copy(memo=memo)
        if isinstance(result, type(self)):
            coeffs = []
            coeffs.extend(self.getCoefficients()[:])
            result.setCoefficients(coeffs)
        return result


class PolynomialModelException(model.ModelException):
    """An Exception raised by Polynomial Relations"""

    pass


class PolynomialRelation(relation.Relation):
    """A relation defined with polynomials"""

    @classmethod
    def createPolynomialModel(cls, name, coefficients=None, domain=None, active=True, parent=None):
        """Factory method: Create a new polynomial.

        :param name: Name of the polynomial
        :type name: int
        :param coefficients: Coefficients, in ascending order (1st order = 0, 2nd order = 1, ...)
        :type coefficients: None | list[float]
        :param domain: Domain specification, default to [-Inf, Inf]
        :type domain: None | str | pycultivator.relation.polynomial.Domain
        :param state: Activity state of the polynomial (active)
        :type state: bool
        :param parent: Parent of the polynomial
        :type parent: None | pycultivator.relation.polynomial.PolynomialModel
        :return: The created polynomial
        :rtype: pycultivator.relation.polynomial.Polynomial
        """
        return PolynomialModel(name, coefficients=coefficients, domain=domain, active=active, parent=parent)

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PolynomialRelation, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        # convert names to integers
        self.setIdentity(identity)
        # create the default polynomial (used when no other polynomial is defined)
        self._default_polynomial = PolynomialModel(0, self, coefficients=[0, 1])
        # list for storing custom polynomials
        self._polynomials = []
        self.clear()

    def makeIdentity(self, name):
        name = Parser.to_int(name)
        if name is None:
            raise PolynomialRelationException(
                "Invalid name, {} is not an integer or cannot parsed to integer".format(name)
            )
        return name

    @property
    def children(self):
        return self._polynomials

    # polynomial methods

    def getDefaultPolynomial(self):
        """Returns the default polynomial

        :rtype: pycultivator.relation.polynomial.Polynomial
        """
        return self._default_polynomial

    @property
    def default(self):
        return self.getDefaultPolynomial()

    def getPolynomials(self, only_active=False):
        """Return a dictionary of all Polynomials included in this dependency

        :param only_active: Whether to only return active polynomials
            (copies the dictionary if True)
        :type only_active: bool
        :return: Dictionary of all (active) polynomials contained in this definition
        :rtype: list[pycultivator.relation.polynomial.PolynomialModel]
        """
        results = self._polynomials
        if only_active:
            results = filter(lambda p: p.is_active, results)
        return results

    @property
    def polynomials(self):
        return self.getPolynomials()

    def clear(self):
        """Removes all Polynomials included in this dependency"""
        self._polynomials = []

    def hasPolynomials(self):
        """Whether this definition has any custom polynomial.

        NOTE: A definition can always calculate values, without polynomials it will use the default.

        :rtype: bool
        """
        return self.count() > 0

    def count(self):
        return len(self.getPolynomials())

    def hasPolynomial(self, polynomial):
        """Returns whether the given polynomial object is in this definition

        :type polynomial: pycultivator.relation.polynomial.PolynomialModel
        :rtype: bool
        """
        return self.has(polynomial)

    def has(self, item):
        return item in self.polynomials

    def hasValidPolynomial(self, value):
        """Whether there is at least one polynomial with a domain containing the given value

        :type value: float or int
        :rtype: bool
        """
        return self.has_valid(value)

    def has_valid(self, value):
        result = False
        for polynomial in self.getPolynomials():
            if value in polynomial:
                result = True
                break
        return result

    def add(self, polynomial):
        """Adds a polynomial to this definition

        :param polynomial: The model to add
        :type polynomial: pycultivator.relation.polynomial.PolynomialModel
        :return: The polynomial
        :rtype: pycultivator.relation.polynomial.PolynomialModel
        """
        if not isinstance(polynomial, PolynomialModel):
            raise PolynomialRelationException("Invalid polynomial given, expected {} but received {}".format(
                type(polynomial), PolynomialModel.__name__
            ))
        if self.has(polynomial):
            raise PolynomialRelationException("{} - is already registered".format(polynomial.getName()))
        return self.setPolynomial(polynomial)

    def remove(self, key):
        if key in self.getPolynomials():
            raise PolynomialRelationException("No polynomial with key - {}".format(key))
        return self._polynomials.pop(key)

    def setPolynomial(self, polynomial):
        return self.set(polynomial)

    def set(self, polynomial):
        if polynomial.getName() is None:
            raise PolynomialRelationException("Invalid polynomial name, name is None")
        self._polynomials.append(polynomial)
        polynomial.setParent(self)
        return self.getPolynomial(polynomial.getName())

    def getPolynomial(self, key):
        """Returns the polynomial at the given position.

        :param key: Name of the polynomial
        :type key: str | int
        :return:
        :rtype: pycultivator.relation.polynomial.PolynomialModel
        """
        return self.get(key)

    def get(self, key):
        if not self.has(key):
            raise PolynomialRelationException("No polynomial with key - {}".format(key))
        index = self._polynomials.index(key)
        return self._polynomials[index]

    def getValidPolynomial(self, value):
        """Returns the polynomial that is valid for the given value"""
        return self.get_valid(value)

    def get_valid(self, value):
        result = None
        polynomials = self.getPolynomials()
        if len(polynomials) == 0:
            polynomials = [self.getDefaultPolynomial()]
        for polynomial in polynomials:
            if value in polynomial:
                result = polynomial
                break
        return result

    def calculate(self, value=0.0, inverse=False):
        """Solves polynomial for value

        If inverse is False: solves result for `f(value) = result`
        If inverse is True solves result for `f(result) = value`

        Does not check if this definition is active! Check `PolynomialDefinition.is_active` first

        :param value: Value to solve the polynomial for
        :type value: float
        :param inverse: Whether to solve for x instead of y
        :type inverse: bool
        :return: The solution to `f(value) = result` or `f(result) = value`
        :rtype: float
        """
        result = 0
        polynomial = self.getValidPolynomial(value)
        if not inverse:
            # check if we have a polynomial, if it is active and if it is valid
            if polynomial is not None and polynomial.isActive() and polynomial.contains(value):
                result = polynomial.calculate(value)
        else:
            result = self.calculate_inverse(value)
        return result

    def calculate_inverse(self, value):
        """Solves result for `value = f(result)`, using the polynomial valid for result

        Tries all polynomials and returns the FIRST solution obtained from a polynomial containing that solution.

        Does not check if this definition is active! Check `PolynomialDefinition.is_active` first

        :param value: The value to solve the polynomial for
        :type value: float
        :return: The solution to `f(result) = value`
        :rtype: float
        """
        # default result
        result = self.getDefaultPolynomial().calculate_inverse(value)
        for p in self.getPolynomials(True):
            if p.isActive():
                y = p.calculate_inverse(value)
                # return first valid polynomial
                if p.contains(y):
                    result = y
                    break
        return result

    def copy(self, memo=None):
        """Create a new copy of this polynomial

        :rtype: pycultivator.relation.polynomial.PolynomialModel
        """
        # first do the basic stuff
        result = super(PolynomialRelation, self).copy(memo=memo)
        result.clear()
        import copy
        for poly in self.polynomials:
            result.add(copy.deepcopy(poly, memo=memo))
        return result

    def __contains__(self, item):
        return self.has(item)

    def __eq__(self, other):
        """Returns whether this polynomial is equal to the other

        :param other:
        :type other: object
        :return:
        :rtype: bool
        """
        if isinstance(other, PolynomialModel):
            result = True
            for polynomial in self.getPolynomials():
                result = result and other.has(polynomial)
        return super(PolynomialRelation, self).__eq__(other)

    def __ne__(self, other):
        return not self == other


class PolynomialRelationException(model.ModelException):
    """An Exception raised by Polynomial Relations"""

    pass
