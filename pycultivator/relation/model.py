"""The model class implements the foundation of mathematical models describing the relation between two things"""

from pycultivator.core import ConfigurableObject, HierarchicalObject, PCException
from domain import Domain

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Model(HierarchicalObject, ConfigurableObject):
    """A Model defines how two things are related and can be converted into each other"""

    namespace = "model"
    default_settings = {
        # add default settings for this class
    }

    def __init__(self, identity, domain=None, active=False, parent=None, settings=None, **kwargs):
        super(Model, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        self._is_active = active
        self._domain = Domain.createFromString("(,)")
        if isinstance(domain, str):
            self._domain = Domain.createFromString(domain)
        if isinstance(domain, Domain):
            self._domain = domain

    # property methods

    def getDomain(self):
        """Returns the domain of this polynomial"""
        return self._domain

    @property
    def domain(self):
        return self.getDomain()

    def setDomain(self, domain):
        """Set the domain of this polynomial

        :type domain: str or pycultivator.relation.polynomial.Domain
        """
        if isinstance(domain, str):
            domain = Domain.createFromString(domain)
        if not isinstance(domain, Domain):
            raise ModelException(
                "Expected a Domain class, received - {}".format(type(domain))
            )
        self._domain = domain
        return self.getDomain()

    @domain.setter
    def domain(self, value):
        self.setDomain(value)

    def isActive(self):
        return self._is_active is True

    @property
    def is_active(self):
        return self.isActive()

    def setActive(self, state):
        self._is_active = state is True

    @is_active.setter
    def is_active(self, state):
        self.setActive(state)

    # behaviour methods

    def calculate(self, value, inverse=False):
        """Will adjust the target by using measurements from the source"""
        raise NotImplementedError

    def contains(self, x):
        return self.getDomain().contains(x)

    def copy(self, memo=None):
        """Create a new deepcopy of this object

        :return: The other object as a copy of this
        :rtype: pycultivator.relation.definition.Definition
        """
        result = super(Model, self).copy(memo)
        result.active = self.isActive()
        import copy
        result.setDomain(copy.deepcopy(self.getDomain(), memo=memo))
        return result

    def __contains__(self, item):
        return self.contains(item)


class ModelException(PCException):
    """An exception raised by a definition object"""

    pass
