
from .base import BaseManager, locked


class DeviceManager(BaseManager):

    manager_name = "device"

    def __init__(self, name, parent=None, settings=None, **kwargs):
        super(DeviceManager, self).__init__(name, parent=parent, settings=settings, **kwargs)

    @property
    def devices(self):
        return self.items

    @locked
    def load(self, name, config, **kwargs):
        """Load a device from a configuration source

        :param name:
        :type config: pycultivator.config.Config
        :param kwargs:
        :return: Loaded device
        """
        from pycultivator.config import load_device, ConfigException
        device = None
        try:
            device = load_device(config, **kwargs)
        except ConfigException as ce:
            self.log.error("Unable to load Config: {}".format(ce))
        else:
            self.add(name, device)
        return device

    @locked
    def add(self, name, device):
        if self.has(name):
            raise KeyError("Another device was already registered with the same name: '{}'".format(name))
        from pycultivator.device import BaseDevice
        if not isinstance(device, BaseDevice):
            raise TypeError("Can only register devices of the type Device not: '{}'".format(type(device)))
        # change parent and set name
        device.parent = self
        device.name = name
        self._items[name] = device

    @locked
    def remove(self, name):
        if not self.has(name):
            raise KeyError("No device registered with the name: '{}'".format(name))
        item = self._items.pop(name)
        """:type: pycultivator.device.ConnectedDevice"""
        from pycultivator.device import ConnectedDevice
        if isinstance(item, ConnectedDevice) and item.isConnected():
            item.disconnect()
            self.log.info("Disconnected {}: {}".format(name, not item.isConnected()))
        item.parent = None

    def measure(self, variable, path=None, **kwargs):
        if path is None:
            items = self.devices.values()
        else:
            items = filter(lambda i: hasattr(i, "measure"), self.path.resolve(path))
        for item in items:
            yield item.measure(variable=variable, **kwargs)

    def control(self, variable, value, path=None, **kwargs):
        if path is None:
            items = self.devices.values()
        else:
            items = filter(lambda i: hasattr(i, "control"), self.path.resolve(path))
        for item in items:
            yield item.control(variable=variable, value=value, **kwargs)
