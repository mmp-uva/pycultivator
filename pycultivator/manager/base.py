
from pycultivator.core import ConfigurableObject, HierarchicalObject
from pycultivator.context.base import BaseContext
import threading


def locked(f):
    """ Decorator to restrict access to function with a lock"""

    def magic(self, *args, **kwargs):
        with self._lock:
            result = f(self, *args, **kwargs)
        # pass result
        return result

    return magic


class BaseManager(HierarchicalObject, ConfigurableObject):
    """A manager keeps track of and provides access to objects"""

    manager_name = "base"
    required_parent = BaseContext

    @classmethod
    def assert_parent(cls, parent, required_parent=None):
        if required_parent is None:
            required_parent = cls.required_parent
        if parent is None:
            raise ValueError("Parent is not allowed to be None")
        if not issubclass(required_parent, BaseContext):
            raise TypeError("The required parent must descend from BaseContext")
        if not isinstance(parent, required_parent):
            raise TypeError("This object requires it's parent to inherit from {}".format(required_parent.__name__))
        return True

    def __init__(self, name, parent=None, settings=None, **kwargs):
        self.assert_parent(parent)
        super(BaseManager, self).__init__(name, parent=parent, settings=settings, **kwargs)
        self._lock = threading.RLock()
        self._items = {}

    @property
    def context(self):
        return self.parent

    @property
    @locked
    def items(self):
        return self._items

    @property
    @locked
    def children(self):
        return self._items.values()

    def add(self, name, item):
        raise NotImplementedError

    @locked
    def get(self, name):
        if not self.has(name):
            raise KeyError("No item registered with the name {}".format(name))
        return self._items[name]

    @locked
    def has(self, name):
        return name in self._items.keys()

    def remove(self, name):
        raise NotImplementedError

    @locked
    def clear(self):
        while len(self._configs.keys()) > 0:
            name = self._configs.keys()[0]
            self.remove(name)

    def clean(self):
        self.clear()

    @property
    def size(self):
        return len(self.items)

    def __contains__(self, item):
        return self.has(item)
