
from .base import BaseManager, locked


class ConfigManager(BaseManager):

    manager_name = "config"

    def __init__(self, name, parent=None, settings=None, **kwargs):
        super(ConfigManager, self).__init__(name, parent=parent, settings=settings, **kwargs)

    @property
    def configs(self):
        return self.items

    @locked
    def load(self, name, location, search_dirs=None, **kwargs):
        """Load a configuration"""
        from pycultivator.config import load_config, Config, ConfigException
        config = None
        try:
            if search_dirs is None:
                search_dirs = []
            if isinstance(search_dirs, (tuple, set)):
                search_dirs = list(search_dirs)
            if not isinstance(search_dirs, list):
                search_dirs = [search_dirs]
            search_dirs.append(self.context.getRootDirectory())
            config = load_config(location, search_dirs=search_dirs, **kwargs)
        except ConfigException as ce:
            self.log.error("Unable to load Config: {}".format(ce))
        if isinstance(config, Config):
            # register config
            self.add(name, config)
        return config

    def validate(self, location, search_dirs=None, silent=False, **kwargs):
        from pycultivator.config import xmlConfig
        result = False
        try:
            if search_dirs is None:
                search_dirs = []
            if isinstance(search_dirs, (tuple, set)):
                search_dirs = list(search_dirs)
            if not isinstance(search_dirs, list):
                search_dirs = [search_dirs]
            search_dirs.append(self.context.getRootDirectory())
            result = xmlConfig.XMLConfig.check(location, search_dirs=search_dirs, **kwargs)
        except xmlConfig.XMLConfigException as xce:
            if silent:
                self.log.error(xce, exc_info=1)
            else:
                raise
        return result

    @locked
    def add(self, name, config):
        if self.has(name):
            raise KeyError("Another configuration was already registered with the same name: '{}'".format(name))
        from pycultivator.config import Config
        if not isinstance(config, Config):
            raise TypeError("Can only register configurations of the type Config not: '{}'".format(type(config)))
        self._items[name] = config

    @locked
    def remove(self, name):
        if not self.has(name):
            raise KeyError("No configuration registered with the name: '{}'".format(name))
        self._items.pop(name)
