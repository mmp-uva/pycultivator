====================
PyCultivator Package
====================

This package provides the core pyCultivator classes. Most of these classes are abstract interfaces that only describe
 the basic interface and require further implementations. Other packages are built on top of pyCultivator, such as
 pycultivator-plate (implementing pyCultivator for a 24-well plate incubator) or pycultivator-psimc (implementing
 pyCultivator for the Multi-Cultivator MC-1000-OD from Photon Systems Instruments).

The philosophy of pyCultivator is the create a basic (generic) software environment for creating software systems
that interact with measurement or control devices. These devices can range from simple pH-Sensors to advanced
bio-reactors. The aim is to provide one platform for modelling and interacting with these devices. PyCultivator will
therefore only implement the basic parts; everything you need to use your devices in your software, but you still
need the software that eventually works with these devices. PyCultivator-lab is intended to provide the latter part.

Structure
=========

PyCultivator consists of a set of packages, of which the "core" package is the most basic, implementing essential
features used throughout the whole package. The device and instrument package provide

Config
------

The config package provides classes that serve as an interface between the package and a configuration source.
Configuration source can be anything, like XML fles or SQL databases. PyCultivator provides XML configuration, by
default.

The package consists of two modules; baseConfig and xmlConfig. The baseConfig module provides as much as possible of
the functionality without making assumptions on the type of configuration source. The baseConfig classes are
*abstract*, so they need a specific implementation to work. The xmlConfig package provides such implementation.

Connection
----------

The connection package provides

Core
----

Core package with classes for basic functionality, like the standard object, xml, settings, exception and loggers.

Data
----





